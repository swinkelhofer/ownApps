#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		printf("Usage: DumpFloat.exe <float> [float2] [op]\n");
		exit(0);
	}
	float zahl = atof(argv[1]);
	float zahl2;
	if(argc == 4)
	{
		zahl2 = atof(argv[2]);
		switch(argv[3][0])
		{
			case 'x':	zahl *= zahl2;
						break;
			case '/':	zahl /= zahl2;
						break;
			case '+':	zahl += zahl2;
						break;
			case '-':	zahl -= zahl2;
						break;
			default:	break;
		}
	}
	int *ptr = (int*)&zahl;
	char erg[32];
	itoa(*ptr, erg, 2);
	printf("%s\n%f", erg, zahl);
	return 0;
}

// 23 *
// 23 2 *
// 23 2 * -
// 23 2 * 4 -
// 23 * 2 - 4 --> 23 2 * 4 -
// 46 4 -
// 42