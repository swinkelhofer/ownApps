#include "APIHack.h"
#include "mhook.h"
#include <stdio.h>
#include <tchar.h>
#include <ctype.h>
#include <Tlhelp32.h>
#include <tchar.h>
#include <psapi.h>

#define DllExport	extern "C" __declspec(dllexport)
#define MESSAGE_NO 0x678
WCHAR const Kernel32[] = L"C:\\Windows\\system32\\kernel32.dll";
char Path[]="C:\\ownApps\\APIHack\\APIHack.dll";
#define SIZEOF_PATH 34

NTSTATUS HookNtCreateSection(OUT PHANDLE SectionHandle, IN ACCESS_MASK DesiredAccess, IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN PLARGE_INTEGER MaximumSize, IN ULONG SectionPageProtection, IN ULONG AllocationAttributes, IN HANDLE FileHandle)
{
	MessageBox(0,"NtCreateSection", "Info", 0);
	NTSTATUS status = TrueNtCreateSection(SectionHandle, DesiredAccess, ObjectAttributes, MaximumSize, SectionPageProtection, AllocationAttributes, FileHandle);
	return status;
}


DllExport LRESULT CALLBACK HookKBLL(int nCode, WPARAM wParam, LPARAM lParam)
{
	return CallNextHookEx(NULL, nCode, wParam, lParam);
}

typedef NTSTATUS (NTAPI* _NtQueryInformationThread)(IN HANDLE ThreadHandle,
    IN THREADINFOCLASS ThreadInformationClass,
    OUT PVOID ThreadInformation,
    IN ULONG ThreadInformationLength, 
    OUT PULONG ReturnLength OPTIONAL
  );

struct NtCreateThreadExBuffer
{
  ULONG Size;
  ULONG Unknown1;
  ULONG Unknown2;
  PULONG Unknown3;
  ULONG Unknown4;
  ULONG Unknown5;
  ULONG Unknown6;
  PULONG Unknown7;
  ULONG Unknown8;
}; 

HANDLE HookCreateRemoteThread(IN HANDLE hProcess, IN LPSECURITY_ATTRIBUTES lpThreadAttributes, IN SIZE_T dwStackSize,
		LPTHREAD_START_ROUTINE pThreadProc, IN LPVOID pRemoteBuf, IN DWORD dwCreationFlags, OUT LPDWORD lpThreadId)
{
    HANDLE  hThread = NULL;
	BOOL Suspended = FALSE;
	
	NtCreateThreadExBuffer ntbuffer;

	memset (&ntbuffer,0,sizeof(NtCreateThreadExBuffer));
	DWORD temp1 = 0;
	DWORD temp2 = 0;

	ntbuffer.Size = sizeof(NtCreateThreadExBuffer);
	ntbuffer.Unknown1 = 0x10003;
	ntbuffer.Unknown2 = 0x8;
	ntbuffer.Unknown3 = &temp2;
	ntbuffer.Unknown4 = 0;
	ntbuffer.Unknown5 = 0x10004;
	ntbuffer.Unknown6 = 4;
	ntbuffer.Unknown7 = &temp1;
	ntbuffer.Unknown8 = 0;
	
	if(dwCreationFlags == CREATE_SUSPENDED)
		Suspended = TRUE;
	NtCreateThreadEx(&hThread, 0x1FFFFF, NULL, hProcess, pThreadProc, pRemoteBuf, Suspended, 0, 0, 0, &ntbuffer);
    if(hThread == NULL)
    {
        printf("NtCreateThreadEx() failed!!! [%d]\n", (int)GetLastError());
    }
	if(lpThreadId != 0 && hThread != 0)
		*lpThreadId = GetThreadId(hThread);
	return hThread;
}



NTSTATUS NTAPI HookNtResumeThread(IN HANDLE ThreadHandle, OUT PULONG SuspendCount)
{
	NTSTATUS result = TrueNtResumeThread(ThreadHandle, SuspendCount);
	if(*SuspendCount == 1)
	{
		DWORD processID = 0;
		processID = GetProcessIdOfThread(ThreadHandle);
		DWORD currentID = 0;
		currentID = GetCurrentProcessId();
		//LPTHREADENTRY32 entry;
		//entry->dwSize = sizeof(THREADENTRY32);
		//Thread32First(CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, processID), entry);
		//if((entry->th32ThreadID == GetThreadId(ThreadHandle)) && *SuspendCount == 1)
		
		//THREAD_BASIC_INFORMATION info;
		//DWORD size;
		//_NtQueryInformationThread NtQueryInformationThread = (_NtQueryInformationThread) GetProcAddress(GetModuleHandle(TEXT("ntdll")), "NtQueryInformationThread");
		//NtQueryInformationThread(ThreadHandle, ThreadBasicInformation, &info, sizeof(THREAD_BASIC_INFORMATION), &size);
		
		
		if((processID != currentID) && processID != 0 && currentID != 0)
		{
		//MessageBox(0,"NtResumeThread", "Info", 0);
	
			//SetDebugPrivilege(GetCurrentProcess());
			//MessageBox(0, "NtResumeThread", "Info", 0);
			HANDLE hProcess = NULL, AllocAdresse = NULL, hRemoteThread = NULL;
			//hProcess = info.ClientId.UniqueProcess;
			//CloseHandle(hProcess);
			hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processID);
			//	SetDebugPrivilege(GetCurrentProcess());
			if(hProcess != NULL)
			{
				AllocAdresse = VirtualAllocEx(hProcess, 0, SIZEOF_PATH, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
				if(AllocAdresse == NULL)
					MessageBox(0,"AllocationError", "Error", 0);
				WriteProcessMemory(hProcess, (void*)AllocAdresse, (void*)Path, SIZEOF_PATH, 0);
				
				//SuspendThread(ThreadHandle);
				//SetSystemProfilePrivilege(hProcess);
				//hRemoteThread = CreateRemoteThread(hProcess, 0, 0, (LPTHREAD_START_ROUTINE) GetProcAddress(GetModuleHandle("kernel32.dll"),"LoadLibraryA"), AllocAdresse, 0, 0);
				
				
				NtCreateThreadExBuffer ntbuffer;
				
				memset(&ntbuffer, 0, sizeof(NtCreateThreadExBuffer));
				
				
				ULONG temp1 = 0;
				ULONG temp2 = 0;
				//memset(&temp1, 0, sizeof(DWORD));
				//memset(&temp2, 0, sizeof(DWORD));
				//temp1 = 0;
				//temp2 = 0;
				
				HANDLE dword1, dword2, threadbuffer;
				
				
				dword1 = VirtualAllocEx(hProcess, 0, sizeof(ULONG), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
				dword2 = VirtualAllocEx(hProcess, 0, sizeof(ULONG), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
				if(dword1 == NULL || dword2 == NULL)
					MessageBox(0,"ULONG Error","Error",0);
				WriteProcessMemory(hProcess, (void*)dword1, (void*)&temp1, sizeof(ULONG), 0);
				WriteProcessMemory(hProcess, (void*)dword2, (void*)&temp2, sizeof(ULONG), 0);
				
				ntbuffer.Size = sizeof(NtCreateThreadExBuffer);
				ntbuffer.Unknown1 = 0x10003;
				ntbuffer.Unknown2 = 0x8;
				ntbuffer.Unknown3 = (PULONG)&temp1;
				ntbuffer.Unknown4 = 0;
				ntbuffer.Unknown5 = 0x10004;
				ntbuffer.Unknown6 = 4;
				ntbuffer.Unknown7 = (PULONG)&temp2;
				ntbuffer.Unknown8 = 0;
				
				//NtCreateThreadExBuffer outbuffer = ntbuffer;
				
				threadbuffer = VirtualAllocEx(hProcess, 0, sizeof(NtCreateThreadExBuffer), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
				SIZE_T written;
				if(!WriteProcessMemory(hProcess, (void*)threadbuffer, (void*)&ntbuffer, sizeof(NtCreateThreadExBuffer), &written))
					MessageBox(0,"WriteProcessMemoryError","Error",0);
				if(written != sizeof(NtCreateThreadExBuffer))
					MessageBox(0,"WriteError", "Error",0);

				
			
				
				
				NtCreateThreadEx(&hRemoteThread, 0x1FFFFF, 0, hProcess, (LPTHREAD_START_ROUTINE) GetProcAddress(GetModuleHandle("kernel32.dll"),"LoadLibraryA"),
									AllocAdresse, FALSE, 0, 0, 0, &ntbuffer);
				
				
				//HANDLE objects[1] = {hRemoteThread};
				//ResumeThread(hRemoteThread);
				WaitForSingleObject(hRemoteThread, INFINITE);
				if(hRemoteThread == NULL)
					MessageBox(0,"RemoteThreadError", "Error", 0);
				//CloseHandle(hProcess);
				VirtualFreeEx(hProcess, AllocAdresse, 0, MEM_RELEASE);
				VirtualFreeEx(hProcess, dword1, 0, MEM_RELEASE);
				VirtualFreeEx(hProcess, dword2, 0, MEM_RELEASE);
				VirtualFreeEx(hProcess, threadbuffer, 0, MEM_RELEASE);
				//CloseHandle(AllocAdresse);
				//ResumeThread(ThreadHandle);
				//MsgWaitForMultipleObjects(1, objects, TRUE, INFINITE, QS_ALLINPUT);
				//ResumeThread(ThreadHandle);
				//AttachThreadInput(GetCurrentThreadId(), GetThreadId(ThreadHandle), FALSE);
				CloseHandle(hProcess);
				CloseHandle(hRemoteThread);
			}
		}
	}
	
	//Mhook_Unhook((PVOID*)&TrueNtResumeThread);
	
	//NTSTATUS result = TrueNtResumeThread(ThreadHandle, SuspendCount);
	
	//if(!Mhook_SetHook((PVOID*)&TrueNtResumeThread, (PVOID)HookNtResumeThread))
	//	MessageBox(0, "Couldn't hook NtResumeThread", "Error", 0);
	
	//return HookNtResumeThread(ThreadHandle, SuspendCount);
	return result;
}
/*#######################################################################################################################################
													NtReadFile
#######################################################################################################################################*/
NTSTATUS NTAPI HookNtReadFile(IN HANDLE FileHandle, IN HANDLE Event, IN PIO_APC_ROUTINE ApcRoutine, IN PVOID ApcContext,
		OUT PIO_STATUS_BLOCK IoStatusBlock, OUT PVOID Buffer, IN ULONG Length, IN PLARGE_INTEGER ByteOffset, IN PULONG Key)
{
	return TrueNtReadFile(FileHandle, Event, ApcRoutine, ApcContext, IoStatusBlock, Buffer, Length, ByteOffset, Key);
}
/*#######################################################################################################################################
													NtReadFile
#######################################################################################################################################*/


/*#######################################################################################################################################
													NtCreateFile
#######################################################################################################################################*/
BOOL IsMedia(const WCHAR* temp)
{
	WCHAR media[][5] = {L".mov", L".avi", L".flv", L".mpg", L".vob", L".mp4", L".mkv", L".wma", L".bup", L".ifo"};
	for(int i = 0; i < 10; ++i)
	{
		if(!_wcsnicmp(temp, media[i], 4))
			return TRUE;
	}
	return FALSE;
}

BOOL isAppStarted()
{
    // Get the list of process identifiers.

    DWORD aProcesses[1024], cbNeeded, cProcesses;
    unsigned int i;

    if ( !EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded ) )
    {
        return 1;
    }


    // Calculate how many process identifiers were returned.

    cProcesses = cbNeeded / sizeof(DWORD);

    // Print the name and process identifier for each process.

    for ( i = 0; i < cProcesses; i++ )
    {
        if( aProcesses[i] != 0 )
        {
            TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");

			// Get a handle to the process.

			HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |
										   PROCESS_VM_READ,
										   FALSE, aProcesses[i] );

			// Get the process name.

			if (NULL != hProcess )
			{
				HMODULE hMod;
				DWORD cbNeeded;

				if ( EnumProcessModules( hProcess, &hMod, sizeof(hMod), 
					 &cbNeeded) )
				{
					GetModuleBaseName( hProcess, hMod, szProcessName, 
									   sizeof(szProcessName)/sizeof(TCHAR) );
				}
			}

			// Print the process name and identifier.

			//printf( TEXT("PID: %u\t%s \n"), processID, szProcessName);
			CloseHandle( hProcess );
			if(!strncmp(szProcessName, "ShowAll.exe", 11))
				return TRUE;
			// Release the handle to the process.

        }
    }

    return FALSE;
}

NTSTATUS NTAPI HookNtCreateFile(OUT PHANDLE FileHandle, IN ACCESS_MASK DesiredAccess, IN POBJECT_ATTRIBUTES ObjectAttributes,
		OUT PIO_STATUS_BLOCK IoStatusBlock, IN PLARGE_INTEGER AllocationSize, IN ULONG FileAttributes, IN ULONG ShareAccess,
		IN ULONG CreateDisposition, IN ULONG CreateOptions, IN PVOID EaBuffer, IN ULONG EaLength)
{
	WCHAR nameoffile[ObjectAttributes->ObjectName->MaximumLength/sizeof(WCHAR)+1];
	wcsncpy(nameoffile, ObjectAttributes->ObjectName->Buffer, ObjectAttributes->ObjectName->MaximumLength/sizeof(WCHAR));
	nameoffile[ObjectAttributes->ObjectName->MaximumLength/sizeof(WCHAR)] = L'\0';
	if(IsMedia(wcsrchr(nameoffile, L'.')) && !isAppStarted())
		return TrueNtCreateFile(NULL, DesiredAccess, ObjectAttributes, IoStatusBlock, AllocationSize, FileAttributes, ShareAccess, CreateDisposition, CreateOptions, EaBuffer, EaLength);
	return TrueNtCreateFile(FileHandle, DesiredAccess, ObjectAttributes, IoStatusBlock, AllocationSize, FileAttributes, ShareAccess, CreateDisposition, CreateOptions, EaBuffer, EaLength);
}


DWORD WINAPI HookNtQueryDirectoryFile(HANDLE FileHandle, HANDLE Event, PIO_APC_ROUTINE ApcRoutine, PVOID ApcContext, PIO_STATUS_BLOCK IoStatusBlock, PVOID FileInformation,
		ULONG Length, FILE_INFORMATION_CLASS FileInformationClass, BOOLEAN ReturnSingleEntry, PUNICODE_STRING FileMask, BOOLEAN RestartScan)
{
	DWORD dwResult, dwNextEntryOffset, dwFileNameLen;
	WCHAR *pwszFileName; BYTE *ptr;
	FILE_BOTH_DIR_INFORMATION *pLast;
   
	dwResult = TrueNtQueryDirectoryFile(FileHandle, Event, ApcRoutine, ApcContext, IoStatusBlock, FileInformation, Length, FileInformationClass, ReturnSingleEntry, FileMask, RestartScan);
	if(dwResult == ERROR_SUCCESS) {
		if(ReturnSingleEntry == FALSE) {
			dwNextEntryOffset = 0; ptr = (BYTE *) FileInformation;
			do {
				if(((FILE_BOTH_DIR_INFORMATION *) ptr)->NextEntryOffset) {
					pLast = (FILE_BOTH_DIR_INFORMATION *) ptr;
				}
				ptr += dwNextEntryOffset;
			   
				if(FileInformationClass == FileBothDirectoryInformation) {
					FILE_BOTH_DIR_INFORMATION *pInformation = (FILE_BOTH_DIR_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwNextEntryOffset = pInformation->NextEntryOffset;
					dwFileNameLen = pInformation->FileNameLength;
				}
				else if(FileInformationClass == FileDirectoryInformation) {
					FILE_DIRECTORY_INFORMATION *pInformation = (FILE_DIRECTORY_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwNextEntryOffset = pInformation->NextEntryOffset;
					dwFileNameLen = pInformation->FileNameLength;
				}
				else if(FileInformationClass == FileFullDirectoryInformation) {
					FILE_FULL_DIR_INFORMATION *pInformation = (FILE_FULL_DIR_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwNextEntryOffset = pInformation->NextEntryOffset;
					dwFileNameLen = pInformation->FileNameLength;
				}
				else if(FileInformationClass == FileIdBothDirectoryInformation) {
					FILE_ID_BOTH_DIR_INFORMATION *pInformation = (FILE_ID_BOTH_DIR_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwNextEntryOffset = pInformation->NextEntryOffset;
					dwFileNameLen = pInformation->FileNameLength;
				}
				else if(FileInformationClass == FileIdFullDirectoryInformation) {
					FILE_ID_FULL_DIR_INFORMATION *pInformation = (FILE_ID_FULL_DIR_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwNextEntryOffset = pInformation->NextEntryOffset;
					dwFileNameLen = pInformation->FileNameLength;
				}
				else if(FileInformationClass == FileNamesInformation) {
					FILE_NAMES_INFORMATION *pInformation = (FILE_NAMES_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwNextEntryOffset = pInformation->NextEntryOffset;
					dwFileNameLen = pInformation->FileNameLength;
				}
				else {
					break;
				}
			   
				WCHAR nameoffile[dwFileNameLen/sizeof(WCHAR)+1];
				wcsncpy(nameoffile, pwszFileName, dwFileNameLen/sizeof(WCHAR));
				nameoffile[dwFileNameLen/sizeof(WCHAR)] = L'\0';
				if(IsMedia(wcsrchr(nameoffile, L'.')) && !isAppStarted()) {
					if(dwNextEntryOffset) pLast->NextEntryOffset += dwNextEntryOffset;
					else pLast->NextEntryOffset = 0;
					memset(ptr, 0, dwNextEntryOffset);
				}
			} while (dwNextEntryOffset);
		}
		else {
			return dwResult;
			/*int ok = 0;
			do {
				if(FileInformationClass == FileBothDirectoryInformation) {
					FILE_BOTH_DIR_INFORMATION *pInformation = (FILE_BOTH_DIR_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwFileNameLen = pInformation->FileNameLength;
				}
				else if(FileInformationClass == FileDirectoryInformation) {
					FILE_DIRECTORY_INFORMATION *pInformation = (FILE_DIRECTORY_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwFileNameLen = pInformation->FileNameLength;
				}
				else if(FileInformationClass == FileFullDirectoryInformation) {
					FILE_FULL_DIR_INFORMATION *pInformation = (FILE_FULL_DIR_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwFileNameLen = pInformation->FileNameLength;
				}
				else if(FileInformationClass == FileIdBothDirectoryInformation) {
					FILE_ID_BOTH_DIR_INFORMATION *pInformation = (FILE_ID_BOTH_DIR_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwFileNameLen = pInformation->FileNameLength;
				}
				else if(FileInformationClass == FileIdFullDirectoryInformation) {
					FILE_ID_FULL_DIR_INFORMATION *pInformation = (FILE_ID_FULL_DIR_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwFileNameLen = pInformation->FileNameLength;
				}
				else if(FileInformationClass == FileNamesInformation) {
					FILE_NAMES_INFORMATION *pInformation = (FILE_NAMES_INFORMATION *) ptr;
					pwszFileName = pInformation->FileName; dwFileNameLen = pInformation->FileNameLength;
				}
				else {
					break;
				}
				if(!_wcsnicmp(pwszFileName, RTK_FILE_CHAR, RTK_LENGTH)) {
					return HookNtQueryDirectoryFile(FileHandle, Event, ApcRoutine, ApcContext, IoStatusBlock, FileInformation, Length, FileInformationClass, ReturnSingleEntry, FileMask, RestartScan);
				}
				else {
					ok = 1;
				}
			} while (ok == 0);*/
		}
	}
	return dwResult;
}



DllExport BOOL WINAPI DllMain(HINSTANCE hInst,DWORD reason,LPVOID reserved)
{
	HHOOK hook;
	HOOKPROC hookproc;
	switch (reason)
	{
		case DLL_PROCESS_ATTACH:
			//MessageBox(NULL, "Attached!", "Debugging", MB_ICONINFORMATION);
			if(!Mhook_SetHook((PVOID*)&TrueNtQueryDirectoryFile, (PVOID)HookNtQueryDirectoryFile))
				MessageBox(0, "Couldn't hook NtQueryDirectoryFile", "Error", 0);
			//if(!Mhook_SetHook((PVOID*)&TrueNtResumeThread, (PVOID)HookNtResumeThread))
			//	MessageBox(0, "Couldn't hook NtResumeThread", "Error", 0);
			if(!Mhook_SetHook((PVOID*)&TrueNtReadFile, (PVOID)HookNtReadFile))
				MessageBox(0, "Couldn't hook NtReadFile", "Error", 0);
			if(!Mhook_SetHook((PVOID*)&TrueNtCreateFile, (PVOID)HookNtCreateFile))
				MessageBox(0, "Couldn't hook NtCreateFile", "Error", 0);
			/*char lib_name[MAX_PATH];
			GetModuleFileName(hInst, lib_name, MAX_PATH);
			LoadLibrary(lib_name);*/
			hookproc = (HOOKPROC) GetProcAddress(hInst, TEXT("HookKBLL"));
			if(hookproc == NULL)
			{
				printf("Cannot find HookProc");
			}
			hook = SetWindowsHookEx(WH_CALLWNDPROC, hookproc, hInst, 0);
			break;
		
		case DLL_PROCESS_DETACH:
			//MessageBox(NULL, "Detached!", "Debugging", MB_ICONINFORMATION);
			Mhook_Unhook((PVOID*)&TrueNtQueryDirectoryFile);
			//Mhook_Unhook((PVOID*)&TrueNtResumeThread);
			Mhook_Unhook((PVOID*)&TrueNtReadFile);
			Mhook_Unhook((PVOID*)&TrueNtCreateFile);
			UnhookWindowsHookEx(hook);
			//Mhook_Unhook((PVOID*)&TrueCreateRemoteThread);
			//Mhook_Unhook((PVOID*)&TrueNtCreateSection);
			break;
	}
	return TRUE;
}

