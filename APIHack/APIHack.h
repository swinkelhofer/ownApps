#include <windows.h>
#include <ddk/ntapi.h>
#include <ddk/winddk.h>
typedef enum _FILE_INFO_BY_HANDLE_CLASS {
  FileBasicInfo,
  FileStandardInfo,
  FileNameInfo,
  FileRenameInfo,
  FileDispositionInfo,
  FileAllocationInfo,
  FileEndOfFileInfo,
  FileStreamInfo,
  FileCompressionInfo,
  FileAttributeTagInfo,
  FileIdBothDirectoryInfo,
  FileIdBothDirectoryRestartInfo,
  FileIoPriorityHintInfo,
  MaximumFileInfoByHandlesClass 
} FILE_INFO_BY_HANDLE_CLASS, *PFILE_INFO_BY_HANDLE_CLASS;

typedef NTSTATUS (NTAPI* _NtCreateSection)(OUT PHANDLE SectionHandle, IN ACCESS_MASK DesiredAccess, IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN PLARGE_INTEGER MaximumSize, IN ULONG SectionPageProtection, IN ULONG AllocationAttributes, IN HANDLE FileHandle);
_NtCreateSection TrueNtCreateSection = (_NtCreateSection) GetProcAddress(GetModuleHandle(TEXT("ntdll")), "NtCreateSection");

typedef HINSTANCE* PHINSTANCE;

typedef NTSTATUS (NTAPI* _LdrLoadDll)(IN PWSTR szcwPath, OUT PDWORD pdwLdrErr, OUT PUNICODE_STRING pUniModuleName, OUT PHINSTANCE pResultInstance);
_LdrLoadDll TrueLdrLoadDll =  (_LdrLoadDll) GetProcAddress(GetModuleHandle(TEXT("ntdll")), "LdrLoadDll");


typedef NTSTATUS (NTAPI* _NtReadFile)(IN HANDLE FileHandle, IN HANDLE Event, IN PIO_APC_ROUTINE ApcRoutine, IN PVOID ApcContext,
		OUT PIO_STATUS_BLOCK IoStatusBlock, OUT PVOID Buffer, IN ULONG Length, IN PLARGE_INTEGER ByteOffset, IN PULONG Key);
		
_NtReadFile TrueNtReadFile =  (_NtReadFile) GetProcAddress(GetModuleHandle(TEXT("ntdll")), "NtReadFile");

typedef NTSTATUS (NTAPI* _NtCreateFile)(OUT PHANDLE FileHandle, IN ACCESS_MASK DesiredAccess, IN POBJECT_ATTRIBUTES ObjectAttributes,
		OUT PIO_STATUS_BLOCK IoStatusBlock, IN PLARGE_INTEGER AllocationSize, IN ULONG FileAttributes, IN ULONG ShareAccess,
		IN ULONG CreateDisposition, IN ULONG CreateOptions, IN PVOID EaBuffer, IN ULONG EaLength);

_NtCreateFile TrueNtCreateFile = (_NtCreateFile) GetProcAddress(GetModuleHandle(TEXT("ntdll")), "NtCreateFile");

typedef NTSTATUS (NTAPI* _NtQueryDirectoryFile)(IN HANDLE FileHandle, IN HANDLE Event, IN PIO_APC_ROUTINE ApcRoutine,
			IN PVOID ApcContext, OUT PIO_STATUS_BLOCK IoStatusBlock, OUT PVOID FileInformation, IN ULONG Length,
			IN FILE_INFORMATION_CLASS FileInformationClass, IN BOOLEAN ReturnSingleEntry, IN PUNICODE_STRING FileName,
			IN BOOLEAN RestartScan);
			
typedef NTSTATUS (NTAPI* _NtCreateUserProcess)(PHANDLE ProcessHandle, PHANDLE ThreadHandle, PVOID Parameter2,
		PVOID Parameter3, PVOID ProcessSecurityDescriptor, PVOID ThreadSecurityDescriptor, PVOID Parameter6,
		PVOID Parameter7, PRTL_USER_PROCESS_PARAMETERS ProcessParameters, PVOID Parameter9, PVOID pProcessUnKnow);

_NtCreateUserProcess TrueNtCreateUserProcess = (_NtCreateUserProcess) GetProcAddress(GetModuleHandle(TEXT("ntdll")), "NtCreateUserProcess");

typedef DWORD (WINAPI* _GetProcessIdOfThread)(IN HANDLE ThreadHandle);
_GetProcessIdOfThread GetProcessIdOfThread = (_GetProcessIdOfThread) GetProcAddress(GetModuleHandle(TEXT("kernel32")), "GetProcessIdOfThread");

typedef NTSTATUS (NTAPI* _NtResumeThread)(IN HANDLE ThreadHandle, OUT PULONG SuspendCount);
_NtResumeThread TrueNtResumeThread = (_NtResumeThread) GetProcAddress(GetModuleHandle(TEXT("ntdll")), "NtResumeThread");

typedef DWORD (WINAPI* _GetProcessId)(IN HANDLE ProcessHandle);
_GetProcessId GetProcessId = (_GetProcessId) GetProcAddress(GetModuleHandle("kernel32.dll"), "GetProcessId");
_GetProcessId GetThreadId = (_GetProcessId) GetProcAddress(GetModuleHandle("kernel32.dll"), "GetThreadId");


typedef NTSTATUS (NTAPI* _NtCreateThreadEx)(OUT PHANDLE hThread, IN ACCESS_MASK DesiredAccess, IN LPVOID ObjectAttributes,
		IN HANDLE ProcessHandle, IN LPTHREAD_START_ROUTINE lpStartAddress, IN LPVOID lpParameter, IN BOOL CreateSuspended,
		IN ULONG StackZeroBits, IN ULONG SizeOfStackCommit, IN ULONG SizeOfStackReserve, OUT LPVOID lpBytesBuffer);
_NtCreateThreadEx NtCreateThreadEx = (_NtCreateThreadEx) GetProcAddress(GetModuleHandle(TEXT("ntdll")), "NtCreateThreadEx");


typedef HANDLE (WINAPI* _CreateRemoteThread)(IN HANDLE hProcess, IN LPSECURITY_ATTRIBUTES lpThreadAttributes, IN SIZE_T dwStackSize,
		LPTHREAD_START_ROUTINE lpStartAddress, IN LPVOID lpParameter, IN DWORD dwCreationFlags, OUT LPDWORD lpThreadId);
_CreateRemoteThread TrueCreateRemoteThread = (_CreateRemoteThread) GetProcAddress(GetModuleHandle("kernel32.dll"), "CreateRemoteThread");


typedef struct _FILE_ID_BOTH_DIR_INFORMATION {
  ULONG  NextEntryOffset;
  ULONG  FileIndex;
  LARGE_INTEGER  CreationTime;
  LARGE_INTEGER  LastAccessTime;
  LARGE_INTEGER  LastWriteTime;
  LARGE_INTEGER  ChangeTime;
  LARGE_INTEGER  EndOfFile;
  LARGE_INTEGER  AllocationSize;
  ULONG  FileAttributes;
  ULONG  FileNameLength;
  ULONG  EaSize;
  CCHAR  ShortNameLength;
  WCHAR  ShortName[12];
  LARGE_INTEGER  FileId;
  WCHAR  FileName[1];
} FILE_ID_BOTH_DIR_INFORMATION, *PFILE_ID_BOTH_DIR_INFORMATION;

typedef struct _FILE_BOTH_DIR_INFORMATION {
  ULONG  NextEntryOffset;
  ULONG  FileIndex;
  LARGE_INTEGER  CreationTime;
  LARGE_INTEGER  LastAccessTime;
  LARGE_INTEGER  LastWriteTime;
  LARGE_INTEGER  ChangeTime;
  LARGE_INTEGER  EndOfFile;
  LARGE_INTEGER  AllocationSize;
  ULONG  FileAttributes;
  ULONG  FileNameLength;
  ULONG  EaSize;
  CCHAR  ShortNameLength;
  WCHAR  ShortName[12];
  WCHAR  FileName[1];
} FILE_BOTH_DIR_INFORMATION, *PFILE_BOTH_DIR_INFORMATION;

typedef struct _FILE_DIRECTORY_INFORMATION {
  ULONG  NextEntryOffset;
  ULONG  FileIndex;
  LARGE_INTEGER  CreationTime;
  LARGE_INTEGER  LastAccessTime;
  LARGE_INTEGER  LastWriteTime;
  LARGE_INTEGER  ChangeTime;
  LARGE_INTEGER  EndOfFile;
  LARGE_INTEGER  AllocationSize;
  ULONG  FileAttributes;
  ULONG  FileNameLength;
  WCHAR  FileName[1];
} FILE_DIRECTORY_INFORMATION, *PFILE_DIRECTORY_INFORMATION;

typedef struct _FILE_FULL_DIR_INFORMATION {
  ULONG  NextEntryOffset;
  ULONG  FileIndex;
  LARGE_INTEGER  CreationTime;
  LARGE_INTEGER  LastAccessTime;
  LARGE_INTEGER  LastWriteTime;
  LARGE_INTEGER  ChangeTime;
  LARGE_INTEGER  EndOfFile;
  LARGE_INTEGER  AllocationSize;
  ULONG  FileAttributes;
  ULONG  FileNameLength;
  ULONG  EaSize;
  WCHAR  FileName[1];
} FILE_FULL_DIR_INFORMATION, *PFILE_FULL_DIR_INFORMATION;

typedef struct _FILE_ID_FULL_DIR_INFORMATION {
  ULONG  NextEntryOffset;
  ULONG  FileIndex;
  LARGE_INTEGER  CreationTime;
  LARGE_INTEGER  LastAccessTime;
  LARGE_INTEGER  LastWriteTime;
  LARGE_INTEGER  ChangeTime;
  LARGE_INTEGER  EndOfFile;
  LARGE_INTEGER  AllocationSize;
  ULONG  FileAttributes;
  ULONG  FileNameLength;
  ULONG  EaSize;
  LARGE_INTEGER  FileId;
  WCHAR  FileName[1];
} FILE_ID_FULL_DIR_INFORMATION, *PFILE_ID_FULL_DIR_INFORMATION;


typedef struct _FILE_NAMES_INFORMATION {
  ULONG  NextEntryOffset;
  ULONG  FileIndex;
  ULONG  FileNameLength;
  WCHAR  FileName[1];
} FILE_NAMES_INFORMATION, *PFILE_NAMES_INFORMATION;

_NtQueryDirectoryFile TrueNtQueryDirectoryFile = (_NtQueryDirectoryFile) GetProcAddress(GetModuleHandle(TEXT("ntdll")), "NtQueryDirectoryFile");

//typedef DWORD (WINAPI* _GetThreadId)(IN HANDLE Thread);
//_GetThreadId GetThreadId = (_GetThreadId) GetProcAddress(GetModuleHandle(TEXT("kernel32")), "GetThreadId");