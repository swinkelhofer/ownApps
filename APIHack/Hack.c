#include "windows.h"
#include <stdio.h>

using namespace std;

char const Path[]="C:\\ownApps\\APIHack\\APIHack.dll";

void SetDebugPrivilege() 
{
	HANDLE hProcess = GetCurrentProcess(), hToken;
	TOKEN_PRIVILEGES priv;
	LUID luid;

	OpenProcessToken(hProcess, TOKEN_ADJUST_PRIVILEGES, &hToken);//�ffnet unser Prozess token zum "anpassen"(adjust) und speichert ein handle zum verwalten in hToken ab.
	LookupPrivilegeValue(0, "seDebugPrivilege", &luid);/*speichert den LUID wert von "seDebugPrivilege" in die LUID-Variable. 0 hei�t, 
	dass es die LUID von unserem System und nicht etwa die eines remote-systems sein soll*/

    priv.PrivilegeCount = 1;
	priv.Privileges[0].Luid = luid;
	priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;//Da wir das Privileg aktivieren wollen und nicht deaktivieren(was keinen effekt h�tte), setzen wir als Attribut "SE_PRIVILEGE_ENABLED"
    AdjustTokenPrivileges(hToken, false, &priv, 0, 0, 0);/*Parameter 1: wir wollen eine Privilegie in eben DIESEM token anpassen; Parameter 2: Alle anderen Privilegien sollen bleiben, wie sie sind;
    Parameter 3: Dass ist unser neues Privileg; Parameter 4-6 sind nur relevant, wenn wir noch das "alte" privileg zur�ckbekommen und speichern wollen -> wollen wir aber nicht, also NULL
    /*wer das vielleicht trotzdem mal muss kriegt hier mehr auskunft �ber die funktion: [Only registered and activated users can see links. ]     */

	CloseHandle(hToken); //Schlie�t das Token wieder -> es m�sste nun wieder mit OpenProcessToken ge�ffnet werden.
	CloseHandle(hProcess);//Schlie�t das Prozess-Objekt, �ber dass auf Prozess-Resourcen zugegriffen werden kann wieder.
}

int main(int argc, char* argv[])
{
	SetDebugPrivilege();
	
	//HMODULE hmod = LoadLibrary("APIHacks.dll");
	//HOOKPROC kbll = (HOOKPROC) GetProcAddress(hmod, "HookKBLL");
	//SetWindowsHookEx(WH_KEYBOARD_LL, kbll, hmod, NULL);
	
    HANDLE hWnd, hProcess, AllocAdresse, hRemoteThread;
    DWORD PID;

    hWnd = FindWindow(0,argv[1]);
	if(hWnd == NULL)
	{
		printf("Couldn't find window");
		return 0;
	}
    GetWindowThreadProcessId((HWND)hWnd, &PID);
    
    hProcess = OpenProcess(PROCESS_ALL_ACCESS, false, PID);

    AllocAdresse = VirtualAllocEx(hProcess, 0, sizeof(Path), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
    WriteProcessMemory(hProcess, (void*)AllocAdresse, (void*)Path, sizeof(Path), 0);
    hRemoteThread=CreateRemoteThread(hProcess, 0, 0, (LPTHREAD_START_ROUTINE) GetProcAddress(GetModuleHandle("kernel32.dll"),"LoadLibraryA"), AllocAdresse, 0, 0);
    
    WaitForSingleObject(hRemoteThread, INFINITE);

    VirtualFreeEx(hProcess, AllocAdresse, sizeof(Path), MEM_DECOMMIT);
    CloseHandle(hProcess);
	return 0;
}