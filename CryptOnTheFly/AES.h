#ifndef AES
#define AES
#include "CryptString.h"
using namespace std;

void FirstRound();
void inv_LastRound();
void inv_Combined();
void FastBlockEnc128();
void FastBlockDec128();
void FastBlockEnc192();
void FastBlockDec192();
void FastBlockEnc256();
void FastBlockDec256();
void KeyExpansion();
void FirstRound();
void LastRound();
void Combined();
bool encrypt(const char *infile, const char *outfile, CryptString key, int keylength);
bool decrypt(const char *infile, const char *outfile, CryptString key);
bool selfEncrypt(const char *infile, const char *outfile, CryptString key, int keylength);
#endif
