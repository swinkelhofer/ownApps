#include <windows.h>
#define DllExport	extern "C" __declspec(dllexport)

DllExport bool UPLAY_HasOverlappedOperationCompleted(...)
{
	return true;
}

DllExport int UPLAY_Init(...)
{
	return 1;
}

DllExport bool UPLAY_OVERLAY_Show(...)
{
	return true;
}


DllExport bool UPLAY_Quit(...)
{
	return true;
}

DllExport int UPLAY_GetLastError(...)
{
	return 0;
}

DllExport int* UPLAY_GetNextEvent(...)
{
	return NULL;
}

DllExport bool UPLAY_ACH_EarnAchievement(...)
{
	return false;
}

DllExport int* UPLAY_ACH_GetAchievements(...)
{
	return NULL;
}

DllExport int* UPLAY_ACH_GetAchievementImage(...)
{
	return NULL;
}

DllExport void UPLAY_ACH_Write(...)
{
}

DllExport int UPLAY_AVATAR_GetAvatarIdForCurrentUser(...)
{
	return 0;
}

DllExport int* UPLAY_AVATAR_GetBitmap(...)
{
	return NULL;
}

DllExport bool UPLAY_ClearGameSession(...)
{
	return false;
}

DllExport int UPLAY_GetInstallationError(...)
{
	return 0;
}

DllExport int UPLAY_GetOverlappedOperationResult(...)
{
	return 1;
}

DllExport bool UPLAY_Release(...)
{
	return true;
}

DllExport void UPLAY_SetGameSession(...)
{
}

DllExport bool UPLAY_Update(...)
{
	return true;
}