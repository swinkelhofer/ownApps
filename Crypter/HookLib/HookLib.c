#include <ctype.h>
#include "HookLib.h"
#define OWN_MSG 0x666


LRESULT CALLBACK HookKB(int nCode, WPARAM wParam, LPARAM lParam)
{
	HWND foreground = GetForegroundWindow();
	DWORD currentThreadId = GetCurrentThreadId();
	DWORD windowThreadId = GetWindowThreadProcessId(foreground, 0);
	if(((wParam >= 0x41 && wParam <= 0x5A) || wParam == VK_SPACE) && !(GetAsyncKeyState(VK_MENU) >> 7) && !(GetAsyncKeyState(VK_CONTROL) >> 7) && !(lParam >> 31))
	{
		if(GetAsyncKeyState(VK_SHIFT) >> 7 || GetKeyState(VK_CAPITAL) >> 7 || GetKeyState(VK_CAPITAL) & 0x1)
		{
			AttachThreadInput(currentThreadId, windowThreadId, true);
			PostMessage(GetFocus(), WM_CHAR, wParam, 1);
			AttachThreadInput(currentThreadId, windowThreadId, false);
		}
		else
		{
			AttachThreadInput(currentThreadId, windowThreadId, true);
			PostMessage(GetFocus(), WM_CHAR, (WPARAM)tolower((char)wParam), 1);
			AttachThreadInput(currentThreadId, windowThreadId, false);
		}
		return 0x1;
	}
	else if(wParam >= 0x30 && wParam <= 0x39 && !(GetAsyncKeyState(VK_CONTROL) >> 7) && !(lParam >> 31))
	{
		if(GetAsyncKeyState(VK_SHIFT) >> 7)
		{
			char send;
			switch((char)wParam)
			{
				case 0x30:	send = '=';
							break;
				case 0x31:	send = '!';
							break;
				case 0x32:	send = '\"';
							break;
				case 0x33:	send = '�';
							break;
				case 0x34:	send = '$';
							break;
				case 0x35:	send = '%';
							break;
				case 0x36:	send = '&';
							break;
				case 0x37:	send = '/';
							break;
				case 0x38:	send = '(';
							break;
				case 0x39:	send = ')';
							break;
			}
			AttachThreadInput(currentThreadId, windowThreadId, true);
			PostMessage(GetFocus(), WM_CHAR, (WPARAM)send, 1);
			AttachThreadInput(currentThreadId, windowThreadId, false);
		}
		else
		{
			AttachThreadInput(currentThreadId, windowThreadId, true);
			PostMessage(GetFocus(), WM_CHAR, wParam, 1);
			AttachThreadInput(currentThreadId, windowThreadId, false);
		}
		return 0x1;
	}
	else if(lParam >> 31)
		return 0x1;
	else
		return CallNextHookEx(NULL, nCode, wParam, lParam);
}

LRESULT CALLBACK HookKBLL(int nCode, WPARAM wParam, LPARAM lParam)
{
	return 0;
}

LRESULT CALLBACK HookWnds(int nCode, WPARAM wParam, LPARAM lParam)
{
	if(nCode == HCBT_DESTROYWND && IsWindow((HWND)wParam) && (HWND)wParam != FindWindow(NULL, TEXT("CryptSuiteWindow")) && ((HWND)wParam != FindWindow(NULL, TEXT("Windows Explorer")) || (HWND)wParam != FindWindow(NULL, TEXT("explorer.exe"))))
	{
		SendMessage(FindWindow(NULL, TEXT("CryptSuiteWindow")), OWN_MSG, wParam,0);
	}
	return CallNextHookEx(NULL, nCode, wParam, lParam);
}