#include <QtGui>
#include "MenuButton.h"
#include "../ColorDefines.h"
#include <windows.h>


MenuButton::MenuButton(QWidget *parent):QWidget(parent)
{
	mousePressed = false;
	effect = new QGraphicsDropShadowEffect;
	effect->setBlurRadius(80);
	alpha = 5;
	effect->setColor(EFFECT_COLOR_MENU);
	effect->setOffset(QPoint(0,0));
	hovered = false;
	setFixedSize(56,56);
	setMouseTracking(true);
	hoverTimer = new QTimer;
	hoverTimer->setInterval(40);
	hoverOutTimer = new QTimer;
	hoverOutTimer->setInterval(40);
	connect(hoverTimer, SIGNAL(timeout()), this, SLOT(updateHover()));
	connect(hoverOutTimer, SIGNAL(timeout()), this, SLOT(hoverOut()));
}

void MenuButton::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	QPixmap pix(":/img/NSA.png");
	painter.drawPixmap(0, 0, pix.scaled(56,56, Qt::KeepAspectRatio, Qt::SmoothTransformation));
}

void MenuButton::mouseMoveEvent(QMouseEvent *event)
{
	if(underMouse() && !hovered)
	{
		alpha = 5;
		effect->setColor(EFFECT_COLOR_MENU);
		effect->setEnabled(true);
		setGraphicsEffect(effect);
		hoverTimer->start();
		hovered = true;
	}
}

void MenuButton::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
		mousePressed = true;
	else
		mousePressed = false;
}

void MenuButton::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed && rect().contains(event->pos()))
		emit clicked();
}

void MenuButton::updateHover()
{
	if(underMouse() && hovered)
	{
		if(alpha <= 205)
			alpha += 50;
		effect->setColor(EFFECT_COLOR_MENU);
		setGraphicsEffect(effect);
	}
	else
	{
		hoverOutTimer->start();
		hoverTimer->stop();
		hovered = false;
	}
}

void MenuButton::hoverOut()
{
	if(alpha > 50 && !hovered)
	{
		alpha -= 50;
		effect->setColor(EFFECT_COLOR_MENU);
		setGraphicsEffect(effect);
	}
	else
	{
		hovered = false;
		hoverOutTimer->stop();
		effect->setEnabled(false);
		setGraphicsEffect(effect);
	}
}