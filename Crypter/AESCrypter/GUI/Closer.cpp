#include <QtGui>
#include "Closer.h"
#include "../ColorDefines.h"

Closer::Closer(QWidget *parent):QWidget(parent)
{
	hoverTimer = new QTimer;
	hoverTimer->setInterval(100);
	hovered = false;
	mousePressed = false;
	setFixedSize(15,14);
	setMouseTracking(true);
	connect(hoverTimer, SIGNAL(timeout()), this, SLOT(updateHover()));
}

void Closer::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.drawPixmap(0,0,15,14,QPixmap(":/img/Close.png").scaled(15,14, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
}

void Closer::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		QGraphicsColorizeEffect *effect = new QGraphicsColorizeEffect;
		effect->setColor(EFFECT_COLOR_CLOSER);
		effect->setStrength(0.7);
		setGraphicsEffect(effect);
		mousePressed = true;
	}
	else
		mousePressed = false;
}

void Closer::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed && rect().contains(event->pos()))
		emit clicked();
	else
		mousePressed = false;
}

void Closer::mouseMoveEvent(QMouseEvent *event)
{
	if(underMouse() && !hovered)
	{
		hoverTimer->start();
		QGraphicsColorizeEffect *effect = new QGraphicsColorizeEffect;
		effect->setStrength(0.4);
		effect->setColor(EFFECT_COLOR_CLOSER);
		setGraphicsEffect(effect);
	}
}

void Closer::updateHover()
{
	if(!underMouse())
	{
		hovered = false;
		hoverTimer->stop();
		QGraphicsBlurEffect *effect = new QGraphicsBlurEffect;
		effect->setEnabled(false);
		setGraphicsEffect(effect);
	}
}
