#include <QWidget>
#include <QListWidget>
#include <QPushButton>
class QListWidgetItem;
class Menu:public QWidget
{
	Q_OBJECT
public:
	Menu(QWidget *parent = 0);
protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void resizeEvent(QResizeEvent *event);
private slots:
	void hideAfterDoubleClick(QListWidgetItem *item);
	void openControl();
private:
	QListWidget *list;
	QPushButton *options;
signals:
	void itemDoubleClicked(QListWidgetItem *item);
	void ctrlChanged();
};
