#include <QtGui>
#include "Menu.h"
#include "../Dialogs/ControlDialog.h"
#include "../ColorDefines.h"

Menu::Menu(QWidget *parent):QWidget(parent)
{
	setFixedWidth(200);
	setFixedHeight(250);
	list = new QListWidget(this);
	options = new QPushButton("Einstellungen", this);
	options->move(200 - options->size().width() - 3, 250 - options->size().height() - 3);
	QPalette listPalette;
	listPalette.setColor(QPalette::Window, TRANSPARENT_COLOR);
	listPalette.setColor(QPalette::Text, BLACK_COLOR);
	listPalette.setColor(QPalette::Base, TRANSPARENT_COLOR);
	listPalette.setColor(QPalette::Button, TRANSPARENT_COLOR);
	list->setFrameStyle(QFrame::NoFrame | QFrame::Plain);
	list->setPalette(listPalette);
	list->setGeometry(10,10,180, 250 - options->size().height() - 3);
	list->setIconSize(QSize(24,24));
	
	
	QListWidgetItem *self = new QListWidgetItem(QIcon(QPixmap(":/img/NSA.png").scaled(24,24,Qt::KeepAspectRatio,Qt::SmoothTransformation)),"SelfDecrypt");
	self->setToolTip("Verschlüsselt eine gewählte Datei in eine sich selbst entschlüsselnde .exe-Datei,\nfür deren Entschlüsselung das gerade verwendete Programm nicht auf dem jeweiligen\nPC vorhanden sein muss.");
	list->addItem(self);
	QListWidgetItem *pwcreator = new QListWidgetItem(QIcon(QPixmap(":/img/NSA.png").scaled(24,24,Qt::KeepAspectRatio,Qt::SmoothTransformation)),"PasswordCreator");
	pwcreator->setToolTip("Erstellt sichere Passwörter.");
	list->addItem(pwcreator);
	QListWidgetItem *pwtester = new QListWidgetItem(QIcon(QPixmap(":/img/NSA.png").scaled(24,24,Qt::KeepAspectRatio,Qt::SmoothTransformation)),"PasswordTester");
	pwtester->setToolTip("Testet die Sicherheit eines Passworts.");
	list->addItem(pwtester);
	//list->addItem("RSA KeyPair Creator");
	QListWidgetItem *kb = new QListWidgetItem(QIcon(QPixmap(":/img/NSA.png").scaled(24,24,Qt::KeepAspectRatio,Qt::SmoothTransformation)),"Bildschirmtastatur");
	kb->setToolTip("Sichere Texteingabe, beispielsweise für Passwörter,\ndie nicht von einem Keylogger abgefangen werden kann.");
	list->addItem(kb);
	

	//QListWidgetItem *arch = new QListWidgetItem(QIcon(QPixmap(":/img/NSA.png").scaled(24,24,Qt::KeepAspectRatio,Qt::SmoothTransformation)),"ArchivCrypt");
	//arch->setToolTip("Erstellen und Verwalten von verschlüsselten Archiven");
	//list->addItem(arch);
	connect(list, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SIGNAL(itemDoubleClicked(QListWidgetItem *)));
	connect(list, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(hideAfterDoubleClick(QListWidgetItem *)));
	connect(options, SIGNAL(clicked()), this, SLOT(openControl()));
}

void Menu::openControl()
{
	hide();
	ControlDialog *ctrl = new ControlDialog;
	ctrl->setModal(true);
	ctrl->show();
	connect(ctrl, SIGNAL(closed()), this, SIGNAL(ctrlChanged()));
}

void Menu::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QLinearGradient grad;
	grad.setStart(0.5,0);
	grad.setFinalStop(0.5, 1);
	grad.setColorAt(0, FOREGROUND_COLOR);
	grad.setColorAt(0.64, MENU_MIDDLE_COLOR);
	grad.setColorAt(1, FOREGROUND_COLOR);
	grad.setCoordinateMode(QGradient::StretchToDeviceMode);
	painter.setBrush(QBrush(grad));
	painter.setPen(Qt::NoPen);
	painter.drawRect(0,0,200, 250);
}

void Menu::resizeEvent(QResizeEvent *event)
{
	options->move(200 - options->size().width() - 3, 250 - options->size().height() - 3);
}

void Menu::mousePressEvent(QMouseEvent *event)
{
}

void Menu::mouseReleaseEvent(QMouseEvent *event)
{
}

void Menu::hideAfterDoubleClick(QListWidgetItem *item)
{
	list->setCurrentRow(-1, QItemSelectionModel::Clear);
	hide();
}