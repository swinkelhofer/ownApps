#include <QtGui>
#include "ClickButton.h"

ClickButton::ClickButton(QWidget *parent):QWidget(parent)
{
	mousePressed = false;
	toggle = false;
	mainPix = QPixmap(":/img/SecureOff.png").scaled(56,56, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	setFixedSize(56,56);
}

void ClickButton::setState(bool toggled)
{
	toggle = toggled;
	if(toggle)
	{
		mainPix = QPixmap(":/img/SecureOn.png").scaled(56,56, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	}
	else
	{
		mainPix = QPixmap(":/img/SecureOff.png").scaled(56,56, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	}
	update();
}

void ClickButton::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.drawPixmap(0,0,56,56,mainPix);
}

void ClickButton::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
		mousePressed = true;
	else
		mousePressed = false;
}
	
void ClickButton::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed && rect().contains(event->pos()))
	{
		toggle = !toggle;
		emit toggled(toggle);
		if(toggle)
		{
			mainPix = QPixmap(":/img/SecureOn.png").scaled(56,56, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
		}
		else
		{
			mainPix = QPixmap(":/img/SecureOff.png").scaled(56,56, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
		}
		update();
	}
}