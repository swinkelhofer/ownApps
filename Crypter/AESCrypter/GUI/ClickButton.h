#include <QWidget>
#ifndef CLICK_BUTTON
#define CLICK_BUTTON
class QPixmap;
class ClickButton:public QWidget
{
	Q_OBJECT
public:
	ClickButton(QWidget *parent = 0);
	void setState(bool toggled);
protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
private:
	QPixmap mainPix;
	bool mousePressed;
	bool toggle;
signals:
	void toggled(bool toggleState);
};
#endif
