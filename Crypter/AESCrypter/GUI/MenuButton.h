#include <QWidget>

class QTimer;
class QGraphicsDropShadowEffect;

class MenuButton:public QWidget
{
	Q_OBJECT
public:
	MenuButton(QWidget *parent = 0);
protected:
	void paintEvent(QPaintEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
private:
	QTimer *hoverTimer, *hoverOutTimer;
	int alpha;
	bool hovered, mousePressed;
	QGraphicsDropShadowEffect *effect;
private slots:
	void updateHover();
	void hoverOut();
signals:
	void clicked();
};