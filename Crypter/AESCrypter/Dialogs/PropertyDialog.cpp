#include <QtGui>
#include "PropertyDialog.h"
#include "../GUI/Closer.h"
#include "../ColorDefines.h"

PropertyDialog::PropertyDialog(QString filename, QWidget *parent):QDialog(parent)
{
	fileName = filename;
	mousePressed = false;
	setAttribute(Qt::WA_TranslucentBackground,true);
	setWindowFlags(Qt::FramelessWindowHint);
	
	QPalette listPalette;
	listPalette.setColor(QPalette::Text, FOREGROUND_TEXT_COLOR);
	listPalette.setColor(QPalette::WindowText, FOREGROUND_TEXT_COLOR);
	setPalette(listPalette);
	
	closeButton = new Closer(this);
	closeButton->setGeometry(size().width() - 23, 8, 15, 14);
	QGridLayout *grid = new QGridLayout;
	QLabel *icon = new QLabel;
	QPixmap pix;
	if(pix.load(filename))
		icon->setPixmap(pix.scaled(64,64, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
	else
		icon->setPixmap(QFileIconProvider().icon(QFileInfo(filename)).pixmap(64, 64));
	grid->addItem(new QSpacerItem(10, 30),0,0);
	grid->addWidget(icon, 1, 0);
	grid->addWidget(new QLabel("Dateiname:"), 2, 0);
	grid->addWidget(new QLabel("Ort:"), 3, 0);
	grid->addWidget(new QLabel("Größe:"), 4, 0);
	grid->addWidget(new QLabel("Erstelldatum:"), 5, 0);
	grid->addWidget(new QLabel("Änderungsdatum:"), 6, 0);
	grid->addWidget(new QLabel("Letzter Zugriff:"), 7, 0);
	grid->addWidget(new QLabel(QFileInfo(filename).fileName()), 2,1);
	grid->addWidget(new QLabel(QFileInfo(filename).absolutePath()), 3,1);
	if((long double)QFileInfo(filename).size() / (long double)1024 > 1000)
		grid->addWidget(new QLabel(QString::number((double)((long double)QFileInfo(filename).size() / (long double)1048576), 'f', 2) + QString(" MB (") + QString::number(QFileInfo(filename).size()) + QString(" Bytes)")), 4,1);
	else
		grid->addWidget(new QLabel(QString::number((double)((long double)QFileInfo(filename).size() / (long double)1024), 'f', 2) + QString(" KB (") + QString::number(QFileInfo(filename).size()) + QString(" Bytes)")), 4,1);
	grid->addWidget(new QLabel(QFileInfo(filename).created().toString("dddd, dd. MMMM yyyy, hh:mm:ss")), 5,1);
	grid->addWidget(new QLabel(QFileInfo(filename).lastModified().toString("dddd, dd. MMMM yyyy, hh:mm:ss")), 6,1);
	grid->addWidget(new QLabel(QFileInfo(filename).lastRead().toString("dddd, dd. MMMM yyyy, hh:mm:ss")), 7,1);
	setLayout(grid);
	connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
}

void PropertyDialog::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setBrush(FOREGROUND_COLOR);
	painter.setPen(Qt::NoPen);
	painter.drawRect(0,0,size().width(), size().height());
	painter.drawPixmap(0, 0, size().width(), 30, QPixmap(":/img/Leiste.png").scaled(600, 30, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
	painter.setPen(TITLEBAR_TEXT_COLOR);
	painter.setFont(QFont("Arial", 9, QFont::Bold));
	painter.drawText(8,8,size().width() - 8, 22, Qt::AlignLeft, QFileInfo(fileName).fileName());
}

void PropertyDialog::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		currentGlobalPos = event->globalPos();
		mousePressed = true;
	}
	else
		mousePressed = false;
}

void PropertyDialog::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed)
	{
		mousePressed = false;
		move(pos() - (currentGlobalPos - event->globalPos()));
	}
}

void PropertyDialog::mouseMoveEvent(QMouseEvent *event)
{
	if(mousePressed)
	{
		move(pos() - (currentGlobalPos - event->globalPos()));
		currentGlobalPos = event->globalPos();
	}
}

void PropertyDialog::resizeEvent(QResizeEvent *event)
{
	closeButton->setGeometry(size().width() - 23, 8, 15, 14);
}
