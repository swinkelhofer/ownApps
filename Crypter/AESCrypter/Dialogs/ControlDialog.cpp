#include <QtGui>
#include "ControlDialog.h"
#include "../GUI/Closer.h"
#include "../ColorDefines.h"

ControlDialog::ControlDialog(QWidget *parent):QDialog(parent)
{
	mousePressed = false;
	saveMode = new QCheckBox("Sicheren Eingabemodus zur Verf�gung stellen");
	Ok = new QPushButton("OK");
	Cancel = new QPushButton("Abbrechen");
	setAttribute(Qt::WA_TranslucentBackground,true);
	setWindowFlags(Qt::FramelessWindowHint);
	
	QPalette listPalette;
	listPalette.setColor(QPalette::Text, FOREGROUND_TEXT_COLOR);
	listPalette.setColor(QPalette::WindowText, FOREGROUND_TEXT_COLOR);
	setPalette(listPalette);
	QHBoxLayout *down =  new QHBoxLayout;
	down->addWidget(Ok);
	down->addWidget(Cancel);
	QVBoxLayout *main = new QVBoxLayout;
	main->addSpacing(30);
	main->addWidget(saveMode);
	main->addLayout(down);
	setLayout(main);
	
	
	QFile controlFile("Control.dll");
	controlFile.open(QIODevice::ReadOnly);
	QTextStream controlStream(&controlFile);
	QString control;
	do
	{
		control = controlStream.readLine();
		if(control.startsWith("SaveMode="))
		{
			if(control.endsWith("On"))
				saveMode->setChecked(true);
			else
				saveMode->setChecked(false);
		}
	}
	while(!control.isNull());
	controlFile.close();
	
	
	closeButton = new Closer(this);
	closeButton->setGeometry(size().width() - 23, 8, 15, 14);
	
	connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
	connect(Cancel, SIGNAL(clicked()), this, SLOT(close()));
	connect(Ok, SIGNAL(clicked()), this, SLOT(save()));
}

void ControlDialog::save()
{
	QFile controlFile("Control.dll");
	controlFile.open(QIODevice::WriteOnly);
	QTextStream controlStream(&controlFile);
	
	controlStream << "SaveMode=";
	if(saveMode->isChecked())
		controlStream << "On\r\n";
	else
		controlStream << "Off\r\n";
	controlFile.close();
	emit closed();
	close();
}

void ControlDialog::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setBrush(FOREGROUND_COLOR);
	painter.setPen(Qt::NoPen);
	painter.drawRect(0,0,size().width(), size().height());
	painter.drawPixmap(0, 0, size().width(), 30, QPixmap(":/img/Leiste.png").scaled(600, 30, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
	painter.setPen(TITLEBAR_TEXT_COLOR);
	painter.setFont(QFont("Arial", 9, QFont::Bold));
	painter.drawText(8,8,size().width() - 8, 22, Qt::AlignLeft, "Einstellungen");
}

void ControlDialog::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		currentGlobalPos = event->globalPos();
		mousePressed = true;
	}
	else
		mousePressed = false;
}

void ControlDialog::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed)
	{
		mousePressed = false;
		move(pos() - (currentGlobalPos - event->globalPos()));
	}
}

void ControlDialog::mouseMoveEvent(QMouseEvent *event)
{
	if(mousePressed)
	{
		move(pos() - (currentGlobalPos - event->globalPos()));
		currentGlobalPos = event->globalPos();
	}
}

void ControlDialog::resizeEvent(QResizeEvent *event)
{
	move(QApplication::desktop()->screenGeometry().width()/2 - size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2);
	closeButton->setGeometry(size().width() - 23, 8, 15, 14);
}
