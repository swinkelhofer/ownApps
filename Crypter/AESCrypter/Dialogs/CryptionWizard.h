#ifndef CRYPTION_WIZARD
#define CRYPTION_WIZARD
#include <QWizardPage>
#include <windows.h>
#include "../SHA2Lib.h"

class QProgressBar;
class QComboBox;
class QLabel;
class QLineEdit;
class QCheckBox;
class QTextEdit;
class QWidget;
class QPushButton;

class createPasswordPage:public QWizardPage
{
	Q_OBJECT
public:
	createPasswordPage(bool Encryption = false, bool Decryption = false, QWidget *parent = 0);
	int nextId() const;
private:
	QProgressBar *strong;
	QComboBox *combo;
	QLabel *strongText;
	QLineEdit *pw1, *pw2;
	QCheckBox *delOld;
	bool encrypt, decrypt;
private slots:
	void updateStrongness(const QString &text);
};


class startEncryptDecryptPage:public QWizardPage
{
	Q_OBJECT
public:
	startEncryptDecryptPage(QWidget *parent = 0);
	void initializePage();
protected:
	void keyPressEvent(QKeyEvent *event);
private:
	QTextEdit *edit;
	bool encrypt, decrypt, deleteOldFile;
	int comboItem;
	QString password;
};


class encryptDecryptPage:public QWizardPage
{
	Q_OBJECT
public:
	encryptDecryptPage(QStringList files, bool Encryption = false, bool Decryption = false, QWidget *parent = 0);
	void initializePage();
private:
	QProgressBar *progress;
	QTextEdit *out;
	QStringList files0;
	bool encrypt, decrypt;
	HMODULE hmod;
	SHA256Function SHA256;
signals:
	void fileEncrypted(QString file);
	void fileDecrypted(QString file);
};

class selectSelfEncryptPage:public QWizardPage
{
	Q_OBJECT
public:
	selectSelfEncryptPage(QWidget *parent = 0);
	int nextId() const;
private:
	QProgressBar *strong;
	QComboBox *combo;
	QLabel *strongText;
	QLineEdit *pw1, *pw2;
	QCheckBox *delOld;
	QPushButton *changeFile;
	QLineEdit *fileEdit;
private slots:
	void insertFile();
	void updateStrongness(const QString &text);
};
#endif
