#include <QtGui>
#include <math.h>
#include "CryptionWizard.h"
#include "../Cryptography/CryptString.h"
#include "../Cryptography/AES.h"
#include "../Cryptography/SecureDeletion.h"
//#include "../Cryptography/SHA2.h"
//#include "../SHA2Lib.h"

createPasswordPage::createPasswordPage(bool Encryption, bool Decryption, QWidget *parent):QWizardPage(parent)
{
	//setTitle("Passwort");
	pw1 = new QLineEdit;
	pw2 = new QLineEdit;
	encrypt = Encryption;
	decrypt = Decryption;
	if(encrypt && !decrypt)
	{
		delOld = new QCheckBox("Alte Datei(en) sicher löschen");
		strong = new QProgressBar;
		strong->setOrientation(Qt::Horizontal);
		strong->setRange(0,100);
		strong->setTextVisible(false);
		strong->setValue(0);
		strongText = new QLabel("sehr unsicher");
		pw1->setEchoMode(QLineEdit::Password);
		pw2->setEchoMode(QLineEdit::Password);
		combo = new QComboBox;
		combo->addItem("AES256 (Standard)");
		combo->addItem("AES192");
		combo->addItem("AES128 (Unsicher)");
		//combo->addItem("SASCHA256");
		QFrame *groupBox1 = new QFrame;
		QGridLayout *group1Layout = new QGridLayout;
		group1Layout->addWidget(new QLabel("Passwort:"), 0,0);
		group1Layout->addWidget(new QLabel("Passwort wiederholen:"), 1, 0);
		group1Layout->addWidget(pw1, 0, 1);
		group1Layout->addWidget(pw2, 1, 1);
		group1Layout->addWidget(strong, 2,1);
		group1Layout->addWidget(strongText, 3,1);
		groupBox1->setLayout(group1Layout);
		
		QGroupBox *groupBox2 = new QGroupBox("Optionen");
		QVBoxLayout *group2Layout = new QVBoxLayout;
		group2Layout->addWidget(delOld);
		
		QHBoxLayout *comboLayout = new QHBoxLayout;
		comboLayout->addWidget(new QLabel("Verschlüsselungsstandard:"));
		comboLayout->addWidget(combo);
		group2Layout->addLayout(comboLayout);
		groupBox2->setLayout(group2Layout);
		
		QVBoxLayout *main = new QVBoxLayout;
		main->addWidget(groupBox1);
		main->addStretch();
		main->addWidget(groupBox2);
		setLayout(main);
		connect(pw1, SIGNAL(textChanged(const QString &)), this, SLOT(updateStrongness(const QString &)));
		registerField("password*", pw1);
		registerField("password2*", pw2);
		registerField("delOldFileChecked", delOld);
		registerField("encryptionStandard", combo);
	}
	else if(decrypt && !encrypt)
	{
		QVBoxLayout *main = new QVBoxLayout;
		pw1->setEchoMode(QLineEdit::Password);
		main->addWidget(new QLabel("Bitte Passwort zum Entschlüsseln eingeben:"));
		main->addWidget(pw1);
		setLayout(main);
		registerField("passwordDec*", pw1);
	}
}

int createPasswordPage::nextId() const
{
	if(encrypt && !decrypt)
	{
		if((field("password").toString() == field("password2").toString()) && field("password").toString().length() > 0)
		{
			return 1;
		}
		else
		{
			pw1->setText("");
			pw2->setText("");
			strong->setValue(0);
			strongText->setText("sehr unsicher");
			return 0;
		}
	}
	return 1;
}

void createPasswordPage::updateStrongness(const QString &text)
{
	if(encrypt && !decrypt)
	{
		if(text.length() == 0)
		{
			strong->setValue(0);
			return;
		}
		int A = 0, ka = 0, z = 0, s = 0, strongness = 0;
		for(int i = 0; i < text.length(); ++i)
		{
			if(text[i].isDigit())
				++z;
			else if(text[i].isUpper())
				++A;
			else if(text[i].isLower())
				++ka;
			else if(text[i].isPrint())
				++s;
		}
		strongness = (int)((int)(((double)A*log10(29) + (double)ka*log10(30) + (double)z*log10(10) + (double)s*log10(36))*4.55) + (int)((double)text.length() * log10(29*(A > 0) + 30*(ka > 0) + 10*(z > 0) + 36*(s > 0)) * 4.13)) / 2;
		if(strongness > 100)
			strongness = 100;
		strong->setValue(strongness);
		
		if(strongness < 30)
			strongText->setText("sehr unsicher");
		else if(strongness >= 30 && strongness < 45)
			strongText->setText("unsicher");
		else if(strongness >= 45 && strongness < 80)
			strongText->setText("normal");
		else if(strongness >= 80 && strongness < 95)
			strongText->setText("sicher");
		else if(strongness >= 95)
			strongText->setText("sehr sicher");
	}
}

startEncryptDecryptPage::startEncryptDecryptPage(QWidget *parent):QWizardPage(parent)
{
	password = QString("");
	deleteOldFile = false;
	edit = new QTextEdit;
	edit->setReadOnly(true);
	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(new QLabel("Es wird nun mit dem Verschlüsseln der ausgewählten Dateien fortgefahren.<br>Folgende Optionen wurden gesetzt:<br>"));
	mainLayout->addWidget(edit);
	setLayout(mainLayout);
}

void startEncryptDecryptPage::keyPressEvent(QKeyEvent *event)
{
	if(event->key() == Qt::Key_Return)
		return;
	else
		QWizardPage::keyPressEvent(event);
}

void startEncryptDecryptPage::initializePage()
{
	password = field("password").toString();
	deleteOldFile = field("delOldFileChecked").toBool();
	comboItem = field("encryptionStandard").toInt();
	QString conclusionText = "";
	if(password.length() > 0)
		conclusionText += "+ Passwort gesetzt<br>";
	if(deleteOldFile)
		conclusionText += "+ Alte Datei(en) sicher löschen<br>";
	switch(comboItem)
	{
		case 0: 	conclusionText += "+ Verschlüsselungsstandard: AES256 (Standard)<br>";
					break;
		case 1: 	conclusionText += "+ Verschlüsselungsstandard: AES192<br>";
					break;
		case 2: 	conclusionText += "+ Verschlüsselungsstandard: AES128 (Unsicher)<br>";
					break;
		default:	conclusionText += "- Fehler bei der Auswahl des Verschlüsselungsstandards<br>";
					break;
	}
	edit->setText(conclusionText);
}

CryptString SHA256ToCryptString(SHA256Hash a)
{
	CryptString b;
	for(int i = 0; i < 64; ++i)
	{
		switch(a.hash[i])
		{
			case '0':	b.str[i >> 1] |= (0x0 << (((i+1) & 0x1) << 2));
						break;
			case '1':	b.str[i >> 1] |= (0x1 << (((i+1) & 0x1) << 2));
						break;
			case '2':	b.str[i >> 1] |= (0x2 << (((i+1) & 0x1) << 2));
						break;
			case '3':	b.str[i >> 1] |= (0x3 << (((i+1) & 0x1) << 2));
						break;
			case '4':	b.str[i >> 1] |= (0x4 << (((i+1) & 0x1) << 2));
						break;
			case '5':	b.str[i >> 1] |= (0x5 << (((i+1) & 0x1) << 2));
						break;
			case '6':	b.str[i >> 1] |= (0x6 << (((i+1) & 0x1) << 2));
						break;
			case '7':	b.str[i >> 1] |= (0x7 << (((i+1) & 0x1) << 2));
						break;
			case '8':	b.str[i >> 1] |= (0x8 << (((i+1) & 0x1) << 2));
						break;
			case '9':	b.str[i >> 1] |= (0x9 << (((i+1) & 0x1) << 2));
						break;
			case 'a':	b.str[i >> 1] |= (0xa << (((i+1) & 0x1) << 2));
						break;
			case 'b':	b.str[i >> 1] |= (0xb << (((i+1) & 0x1) << 2));
						break;
			case 'c':	b.str[i >> 1] |= (0xc << (((i+1) & 0x1) << 2));
						break;
			case 'd':	b.str[i >> 1] |= (0xd << (((i+1) & 0x1) << 2));
						break;
			case 'e':	b.str[i >> 1] |= (0xe << (((i+1) & 0x1) << 2));
						break;
			case 'f':	b.str[i >> 1] |= (0xf << (((i+1) & 0x1) << 2));
						break;
		}
	}
	return b;
}

encryptDecryptPage::encryptDecryptPage(QStringList files, bool Encryption, bool Decryption, QWidget *parent):QWizardPage(parent)
{
	files0 = files;
	encrypt = Encryption;
	decrypt = Decryption;
	progress = new QProgressBar;
	progress->setOrientation(Qt::Horizontal);
	progress->setRange(0,100);
	progress->setTextVisible(false);
	progress->setValue(0);
	out = new QTextEdit;
	out->setReadOnly(true);
	QVBoxLayout *main = new QVBoxLayout;
	main->addWidget(progress);
	main->addWidget(out);
	
	hmod = LoadLibrary(TEXT("SHA2Lib.dll"));
	SHA256 = (SHA256Function) GetProcAddress(hmod, "SHA256");
	if(hmod == NULL || SHA256 == NULL)
		exit(0);
	
	setLayout(main);
}

void encryptDecryptPage::initializePage()
{
	QString text = "";
	if(encrypt && !decrypt)
	{
		int keylength = 256;
		CryptString key = SHA256ToCryptString(SHA256(field("password").toString().toStdString().c_str()));
		QString chpw = field("password").toString();
		//for(int i = 0; i < chpw.length(); ++i)
		//	chpw[i] = '0';
		//setField("password", chpw);
		//key.set(field("password").toString().toStdString().c_str());
		//key.print();
		switch(field("encryptionStandard").toInt())
		{
			case 0:		keylength = 256;
						break;
			case 1:		keylength = 192;
						break;
			case 2:		keylength = 128;
						break;
		}
		for(int i = 0; i < files0.count(); ++i)
		{
			progress->setValue(((i + 1) * 100) / files0.count());
			if(::encrypt(files0.at(i).toStdString().c_str(), (files0.at(i) + QString(".crypt")).toStdString().c_str(), key, keylength))
			{
				emit fileEncrypted(files0.at(i));
				text += QString("+ Verschlüsselt: ") + files0.at(i) + QString("<br>");
				if(field("delOldFileChecked").toBool())
				{
					if(::delSecure(files0.at(i).toStdString().c_str(), 3))
						text += QString("+ ") + files0.at(i) + QString(" wurde erfolgreich gelöscht<br>");
					else
						text += QString("+ Fehler beim Löschen von ") + files0.at(i) + QString("<br>");
				}
			}
			else
				text += QString("+ Fehler beim Verschlüsseln von ") + files0.at(i) + QString("<br>");
			out->setHtml(text);
		}
	}
	else if(decrypt && !encrypt)
	{
		CryptString key = SHA256ToCryptString(SHA256(field("passwordDec").toString().toStdString().c_str()));
		//key.set(field("passwordDec").toString().toStdString().c_str());
		//key.print();
		QString chpw = field("passwordDec").toString();
		for(int i = 0; i < chpw.length(); ++i)
			chpw[i] = '0';
		setField("passwordDec", chpw);
		for(int i = 0; i < files0.count(); ++i)
		{
			progress->setValue(((i + 1) * 100) / files0.count());
			QString file = files0.at(i);
			file.remove(0, 16);
			
			if(::decrypt(file.toStdString().c_str(), file.left(file.length() - 6).toStdString().c_str(), key))
			{
				emit fileDecrypted(file);
				text += QString("+ Entschlüsselt: ") + file + QString("<br>");
			}
			else
				text += QString("+ Fehler beim Entschlüsseln von ") + file + QString("<br>");
			out->setHtml(text);
		}
	}
	else if(encrypt && decrypt)
	{
		int keylength = 256;
		switch(field("encryptionStandard").toInt())
		{
			case 0:		keylength = 256;
						break;
			case 1:		keylength = 192;
						break;
			case 2:		keylength = 128;
						break;
		}
		CryptString key = SHA256ToCryptString(SHA256(field("passwordSelf").toString().toStdString().c_str()));
		QString chpw = field("passwordSelf").toString();
		for(int i = 0; i < chpw.length(); ++i)
			chpw[i] = '0';
		setField("passwordSelf", chpw);
		//key.set(field("passwordSelf").toString().toStdString().c_str());
		progress->setValue(100);
		if(::selfEncrypt(field("file").toString().toStdString().c_str(), (field("file").toString() + QString(".exe")).toStdString().c_str(), key, keylength))
		{
			if(field("delOldFileChecked").toBool())
			{
				if(::delSecure(field("file").toString().toStdString().c_str(), 3))
					text += QString("+ ") + field("file").toString() + QString(" wurde erfolgreich gelöscht<br>");
				else
					text += QString("+ Fehler beim Löschen von ") + field("file").toString() + QString("<br>");
			}
			text += QString("+ In selbstentschlüsselnde Datei verschlüsselt: ") + field("file").toString() + QString("<br>");
		}
		else
			text += QString("+ Fehler beim Verschlüsseln in selbstentschlüsselnde Datei<br>");
		out->setHtml(text);
	}
}

selectSelfEncryptPage::selectSelfEncryptPage(QWidget *parent):QWizardPage(parent)
{
	delOld = new QCheckBox("Alte Datei sicher löschen");
	strong = new QProgressBar;
	strong->setOrientation(Qt::Horizontal);
	strong->setRange(0,100);
	strong->setTextVisible(false);
	strong->setValue(0);
	strongText = new QLabel("sehr unsicher");
	pw1 = new QLineEdit;
	pw2 = new QLineEdit;
	pw1->setEchoMode(QLineEdit::Password);
	pw2->setEchoMode(QLineEdit::Password);		
	combo = new QComboBox;
	combo->addItem("AES256 (Standard)");
	combo->addItem("AES192");
	combo->addItem("AES128 (Unsicher)");
	//combo->addItem("SASCHA256");
	QFrame *groupBox1 = new QFrame;
	QGridLayout *group1Layout = new QGridLayout;
	group1Layout->addWidget(new QLabel("Passwort:"), 0,0);
	group1Layout->addWidget(new QLabel("Passwort wiederholen:"), 1, 0);
	group1Layout->addWidget(pw1, 0, 1);
	group1Layout->addWidget(pw2, 1, 1);
	group1Layout->addWidget(strong, 2,1);
	group1Layout->addWidget(strongText, 3,1);
	groupBox1->setLayout(group1Layout);
	
	QGroupBox *groupBox2 = new QGroupBox("Optionen");
	QVBoxLayout *group2Layout = new QVBoxLayout;
	group2Layout->addWidget(delOld);
	
	QGroupBox *groupFileBox = new QGroupBox("Datei auswählen");
	QHBoxLayout *groupFileLayout = new QHBoxLayout;
	fileEdit = new QLineEdit;
	fileEdit->setReadOnly(true);
	changeFile = new QPushButton("Ändern");
	groupFileLayout->addWidget(fileEdit);
	groupFileLayout->addWidget(changeFile);
	groupFileBox->setLayout(groupFileLayout);
	
	
	QHBoxLayout *comboLayout = new QHBoxLayout;
	comboLayout->addWidget(new QLabel("Verschlüsselungsstandard:"));
	comboLayout->addWidget(combo);
	group2Layout->addLayout(comboLayout);
	groupBox2->setLayout(group2Layout);
	
	QVBoxLayout *main = new QVBoxLayout;
	main->addWidget(groupFileBox);
	main->addWidget(groupBox1);
	main->addStretch();
	main->addWidget(groupBox2);
	setLayout(main);
	connect(changeFile, SIGNAL(clicked()), this, SLOT(insertFile()));
	connect(pw1, SIGNAL(textChanged(const QString &)), this, SLOT(updateStrongness(const QString &)));
	connect(pw1, SIGNAL(returnPressed()), this, SLOT(update()));
	connect(pw2, SIGNAL(returnPressed()), this, SLOT(update()));
	connect(fileEdit, SIGNAL(returnPressed()), this, SLOT(update()));
	registerField("passwordSelf*", pw1);
	registerField("passwordSelf2*", pw2);
	registerField("file*", fileEdit);
	registerField("delOldFileChecked", delOld);
	registerField("encryptionStandard", combo);
}

int selectSelfEncryptPage::nextId() const
{
	if((field("passwordSelf").toString() == field("passwordSelf2").toString()) && field("passwordSelf").toString().length() > 0)
	{
		return 1;
	}
	else
	{
		pw1->setText("");
		pw2->setText("");
		strong->setValue(0);
		strongText->setText("sehr unsicher");
		return 0;
	}
	return 1;
}

void selectSelfEncryptPage::insertFile()
{
	fileEdit->setText(QFileDialog::getOpenFileName(this, "Datei auswählen", "C:/"));
}

void selectSelfEncryptPage::updateStrongness(const QString &text)
{
	if(text.length() == 0)
	{
		strong->setValue(0);
		return;
	}
	int A = 0, ka = 0, z = 0, s = 0, strongness = 0;
	for(int i = 0; i < text.length(); ++i)
	{
		if(text[i].isDigit())
			++z;
		else if(text[i].isUpper())
			++A;
		else if(text[i].isLower())
			++ka;
		else if(text[i].isPrint())
			++s;
	}
	strongness = (int)((int)(((double)A*log10(29) + (double)ka*log10(30) + (double)z*log10(10) + (double)s*log10(36))*4.55) + (int)((double)text.length() * log10(29*(A > 0) + 30*(ka > 0) + 10*(z > 0) + 36*(s > 0)) * 4.13)) / 2;
	if(strongness > 100)
		strongness = 100;
	strong->setValue(strongness);
	
	if(strongness < 30)
		strongText->setText("sehr unsicher");
	else if(strongness >= 30 && strongness < 45)
		strongText->setText("unsicher");
	else if(strongness >= 45 && strongness < 80)
		strongText->setText("normal");
	else if(strongness >= 80 && strongness < 95)
		strongText->setText("sicher");
	else if(strongness >= 95)
		strongText->setText("sehr sicher");
}

