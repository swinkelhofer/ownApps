#ifndef PROPERTY_DIALOG
#define PROPERTY_DIALOG
#include <QDialog>

class Closer;
class QPoint;
class PropertyDialog:public QDialog
{
	Q_OBJECT
public:
	PropertyDialog(QString filename, QWidget *parent = 0);
protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void resizeEvent(QResizeEvent *event);
private:
	bool mousePressed;
	QPoint currentGlobalPos;
	Closer *closeButton;
	QString fileName;
};
#endif
