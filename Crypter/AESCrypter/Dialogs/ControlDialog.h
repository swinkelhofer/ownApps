#ifndef CONTROL_DIALOG
#define CONTROL_DIALOG
#include <QDialog>

class Closer;
class QPoint;
class QCheckBox;
class QPushButton;
class ControlDialog:public QDialog
{
	Q_OBJECT
public:
	ControlDialog(QWidget *parent = 0);
protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void resizeEvent(QResizeEvent *event);
private:
	bool mousePressed;
	QPoint currentGlobalPos;
	Closer *closeButton;
	QCheckBox *saveMode;
	QPushButton *Ok, *Cancel;
private slots:
	void save();
signals:
	void closed();
};
#endif