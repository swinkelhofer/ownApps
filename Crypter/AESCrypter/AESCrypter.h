#ifndef AES_CRYPTER
#define AES_CRYPTER
#include <QMainWindow>
#include <windows.h>

class QListWidget;
class QLabel;
class QPushButton;
class QToolButton;
class QWidget;
class QAction;
class QStringList;
class Closer;
class ClickButton;
class MenuButton;
class Menu;
class QListWidgetItem;
class QTimer;

class AESCrypter:public QMainWindow
{
	Q_OBJECT
public:
	AESCrypter(char *appName, QWidget *parent = 0);
	void unhookWhileClosing(MSG *msg);
	void setSaveMode(bool on);
private:
	QListWidget *list;
	QLabel *fileInfos, *buttonBar;
	QString progName;
	QPushButton *EncDec, *Del;
	ClickButton *secureButton;
	QAction *deleteAction, *encryptAction, *propertyAction, *actualizeAction, *insertAction;
	__int64 fileSizes;
	QStringList files0;
	QPoint currentGlobalPos;
	bool mousePressed, useSaveMode, noSecureButtonMistake;
	Closer *closeButton;
	MenuButton *menuButton;
	Menu *menu;
	QTimer *timer1;
	HHOOK hook, hook2, hook3;
	HMODULE hmod_libname;
	void addFolder(QString folderName);
protected:
	void dropEvent(QDropEvent *event);
	void dragEnterEvent(QDragEnterEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void resizeEvent(QResizeEvent *event);
	void keyReleaseEvent(QKeyEvent *event);
	void paintEvent(QPaintEvent *event);
	void closeEvent(QCloseEvent *event);
	void selfDecryptFile();
protected slots:
	void delSelectedItems();
	void encOrDec();
	void showProperties();
	void encDecSelectedItems();
	void updateItemsEnc(QString file);
	void updateItemsDec(QString file);
	void testExistance();
	void openCloseMenu();
	void closeMenu();
	void selectCurrentWizzard(QListWidgetItem *item);
	void insertItems();
	void changeSecureArea(bool checked);
	void restartHooks();
	void changeCtrl();
};
#endif