/****************************************************************************
** Meta object code from reading C++ file 'CryptionWizard.h'
**
** Created: Tue 31. Jan 14:19:08 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Dialogs/CryptionWizard.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CryptionWizard.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_createPasswordPage[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   20,   19,   19, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_createPasswordPage[] = {
    "createPasswordPage\0\0text\0"
    "updateStrongness(QString)\0"
};

const QMetaObject createPasswordPage::staticMetaObject = {
    { &QWizardPage::staticMetaObject, qt_meta_stringdata_createPasswordPage,
      qt_meta_data_createPasswordPage, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &createPasswordPage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *createPasswordPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *createPasswordPage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_createPasswordPage))
        return static_cast<void*>(const_cast< createPasswordPage*>(this));
    return QWizardPage::qt_metacast(_clname);
}

int createPasswordPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWizardPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: updateStrongness((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_startEncryptDecryptPage[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_startEncryptDecryptPage[] = {
    "startEncryptDecryptPage\0"
};

const QMetaObject startEncryptDecryptPage::staticMetaObject = {
    { &QWizardPage::staticMetaObject, qt_meta_stringdata_startEncryptDecryptPage,
      qt_meta_data_startEncryptDecryptPage, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &startEncryptDecryptPage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *startEncryptDecryptPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *startEncryptDecryptPage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_startEncryptDecryptPage))
        return static_cast<void*>(const_cast< startEncryptDecryptPage*>(this));
    return QWizardPage::qt_metacast(_clname);
}

int startEncryptDecryptPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWizardPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_encryptDecryptPage[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      25,   20,   19,   19, 0x05,
      48,   20,   19,   19, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_encryptDecryptPage[] = {
    "encryptDecryptPage\0\0file\0"
    "fileEncrypted(QString)\0fileDecrypted(QString)\0"
};

const QMetaObject encryptDecryptPage::staticMetaObject = {
    { &QWizardPage::staticMetaObject, qt_meta_stringdata_encryptDecryptPage,
      qt_meta_data_encryptDecryptPage, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &encryptDecryptPage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *encryptDecryptPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *encryptDecryptPage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_encryptDecryptPage))
        return static_cast<void*>(const_cast< encryptDecryptPage*>(this));
    return QWizardPage::qt_metacast(_clname);
}

int encryptDecryptPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWizardPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: fileEncrypted((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: fileDecrypted((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void encryptDecryptPage::fileEncrypted(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void encryptDecryptPage::fileDecrypted(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
static const uint qt_meta_data_selectSelfEncryptPage[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      23,   22,   22,   22, 0x08,
      41,   36,   22,   22, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_selectSelfEncryptPage[] = {
    "selectSelfEncryptPage\0\0insertFile()\0"
    "text\0updateStrongness(QString)\0"
};

const QMetaObject selectSelfEncryptPage::staticMetaObject = {
    { &QWizardPage::staticMetaObject, qt_meta_stringdata_selectSelfEncryptPage,
      qt_meta_data_selectSelfEncryptPage, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &selectSelfEncryptPage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *selectSelfEncryptPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *selectSelfEncryptPage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_selectSelfEncryptPage))
        return static_cast<void*>(const_cast< selectSelfEncryptPage*>(this));
    return QWizardPage::qt_metacast(_clname);
}

int selectSelfEncryptPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWizardPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: insertFile(); break;
        case 1: updateStrongness((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
