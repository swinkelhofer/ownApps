/****************************************************************************
** Meta object code from reading C++ file 'AESCrypter.h'
**
** Created: Mon 2. Jan 13:00:14 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../AESCrypter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AESCrypter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AESCrypter[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x09,
      31,   11,   11,   11, 0x09,
      42,   11,   11,   11, 0x09,
      59,   11,   11,   11, 0x09,
      86,   81,   11,   11, 0x09,
     110,   81,   11,   11, 0x09,
     134,   11,   11,   11, 0x09,
     150,   11,   11,   11, 0x09,
     166,   11,   11,   11, 0x09,
     183,  178,   11,   11, 0x09,
     222,   11,   11,   11, 0x09,
     244,  236,   11,   11, 0x09,
     267,   11,   11,   11, 0x09,
     282,   11,   11,   11, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_AESCrypter[] = {
    "AESCrypter\0\0delSelectedItems()\0"
    "encOrDec()\0showProperties()\0"
    "encDecSelectedItems()\0file\0"
    "updateItemsEnc(QString)\0updateItemsDec(QString)\0"
    "testExistance()\0openCloseMenu()\0"
    "closeMenu()\0item\0"
    "selectCurrentWizzard(QListWidgetItem*)\0"
    "insertItems()\0checked\0changeSecureArea(bool)\0"
    "restartHooks()\0changeCtrl()\0"
};

const QMetaObject AESCrypter::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_AESCrypter,
      qt_meta_data_AESCrypter, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AESCrypter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AESCrypter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AESCrypter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AESCrypter))
        return static_cast<void*>(const_cast< AESCrypter*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int AESCrypter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: delSelectedItems(); break;
        case 1: encOrDec(); break;
        case 2: showProperties(); break;
        case 3: encDecSelectedItems(); break;
        case 4: updateItemsEnc((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: updateItemsDec((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: testExistance(); break;
        case 7: openCloseMenu(); break;
        case 8: closeMenu(); break;
        case 9: selectCurrentWizzard((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 10: insertItems(); break;
        case 11: changeSecureArea((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: restartHooks(); break;
        case 13: changeCtrl(); break;
        default: ;
        }
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
