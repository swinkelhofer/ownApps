#include <QApplication>
#define OWN_MSG 0x666
class QWidget;
class MSGApplication:public QApplication
{
	Q_OBJECT
public:
	MSGApplication(int argc, char *argv[]);
	virtual bool winEventFilter(MSG *msg, long *result);
	void setMainWidget(QWidget *mainWindow);
private:
	QWidget *mainWidget;
};
