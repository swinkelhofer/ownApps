#include <QtGui>
#include "AESCrypter.h"
#include "GUI/Closer.h"
#include "GUI/MenuButton.h"
#include "GUI/Menu.h"
#include "GUI/ClickButton.h"
#include "Dialogs/CryptionWizard.h"
#include "Dialogs/PropertyDialog.h"
#include "ColorDefines.h"
#include <stdio.h>
#define TITLEBAR_HEIGHT 30

AESCrypter::AESCrypter(char *appName, QWidget *parent):QMainWindow(parent)
{
	/**********************************
	Informations- & Schaltflächenleiste
	**********************************/
	progName = QString(appName);
	setAcceptDrops(true);
	fileSizes = 0;
	fileInfos = new QLabel("0 Dateien (0 KB) 0 Markierte");
	EncDec = new QPushButton("Verschlüsseln");
	EncDec->setEnabled(false);
	Del = new QPushButton("Element(e) entfernen");
	Del->setEnabled(false);
	QHBoxLayout *upper = new QHBoxLayout;
	upper->addWidget(EncDec);
	upper->addWidget(Del);
	QHBoxLayout *lower = new QHBoxLayout;
	lower->addStretch();
	lower->addWidget(fileInfos);
	lower->addStretch();
	QVBoxLayout *bar = new QVBoxLayout;
	bar->addLayout(upper);
	bar->addLayout(lower);
	buttonBar = new QLabel(this);
	buttonBar->setPixmap(QPixmap(":/img/Leiste.png").scaled(600, 100, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
	buttonBar->setLayout(bar);
	buttonBar->setGeometry(0, size().height() - 100, size().width(), 100);
	
	
	/**********************************
				Hauptwidget
	**********************************/
	move(QApplication::desktop()->screenGeometry().width()/2 - size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2);
	list = new QListWidget(this);
	list->setMinimumSize(600, 400);
	list->setGeometry(0, TITLEBAR_HEIGHT, list->size().width(), list->size().height());
	list->setFrameStyle(QFrame::NoFrame | QFrame::Plain);
	setMinimumSize(600, 500 + TITLEBAR_HEIGHT);
	
	/**********************************
				TitleBar
	**********************************/
	closeButton = new Closer(this);
	closeButton->setGeometry(size().width() - 23, 8, 15, 14);
	mousePressed = false;
	setWindowTitle("CryptSuiteWindow");
	setWindowIcon(QIcon(QPixmap(":/img/NSA.png").scaled(32, 32, Qt::IgnoreAspectRatio, Qt::SmoothTransformation)));
	
	/**********************************
				Menu
	**********************************/
	menu = new Menu(this);
	menu->hide();
	menu->move(38, size().height() - 38 - menu->size().height());
	menuButton = new MenuButton(this);
	menuButton->setGeometry(10, size().height() - 66, 56, 56);
	
	
	/**********************************
				Schutzschalter
	**********************************/
	QFile controlFile("Control.dll");
	controlFile.open(QIODevice::ReadOnly);
	QTextStream controlStream(&controlFile);
	QString control;
	useSaveMode = false;
	do
	{
		control = controlStream.readLine();
		if(control.startsWith("SaveMode="))
		{
			if(control.endsWith("On"))
				useSaveMode = true;
			else
				useSaveMode = false;
		}
	}
	while(!control.isNull());
	controlFile.close();
	secureButton = new ClickButton(this);
	secureButton->move(size().width() - secureButton->size().width() - 10, size().height() - 60);
	secureButton->setToolTip("Schaltet in einen sicheren Modus, in dem keine Passwörter\noder ähnliche sensible Daten mitprotoliert werden können.");
	hmod_libname = ::LoadLibrary(TEXT("HookLib.dll"));
	noSecureButtonMistake = true;
	if(hmod_libname == NULL)
	{
		QMessageBox::warning(this, "Fehler", "Fehler: HookLib.dll konnte nicht geladen werden", QMessageBox::Ok, QMessageBox::NoButton);
		secureButton->hide();
		noSecureButtonMistake = false;
	}
	if(!useSaveMode)
	{
		secureButton->hide();
	}
	
	/**********************************
				Färbung
	**********************************/
	QPalette listPalette;
	listPalette.setColor(QPalette::Active, QPalette::Window, FOREGROUND_COLOR);
	listPalette.setColor(QPalette::Active, QPalette::Text, FOREGROUND_TEXT_COLOR);
	listPalette.setColor(QPalette::Active, QPalette::Base, FOREGROUND_COLOR);
	listPalette.setColor(QPalette::Active, QPalette::Button, FOREGROUND_COLOR);
	listPalette.setColor(QPalette::Active, QPalette::Light, QColor(256,0,0,128));
	listPalette.setColor(QPalette::Active, QPalette::Dark, QColor(0,0,256,128));
	listPalette.setColor(QPalette::Active, QPalette::Shadow, QColor(0,256,0,128));
	listPalette.setColor(QPalette::Inactive, QPalette::Window, FOREGROUND_INACTIVE_COLOR);
	listPalette.setColor(QPalette::Inactive, QPalette::Base, FOREGROUND_INACTIVE_COLOR);
	listPalette.setColor(QPalette::Inactive, QPalette::Button, FOREGROUND_INACTIVE_COLOR);
	listPalette.setColor(QPalette::Inactive, QPalette::Text, FOREGROUND_INACTIVE_TEXT_COLOR);
	list->setPalette(listPalette);
	list->setSelectionMode(QAbstractItemView::ExtendedSelection);
	list->setSelectionRectVisible(true);
	list->setIconSize(QSize(24,24));
	
	
	setAttribute(Qt::WA_TranslucentBackground, true);
	setWindowFlags(Qt::FramelessWindowHint);
	
	/**********************************
				Actions
	**********************************/
	timer1 = new QTimer;
	timer1->start(5000);
	deleteAction = new QAction(list);
	deleteAction->setEnabled(false);
	deleteAction->setText("Element(e) entfernen");
	list->addAction(deleteAction);
	encryptAction = new QAction(list);
	encryptAction->setEnabled(false);
	encryptAction->setText("Verschlüsseln");
	list->addAction(encryptAction);
	insertAction = new QAction(list);
	insertAction->setEnabled(true);
	insertAction->setText("Dateien hinzufügen");
	list->addAction(insertAction);
	propertyAction = new QAction(list);
	propertyAction->setEnabled(false);
	propertyAction->setText("Dateieigenschaften");
	list->addAction(propertyAction);
	actualizeAction = new QAction(list);
	actualizeAction->setText("Aktualisieren");
	list->addAction(actualizeAction);
	list->setContextMenuPolicy(Qt::ActionsContextMenu);
	
	/**********************************
				Connections
	**********************************/
	connect(actualizeAction, SIGNAL(triggered()), this, SLOT(testExistance()));
	connect(deleteAction, SIGNAL(triggered()), this, SLOT(delSelectedItems()));
	connect(encryptAction, SIGNAL(triggered()), this, SLOT(encDecSelectedItems()));
	connect(insertAction, SIGNAL(triggered()), this, SLOT(insertItems()));
	connect(propertyAction, SIGNAL(triggered()), this, SLOT(showProperties()));
	
	connect(list, SIGNAL(itemSelectionChanged()), this, SLOT(encOrDec()));
	connect(list, SIGNAL(viewportEntered()), this, SLOT(closeMenu()));
	connect(timer1, SIGNAL(timeout()), this, SLOT(testExistance()));
	
	connect(Del, SIGNAL(clicked()), this, SLOT(delSelectedItems()));
	connect(Del, SIGNAL(clicked()), this, SLOT(closeMenu()));
	connect(EncDec, SIGNAL(clicked()), this, SLOT(encDecSelectedItems()));
	connect(EncDec, SIGNAL(clicked()), this, SLOT(closeMenu()));
	
	connect(secureButton, SIGNAL(toggled(bool)), this, SLOT(changeSecureArea(bool)));
	
	connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
	connect(menuButton, SIGNAL(clicked()), this, SLOT(openCloseMenu()));
	connect(menu, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(selectCurrentWizzard(QListWidgetItem *)));
	connect(menu, SIGNAL(ctrlChanged()), this, SLOT(changeCtrl()));
}

void AESCrypter::changeCtrl()
{
	UnhookWindowsHookEx(hook);
	UnhookWindowsHookEx(hook2);
	UnhookWindowsHookEx(hook3);
	secureButton->setState(false);
	QFile controlFile("Control.dll");
	controlFile.open(QIODevice::ReadOnly);
	QTextStream controlStream(&controlFile);
	QString control;
	useSaveMode = false;
	do
	{
		control = controlStream.readLine();
		if(control.startsWith("SaveMode="))
		{
			if(control.endsWith("On"))
				useSaveMode = true;
			else
				useSaveMode = false;
		}
	}
	while(!control.isNull());
	controlFile.close();
	if(useSaveMode && noSecureButtonMistake)
		secureButton->show();
	else
		secureButton->hide();
}

void AESCrypter::setSaveMode(bool on)
{
	if(on)
	{
		if(!useSaveMode)
		{
			useSaveMode = true;
			secureButton->show();
		}
	}
	else
	{
		if(useSaveMode)
		{
			useSaveMode = false;
			secureButton->hide();
		}
	}
}
	

void AESCrypter::unhookWhileClosing(MSG *msg)
{
	UnhookWindowsHookEx(hook);
	UnhookWindowsHookEx(hook2);
	UnhookWindowsHookEx(hook3);
	FreeLibrary(hmod_libname);
	QTimer *hookTimer = new QTimer;
	hookTimer->start(50);
	hookTimer->setSingleShot(true);
	connect(hookTimer, SIGNAL(timeout()), this, SLOT(restartHooks()));
}

void AESCrypter::restartHooks()
{
	hmod_libname = ::LoadLibrary(TEXT("HookLib.dll"));
	if(hmod_libname == NULL)
	{
		QMessageBox::warning(this, "Fehler", "Fehler: HookLib.dll konnte nicht geladen werden", QMessageBox::Ok, QMessageBox::NoButton);
		secureButton->hide();
		return;
	}
	changeSecureArea(true);
}

void AESCrypter::changeSecureArea(bool checked)
{
	if(checked)
	{
		HOOKPROC proc, proc2, proc3;
		proc = (HOOKPROC)::GetProcAddress(hmod_libname, "HookKBLL");
		/*if(proc == NULL)
		{
			QMessageBox::warning(this, "Fehler", "Fehler: Adresse von HookKBLL konnte nicht geladen werden", QMessageBox::Ok, QMessageBox::NoButton);
		}*/
		hook = ::SetWindowsHookEx(WH_KEYBOARD_LL,proc, hmod_libname, 0);
	
		
		proc2 = (HOOKPROC)::GetProcAddress(hmod_libname, "HookKB");
		/*if(proc2 == NULL)
		{
			QMessageBox::warning(this, "Fehler", "Fehler: Adresse von HookKB konnte nicht geladen werden", QMessageBox::Ok, QMessageBox::NoButton);
		}*/
		hook2 = ::SetWindowsHookEx(WH_KEYBOARD, proc2, hmod_libname, 0); 
		
		proc3 = (HOOKPROC)::GetProcAddress(hmod_libname, "HookWnds");
		/*if(proc3 == NULL)
		{
			QMessageBox::warning(this, "Fehler", "Fehler: Adresse von HookWnds konnte nicht geladen werden", QMessageBox::Ok, QMessageBox::NoButton);
		}*/
		hook3 = ::SetWindowsHookEx(WH_CBT, proc3, hmod_libname, 0); 
		
		if(hook == NULL || hook2 == NULL || hook3 == NULL || proc == NULL || proc2 == NULL || proc3 == NULL)
		{
			QMessageBox::warning(this, "Fehler", "Beim aktivieren des Schutzes ist ein Fehler aufgetreten", QMessageBox::Ok, QMessageBox::NoButton);
			secureButton->hide();
		}
	}
	
	if(!checked)
	{
		if(!UnhookWindowsHookEx(hook) || !UnhookWindowsHookEx(hook2) || !UnhookWindowsHookEx(hook3))
		{
			QMessageBox::warning(this, "Fehler", "Beim deaktivieren des Schutzes ist ein Fehler aufgetreten", QMessageBox::Ok, QMessageBox::NoButton);
		}
	}
}

void AESCrypter::closeEvent(QCloseEvent *event)
{
	UnhookWindowsHookEx(hook);
	UnhookWindowsHookEx(hook2);
	UnhookWindowsHookEx(hook3);
	FreeLibrary(hmod_libname);
	event->accept();
}

void AESCrypter::openCloseMenu()
{
	if(!menu->isVisible())
		menu->show();
	else
		menu->hide();
}

void AESCrypter::closeMenu()
{
	if(menu->isVisible())
		menu->hide();
}

void AESCrypter::dragEnterEvent(QDragEnterEvent *event)
{
	if(event->mimeData()->hasUrls())
		event->acceptProposedAction();
}

void AESCrypter::dropEvent(QDropEvent *event)
{
	if(event->mimeData()->hasUrls())
	{
		for(int i = 0; i < event->mimeData()->urls().count(); ++i)
		{
			if(list->findItems(event->mimeData()->urls().at(i).toLocalFile(), Qt::MatchExactly).isEmpty() && list->findItems(QString("(Verschlüsselt) ") + event->mimeData()->urls().at(i).toLocalFile(), Qt::MatchExactly).isEmpty() && QFileInfo(event->mimeData()->urls().at(i).toLocalFile()).isFile() && QFileInfo(event->mimeData()->urls().at(i).toLocalFile()) != QFileInfo(progName))
			{
				QListWidgetItem *item;
				QPixmap pix;
				if(!event->mimeData()->urls().at(i).toLocalFile().endsWith(".crypt"))
					if(pix.load(event->mimeData()->urls().at(i).toLocalFile()))
						item = new QListWidgetItem(QIcon(pix.scaled(24,24,Qt::IgnoreAspectRatio, Qt::SmoothTransformation)), event->mimeData()->urls().at(i).toLocalFile(), list);
					else
						item = new QListWidgetItem(QFileIconProvider().icon(QFileInfo(event->mimeData()->urls().at(i).toLocalFile())), event->mimeData()->urls().at(i).toLocalFile(), list);
				else
					item = new QListWidgetItem(QString("(Verschlüsselt) ") + event->mimeData()->urls().at(i).toLocalFile(), list);
				list->addItem(item);
				fileSizes += QFileInfo(event->mimeData()->urls().at(i).toLocalFile()).size();
			}
			else if(list->findItems(event->mimeData()->urls().at(i).toLocalFile(), Qt::MatchExactly).isEmpty() && list->findItems(QString("(Verschlüsselt) ") + event->mimeData()->urls().at(i).toLocalFile(), Qt::MatchExactly).isEmpty() && QFileInfo(event->mimeData()->urls().at(i).toLocalFile()).isDir())
			{
				addFolder(event->mimeData()->urls().at(i).toLocalFile());
			}
		}
		encOrDec();
		event->acceptProposedAction();
	}
}

void AESCrypter::resizeEvent(QResizeEvent *event)
{
	move(QApplication::desktop()->screenGeometry().width()/2 - size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2);
	closeButton->setGeometry(size().width() - 23, 8, 15, 14);
	menuButton->setGeometry(10, size().height() - 66, 56, 56);
	menu->move(38, size().height() - 38 - menu->size().height());
	buttonBar->setGeometry(0, size().height() - 100, size().width(), 100);
	list->setGeometry(0, TITLEBAR_HEIGHT, size().width(), size().height() - 100 - TITLEBAR_HEIGHT);
	secureButton->move(size().width() - secureButton->size().width() - 10, size().height() - 60);
}

void AESCrypter::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setBrush(BACKGROUND_MAIN_WINDOW_COLOR);
	painter.setPen(Qt::NoPen);
	painter.drawRect(0,0,size().width(), size().height());
	painter.drawPixmap(0, 0, size().width(), TITLEBAR_HEIGHT, QPixmap(":/img/Leiste.png").scaled(600, TITLEBAR_HEIGHT, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
	painter.drawPixmap(size().width() / 2 - 150, size().height() / 2 - 155 - TITLEBAR_HEIGHT, 300, 300, QPixmap(":/img/NSA.png").scaled(300, 300, Qt::KeepAspectRatio, Qt::SmoothTransformation));
	painter.setPen(Qt::black);
	painter.setFont(QFont("Arial", 10, QFont::Bold));
	painter.drawText(0, size().height() / 2 + 125 - TITLEBAR_HEIGHT, size().width(), 85, Qt::AlignVCenter | Qt::AlignHCenter, "Dateien per Drag'n'Drop in das Fenster ziehen");
	painter.setPen(TITLEBAR_TEXT_COLOR);
	painter.setFont(QFont("Arial", 9, QFont::Bold));
	painter.drawText(8,8,size().width() - 8, 22, Qt::AlignLeft, "AESCrypter");
}

void AESCrypter::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		currentGlobalPos = event->globalPos();
		mousePressed = true;
		closeMenu();
	}
	else
		mousePressed = false;
}

void AESCrypter::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed)
	{
		mousePressed = false;
		move(pos() - (currentGlobalPos - event->globalPos()));
	}
}

void AESCrypter::mouseMoveEvent(QMouseEvent *event)
{
	if(mousePressed)
	{
		move(pos() - (currentGlobalPos - event->globalPos()));
		currentGlobalPos = event->globalPos();
	}
}

void AESCrypter::keyReleaseEvent(QKeyEvent *event)
{
	if(event->key() == Qt::Key_Delete && deleteAction->isEnabled())
		delSelectedItems();
	else if(event->key() == Qt::Key_F5)
		testExistance();
}

void AESCrypter::delSelectedItems()
{
	int cnt = list->selectedItems().count();
	for(int i = 0; i < cnt; ++i)
	{
		if(!list->selectedItems().at(0)->text().endsWith(".crypt"))
			fileSizes -= QFileInfo(list->selectedItems().at(0)->text()).size();
		else
		{
			QString file = list->selectedItems().at(0)->text();
			file.remove(0,16);
			fileSizes -= QFileInfo(file).size();
		}
		delete(list->selectedItems().at(0));
	}
	encOrDec();
}

void AESCrypter::insertItems()
{
	QStringList files = QFileDialog::getOpenFileNames(this, "Dateien hinzufügen", "C:\\", 0);
	for(int i = 0; i < files.count(); ++i)
	{
		if(list->findItems(files.at(i), Qt::MatchExactly).isEmpty() && list->findItems(QString("(Verschlüsselt) ") + files.at(i), Qt::MatchExactly).isEmpty() && QFileInfo(files.at(i)).isFile() && QFileInfo(files.at(i)) != QFileInfo(progName))
		{
			QListWidgetItem *item;
			if(!files.at(i).endsWith(".crypt"))
				item = new QListWidgetItem(QFileIconProvider().icon(QFileInfo(files.at(i))), files.at(i), list);
			else
				item = new QListWidgetItem(QString("(Verschlüsselt) ") + files.at(i), list);
			list->addItem(item);
			fileSizes += QFileInfo(files.at(i)).size();
		}
	}
	encOrDec();
}

void AESCrypter::addFolder(QString folderName)
{
	folderName += "/";
	QDir dir(folderName);
	for(int i = 0; i < dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot).count(); ++i)
	{
		addFolder(folderName + dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot).at(i));
	}
	for(int i = 0; i < dir.entryList(QDir::Files).count(); ++i)
	{
		if(list->findItems(folderName + dir.entryList(QDir::Files).at(i), Qt::MatchExactly).isEmpty() && list->findItems(QString("(Verschlüsselt) ") + folderName + dir.entryList(QDir::Files).at(i), Qt::MatchExactly).isEmpty() && QFileInfo(folderName + dir.entryList(QDir::Files).at(i)).isFile() && QFileInfo(folderName + dir.entryList(QDir::Files).at(i)) != QFileInfo(progName))
		{
			QPixmap pix;
			QListWidgetItem *item;
			if(!dir.entryList(QDir::Files).at(i).endsWith(".crypt"))
				if(pix.load(folderName + dir.entryList(QDir::Files).at(i)))
					item = new QListWidgetItem(QIcon(pix.scaled(24,24,Qt::IgnoreAspectRatio, Qt::SmoothTransformation)), folderName + dir.entryList(QDir::Files).at(i), list);
				else
					item = new QListWidgetItem(QFileIconProvider().icon(QFileInfo(folderName + dir.entryList(QDir::Files).at(i))), folderName + dir.entryList(QDir::Files).at(i), list);
			else
				item = new QListWidgetItem(QString("(Verschlüsselt) ") + folderName + dir.entryList(QDir::Files).at(i), list);
			list->addItem(item);
			fileSizes += QFileInfo(folderName + dir.entryList(QDir::Files).at(i)).size();
		}
	}
}

void AESCrypter::encOrDec()
{
	if(list->count() == 0)
		fileSizes = 0;
	QString fileInfoText;
	if(list->count() == 1)
		fileInfoText = "1 Datei (";
	else
		fileInfoText = QString::number(list->count()) + QString(" Dateien (");
	if((long double)fileSizes/(long double)1024 > (long double) 1000)
		fileInfoText += QString::number((double)((long double)fileSizes/(long double)1048576), 'f', 2).replace(QChar('.'), ",") + QString(" MB) ") + QString::number(list->selectedItems().count()) + QString(" Markierte");
	else
		fileInfoText += QString::number((double)((long double)fileSizes/(long double)1024), 'f', 2).replace(QChar('.'), ",") + QString(" KB) ") + QString::number(list->selectedItems().count()) + QString(" Markierte");
	fileInfos->setText(fileInfoText);
	propertyAction->setEnabled(false);
	if(list->selectedItems().count() > 0)
	{
		if(list->selectedItems().count() == 1)
			propertyAction->setEnabled(true);
		deleteAction->setEnabled(true);
		Del->setEnabled(true);
		int crypt = 0, plain = 0;
		for(int i = 0; i < list->selectedItems().count(); ++i)
		{
			if(list->selectedItems().at(i)->text().endsWith(".crypt"))
				++crypt;
			else
				++plain;
		}
		if(crypt != 0 && plain != 0)
		{
			encryptAction->setEnabled(false);
			EncDec->setEnabled(false);
		}
		else if(crypt > 0)
		{
			encryptAction->setEnabled(true);
			encryptAction->setText("Entschlüsseln");
			EncDec->setEnabled(true);
			EncDec->setText("Entschlüsseln");
		}
		else if(plain > 0)
		{
			encryptAction->setEnabled(true);
			encryptAction->setText("Verschlüsseln");
			EncDec->setEnabled(true);
			EncDec->setText("Verschlüsseln");
		}
	}
	else
	{
		if(list->count() == 1)
			propertyAction->setEnabled(false);
		deleteAction->setEnabled(false);
		Del->setEnabled(false);
		if(list->count() > 0)
		{
			int crypt = 0, plain = 0;
			for(int i = 0; i < list->count(); ++i)
			{
				if(list->item(i)->text().endsWith(".crypt"))
					++crypt;
				else
					++plain;
			}
			if(crypt != 0 && plain != 0)
			{
				encryptAction->setEnabled(false);
				EncDec->setEnabled(false);
			}
			else if(crypt > 0)
			{
				encryptAction->setEnabled(true);
				encryptAction->setText("Alle Entschlüsseln");
				EncDec->setEnabled(true);
				EncDec->setText("Alle Entschlüsseln");
			}
			else if(plain > 0)
			{
				encryptAction->setEnabled(true);
				encryptAction->setText("Alle Verschlüsseln");
				EncDec->setEnabled(true);
				EncDec->setText("Alle Verschlüsseln");
			}
		}
		else
		{
			EncDec->setEnabled(false);
			encryptAction->setEnabled(false);
		}
	}
}

void AESCrypter::showProperties()
{
	if(!list->selectedItems().at(0)->text().endsWith(".crypt"))
	{
		PropertyDialog *props = new PropertyDialog(list->selectedItems().at(0)->text());
		props->setModal(true);
		props->show();
	}
	else
	{
		QString text = list->selectedItems().at(0)->text();
		text.remove(0,16);
		PropertyDialog *props = new PropertyDialog(text);
		props->setModal(true);
		props->show();
	}
}

void AESCrypter::encDecSelectedItems()
{
	files0.clear();
	int selected = list->selectedItems().count();
	if(selected > 0)
		for(int i = 0; i < selected; ++i)
			files0 << list->selectedItems().at(i)->text();
	else
		for(int i = 0; i < list->count(); ++i)
			files0 << list->item(i)->text();
	
	if(!files0.at(0).endsWith(".crypt"))
	{
		QWizard *CryptWiz = new QWizard;		
		CryptWiz->setButtonText(QWizard::BackButton, "Zurück");
		CryptWiz->setWindowFlags(Qt::WindowStaysOnTopHint);
		CryptWiz->setButtonText(QWizard::NextButton, "Weiter");
		CryptWiz->setButtonText(QWizard::FinishButton, "Beenden");
		CryptWiz->setButtonText(QWizard::CancelButton, "Abbrechen");
		encryptDecryptPage *page2 = new encryptDecryptPage(files0, true, false);
		connect(page2, SIGNAL(fileEncrypted(QString)), this, SLOT(updateItemsEnc(QString)));
		CryptWiz->setPage(0, new createPasswordPage(true, false));
		CryptWiz->setPage(1, new startEncryptDecryptPage);
		CryptWiz->setPage(2, page2);
		CryptWiz->setWindowTitle("Verschlüsselung");
		CryptWiz->show();
	}
	else
	{
		QWizard *CryptWiz2 = new QWizard;
		CryptWiz2->setWindowFlags(Qt::WindowStaysOnTopHint);
		CryptWiz2->setButtonText(QWizard::BackButton, "Zurück");
		CryptWiz2->setButtonText(QWizard::NextButton, "Weiter");
		CryptWiz2->setButtonText(QWizard::FinishButton, "Beenden");
		CryptWiz2->setButtonText(QWizard::CancelButton, "Abbrechen");
		encryptDecryptPage *page2 = new encryptDecryptPage(files0, false, true);
		connect(page2, SIGNAL(fileDecrypted(QString)), this, SLOT(updateItemsDec(QString)));
		CryptWiz2->setPage(0, new createPasswordPage(false, true));
		CryptWiz2->setPage(1, page2);
		CryptWiz2->setWindowTitle("Entschlüsselung");
		CryptWiz2->show();
	}
}

void AESCrypter::selfDecryptFile()
{
	QWizard *CryptWiz = new QWizard;
	CryptWiz->setWindowFlags(Qt::WindowStaysOnTopHint);
	CryptWiz->setWindowTitle("SelfDecrypt");
	CryptWiz->setButtonText(QWizard::BackButton, "Zurück");
	CryptWiz->setButtonText(QWizard::NextButton, "Weiter");
	CryptWiz->setButtonText(QWizard::FinishButton, "Beenden");
	CryptWiz->setButtonText(QWizard::CancelButton, "Abbrechen");
	CryptWiz->setPage(0, new selectSelfEncryptPage);
	CryptWiz->setPage(1, new encryptDecryptPage(QStringList(), true,true));
	CryptWiz->show();	
}

void AESCrypter::selectCurrentWizzard(QListWidgetItem *item)
{
	if(item->text() == QString("SelfDecrypt"))
		selfDecryptFile();
	else if(item->text() == QString("Bildschirmtastatur"))
		QProcess::startDetached("ScreenKeyboard.exe", QStringList());
	else if(item->text() == QString("PasswordTester"))
		QProcess::startDetached("PassTest.exe", QStringList());
	else if(item->text() == QString("PasswordCreator"))
		QProcess::startDetached("PassGen.exe", QStringList());	
}

void AESCrypter::updateItemsEnc(QString file)
{
	list->findItems(file,Qt::MatchExactly).at(0)->setIcon(QIcon());
	list->findItems(file,Qt::MatchExactly).at(0)->setText(QString("(Verschlüsselt) ") + file + QString(".crypt"));
	encOrDec();
}

void AESCrypter::updateItemsDec(QString file)
{
	list->findItems(QString("(Verschlüsselt) ") + file,Qt::MatchExactly).at(0)->setIcon(QFileIconProvider().icon(QFileInfo(file.left(file.length() - 6))));
	list->findItems(QString("(Verschlüsselt) ") + file,Qt::MatchExactly).at(0)->setText(file.left(file.length() - 6));
	encOrDec();
}

void AESCrypter::testExistance()
{
	QStringList errorFileList;
	QString errorFiles;
	for(int i = 0; i < list->count(); ++i)
	{
		if(!list->item(i)->text().endsWith(".crypt"))
		{
			if(!QFileInfo(list->item(i)->text()).exists())
			{
				errorFiles += list->item(i)->text() + "\n";
				errorFileList << list->item(i)->text();
			}
		}
		else
		{
			if(!QFileInfo(list->item(i)->text().remove(0,16)).exists())
			{
				errorFiles += list->item(i)->text().remove(0,16) + "\n";
				errorFileList << list->item(i)->text();
			}
		}
	}
	if(!errorFileList.isEmpty())
	{
		QMessageBox *fail = new QMessageBox;
		fail->setWindowFlags(Qt::WindowStaysOnTopHint);
		fail->setText(QString("Eine oder mehere Dateien wurden verschoben, umbenannt oder gelöscht:\n") + errorFiles + QString("\nDatei(en) werden aus der Liste entfernt"));
		fail->addButton("OK", QMessageBox::YesRole);
		fail->exec();
		for(int i = 0; i < errorFileList.count(); ++i)
			delete(list->findItems(errorFileList.at(i), Qt::MatchExactly).at(0));
		fileSizes = 0;
		for(int i = 0; i < list->count(); ++i)
		{
			if(!list->item(i)->text().endsWith(".crypt"))
				fileSizes += QFileInfo(list->item(i)->text()).size();
			else
				fileSizes += QFileInfo(list->item(i)->text().remove(0,16)).size();
		}
		encOrDec();
	}
}