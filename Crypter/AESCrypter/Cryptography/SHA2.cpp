#include "SHA2.h"
#include <string.h>
#include <string>
#include "CryptString.h"
#include <stdio.h>
using namespace std;

#define ext1(a) (((a >> 7) + (a << 25)) ^ ((a >> 18) + (a << 14)) ^ (a >> 3))
#define ext2(a) (((a >> 17) + (a << 15)) ^ ((a >> 19) + (a << 13)) ^ (a >> 10))
#define rot1(a) (((a >> 2) + (a << 30)) ^ ((a >> 13) + (a << 19)) ^ ((a >> 22) + (a << 10)))
#define rot2(a) (((a >> 6) + (a << 26)) ^ ((a >> 11) + (a << 21)) ^ ((a >> 25) + (a << 7)))

CryptString SHA2(const char *msg)
{
	unsigned int k[64] =
	   {0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	   0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	   0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	   0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	   0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	   0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	   0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	   0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};
	unsigned int h0 = 0x6a09e667;
	unsigned int h1 = 0xbb67ae85;
	unsigned int h2 = 0x3c6ef372;
	unsigned int h3 = 0xa54ff53a;
	unsigned int h4 = 0x510e527f;
	unsigned int h5 = 0x9b05688c;
	unsigned int h6 = 0x1f83d9ab;
	unsigned int h7 = 0x5be0cd19;
	
	unsigned int a, b, c, d, e, f, g, h, s0, maj, t2, s1, ch, t1;
	
	int msgLength = strlen(msg);
	int over = 64 - (msgLength + 9) % 64;
	if(over == 64)
		over = 0;
	int maxlength = msgLength + 9 + over;
	unsigned char msg0[maxlength];
	for(int i = 0; i < msgLength; ++i)
		msg0[i] = msg[i];
	
	msg0[msgLength] = 0x80;
	for(int i = 1; i <= over; ++i)
		msg0[msgLength + i] = 0x00;
	int lengthBits = msgLength * 8;
	msg0[msgLength + over + 1] = (char)(((__int64)lengthBits >> 56) & 0xff);
	msg0[msgLength + over + 2] = (char)(((__int64)lengthBits >> 48) & 0xff);
	msg0[msgLength + over + 3] = (char)(((__int64)lengthBits >> 40) & 0xff);
	msg0[msgLength + over + 4] = (char)(((__int64)lengthBits >> 32) & 0xff);
	msg0[msgLength + over + 5] = (char)((lengthBits >> 24) & 0xff);
	msg0[msgLength + over + 6] = (char)((lengthBits >> 16) & 0xff);
	msg0[msgLength + over + 7] = (char)((lengthBits >> 8) & 0xff);
	msg0[msgLength + over + 8] = (char)((lengthBits) & 0xff);
	
	unsigned int w[64];
	for(int m = 0; m < maxlength / 64; ++m)
	{
		for(int i = 0; i < 16; ++i)
			w[i] = ((unsigned int)msg0[m * 64 + i * 4] << 24) + ((unsigned int)msg0[m * 64 + i * 4 + 1] << 16) + ((unsigned int)msg0[m * 64 + i * 4 + 2] << 8) + (unsigned int)msg0[m * 64 + i * 4 + 3];
		
		for(int i = 16; i < 64; ++i)
			w[i] = w[i - 16] + ext1(w[i - 15]) + w[i - 7] + ext2(w[i - 2]);
		
		a = h0;
		b = h1;
		c = h2;
		d = h3;
		e = h4;
		f = h5;
		g = h6;
		h = h7;
		
		for(int i = 0; i < 64; ++i)
		{
			s0 = rot1(a);
			maj = (a & b) ^ (a & c) ^ (b & c);
			t2 = s0 + maj;
			s1 = rot2(e);
			ch = (e & f) ^ ((~e) & g);
			t1 = h + s1 + ch + k[i] + w[i];
		
			h = g;
			g = f;
			f = e;
			e = d + t1;
			d = c;
			c = b;
			b = a;
			a = t1 + t2;
			
			h0 += a;
			h1 += b;
			h2 += c;
			h3 += d;
			h4 += e;
			h5 += f;
			h6 += g;
			h7 += h;
		}
	}
	CryptString ret;
	ret.str[0] = (char) (h0 >> 24) & 0xff;
	ret.str[1] = (char) (h0 >> 16) & 0xff;
	ret.str[2] = (char) (h0 >> 8) & 0xff;
	ret.str[3] = (char) (h0) & 0xff;
	ret.str[4] = (char) (h1 >> 24) & 0xff;
	ret.str[5] = (char) (h1 >> 16) & 0xff;
	ret.str[6] = (char) (h1 >> 8) & 0xff;
	ret.str[7] = (char) (h1) & 0xff;
	ret.str[8] = (char) (h2 >> 24) & 0xff;
	ret.str[9] = (char) (h2 >> 16) & 0xff;
	ret.str[10] = (char) (h2 >> 8) & 0xff;
	ret.str[11] = (char) (h2) & 0xff;
	ret.str[12] = (char) (h3 >> 24) & 0xff;
	ret.str[13] = (char) (h3 >> 16) & 0xff;
	ret.str[14] = (char) (h3 >> 8) & 0xff;
	ret.str[15] = (char) (h3) & 0xff;
	ret.str[16] = (char) (h4 >> 24) & 0xff;
	ret.str[17] = (char) (h4 >> 16) & 0xff;
	ret.str[18] = (char) (h4 >> 8) & 0xff;
	ret.str[19] = (char) (h4) & 0xff;
	ret.str[20] = (char) (h5 >> 24) & 0xff;
	ret.str[21] = (char) (h5 >> 16) & 0xff;
	ret.str[22] = (char) (h5 >> 8) & 0xff;
	ret.str[23] = (char) (h5) & 0xff;
	ret.str[24] = (char) (h6 >> 24) & 0xff;
	ret.str[25] = (char) (h6 >> 16) & 0xff;
	ret.str[26] = (char) (h6 >> 8) & 0xff;
	ret.str[27] = (char) (h6) & 0xff;
	ret.str[28] = (char) (h7 >> 24) & 0xff;
	ret.str[29] = (char) (h7 >> 16) & 0xff;
	ret.str[30] = (char) (h7 >> 8) & 0xff;
	ret.str[31] = (char) (h7) & 0xff;
	
	return ret;
}