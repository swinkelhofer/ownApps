#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <time.h>
#include <QProgressDialog>
#include <QLabel>

bool delSecure(const char *fileName, int delRounds)
{
	FILE *delFile = fopen(fileName, "w+b");
	if(delFile == NULL)
		return false;
	struct _stati64 info;
	_stati64(fileName, &info);
	__int64 bytes = info.st_size;
	__int64 rounds = bytes / 8;
	bytes %= 8;
	__int64 random;
	srand(time(0));
	
	QProgressDialog *prog = new QProgressDialog(QString("L�sche Datei ") + QString(fileName), "Abbrechen", 0, 100);
	prog->setValue(0);
	prog->setWindowFlags(Qt::WindowStaysOnTopHint);
	prog->show();
	for(int k = 0; k < delRounds; ++k)
	{
		prog->setValue(100/delRounds * (k+1));
		for(int i = 0; i < rounds; ++i)
		{
			random = rand() * rand() * rand();
			fwrite(&random, sizeof(__int64), 1, delFile);
		}
		for(int i = 0; i < rounds; ++i)
		{
			random = rand() * rand() * rand();
			fwrite(&random, (int)bytes, 1, delFile);
		}
		fseek(delFile, 0, SEEK_SET);
	}
	fclose(delFile);
	delete(prog);
	if(remove(fileName) == -1)
		return false;
	else
		return true;
}