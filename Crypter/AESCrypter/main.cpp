#include <QMessageBox>
#include "AESCrypter.h"
#include "main.h"

MSGApplication::MSGApplication(int argc, char *argv[]):QApplication(argc, argv)
{
	mainWidget = NULL;
}

bool MSGApplication::winEventFilter(MSG *msg, long *result)
{
	if(msg->message == OWN_MSG)
	{
		((AESCrypter*)mainWidget)->unhookWhileClosing(msg);
		return true;
	}
	return QApplication::winEventFilter(msg, result);
}

void MSGApplication::setMainWidget(QWidget *mainWindow)
{
	mainWidget = mainWindow;
}

int main(int argc, char *argv[])
{
	if(FindWindow(NULL, TEXT("CryptSuiteWindow")) == NULL)
	{
		MSGApplication app(argc, argv);
		AESCrypter *window = new AESCrypter(argv[0]);
		app.setMainWidget(window);
		window->show();
		return app.exec();
	}
	else
	{
		AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(FindWindow(NULL, TEXT("CryptSuiteWindow")), 0), true);
		SetFocus(FindWindow(NULL, TEXT("CryptSuiteWindow")));
		AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(FindWindow(NULL, TEXT("CryptSuiteWindow")), 0), false);
		return 0;
	}
}