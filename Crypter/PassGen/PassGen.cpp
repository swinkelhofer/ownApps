#include "PassGen.h"
#include "Closer.h"
#include <time.h>
#include <QtGui>

PassGen::PassGen(QWidget *parent):QWidget(parent)
{

	QPalette listPalette;
	listPalette.setColor(QPalette::Active, QPalette::Window, QColor(255,255,255,255));
	listPalette.setColor(QPalette::Active, QPalette::Text, QColor(0,0,0,255));
	listPalette.setColor(QPalette::Active, QPalette::Base, QColor(255,255,255,255));
	listPalette.setColor(QPalette::Active, QPalette::Button, QColor(255,255,255,255));

	listPalette.setColor(QPalette::Disabled, QPalette::Window, QColor(12,21,51,0));
	listPalette.setColor(QPalette::Disabled, QPalette::Text, QColor(255,255,255,255));
	listPalette.setColor(QPalette::Disabled, QPalette::Base, QColor(12,21,51,0));
	listPalette.setColor(QPalette::Disabled, QPalette::Button, QColor(12,21,51,0));
	
	
	QPalette whiteText;
	
	whiteText.setColor(QPalette::Window, QColor(12,21,51,0));
	whiteText.setColor(QPalette::Base, QColor(12,21,51,0));
	whiteText.setColor(QPalette::Button, QColor(12,21,51,0));
	whiteText.setColor(QPalette::Text, Qt::white);
	whiteText.setColor(QPalette::WindowText, Qt::white);
	whiteText.setColor(QPalette::ButtonText, Qt::white);
	
	
	mousePressed = false;
	output = new QTextEdit;
	output->setReadOnly(true);
	output->setFrameStyle(0x0010);
	output->setFont(QFont("Verdana", 18,QFont::Bold));
	output->setPalette(whiteText);
	closeButton = new Closer(this);
	closeButton->setGeometry(size().width() - 23, 8, 15, 14);
	ownPW = new QLineEdit;
	ownPW->setPalette(listPalette);
	ownPW->setEnabled(false);
	ownPW->setFrame(false);
	generator = new QPushButton("Generiere Passwort");
	radio1 = new QRadioButton("Leichter zu merkendes Passwort aus eigenem Text");
	radio1->setPalette(whiteText);
	radio2 = new QRadioButton("Zufallspasswort mit vorgegebener Zeichenl�nge");
	radio2->setPalette(whiteText);
	QButtonGroup *group = new QButtonGroup;
	group->addButton(radio1);
	group->addButton(radio2);
	ownLength = new QLineEdit;
	ownLength->setEnabled(false);
	ownLength->setPalette(listPalette);
	ownLength->setFixedWidth(25);
	ownLength->setFrame(false);
	ownLength->setValidator(new QIntValidator(1,64,ownLength));
	QVBoxLayout *main = new QVBoxLayout;
	main->addSpacing(30);
	main->addWidget(radio1);
	main->addWidget(ownPW);
	main->addWidget(radio2);
	main->addWidget(ownLength);
	main->addWidget(generator);
	main->addWidget(output);
	setLayout(main);
	setAttribute(Qt::WA_TranslucentBackground);
	setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
	
	move(QApplication::desktop()->screenGeometry().width()/2 - size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2);
	
	connect(radio1, SIGNAL(toggled(bool)), this, SLOT(changeActivation(bool)));
	connect(radio2, SIGNAL(toggled(bool)), this, SLOT(changeActivation(bool)));
	connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
	connect(generator, SIGNAL(clicked()), this, SLOT(generate()));
}

void PassGen::generate()
{
	if(radio1->isChecked())
	{
		QString text = ownPW->text();
		srand((unsigned)time(NULL)+rand());
		for(int i = 0; i < text.length(); ++i)
		{
			text[i] = change(text[i]);
		}
		output->setText(text);
	}

	if(radio2->isChecked())
	{
		srand((unsigned)time(NULL)+rand());
		QString text;
		int random;
		for(int i = 0; i < ownLength->text().toInt(); ++i)
		{
			random = rand() % 105;
			text += QChar(random + 32);
		}
		output->setText(text);
	}
}

QChar PassGen::change(QChar a)
{
	a = a.toUpper();
	int i = 0;
	srand((unsigned)time(NULL)+rand());
	if(a == QChar('A'))
	{
		i = rand();
		char cur[] = {'@', 'A', 'a','4'};
		a = QChar(cur[i%4]);
	}
	if(a == QChar('B'))
	{
		i = rand();
		char cur[] = {'B', 'b', '8', '�'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('C'))
	{
		i = rand();
		char cur[] = {'C', 'c', '<'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('D'))
	{
		i = rand();
		char cur[] = {'D', 'd'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('E'))
	{
		i = rand();
		QChar cur[] = {'e', 'E', 0x20AC, '3'};
		a = cur[i%4];
	}
	if(a == QChar('F'))
	{
		i = rand();
		char cur[] = {'f', 'F'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('G'))
	{
		i = rand();
		char cur[] = {'g', 'G', '9'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('H'))
	{
		i = rand();
		char cur[] = {'h', 'H', '#'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('I'))
	{
		i = rand();
		char cur[] = {'i', 'I', '!', '1'};
		a = QChar(cur[i%4]);
	}
	if(a == QChar('J'))
	{
		i = rand();
		char cur[] = {'j', 'J'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('K'))
	{
		i = rand();
		char cur[] = {'k', 'K'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('L'))
	{
		i = rand();
		char cur[] = {'l', 'L', '1', '!'};
		a = QChar(cur[i%4]);
	}
	if(a == QChar('M'))
	{
		i = rand();
		char cur[] = {'m', 'M'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('N'))
	{
		i = rand();
		char cur[] = {'n', 'N', '�'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('O'))
	{
		i = rand();
		char cur[] = {'o', 'O', '0'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('P'))
	{
		i = rand();
		char cur[] = {'p', 'P'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('Q'))
	{
		i = rand();
		char cur[] = {'q', 'Q'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('R'))
	{
		i = rand();
		char cur[] = {'r', 'R'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('S'))
	{
		i = rand();
		char cur[] = {'s', 'S', '5', '$', '�'};
		a = QChar(cur[i%5]);
	}
	if(a == QChar('T'))
	{
		i = rand();
		char cur[] = {'t', 'T', '+', '7'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('U'))
	{
		i = rand();
		char cur[] = {'u', 'U'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('V'))
	{
		i = rand();
		char cur[] = {'v', 'V'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('W'))
	{
		i = rand();
		char cur[] = {'w', 'W'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('X'))
	{
		i = rand();
		char cur[] = {'x', 'X', '*'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('Y'))
	{
		i = rand();
		char cur[] = {'y', 'Y'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('Z'))
	{
		i = rand();
		char cur[] = {'z', 'Z', '2'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('0'))
	{
		i = rand();
		char cur[] = {'0', 'o', 'O'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('1'))
	{
		i = rand();
		char cur[] = {'1', '!', 'I'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('2'))
	{
		i = rand();
		char cur[] = {'z', 'Z', '2'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('3'))
	{
		i = rand();
		QChar cur[] = {'3', 'E', 0x20AC};
		a = cur[i%3];
	}
	if(a == QChar('4'))
	{
		i = rand();
		char cur[] = {'4', 'A'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('5'))
	{
		i = rand();
		char cur[] = {'5', 's', 'S', '$', '�'};
		a = QChar(cur[i%5]);
	}
	if(a == QChar('6'))
	{
		i = rand();
		char cur[] = {'6', 'b'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('7'))
	{
		i = rand();
		char cur[] = {'7', 'T'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('8'))
	{
		i = rand();
		char cur[] = {'8', 'B', '�'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('9'))
	{
		i = rand();
		char cur[] = {'9', 'g'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar(' '))
	{
		i = rand();
		char cur[] = {'.', ',', '-', ' '};
		a = QChar(cur[i%4]);
	}
	if(a == QChar('�'))
	{
		i = rand();
		char cur[] = {'n', '�'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('-'))
	{
		i = rand();
		char cur[] = {'-', ' '};
		a = QChar(cur[i%2]);
	}
	if(a == QChar(','))
	{
		i = rand();
		char cur[] = {',', ' ', '.'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('.'))
	{
		i = rand();
		char cur[] = {',', '.', ' '};
		a = QChar(cur[i%3]);
	}
	return a;
}


void PassGen::changeActivation(bool checked)
{
	if(radio1->isChecked())
	{
		ownPW->setEnabled(true);
		ownPW->setFrame(true);
		ownPW->setFocus(Qt::OtherFocusReason);
		ownLength->setEnabled(false);
		ownLength->setFrame(false);
	}
	if(radio2->isChecked())
	{
		ownPW->setEnabled(false);
		ownPW->setFrame(false);
		ownLength->setEnabled(true);
		ownLength->setFrame(true);
		ownLength->setFocus(Qt::OtherFocusReason);
	}
}

void PassGen::resizeEvent(QResizeEvent *event)
{
	closeButton->setGeometry(size().width() - 23, 8, 15, 14);
	move(QApplication::desktop()->screenGeometry().width()/2 - size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2);
	
}

void PassGen::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		currentGlobalPos = event->globalPos();
		mousePressed = true;
	}
	else
	{
		mousePressed = false;
	}
}

void PassGen::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed)
	{
		mousePressed = false;
		move(pos() - (currentGlobalPos - event->globalPos()));
	}
}

void PassGen::mouseMoveEvent(QMouseEvent *event)
{
	
	if(mousePressed)
	{
		move(pos() - (currentGlobalPos - event->globalPos()));
		currentGlobalPos = event->globalPos();
	}
}

void PassGen::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setRenderHints(QPainter::Antialiasing);
	painter.setBrush(QColor(12,21,51,210));
	painter.setPen(Qt::NoPen);
	painter.drawRect(0,0,size().width(), size().height());
	painter.drawPixmap(0,0,size().width(), 30, QPixmap(":/img/Leiste.png").scaled(size().width(), 30,Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
	painter.drawPixmap(size().width()/2-50,size().height()/2+40,100, 100, QPixmap(":/img/NSA.png").scaled(100,100,Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
	painter.setBrush(QColor(12,21,51,210));
	painter.drawEllipse(size().width()/2-50,size().height()/2+40,100, 100);
	painter.setPen(QColor(22,31,59));
	painter.setFont(QFont("Arial", 9, QFont::Bold));
	painter.drawText(8,8,size().width() - 8, 22, Qt::AlignLeft, "PassGen");
}