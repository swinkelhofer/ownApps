#include <QtGui>
#include "Closer.h"

Closer::Closer(QWidget *parent):QWidget(parent)
{
	hoverTimer = new QTimer;
	hoverTimer->setInterval(100);
	hovered = false;
	mousePressed = false;
	setFixedSize(15,14);
	//x = new QLabel(this);
	//x->setPixmap(QPixmap(":/img/Close.png").scaled(15,14));
	//x->setMouseTracking(true);
	setMouseTracking(true);
	connect(hoverTimer, SIGNAL(timeout()), this, SLOT(updateHover()));
}

void Closer::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.drawPixmap(0,0,15,14,QPixmap(":/img/Close.png").scaled(15,14, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
}

void Closer::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		QGraphicsColorizeEffect *effect = new QGraphicsColorizeEffect;
		effect->setColor(QColor(45,82,142));
		effect->setStrength(0.7);
		setGraphicsEffect(effect);
		mousePressed = true;
	}
	else
		mousePressed = false;
}

void Closer::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed && underMouse())
		emit clicked();
	else
		mousePressed = false;
}

void Closer::mouseMoveEvent(QMouseEvent *event)
{
	if(underMouse() && !hovered)
	{
		hoverTimer->start();
		QGraphicsColorizeEffect *effect = new QGraphicsColorizeEffect;
		effect->setStrength(0.4);
		effect->setColor(QColor(45,82,142));
		setGraphicsEffect(effect);
	}
}

void Closer::updateHover()
{
	if(!underMouse())
	{
		hovered = false;
		hoverTimer->stop();
		QGraphicsBlurEffect *effect = new QGraphicsBlurEffect;
		effect->setEnabled(false);
		setGraphicsEffect(effect);
	}
}
