#include <QWidget>

class Closer;
class QTextEdit;
class QLineEdit;
class QRadioButton;
class QPushButton;
class QPoint;
class QChar;


class PassGen:public QWidget
{
	Q_OBJECT
public:
	PassGen(QWidget *parent = 0);
private:
	QTextEdit *output;
	Closer *closeButton;
	QLineEdit *ownPW, *ownLength;
	QRadioButton *radio1, *radio2;
	QPushButton *generator;
	QPoint currentGlobalPos;
	bool mousePressed;
	QChar change(QChar a);
protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void resizeEvent(QResizeEvent *event);
private slots:
	void changeActivation(bool checked);
	void generate();
};
