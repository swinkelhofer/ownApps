#include <QWidget>
class QPainter;
class QLineEdit;
class QTextEdit;
class Closer;
class QLabel;
class Strength;
class PassTest:public QWidget
{
	Q_OBJECT
public:
	PassTest(QWidget *parent = 0);
private:
	//QPainter *painter;
	QLineEdit *pw;
	QTextEdit *pwText;
	Closer *closeButton;
	QLabel *lab1;
	bool mousePressed;
	QPoint currentGlobalPos;
	Strength *strength;
	QStringList pwList;
protected:
	void paintEvent(QPaintEvent *event);
	void resizeEvent(QResizeEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
private slots:
	void test(const QString &text);
};

class Strength:public QWidget
{
	Q_OBJECT
public:
	Strength(QWidget *parent = 0);
	void setStrength(int strength /*0-100*/);
protected:
	void paintEvent(QPaintEvent *event);
private:
	int currentStrength;
};