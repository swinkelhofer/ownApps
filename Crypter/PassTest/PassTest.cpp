#include <QtGui>
#include "PassTest.h"
#include "Closer.h"

PassTest::PassTest(QWidget *parent):QWidget(parent)
{
	FILE *wordlist = fopen("Password.lst", "r");
	QTextStream in(wordlist);
	QString line;
	do
	{
		line = in.readLine();
		if(!line.trimmed().isEmpty())
			pwList << line;
	}
	while(!line.isNull());
	fclose(wordlist);
	mousePressed = false;
	pw = new QLineEdit;
	pw->setEchoMode(QLineEdit::Password);
	pwText = new QTextEdit;
	pwText->setFixedHeight(150);
	pwText->setFrameShadow(QFrame::Plain);
	pwText->setFrameShape(QFrame::NoFrame);
	pwText->setReadOnly(true);
	QPalette listPalette;
	listPalette.setColor(QPalette::Window, QColor(12,21,51,0));
	listPalette.setColor(QPalette::Text, QColor(255,255,255,255));
	listPalette.setColor(QPalette::Base, QColor(12,21,51,0));
	listPalette.setColor(QPalette::Button, QColor(12,21,51,0));
	pwText->setPalette(listPalette);
	
	strength = new Strength;
	closeButton = new Closer(this);
	closeButton->show();
	closeButton->setGeometry(size().width() - 23, 8, 15, 14);
	connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
	setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
	setFixedSize(300,250);
	QHBoxLayout *up = new QHBoxLayout;
	lab1 = new QLabel("<font color=white>Passwort: </font>", this);
	up->addWidget(lab1);
	up->addWidget(pw);
	
	QVBoxLayout *main = new QVBoxLayout;
	main->addStretch();
	main->addWidget(strength);
	main->addLayout(up);
	main->addWidget(pwText);
	setLayout(main);
	setAttribute(Qt::WA_TranslucentBackground);
	connect(pw, SIGNAL(textChanged(const QString &)), this, SLOT(test(const QString &)));
}

void PassTest::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setRenderHints(QPainter::Antialiasing);
	painter.setBrush(QColor(12,21,51,210));
	painter.setPen(Qt::NoPen);
	painter.drawRect(0,0,size().width(), size().height());
	painter.drawPixmap(0,0,size().width(), 30, QPixmap(":/img/Leiste.png").scaled(size().width(), 30,Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
	painter.drawPixmap(100,120,100, 100, QPixmap(":/img/NSA.png").scaled(100,100,Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
	painter.setBrush(QColor(12,21,51,210));
	painter.drawEllipse(100,120,100,100);
	painter.setPen(QColor(22,31,59));
	painter.setFont(QFont("Arial", 9, QFont::Bold));
	painter.drawText(8,8,size().width() - 8, 22, Qt::AlignLeft, "PassTest");
}

void PassTest::test(const QString &text)
{
	pwText->setText("");
	QString curText = text, outText;
	QStringList wordcount;
	if(curText.length() == 0)
	{
		strength->setStrength(0);
		pwText->setText("+ Passwort zum Testen eingeben.");
		return;
	}
	for(int i = 0; i < pwList.count(); ++i)
	{
		if(curText.contains(pwList.at(i), Qt::CaseInsensitive))
		{
			wordcount << pwList.at(i);
			curText = curText.replace(pwList.at(i), "", Qt::CaseInsensitive).trimmed();
		}
	}
	int A = 0, ka = 0, z = 0, s = 0, strongness = 0;
	for(int i = 0; i < curText.length(); ++i)
	{
		if(curText[i].isDigit())
			++z;
		else if(curText[i].isUpper())
			++A;
		else if(curText[i].isLower())
			++ka;
		else if(curText[i].isPrint())
			++s;
	}
	strongness = (int)((int)(((double)A*log10(29) + (double)ka*log10(30) + (double)z*log10(10) + (double)s*log10(36))*4.55) + (int)((double)curText.length() * log10(29*(A > 0) + 30*(ka > 0) + 10*(z > 0) + 36*(s > 0)) * 4.13)) / 2;
	if(strongness > 100)
		strongness = 100;
	for(int i = 0; i < wordcount.count(); ++i)
		outText += "+ Das Wort " + wordcount.at(i) + " wurde in einer Passwortliste gefunden<br>";
	if(A < 2)
		outText += "+ Am besten mehr Großbuchstaben benutzen<br>";
	if(ka < 2)
		outText += "+ Am besten mehr Kleinbuchstaben benutzen<br>";
	if(z < 2)
		outText += "+ Am besten mehr Zahlen benutzen<br>";
	if(s < 2)
		outText += "+ Am besten mehr Sonderzeichen benutzen<br>";
	pwText->setText(outText);
	strength->setStrength(strongness);
}

void PassTest::resizeEvent(QResizeEvent *event)
{
	closeButton->setGeometry(size().width() - 23, 8, 15, 14);
}

void PassTest::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		currentGlobalPos = event->globalPos();
		mousePressed = true;
	}
	else
	{
		mousePressed = false;
	}
}

void PassTest::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed)
	{
		mousePressed = false;
		move(pos() - (currentGlobalPos - event->globalPos()));
	}
}

void PassTest::mouseMoveEvent(QMouseEvent *event)
{
	
	if(mousePressed)
	{
		move(pos() - (currentGlobalPos - event->globalPos()));
		currentGlobalPos = event->globalPos();
	}
}

Strength::Strength(QWidget *parent):QWidget(parent)
{
	currentStrength = 0;
	setFixedHeight(20);
}

void Strength::setStrength(int strength)
{
	currentStrength = strength;
	if(currentStrength > 100)
		currentStrength = 100;
	else if(currentStrength < 0)
		currentStrength = 0;
	update();
}

void Strength::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	if(currentStrength < 50)
		painter.setBrush(QColor(255, (int)(currentStrength*5.1), 0));
	else if(currentStrength >= 50)
		painter.setBrush(QColor(255 - (int)((currentStrength - 50)*5.1), 255, 0));
	painter.setPen(Qt::NoPen);
	painter.drawRect(0, 0, size().width(), size().height());
	painter.setPen(Qt::black);
	if(currentStrength < 30)
		painter.drawText(QRect(0, 0, size().width(), size().height()), Qt::AlignHCenter | Qt::AlignVCenter, "Sehr unsicher");
	else if(currentStrength >= 30 && currentStrength < 45)
		painter.drawText(QRect(0, 0, size().width(), size().height()), Qt::AlignHCenter | Qt::AlignVCenter, "Unsicher");
	else if(currentStrength >= 45 && currentStrength < 80)
		painter.drawText(QRect(0, 0, size().width(), size().height()), Qt::AlignHCenter | Qt::AlignVCenter, "Normal");
	else if(currentStrength >= 80 && currentStrength < 95)
		painter.drawText(QRect(0, 0, size().width(), size().height()), Qt::AlignHCenter | Qt::AlignVCenter, "Sicher");
	else if(currentStrength >= 95)
		painter.drawText(QRect(0, 0, size().width(), size().height()), Qt::AlignHCenter | Qt::AlignVCenter, "Sehr sicher");
	
}