#include "SHA2.h"
#include <string.h>
#include <sys/stat.h>
#include <stdio.h>
//#include <time.h>
using namespace std;
#define Little2Big32(a) ((a >> 24) | (a << 24) | ((a & 0x00FF0000) >> 8) | ((a & 0x0000FF00) << 8))
#define Little2Big64(a) ((a >> 56) | (a << 56) | ((a & 0x00FF000000000000ULL) >> 40) | ((a & 0x000000000000FF00ULL) << 40) | ((a & 0x0000FF0000000000ULL) >> 24) | ((a & 0x0000000000FF0000ULL) << 24) | ((a & 0x000000FF00000000ULL) >> 8) | ((a & 0x00000000FF000000ULL) << 8))

#define rotr32(a, n) (((a >> n) ^ (a << (32 - n))) & 0xFFFFFFFF)
#define rotl32(a, n) (((a << n) ^ (a >> (32 - n))) & 0xFFFFFFFF)
#define rotr64(a, n) ((a >> n) ^ (a << (64-n)))

#define ext32_0(a) (rotr32(a, 7) ^ rotr32(a, 18) ^ (a >> 3))
#define ext32_1(a) (rotr32(a, 17) ^ rotr32(a, 19) ^ (a >> 10))
#define ext64_0(a) (rotr64(a, 1) ^ rotr64(a, 8) ^ (a >> 7))
#define ext64_1(a) (rotr64(a, 19) ^ rotr64(a, 61) ^ (a >> 6))

#define sigma32_0(a) (rotr32(a, 2) ^ rotr32(a, 13) ^ rotr32(a, 22))
#define sigma32_1(a) (rotr32(a, 6) ^ rotr32(a, 11) ^ rotr32(a, 25))
#define sigma64_0(a) (rotr64(a, 28) ^ rotr64(a, 34) ^ rotr64(a, 39))
#define sigma64_1(a) (rotr64(a, 14) ^ rotr64(a, 18) ^ rotr64(a, 41))

#define maj(a, b, c) ((a & b) ^ (a & c) ^ (b & c))
#define ch(e, f, g) ((e & f) ^ ((~e) & g))


typedef unsigned long long uint64;
typedef unsigned int uint32;
typedef unsigned char BYTE;


static uint32 k32[64] =
{	
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};

static uint64 k64[80] =
{
	0x428a2f98d728ae22ULL, 0x7137449123ef65cdULL, 0xb5c0fbcfec4d3b2fULL, 0xe9b5dba58189dbbcULL, 0x3956c25bf348b538ULL, 0x59f111f1b605d019ULL, 0x923f82a4af194f9bULL, 0xab1c5ed5da6d8118ULL,
	0xd807aa98a3030242ULL, 0x12835b0145706fbeULL, 0x243185be4ee4b28cULL, 0x550c7dc3d5ffb4e2ULL, 0x72be5d74f27b896fULL, 0x80deb1fe3b1696b1ULL, 0x9bdc06a725c71235ULL, 0xc19bf174cf692694ULL,
	0xe49b69c19ef14ad2ULL, 0xefbe4786384f25e3ULL, 0x0fc19dc68b8cd5b5ULL, 0x240ca1cc77ac9c65ULL, 0x2de92c6f592b0275ULL, 0x4a7484aa6ea6e483ULL, 0x5cb0a9dcbd41fbd4ULL, 0x76f988da831153b5ULL,
	0x983e5152ee66dfabULL, 0xa831c66d2db43210ULL, 0xb00327c898fb213fULL, 0xbf597fc7beef0ee4ULL, 0xc6e00bf33da88fc2ULL, 0xd5a79147930aa725ULL, 0x06ca6351e003826fULL, 0x142929670a0e6e70ULL,
	0x27b70a8546d22ffcULL, 0x2e1b21385c26c926ULL, 0x4d2c6dfc5ac42aedULL, 0x53380d139d95b3dfULL, 0x650a73548baf63deULL, 0x766a0abb3c77b2a8ULL, 0x81c2c92e47edaee6ULL, 0x92722c851482353bULL,
	0xa2bfe8a14cf10364ULL, 0xa81a664bbc423001ULL, 0xc24b8b70d0f89791ULL, 0xc76c51a30654be30ULL, 0xd192e819d6ef5218ULL, 0xd69906245565a910ULL, 0xf40e35855771202aULL, 0x106aa07032bbd1b8ULL,
	0x19a4c116b8d2d0c8ULL, 0x1e376c085141ab53ULL, 0x2748774cdf8eeb99ULL, 0x34b0bcb5e19b48a8ULL, 0x391c0cb3c5c95a63ULL, 0x4ed8aa4ae3418acbULL, 0x5b9cca4f7763e373ULL, 0x682e6ff3d6b2b8a3ULL,
	0x748f82ee5defb2fcULL, 0x78a5636f43172f60ULL, 0x84c87814a1f0ab72ULL, 0x8cc702081a6439ecULL, 0x90befffa23631e28ULL, 0xa4506cebde82bde9ULL, 0xbef9a3f7b2c67915ULL, 0xc67178f2e372532bULL,
	0xca273eceea26619cULL, 0xd186b8c721c0c207ULL, 0xeada7dd6cde0eb1eULL, 0xf57d4f7fee6ed178ULL, 0x06f067aa72176fbaULL, 0x0a637dc5a2c898a6ULL, 0x113f9804bef90daeULL, 0x1b710b35131c471bULL,
	0x28db77f523047d84ULL, 0x32caab7b40c72493ULL, 0x3c9ebe0a15c9bebcULL, 0x431d67c49c100d4cULL, 0x4cc5d4becb3e42b6ULL, 0x597f299cfc657e2aULL, 0x5fcb6fab3ad6faecULL, 0x6c44198c4a475817ULL
};

SHA1Hash SHA1(const char *msg)
{
	uint32 hash[5] = 
	{
		0x67452301,
		0xEFCDAB89, 
		0x98BADCFE, 
		0x10325476, 
		0xC3D2E1F0
	};
	
	uint32 a, b, c, d, e, f, k, t1;
	
	uint64 msgLength = (uint64) strlen(msg);
	uint32 over = 64 - (uint32) ((msgLength + (uint64) 9) % (uint64) 64);
	if(over == 64)
		over = 0;
	uint64 maxlength = msgLength + (uint64) 9 + (uint64) over;
	BYTE msg0[maxlength];
	for(uint64 i = 0; i < msgLength; ++i)
		msg0[i] = msg[i];
	
	msg0[msgLength] = 0x80;
	for(uint32 i = 1; i <= over; ++i)
		msg0[msgLength + i] = 0x00;
	uint64 lengthBits = msgLength << 3;
	msg0[msgLength + over + 1] = (BYTE)((lengthBits >> 56) & 0xff);
	msg0[msgLength + over + 2] = (BYTE)((lengthBits >> 48) & 0xff);
	msg0[msgLength + over + 3] = (BYTE)((lengthBits >> 40) & 0xff);
	msg0[msgLength + over + 4] = (BYTE)((lengthBits >> 32) & 0xff);
	msg0[msgLength + over + 5] = (BYTE)((lengthBits >> 24) & 0xff);
	msg0[msgLength + over + 6] = (BYTE)((lengthBits >> 16) & 0xff);
	msg0[msgLength + over + 7] = (BYTE)((lengthBits >> 8) & 0xff);
	msg0[msgLength + over + 8] = (BYTE)(lengthBits & 0xff);
	
	uint32 w[80];
	for(uint64 m = 0; m < (maxlength >> 6); ++m)
	{
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint32) msg0[(m << 6) + (i << 2)] << 24) | ((uint32) msg0[(m << 6) + (i << 2) + 1] << 16) | ((uint32) msg0[(m << 6) + (i << 2) + 2] << 8) | (uint32) msg0[(m << 6) + (i << 2) + 3];
		
		for(int i = 16; i < 80; ++i)
			w[i] = (uint32) rotl32((w[i - 3] ^ w[i - 8] ^ w[i - 14] ^ w[i - 16]), 1);
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		for(int i = 0; i < 80; ++i)
		{
			if(i < 20)
			{
				f = ch(b, c, d);
				k = 0x5A827999;
			}
			else if(i >= 20 && i < 40)
			{
				f = b ^ c ^ d;
				k = 0x6ED9EBA1;
			}
			else if(i >= 40 && i < 60)
			{
				f = maj(b, c, d);
				k = 0x8F1BBCDC;
			}
			else if(i >= 60)
			{
				f = b ^ c ^ d;
				k = 0xCA62C1D6;
			}
			
			t1 = (uint32) (rotl32(a, 5) + f + e + k + w[i]);
			
			e = d;
			d = c;
			c = (uint32) rotl32(b, 30);
			b = a;
			a = t1;
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
	}
	
	struct SHA1Hash ret;
	for(int i = 0; i < 40; ++i)
	{
		ret.hash[i] = (BYTE) (hash[i / 8] >> (32 - ((i % 8) + 1) * 4)) & 0xf;
		switch(ret.hash[i])
		{
			case 0x0:	ret.hash[i] = '0';
						break;
			case 0x1:	ret.hash[i] = '1';
						break;
			case 0x2:	ret.hash[i] = '2';
						break;
			case 0x3:	ret.hash[i] = '3';
						break;
			case 0x4:	ret.hash[i] = '4';
						break;
			case 0x5:	ret.hash[i] = '5';
						break;
			case 0x6:	ret.hash[i] = '6';
						break;
			case 0x7:	ret.hash[i] = '7';
						break;
			case 0x8:	ret.hash[i] = '8';
						break;
			case 0x9:	ret.hash[i] = '9';
						break;
			case 0xa:	ret.hash[i] = 'a';
						break;
			case 0xb:	ret.hash[i] = 'b';
						break;
			case 0xc:	ret.hash[i] = 'c';
						break;
			case 0xd:	ret.hash[i] = 'd';
						break;
			case 0xe:	ret.hash[i] = 'e';
						break;
			case 0xf:	ret.hash[i] = 'f';
						break;
		}
	}
	ret.hash[40] = 0x0;
	return ret;
}

SHA256Hash SHA256(const char *msg)
{
	uint32 hash[8] =
	{
		0x6a09e667, 
		0xbb67ae85, 
		0x3c6ef372, 
		0xa54ff53a, 
		0x510e527f, 
		0x9b05688c, 
		0x1f83d9ab, 
		0x5be0cd19
	};
	uint32 a, b, c, d, e, f, g, h, t2, t1;
	
	uint64 msgLength = (uint64) strlen(msg);
	uint32 over = 64 - (uint32) ((msgLength + (uint64) 9) % (uint64) 64);
	if(over == 64)
		over = 0;
	uint64 maxlength = msgLength + (uint64) 9 + (uint64) over;
	BYTE msg0[maxlength];
	for(uint64 i = 0; i < msgLength; ++i)
		msg0[i] = msg[i];
	
	msg0[msgLength] = 0x80;
	for(uint32 i = 1; i <= over; ++i)
		msg0[msgLength + i] = 0x00;
	uint64 lengthBits = msgLength << 3;
	msg0[msgLength + over + 1] = (BYTE)((lengthBits >> 56) & 0xff);
	msg0[msgLength + over + 2] = (BYTE)((lengthBits >> 48) & 0xff);
	msg0[msgLength + over + 3] = (BYTE)((lengthBits >> 40) & 0xff);
	msg0[msgLength + over + 4] = (BYTE)((lengthBits >> 32) & 0xff);
	msg0[msgLength + over + 5] = (BYTE)((lengthBits >> 24) & 0xff);
	msg0[msgLength + over + 6] = (BYTE)((lengthBits >> 16) & 0xff);
	msg0[msgLength + over + 7] = (BYTE)((lengthBits >> 8) & 0xff);
	msg0[msgLength + over + 8] = (BYTE)(lengthBits & 0xff);
	
	uint32 w[64];
	for(uint64 m = 0; m < (maxlength >> 6); ++m)
	{
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint32) msg0[(m << 6) + (i << 2)] << 24) | ((uint32) msg0[(m << 6) + (i << 2) + 1] << 16) | ((uint32) msg0[(m << 6) + (i << 2) + 2] << 8) | (uint32) msg0[(m << 6) + (i << 2) + 3];
		
		for(int i = 16; i < 64; ++i)
			w[i] = (uint32) (w[i - 16] + ext32_0(w[i - 15]) + w[i - 7] + ext32_1(w[i - 2]));
		
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 64; ++i)
		{
			t2 = (uint32) (sigma32_0(a) + maj(a, b, c));
			t1 = (uint32) (h + sigma32_1(e) + ch(e, f, g) + k32[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint32) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint32) (t1 + t2);
			
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	struct SHA256Hash ret;
	for(int i = 0; i < 64; ++i)
	{
		ret.hash[i] = (BYTE) (hash[i / 8] >> (32 - ((i % 8) + 1) * 4)) & 0xf;
		switch(ret.hash[i])
		{
			case 0x0:	ret.hash[i] = '0';
						break;
			case 0x1:	ret.hash[i] = '1';
						break;
			case 0x2:	ret.hash[i] = '2';
						break;
			case 0x3:	ret.hash[i] = '3';
						break;
			case 0x4:	ret.hash[i] = '4';
						break;
			case 0x5:	ret.hash[i] = '5';
						break;
			case 0x6:	ret.hash[i] = '6';
						break;
			case 0x7:	ret.hash[i] = '7';
						break;
			case 0x8:	ret.hash[i] = '8';
						break;
			case 0x9:	ret.hash[i] = '9';
						break;
			case 0xa:	ret.hash[i] = 'a';
						break;
			case 0xb:	ret.hash[i] = 'b';
						break;
			case 0xc:	ret.hash[i] = 'c';
						break;
			case 0xd:	ret.hash[i] = 'd';
						break;
			case 0xe:	ret.hash[i] = 'e';
						break;
			case 0xf:	ret.hash[i] = 'f';
						break;
		}
	}
	ret.hash[64] = 0x0;
	return ret;
}

SHA384Hash SHA384(const char *msg)
{

	uint64 hash[8] =
		{
		0xcbbb9d5dc1059ed8ULL,
		0x629a292a367cd507ULL,
		0x9159015a3070dd17ULL,
		0x152fecd8f70e5939ULL,
		0x67332667ffc00b31ULL,
		0x8eb44a8768581511ULL,
		0xdb0c2e0d64f98fa7ULL,
		0x47b5481dbefa4fa4ULL
		};
	uint64 a, b, c, d, e, f, g, h, t2, t1;
	
	uint64 msgLength = (uint64) strlen(msg);
	uint32 over = 128 - (uint32) ((msgLength + (uint64) 17) % (uint64) 128);
	if(over == 128)
		over = 0;
	uint64 maxlength = msgLength + (uint64) 17 + (uint64) over;
	BYTE msg0[maxlength];
	for(uint64 i = 0; i < msgLength; ++i)
		msg0[i] = msg[i];
	
	msg0[msgLength] = 0x80;
	for(uint32 i = 1; i <= over; ++i)
		msg0[msgLength + i] = 0x00;
	uint64 lengthBits = msgLength << 3;
	msg0[msgLength + over + 1] = 0x00;
	msg0[msgLength + over + 2] = 0x00;
	msg0[msgLength + over + 3] = 0x00;
	msg0[msgLength + over + 4] = 0x00;
	msg0[msgLength + over + 5] = 0x00;
	msg0[msgLength + over + 6] = 0x00;
	msg0[msgLength + over + 7] = 0x00;
	msg0[msgLength + over + 8] = 0x00;
	msg0[msgLength + over + 9] = (BYTE)((lengthBits >> 56) & 0xff);
	msg0[msgLength + over + 10] = (BYTE)((lengthBits >> 48) & 0xff);
	msg0[msgLength + over + 11] = (BYTE)((lengthBits >> 40) & 0xff);
	msg0[msgLength + over + 12] = (BYTE)((lengthBits >> 32) & 0xff);
	msg0[msgLength + over + 13] = (BYTE)((lengthBits >> 24) & 0xff);
	msg0[msgLength + over + 14] = (BYTE)((lengthBits >> 16) & 0xff);
	msg0[msgLength + over + 15] = (BYTE)((lengthBits >> 8) & 0xff);
	msg0[msgLength + over + 16] = (BYTE)(lengthBits & 0xff);
	
	uint64 w[80];
	for(uint64 m = 0; m < (maxlength >> 7); ++m)
	{
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint64) msg0[(m << 7) + (i << 3)] << 56) | ((uint64) msg0[(m << 7) + (i << 3) + 1] << 48) | ((uint64) msg0[(m << 7) + (i << 3) + 2] << 40) | ((uint64) msg0[(m << 7) + (i << 3) + 3] << 32) | ((uint64) msg0[(m << 7) + (i << 3) + 4] << 24) | ((uint64) msg0[(m << 7) + (i << 3) + 5] << 16) | ((uint64) msg0[(m << 7) + (i << 3) + 6] << 8) | (uint64) msg0[(m << 7) + (i << 3) + 7];
		
		for(int i = 16; i < 80; ++i)
			w[i] = (uint64) (w[i - 16] + ext64_0(w[i - 15]) + w[i - 7] + ext64_1(w[i - 2]));
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 80; ++i)
		{
		
			t2 = (uint64) (sigma64_0(a) + maj(a,b,c));
			t1 = (uint64) (h + sigma64_1(e) + ch(e,f,g) + k64[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint64) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint64) (t1 + t2);
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	
	struct SHA384Hash ret;
	for(int i = 0; i < 96; ++i)
	{
		ret.hash[i] = (BYTE) (hash[i / 16] >> (64 - ((i % 16) + 1) * 4)) & 0xf;
		switch(ret.hash[i])
		{
			case 0x0:	ret.hash[i] = '0';
						break;
			case 0x1:	ret.hash[i] = '1';
						break;
			case 0x2:	ret.hash[i] = '2';
						break;
			case 0x3:	ret.hash[i] = '3';
						break;
			case 0x4:	ret.hash[i] = '4';
						break;
			case 0x5:	ret.hash[i] = '5';
						break;
			case 0x6:	ret.hash[i] = '6';
						break;
			case 0x7:	ret.hash[i] = '7';
						break;
			case 0x8:	ret.hash[i] = '8';
						break;
			case 0x9:	ret.hash[i] = '9';
						break;
			case 0xa:	ret.hash[i] = 'a';
						break;
			case 0xb:	ret.hash[i] = 'b';
						break;
			case 0xc:	ret.hash[i] = 'c';
						break;
			case 0xd:	ret.hash[i] = 'd';
						break;
			case 0xe:	ret.hash[i] = 'e';
						break;
			case 0xf:	ret.hash[i] = 'f';
						break;
		}
	}
	ret.hash[96] = 0x0;
	return ret;
}

SHA512Hash SHA512(const char *msg)
{

	uint64 hash[8] =
		{
		0x6a09e667f3bcc908ULL,
		0xbb67ae8584caa73bULL,
		0x3c6ef372fe94f82bULL,
		0xa54ff53a5f1d36f1ULL,
		0x510e527fade682d1ULL,
		0x9b05688c2b3e6c1fULL,
		0x1f83d9abfb41bd6bULL,
		0x5be0cd19137e2179ULL
		};
	uint64 a, b, c, d, e, f, g, h, t2, t1;
	
	uint64 msgLength = (uint64) strlen(msg);
	uint32 over = 128 - (uint32) ((msgLength + (uint64) 17) % (uint64) 128);
	if(over == 128)
		over = 0;
	uint64 maxlength = msgLength + (uint64) 17 + (uint64) over;
	BYTE msg0[maxlength];
	for(uint64 i = 0; i < msgLength; ++i)
		msg0[i] = msg[i];
	
	msg0[msgLength] = 0x80;
	for(uint32 i = 1; i <= over; ++i)
		msg0[msgLength + i] = 0x00;
	uint64 lengthBits = msgLength << 3;
	msg0[msgLength + over + 1] = 0x00;
	msg0[msgLength + over + 2] = 0x00;
	msg0[msgLength + over + 3] = 0x00;
	msg0[msgLength + over + 4] = 0x00;
	msg0[msgLength + over + 5] = 0x00;
	msg0[msgLength + over + 6] = 0x00;
	msg0[msgLength + over + 7] = 0x00;
	msg0[msgLength + over + 8] = 0x00;
	msg0[msgLength + over + 9] = (BYTE)((lengthBits >> 56) & 0xff);
	msg0[msgLength + over + 10] = (BYTE)((lengthBits >> 48) & 0xff);
	msg0[msgLength + over + 11] = (BYTE)((lengthBits >> 40) & 0xff);
	msg0[msgLength + over + 12] = (BYTE)((lengthBits >> 32) & 0xff);
	msg0[msgLength + over + 13] = (BYTE)((lengthBits >> 24) & 0xff);
	msg0[msgLength + over + 14] = (BYTE)((lengthBits >> 16) & 0xff);
	msg0[msgLength + over + 15] = (BYTE)((lengthBits >> 8) & 0xff);
	msg0[msgLength + over + 16] = (BYTE)(lengthBits & 0xff);
	
	uint64 w[80];
	for(uint64 m = 0; m < (maxlength >> 7); ++m)
	{
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint64) msg0[(m << 7) + (i << 3)] << 56) | ((uint64) msg0[(m << 7) + (i << 3) + 1] << 48) | ((uint64) msg0[(m << 7) + (i << 3) + 2] << 40) | ((uint64) msg0[(m << 7) + (i << 3) + 3] << 32) | ((uint64) msg0[(m << 7) + (i << 3) + 4] << 24) | ((uint64) msg0[(m << 7) + (i << 3) + 5] << 16) | ((uint64) msg0[(m << 7) + (i << 3) + 6] << 8) | (uint64) msg0[(m << 7) + (i << 3) + 7];
		
		for(int i = 16; i < 80; ++i)
			w[i] = (uint64) (w[i - 16] + ext64_0(w[i - 15]) + w[i - 7] + ext64_1(w[i - 2]));
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 80; ++i)
		{
		
			t2 = (uint64) (sigma64_0(a) + maj(a,b,c));
			t1 = (uint64) (h + sigma64_1(e) + ch(e,f,g) + k64[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint64) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint64) (t1 + t2);
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	
	struct SHA512Hash ret;
	for(int i = 0; i < 128; ++i)
	{
		ret.hash[i] = (BYTE) (hash[i / 16] >> (64 - ((i % 16) + 1) * 4)) & 0xf;
		switch(ret.hash[i])
		{
			case 0x0:	ret.hash[i] = '0';
						break;
			case 0x1:	ret.hash[i] = '1';
						break;
			case 0x2:	ret.hash[i] = '2';
						break;
			case 0x3:	ret.hash[i] = '3';
						break;
			case 0x4:	ret.hash[i] = '4';
						break;
			case 0x5:	ret.hash[i] = '5';
						break;
			case 0x6:	ret.hash[i] = '6';
						break;
			case 0x7:	ret.hash[i] = '7';
						break;
			case 0x8:	ret.hash[i] = '8';
						break;
			case 0x9:	ret.hash[i] = '9';
						break;
			case 0xa:	ret.hash[i] = 'a';
						break;
			case 0xb:	ret.hash[i] = 'b';
						break;
			case 0xc:	ret.hash[i] = 'c';
						break;
			case 0xd:	ret.hash[i] = 'd';
						break;
			case 0xe:	ret.hash[i] = 'e';
						break;
			case 0xf:	ret.hash[i] = 'f';
						break;
		}
	}
	ret.hash[128] = 0x0;
	return ret;
}


SHA1Hash FILE2SHA1(const char *fileName)
{
	uint32 hash[5] = 
	{
		0x67452301,
		0xEFCDAB89, 
		0x98BADCFE, 
		0x10325476, 
		0xC3D2E1F0
	};
	
	FILE *hashFile = fopen(fileName, "rb");
	if(hashFile == NULL)
		exit(0);
	
	struct _stati64 statHash;
	_stati64(fileName, &statHash);
	
	uint32 a, b, c, d, e, f, k, t1;
	
	uint64 fileLength = statHash.st_size;
	uint32 over = 64 - (uint32) ((fileLength + (uint64) 9) & (uint64)0x3f);
	if(over == 64)
		over = 0;
	uint64 maxlength = fileLength + (uint64) 9 + (uint64) over;
	BYTE msg0[64];
	
	uint64 lengthBits = fileLength << 3;
	uint32 w[80];
	for(uint64 m = 0; m < (maxlength >> 6); ++m)
	{
		if(m == (maxlength >> 6) - 1)
		{
			
			fread(msg0, fileLength & 0x3f, 1, hashFile);
			
			msg0[(fileLength & 0x3f)] = 0x80;
			for(uint32 i = 1; i <= over; ++i)
				msg0[(fileLength & 0x3f) + i] = 0x00;
			msg0[(fileLength & 0x3f) + over + 1] = (BYTE)((lengthBits >> 56) & 0xff);
			msg0[(fileLength & 0x3f) + over + 2] = (BYTE)((lengthBits >> 48) & 0xff);
			msg0[(fileLength & 0x3f) + over + 3] = (BYTE)((lengthBits >> 40) & 0xff);
			msg0[(fileLength & 0x3f) + over + 4] = (BYTE)((lengthBits >> 32) & 0xff);
			msg0[(fileLength & 0x3f) + over + 5] = (BYTE)((lengthBits >> 24) & 0xff);
			msg0[(fileLength & 0x3f) + over + 6] = (BYTE)((lengthBits >> 16) & 0xff);
			msg0[(fileLength & 0x3f) + over + 7] = (BYTE)((lengthBits >> 8) & 0xff);
			msg0[(fileLength & 0x3f) + over + 8] = (BYTE)(lengthBits & 0xff);
		}
		else
			fread(msg0, 64, 1, hashFile);
			
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint32) msg0[(i << 2)] << 24) | ((uint32) msg0[(i << 2) + 1] << 16) | ((uint32) msg0[(i << 2) + 2] << 8) | (uint32) msg0[(i << 2) + 3];
		
		for(int i = 16; i < 80; ++i)
			w[i] = (uint32) rotl32((w[i - 3] ^ w[i - 8] ^ w[i - 14] ^ w[i - 16]), 1);
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		for(int i = 0; i < 80; ++i)
		{
			if(i < 20)
			{
				f = ch(b, c, d);
				k = 0x5A827999;
			}
			else if(i >= 20 && i < 40)
			{
				f = b ^ c ^ d;
				k = 0x6ED9EBA1;
			}
			else if(i >= 40 && i < 60)
			{
				f = maj(b, c, d);
				k = 0x8F1BBCDC;
			}
			else if(i >= 60)
			{
				f = b ^ c ^ d;
				k = 0xCA62C1D6;
			}
			
			t1 = (uint32) (rotl32(a, 5) + f + e + k + w[i]);
			
			e = d;
			d = c;
			c = (uint32) rotl32(b, 30);
			b = a;
			a = t1;
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
	}
	
	fclose(hashFile);
	struct SHA1Hash ret;
	for(int i = 0; i < 40; ++i)
	{
		ret.hash[i] = (BYTE) (hash[i / 8] >> (32 - ((i % 8) + 1) * 4)) & 0xf;
		switch(ret.hash[i])
		{
			case 0x0:	ret.hash[i] = '0';
						break;
			case 0x1:	ret.hash[i] = '1';
						break;
			case 0x2:	ret.hash[i] = '2';
						break;
			case 0x3:	ret.hash[i] = '3';
						break;
			case 0x4:	ret.hash[i] = '4';
						break;
			case 0x5:	ret.hash[i] = '5';
						break;
			case 0x6:	ret.hash[i] = '6';
						break;
			case 0x7:	ret.hash[i] = '7';
						break;
			case 0x8:	ret.hash[i] = '8';
						break;
			case 0x9:	ret.hash[i] = '9';
						break;
			case 0xa:	ret.hash[i] = 'a';
						break;
			case 0xb:	ret.hash[i] = 'b';
						break;
			case 0xc:	ret.hash[i] = 'c';
						break;
			case 0xd:	ret.hash[i] = 'd';
						break;
			case 0xe:	ret.hash[i] = 'e';
						break;
			case 0xf:	ret.hash[i] = 'f';
						break;
		}
	}
	ret.hash[40] = 0x0;
	return ret;
}

SHA256Hash FILE2SHA256(const char *fileName)
{
	uint32 hash[8] =
	{
		0x6a09e667, 
		0xbb67ae85, 
		0x3c6ef372, 
		0xa54ff53a, 
		0x510e527f, 
		0x9b05688c, 
		0x1f83d9ab, 
		0x5be0cd19
	};
	FILE *hashFile = fopen(fileName, "rb");
	if(hashFile == NULL)
		exit(0);
	
	uint32 a, b, c, d, e, f, g, h, t2, t1;
	
	struct _stati64 statHash;
	_stati64(fileName, &statHash);
	
	uint64 fileLength = statHash.st_size;
	uint64 bitSize = fileLength << 3;
	uint32 w[64];
	BYTE msg0[64];
	for(uint64 m = (fileLength >> 6); m > 0 ; --m)
	{
		/*fread(w, 16, 4, hashFile);
		for(int i = 0; i < 16; ++i)
			w[i] = Little2Big32(w[i]);*/
		fread(msg0, 64, 1, hashFile);
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint32) msg0[(i << 2)] << 24) | ((uint32) msg0[(i << 2) + 1] << 16) | ((uint32) msg0[(i << 2) + 2] << 8) | (uint32) msg0[(i << 2) + 3];
		
		for(int i = 16; i < 64; ++i)
			w[i] = (uint32) (w[i - 16] + ext32_0(w[i - 15]) + w[i - 7] + ext32_1(w[i - 2]));
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 64; ++i)
		{
			t2 = (uint32) (sigma32_0(a) + maj(a, b, c));
			t1 = (uint32) (h + sigma32_1(e) + ch(e, f, g) + k32[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint32) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint32) (t1 + t2);
			
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	if(64 - (fileLength & 0x3f) >= 9)
	{
		fread(msg0, (fileLength & 0x3f), 1, hashFile);
		msg0[fileLength & 0x3f] = 0x80;
		for(int i = (fileLength & 0x3f) + 1; i < 56; ++i)
			msg0[i] = 0x0;
		
		msg0[56] = (BYTE)((bitSize >> 56) & 0xff);
		msg0[57] = (BYTE)((bitSize >> 48) & 0xff);
		msg0[58] = (BYTE)((bitSize >> 40) & 0xff);
		msg0[59] = (BYTE)((bitSize >> 32) & 0xff);
		msg0[60] = (BYTE)((bitSize >> 24) & 0xff);
		msg0[61] = (BYTE)((bitSize >> 16) & 0xff);
		msg0[62] = (BYTE)((bitSize >> 8) & 0xff);
		msg0[63] = (BYTE)(bitSize & 0xff);
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint32) msg0[(i << 2)] << 24) | ((uint32) msg0[(i << 2) + 1] << 16) | ((uint32) msg0[(i << 2) + 2] << 8) | (uint32) msg0[(i << 2) + 3];
		
		for(int i = 16; i < 64; ++i)
			w[i] = (uint32) (w[i - 16] + ext32_0(w[i - 15]) + w[i - 7] + ext32_1(w[i - 2]));
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 64; ++i)
		{
			t2 = (uint32) (sigma32_0(a) + maj(a, b, c));
			t1 = (uint32) (h + sigma32_1(e) + ch(e, f, g) + k32[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint32) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint32) (t1 + t2);
			
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	else
	{
		
		fread(msg0, (fileLength & 0x3f), 1, hashFile);
		msg0[fileLength & 0x3f] = 0x80;
		for(int i = (fileLength & 0x3f)+1; i < 64; ++i)
			msg0[i] = 0x0;
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint32) msg0[(i << 2)] << 24) | ((uint32) msg0[(i << 2) + 1] << 16) | ((uint32) msg0[(i << 2) + 2] << 8) | (uint32) msg0[(i << 2) + 3];
		
		for(int i = 16; i < 64; ++i)
			w[i] = (uint32) (w[i - 16] + ext32_0(w[i - 15]) + w[i - 7] + ext32_1(w[i - 2]));
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 64; ++i)
		{
			t2 = (uint32) (sigma32_0(a) + maj(a, b, c));
			t1 = (uint32) (h + sigma32_1(e) + ch(e, f, g) + k32[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint32) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint32) (t1 + t2);
			
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
		
		
		for(int i = 0; i < 56; ++i)
			msg0[i] = 0x0;
		
		msg0[56] = (BYTE)((bitSize >> 56) & 0xff);
		msg0[57] = (BYTE)((bitSize >> 48) & 0xff);
		msg0[58] = (BYTE)((bitSize >> 40) & 0xff);
		msg0[59] = (BYTE)((bitSize >> 32) & 0xff);
		msg0[60] = (BYTE)((bitSize >> 24) & 0xff);
		msg0[61] = (BYTE)((bitSize >> 16) & 0xff);
		msg0[62] = (BYTE)((bitSize >> 8) & 0xff);
		msg0[63] = (BYTE)(bitSize & 0xff);
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint32) msg0[(i << 2)] << 24) | ((uint32) msg0[(i << 2) + 1] << 16) | ((uint32) msg0[(i << 2) + 2] << 8) | (uint32) msg0[(i << 2) + 3];
		
		for(int i = 16; i < 64; ++i)
			w[i] = (uint32) (w[i - 16] + ext32_0(w[i - 15]) + w[i - 7] + ext32_1(w[i - 2]));
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 64; ++i)
		{
			t2 = (uint32) (sigma32_0(a) + maj(a, b, c));
			t1 = (uint32) (h + sigma32_1(e) + ch(e, f, g) + k32[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint32) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint32) (t1 + t2);
			
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	
	fclose(hashFile);
	SHA256Hash ret;
	for(int i = 0; i < 64; ++i)
	{
		ret.hash[i] = (BYTE) (hash[i / 8] >> (32 - ((i % 8) + 1) * 4)) & 0xf;
		switch(ret.hash[i])
		{
			case 0x0:	ret.hash[i] = '0';
						break;
			case 0x1:	ret.hash[i] = '1';
						break;
			case 0x2:	ret.hash[i] = '2';
						break;
			case 0x3:	ret.hash[i] = '3';
						break;
			case 0x4:	ret.hash[i] = '4';
						break;
			case 0x5:	ret.hash[i] = '5';
						break;
			case 0x6:	ret.hash[i] = '6';
						break;
			case 0x7:	ret.hash[i] = '7';
						break;
			case 0x8:	ret.hash[i] = '8';
						break;
			case 0x9:	ret.hash[i] = '9';
						break;
			case 0xa:	ret.hash[i] = 'a';
						break;
			case 0xb:	ret.hash[i] = 'b';
						break;
			case 0xc:	ret.hash[i] = 'c';
						break;
			case 0xd:	ret.hash[i] = 'd';
						break;
			case 0xe:	ret.hash[i] = 'e';
						break;
			case 0xf:	ret.hash[i] = 'f';
						break;
		}
	}
	ret.hash[64] = 0x0;
	return ret;
}

SHA384Hash FILE2SHA384(const char *fileName)
{
	uint64 hash[8] =
		{
		0xcbbb9d5dc1059ed8ULL,
		0x629a292a367cd507ULL,
		0x9159015a3070dd17ULL,
		0x152fecd8f70e5939ULL,
		0x67332667ffc00b31ULL,
		0x8eb44a8768581511ULL,
		0xdb0c2e0d64f98fa7ULL,
		0x47b5481dbefa4fa4ULL
		};
		
	FILE *hashFile = fopen(fileName, "rb");
	if(hashFile == NULL)
		exit(0);
	struct _stati64 statHash;
	_stati64(fileName, &statHash);
	
	uint64 a, b, c, d, e, f, g, h, t2, t1;
	
	uint64 fileLength = statHash.st_size;
	uint64 bitSize = fileLength << 3;
	uint64 w[80];
	BYTE msg0[128];
	for(uint64 m = fileLength >> 7; m > 0; --m)
	{
		//fread(w, 16, 8, hashFile);
		fread(msg0, 128, 1, hashFile);
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint64) msg0[(i << 3)] << 56) | ((uint64) msg0[(i << 3) + 1] << 48) | ((uint64) msg0[(i << 3) + 2] << 40) | ((uint64) msg0[(i << 3) + 3] << 32) | ((uint64) msg0[(i << 3) + 4] << 24) | ((uint64) msg0[(i << 3) + 5] << 16) | ((uint64) msg0[(i << 3) + 6] << 8) | (uint64) msg0[(i << 3) + 7];
		
		//for(int i = 0; i < 16; ++i)
		//	w[i] = Little2Big64(w[i]);
		for(int i = 16; i < 80; ++i)
			w[i] = (uint64) (w[i - 16] + ext64_0(w[i - 15]) + w[i - 7] + ext64_1(w[i - 2]));
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 80; ++i)
		{
		
			t2 = (uint64) (sigma64_0(a) + maj(a,b,c));
			t1 = (uint64) (h + sigma64_1(e) + ch(e,f,g) + k64[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint64) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint64) (t1 + t2);
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	if(128 - (fileLength & 0x7f) >= 17)
	{
		fread(msg0, (fileLength & 0x7f), 1, hashFile);
		msg0[fileLength & 0x7f] = 0x80;
		for(int i = (fileLength & 0x7f) + 1; i < 112; ++i)
			msg0[i] = 0x0;
		msg0[112] = (BYTE)0x0;
		msg0[113] = (BYTE)0x0;
		msg0[114] = (BYTE)0x0;
		msg0[115] = (BYTE)0x0;
		msg0[116] = (BYTE)0x0;
		msg0[117] = (BYTE)0x0;
		msg0[118] = (BYTE)0x0;
		msg0[119] = (BYTE)0x0;
		msg0[120] = (BYTE)((bitSize >> 56) & 0xff);
		msg0[121] = (BYTE)((bitSize >> 48) & 0xff);
		msg0[122] = (BYTE)((bitSize >> 40) & 0xff);
		msg0[123] = (BYTE)((bitSize >> 32) & 0xff);
		msg0[124] = (BYTE)((bitSize >> 24) & 0xff);
		msg0[125] = (BYTE)((bitSize >> 16) & 0xff);
		msg0[126] = (BYTE)((bitSize >> 8) & 0xff);
		msg0[127] = (BYTE)(bitSize & 0xff);
		
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint64) msg0[(i << 3)] << 56) | ((uint64) msg0[(i << 3) + 1] << 48) | ((uint64) msg0[(i << 3) + 2] << 40) | ((uint64) msg0[(i << 3) + 3] << 32) | ((uint64) msg0[(i << 3) + 4] << 24) | ((uint64) msg0[(i << 3) + 5] << 16) | ((uint64) msg0[(i << 3) + 6] << 8) | (uint64) msg0[(i << 3) + 7];
		for(int i = 16; i < 80; ++i)
			w[i] = (uint64) (w[i - 16] + ext64_0(w[i - 15]) + w[i - 7] + ext64_1(w[i - 2]));
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 80; ++i)
		{
		
			t2 = (uint64) (sigma64_0(a) + maj(a,b,c));
			t1 = (uint64) (h + sigma64_1(e) + ch(e,f,g) + k64[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint64) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint64) (t1 + t2);
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	else
	{
		fread(msg0, (fileLength & 0x7f), 1, hashFile);
		msg0[fileLength & 0x7f] = 0x80;
		for(int i = (fileLength & 0x7f) + 1; i < 128; ++i)
			msg0[i] = 0x0;
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint64) msg0[(i << 3)] << 56) | ((uint64) msg0[(i << 3) + 1] << 48) | ((uint64) msg0[(i << 3) + 2] << 40) | ((uint64) msg0[(i << 3) + 3] << 32) | ((uint64) msg0[(i << 3) + 4] << 24) | ((uint64) msg0[(i << 3) + 5] << 16) | ((uint64) msg0[(i << 3) + 6] << 8) | (uint64) msg0[(i << 3) + 7];
		for(int i = 16; i < 80; ++i)
			w[i] = (uint64) (w[i - 16] + ext64_0(w[i - 15]) + w[i - 7] + ext64_1(w[i - 2]));
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 80; ++i)
		{
		
			t2 = (uint64) (sigma64_0(a) + maj(a,b,c));
			t1 = (uint64) (h + sigma64_1(e) + ch(e,f,g) + k64[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint64) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint64) (t1 + t2);
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
		
		for(int i = 0; i < 112; ++i)
			msg0[i] = 0x0;
		
		
		msg0[112] = (BYTE)0x0;
		msg0[113] = (BYTE)0x0;
		msg0[114] = (BYTE)0x0;
		msg0[115] = (BYTE)0x0;
		msg0[116] = (BYTE)0x0;
		msg0[117] = (BYTE)0x0;
		msg0[118] = (BYTE)0x0;
		msg0[119] = (BYTE)0x0;
		msg0[120] = (BYTE)((bitSize >> 56) & 0xff);
		msg0[121] = (BYTE)((bitSize >> 48) & 0xff);
		msg0[122] = (BYTE)((bitSize >> 40) & 0xff);
		msg0[123] = (BYTE)((bitSize >> 32) & 0xff);
		msg0[124] = (BYTE)((bitSize >> 24) & 0xff);
		msg0[125] = (BYTE)((bitSize >> 16) & 0xff);
		msg0[126] = (BYTE)((bitSize >> 8) & 0xff);
		msg0[127] = (BYTE)(bitSize & 0xff);
		
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint64) msg0[(i << 3)] << 56) | ((uint64) msg0[(i << 3) + 1] << 48) | ((uint64) msg0[(i << 3) + 2] << 40) | ((uint64) msg0[(i << 3) + 3] << 32) | ((uint64) msg0[(i << 3) + 4] << 24) | ((uint64) msg0[(i << 3) + 5] << 16) | ((uint64) msg0[(i << 3) + 6] << 8) | (uint64) msg0[(i << 3) + 7];
		for(int i = 16; i < 80; ++i)
			w[i] = (uint64) (w[i - 16] + ext64_0(w[i - 15]) + w[i - 7] + ext64_1(w[i - 2]));
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 80; ++i)
		{
		
			t2 = (uint64) (sigma64_0(a) + maj(a,b,c));
			t1 = (uint64) (h + sigma64_1(e) + ch(e,f,g) + k64[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint64) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint64) (t1 + t2);
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}		
	fclose(hashFile);
	SHA384Hash ret;
	for(int i = 0; i < 96; ++i)
	{
		ret.hash[i] = (BYTE) (hash[i >> 4] >> (64 - ((i & 0xf) + 1) << 2)) & 0xf;
		switch(ret.hash[i])
		{
			case 0x0:	ret.hash[i] = '0';
						break;
			case 0x1:	ret.hash[i] = '1';
						break;
			case 0x2:	ret.hash[i] = '2';
						break;
			case 0x3:	ret.hash[i] = '3';
						break;
			case 0x4:	ret.hash[i] = '4';
						break;
			case 0x5:	ret.hash[i] = '5';
						break;
			case 0x6:	ret.hash[i] = '6';
						break;
			case 0x7:	ret.hash[i] = '7';
						break;
			case 0x8:	ret.hash[i] = '8';
						break;
			case 0x9:	ret.hash[i] = '9';
						break;
			case 0xa:	ret.hash[i] = 'a';
						break;
			case 0xb:	ret.hash[i] = 'b';
						break;
			case 0xc:	ret.hash[i] = 'c';
						break;
			case 0xd:	ret.hash[i] = 'd';
						break;
			case 0xe:	ret.hash[i] = 'e';
						break;
			case 0xf:	ret.hash[i] = 'f';
						break;
		}
	}
	ret.hash[96] = 0x0;
	return ret;
}

SHA512Hash FILE2SHA512(const char *fileName)
{
	float start = clock();
	uint64 hash[8] =
	{
		0x6a09e667f3bcc908ULL,
		0xbb67ae8584caa73bULL,
		0x3c6ef372fe94f82bULL,
		0xa54ff53a5f1d36f1ULL,
		0x510e527fade682d1ULL,
		0x9b05688c2b3e6c1fULL,
		0x1f83d9abfb41bd6bULL,
		0x5be0cd19137e2179ULL
	};
		
		
	FILE *hashFile = fopen(fileName, "rb");
	if(hashFile == NULL)
		exit(0);
	struct _stati64 statHash;
	_stati64(fileName, &statHash);
	
	register uint64 a, b, c, d, e, f, g, h, t2, t1;
	
	uint64 fileLength = statHash.st_size;
	uint64 bitSize = fileLength << 3;
	uint64 w[80];
	BYTE msg0[128];
	for(uint64 m = (fileLength >> 7); m > 0 ; --m)
	{
		fread(msg0, 128, 1, hashFile);
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint64) msg0[(i << 3)] << 56) | ((uint64) msg0[(i << 3) + 1] << 48) | ((uint64) msg0[(i << 3) + 2] << 40) | ((uint64) msg0[(i << 3) + 3] << 32) | ((uint64) msg0[(i << 3) + 4] << 24) | ((uint64) msg0[(i << 3) + 5] << 16) | ((uint64) msg0[(i << 3) + 6] << 8) | (uint64) msg0[(i << 3) + 7];
		
		for(int i = 16; i < 80; ++i)
			w[i] = (uint64) (w[i - 16] + ext64_0(w[i - 15]) + w[i - 7] + ext64_1(w[i - 2]));
		

		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		for(int i = 0; i < 80; ++i)
		{
		
			t2 = (uint64) (sigma64_0(a) + maj(a,b,c));
			t1 = (uint64) (h + sigma64_1(e) + ch(e,f,g) + k64[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint64) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint64) (t1 + t2);
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	if(128 - (fileLength & 0x7f) >= 17)
	{
		fread(msg0, (fileLength & 0x7f), 1, hashFile);
		msg0[fileLength & 0x7f] = 0x80;
		for(int i = (fileLength & 0x7f) + 1; i < 112; ++i)
			msg0[i] = 0x0;
		msg0[112] = (BYTE)0x0;
		msg0[113] = (BYTE)0x0;
		msg0[114] = (BYTE)0x0;
		msg0[115] = (BYTE)0x0;
		msg0[116] = (BYTE)0x0;
		msg0[117] = (BYTE)0x0;
		msg0[118] = (BYTE)0x0;
		msg0[119] = (BYTE)0x0;
		msg0[120] = (BYTE)((bitSize >> 56) & 0xff);
		msg0[121] = (BYTE)((bitSize >> 48) & 0xff);
		msg0[122] = (BYTE)((bitSize >> 40) & 0xff);
		msg0[123] = (BYTE)((bitSize >> 32) & 0xff);
		msg0[124] = (BYTE)((bitSize >> 24) & 0xff);
		msg0[125] = (BYTE)((bitSize >> 16) & 0xff);
		msg0[126] = (BYTE)((bitSize >> 8) & 0xff);
		msg0[127] = (BYTE)(bitSize & 0xff);
		
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint64) msg0[(i << 3)] << 56) | ((uint64) msg0[(i << 3) + 1] << 48) | ((uint64) msg0[(i << 3) + 2] << 40) | ((uint64) msg0[(i << 3) + 3] << 32) | ((uint64) msg0[(i << 3) + 4] << 24) | ((uint64) msg0[(i << 3) + 5] << 16) | ((uint64) msg0[(i << 3) + 6] << 8) | (uint64) msg0[(i << 3) + 7];
		for(int i = 16; i < 80; ++i)
			w[i] = (uint64) (w[i - 16] + ext64_0(w[i - 15]) + w[i - 7] + ext64_1(w[i - 2]));
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 80; ++i)
		{
		
			t2 = (uint64) (sigma64_0(a) + maj(a,b,c));
			t1 = (uint64) (h + sigma64_1(e) + ch(e,f,g) + k64[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint64) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint64) (t1 + t2);
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	else
	{
		fread(msg0, (fileLength & 0x7f), 1, hashFile);
		msg0[fileLength & 0x7f] = 0x80;
		for(int i = (fileLength & 0x7f) + 1; i < 128; ++i)
			msg0[i] = 0x0;
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint64) msg0[(i << 3)] << 56) | ((uint64) msg0[(i << 3) + 1] << 48) | ((uint64) msg0[(i << 3) + 2] << 40) | ((uint64) msg0[(i << 3) + 3] << 32) | ((uint64) msg0[(i << 3) + 4] << 24) | ((uint64) msg0[(i << 3) + 5] << 16) | ((uint64) msg0[(i << 3) + 6] << 8) | (uint64) msg0[(i << 3) + 7];
		for(int i = 16; i < 80; ++i)
			w[i] = (uint64) (w[i - 16] + ext64_0(w[i - 15]) + w[i - 7] + ext64_1(w[i - 2]));
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 80; ++i)
		{
		
			t2 = (uint64) (sigma64_0(a) + maj(a,b,c));
			t1 = (uint64) (h + sigma64_1(e) + ch(e,f,g) + k64[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint64) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint64) (t1 + t2);
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
		
		for(int i = 0; i < 112; ++i)
			msg0[i] = 0x0;
		
		
		msg0[112] = (BYTE)0x0;
		msg0[113] = (BYTE)0x0;
		msg0[114] = (BYTE)0x0;
		msg0[115] = (BYTE)0x0;
		msg0[116] = (BYTE)0x0;
		msg0[117] = (BYTE)0x0;
		msg0[118] = (BYTE)0x0;
		msg0[119] = (BYTE)0x0;
		msg0[120] = (BYTE)((bitSize >> 56) & 0xff);
		msg0[121] = (BYTE)((bitSize >> 48) & 0xff);
		msg0[122] = (BYTE)((bitSize >> 40) & 0xff);
		msg0[123] = (BYTE)((bitSize >> 32) & 0xff);
		msg0[124] = (BYTE)((bitSize >> 24) & 0xff);
		msg0[125] = (BYTE)((bitSize >> 16) & 0xff);
		msg0[126] = (BYTE)((bitSize >> 8) & 0xff);
		msg0[127] = (BYTE)(bitSize & 0xff);
		
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint64) msg0[(i << 3)] << 56) | ((uint64) msg0[(i << 3) + 1] << 48) | ((uint64) msg0[(i << 3) + 2] << 40) | ((uint64) msg0[(i << 3) + 3] << 32) | ((uint64) msg0[(i << 3) + 4] << 24) | ((uint64) msg0[(i << 3) + 5] << 16) | ((uint64) msg0[(i << 3) + 6] << 8) | (uint64) msg0[(i << 3) + 7];
		for(int i = 16; i < 80; ++i)
			w[i] = (uint64) (w[i - 16] + ext64_0(w[i - 15]) + w[i - 7] + ext64_1(w[i - 2]));
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 80; ++i)
		{
		
			t2 = (uint64) (sigma64_0(a) + maj(a,b,c));
			t1 = (uint64) (h + sigma64_1(e) + ch(e,f,g) + k64[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint64) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint64) (t1 + t2);
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}		
	fclose(hashFile);
	SHA512Hash ret;
	for(int i = 0; i < 128; ++i)
	{
		ret.hash[i] = (BYTE) (hash[i >> 4] >> (64 - ((i & 0xf) + 1) << 2)) & 0xf;
		switch(ret.hash[i])
		{
			case 0x0:	ret.hash[i] = '0';
						break;
			case 0x1:	ret.hash[i] = '1';
						break;
			case 0x2:	ret.hash[i] = '2';
						break;
			case 0x3:	ret.hash[i] = '3';
						break;
			case 0x4:	ret.hash[i] = '4';
						break;
			case 0x5:	ret.hash[i] = '5';
						break;
			case 0x6:	ret.hash[i] = '6';
						break;
			case 0x7:	ret.hash[i] = '7';
						break;
			case 0x8:	ret.hash[i] = '8';
						break;
			case 0x9:	ret.hash[i] = '9';
						break;
			case 0xa:	ret.hash[i] = 'a';
						break;
			case 0xb:	ret.hash[i] = 'b';
						break;
			case 0xc:	ret.hash[i] = 'c';
						break;
			case 0xd:	ret.hash[i] = 'd';
						break;
			case 0xe:	ret.hash[i] = 'e';
						break;
			case 0xf:	ret.hash[i] = 'f';
						break;
		}
	}
	ret.hash[128] = 0x0;
	float stop = clock();
	printf("\n%f s\n",(stop-start) / 1000);
	return ret;
}