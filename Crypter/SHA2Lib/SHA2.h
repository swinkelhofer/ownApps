#include <windows.h>
#include <sys/time.h>
#define DllExport extern "C" __declspec(dllexport)

struct SHA1Hash
{
	char hash[41];
};
struct SHA256Hash
{
	char hash[65]; 
};
struct SHA384Hash
{
	char hash[97];
};
struct SHA512Hash
{ 
	char hash[129];
};


DllExport SHA1Hash SHA1(const char *msg);
DllExport SHA256Hash SHA256(const char *msg);
DllExport SHA384Hash SHA384(const char *msg);
DllExport SHA512Hash SHA512(const char *msg);
DllExport SHA1Hash FILE2SHA1(const char *fileName);
DllExport SHA256Hash FILE2SHA256(const char *fileName);
DllExport SHA384Hash FILE2SHA384(const char *fileName);
DllExport SHA512Hash FILE2SHA512(const char *fileName);