#include <windows.h>
#include <stdio.h>
#include "SHA2Lib.h"
int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		printf("Usage: ProgName <String>\n\n");
		exit(0);
	}
	HMODULE hmod = LoadLibrary("SHA2Lib.dll");
	if(hmod == NULL)
	{
		printf("Error loading lib\n\n");
		exit(0);
	}
	FILE2SHA1Function FILE2SHA1 = (FILE2SHA1Function) GetProcAddress(hmod, "FILE2SHA1");
	FILE2SHA256Function FILE2SHA256 = (FILE2SHA256Function) GetProcAddress(hmod, "FILE2SHA256");
	FILE2SHA384Function FILE2SHA384 = (FILE2SHA384Function) GetProcAddress(hmod, "FILE2SHA384");
	FILE2SHA512Function FILE2SHA512 = (FILE2SHA512Function) GetProcAddress(hmod, "FILE2SHA512");
	SHA1Function SHA1 = (SHA1Function) GetProcAddress(hmod, "SHA1");
	SHA256Function SHA256 = (SHA256Function) GetProcAddress(hmod, "SHA256");
	SHA384Function SHA384 = (SHA384Function) GetProcAddress(hmod, "SHA384");
	SHA512Function SHA512 = (SHA512Function) GetProcAddress(hmod, "SHA512");
	
	if(SHA384 == NULL | SHA256 == NULL | SHA512 == NULL)
	{
		printf("Error loading func\n\n");
		exit(0);
	}
	//SHA256Hash var = SHA256(argv[1]);
	//SHA384Hash var1 = SHA384(argv[1]);
	//SHA512Hash var2 = SHA512(argv[1]);
	/*SHA256Hash file = FILE2SHA256(argv[1]);
	printf("\n\nFILE2SHA256: 0x%s", file.hash);
	SHA384Hash file1 = FILE2SHA384(argv[1]);
	printf("\n\nFILE2SHA384: 0x%s", file1.hash);*/
	SHA512Hash file2 = FILE2SHA512(argv[1]);
	printf("\n\nFILE2SHA512: 0x%s", file2.hash);
	return 0;
}