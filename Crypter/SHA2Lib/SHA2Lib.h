/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                           @      /^\   /^\
@  @@@@@    @@  @@    @@@@         @@@@@    @     // \\ // \\
@ @@   @@   @@  @@   @@  @@       @@   @@   @    @           @
@ @@        @@  @@   @@  @@            @@   @   |             |
@  @@@@@    @@@@@@   @@@@@@  @@@@     @@    @   |   (o) (o)   |
@      @@   @@  @@   @@  @@         @@@     @    4           2
@      @@   @@  @@   @@  @@        @@       @     \         /
@ @@@@@     @@  @@   @@  @@       @@@@@@@@  @    ___\ | | /___
@                                           @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><


The Function prologues are:

	SHA256Hash SHA256(const char *)
	SHA384Hash SHA384(const char *)
	SHA512Hash SHA512(const char *)
	SHA256Hash FILE2SHA256(const char *)
	SHA384Hash FILE2SHA384(const char *)
	SHA512Hash FILE2SHA512(const char *)

	
You can use the SHA-2-Functions in the library the following way:
	
	HMODULE hmod = LoadLibrary("SHA2Lib.dll");
	FILE2SHA256Function FILE2SHA256 = (FILE2SHA256Function) GetProcAddress(hmod, "FILE2SHA256");
	FILE2SHA384Function FILE2SHA384 = (FILE2SHA384Function) GetProcAddress(hmod, "FILE2SHA384");
	FILE2SHA512Function FILE2SHA512 = (FILE2SHA512Function) GetProcAddress(hmod, "FILE2SHA512");
	SHA256Function SHA256 = (SHA256Function) GetProcAddress(hmod, "SHA256");
	SHA384Function SHA384 = (SHA384Function) GetProcAddress(hmod, "SHA384");
	SHA512Function SHA512 = (SHA512Function) GetProcAddress(hmod, "SHA512");
	SHA256Hash file1 = FILE2SHA256("FileName");
	SHA384Hash file2 = FILE2SHA384("FileName");
	SHA512Hash file3 = FILE2SHA512("FileName");
	SHA256Hash var1 = SHA256("String");
	SHA384Hash var2 = SHA384("String");
	SHA512Hash var3 = SHA512("String");
	
	� 2011, Sascha Winkelhofer
*/

#include <windows.h>

struct SHA_1Hash
{
	char hash[41];
};
struct SHA_256Hash
{
	char hash[65]; 
};
struct SHA_384Hash
{
	char hash[97];
};
struct SHA_512Hash
{ 
	char hash[129];
};

typedef struct SHA_1Hash SHA1Hash;
typedef struct SHA_256Hash SHA256Hash;
typedef struct SHA_384Hash SHA384Hash;
typedef struct SHA_512Hash SHA512Hash;
typedef SHA1Hash (*SHA1Function)(const char*);
typedef SHA256Hash (*SHA256Function)(const char*);
typedef SHA384Hash (*SHA384Function)(const char*);
typedef SHA512Hash (*SHA512Function)(const char*);
typedef SHA1Hash (*FILE2SHA1Function)(const char*);
typedef SHA256Hash (*FILE2SHA256Function)(const char*);
typedef SHA384Hash (*FILE2SHA384Function)(const char*);
typedef SHA512Hash (*FILE2SHA512Function)(const char*);
