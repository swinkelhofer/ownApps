class CryptString;
CryptString SHA256(const char *msg);
CryptString SHA384(const char *msg);
CryptString SHA512(const char *msg);