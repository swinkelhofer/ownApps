#include "CryptString.h"
#include "SHA2.h"
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		printf("Usage: SHA2 <HashText>");
		exit(0);
	}
	printf("SHA-256: ");
	CryptString crypt = SHA256(argv[1]);
	crypt.print();
	printf("\n\nSHA-384: ");
	
	CryptString crypt2 = SHA384(argv[1]);
	crypt2.print();
	printf("\n\nSHA-512: ");
	
	
	CryptString crypt3 = SHA512(argv[1]);
	crypt3.print();

	return 0;
}