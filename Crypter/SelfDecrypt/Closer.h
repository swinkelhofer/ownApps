#include <QWidget>
#ifndef CLOSER
#define CLOSER
class QTimer;
class QLabel;
class Closer:public QWidget
{
	Q_OBJECT
public:
	Closer(QWidget *parent = 0);
protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void paintEvent(QPaintEvent *event);
private:
	bool mousePressed, hovered;
	QTimer *hoverTimer;
	//QLabel *x;
signals:
	void clicked();
private slots:
	void updateHover();
};
#endif
