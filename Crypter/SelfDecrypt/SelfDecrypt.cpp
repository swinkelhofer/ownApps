#include <QtGui>
#include "SelfDecrypt.h"
#include "AES.h"
#include "CryptString.h"
#include "Closer.h"
#include "SHA2.h"

SelfDecrypt::SelfDecrypt(char *fileName, QWidget *parent):QWidget(parent)
{
	setAttribute(Qt::WA_TranslucentBackground, true);
	setWindowFlags(Qt::FramelessWindowHint);
	setFixedSize(300,80);
	appName = fileName;
	OK = new QPushButton("Entschlüsseln");
	pw = new QLineEdit;
	pw->setEchoMode(QLineEdit::Password);
	QHBoxLayout *bottom = new QHBoxLayout;
	bottom->addWidget(new QLabel("<font color=white>Passwort: </font>"));
	bottom->addWidget(pw);
	bottom->addWidget(OK);
	QVBoxLayout *main = new QVBoxLayout;
	main->addStretch();
	main->addLayout(bottom);
	setLayout(main);
	Closer *closeButton = new Closer(this);
	closeButton->setGeometry(size().width() - 23, 8,15,14);
	
	connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
	connect(OK, SIGNAL(clicked()), this, SLOT(encryptSelf()));
}

void SelfDecrypt::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QPixmap pix = QPixmap(":/img/Leiste.png").scaled(size().width(), 30, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	painter.setPen(Qt::NoPen);
	painter.setBrush(QColor(12,21,51,210));
	painter.drawRect(0,0,size().width(),size().height());
	painter.drawPixmap(0,0,size().width(),30,pix);
	painter.setPen(QColor(22,31,59));
	painter.setFont(QFont("Arial", 9, QFont::Bold));
	painter.drawText(8,8,size().width() - 8, 22, Qt::AlignLeft, "SelfDecrypt");
}

void SelfDecrypt::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		currentGlobalPos = event->globalPos();
		mousePressed = true;
	}
	else
	{
		mousePressed = false;
	}
}

void SelfDecrypt::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed)
	{
		mousePressed = false;
		move(pos() - (currentGlobalPos - event->globalPos()));
	}
}

void SelfDecrypt::mouseMoveEvent(QMouseEvent *event)
{
	
	if(mousePressed)
	{
		move(pos() - (currentGlobalPos - event->globalPos()));
		currentGlobalPos = event->globalPos();
	}
}


void SelfDecrypt::encryptSelf()
{
	if(!pw->text().isEmpty())
	{
		CryptString key = SHA2(pw->text().toStdString().c_str());
		QString outfile = QString(appName);
		outfile.chop(4);
		::selfDecrypt(appName, outfile.toStdString().c_str(), key);
	}
}