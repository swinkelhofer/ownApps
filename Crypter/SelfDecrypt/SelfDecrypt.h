#include <QWidget>

class QPushButton;
class QLineEdit; 
class QPoint;
class SelfDecrypt:public QWidget
{
	Q_OBJECT
public:
	SelfDecrypt(char *fileName, QWidget *parent = 0);
private:
	QPushButton *OK;
	QLineEdit *pw;
	char *appName;
	QPoint currentGlobalPos;
	bool mousePressed;
protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
private slots:
	void encryptSelf();
};