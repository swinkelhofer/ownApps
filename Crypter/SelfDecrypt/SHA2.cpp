#include "SHA2.h"
#include <string.h>
#include <string>
#include "CryptString.h"
#include <stdio.h>
using namespace std;
#define rotr32(a, n) (((a >> n) | (a << (32 - n))) & 0xFFFFFFFF)
#define rotl32(a, n) (((a << n) | (a >> (32 - n))) & 0xFFFFFFFF)
#define rotr64(a, n) ((a >> n) | (a << (64-n)))

#define ext32_0(a) (rotr32(a, 7) ^ rotr32(a, 18) ^ (a >> 3))
#define ext32_1(a) (rotr32(a, 17) ^ rotr32(a, 19) ^ (a >> 10))
#define ext64_0(a) (rotr64(a, 1) ^ rotr64(a, 8) ^ (a >> 7))
#define ext64_1(a) (rotr64(a, 19) ^ rotr64(a, 61) ^ (a >> 6))

#define sigma32_0(a) (rotr32(a, 2) ^ rotr32(a, 13) ^ rotr32(a, 22))
#define sigma32_1(a) (rotr32(a, 6) ^ rotr32(a, 11) ^ rotr32(a, 25))
#define sigma64_0(a) (rotr64(a, 28) ^ rotr64(a, 34) ^ rotr64(a, 39))
#define sigma64_1(a) (rotr64(a, 14) ^ rotr64(a, 18) ^ rotr64(a, 41))

#define maj(a, b, c) ((a & b) ^ (a & c) ^ (b & c))
#define ch(e, f, g) ((e & f) ^ ((~e) & g))

typedef unsigned long long uint64;
typedef unsigned int uint32;
typedef unsigned char BYTE;
static uint32 k32[64] =
{	
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};
CryptString SHA2(const char *msg)
{
	
	uint32 hash[8] =
	{
		0x6a09e667, 
		0xbb67ae85, 
		0x3c6ef372, 
		0xa54ff53a, 
		0x510e527f, 
		0x9b05688c, 
		0x1f83d9ab, 
		0x5be0cd19
	};
	uint32 a, b, c, d, e, f, g, h, t2, t1;
	
	uint64 msgLength = (uint64) strlen(msg);
	uint32 over = 64 - (uint32) ((msgLength + (uint64) 9) % (uint64) 64);
	if(over == 64)
		over = 0;
	uint64 maxlength = msgLength + (uint64) 9 + (uint64) over;
	BYTE msg0[maxlength];
	for(uint64 i = 0; i < msgLength; ++i)
		msg0[i] = msg[i];
	
	msg0[msgLength] = 0x80;
	for(uint32 i = 1; i <= over; ++i)
		msg0[msgLength + i] = 0x00;
	uint64 lengthBits = msgLength << 3;
	msg0[msgLength + over + 1] = (BYTE)((lengthBits >> 56) & 0xff);
	msg0[msgLength + over + 2] = (BYTE)((lengthBits >> 48) & 0xff);
	msg0[msgLength + over + 3] = (BYTE)((lengthBits >> 40) & 0xff);
	msg0[msgLength + over + 4] = (BYTE)((lengthBits >> 32) & 0xff);
	msg0[msgLength + over + 5] = (BYTE)((lengthBits >> 24) & 0xff);
	msg0[msgLength + over + 6] = (BYTE)((lengthBits >> 16) & 0xff);
	msg0[msgLength + over + 7] = (BYTE)((lengthBits >> 8) & 0xff);
	msg0[msgLength + over + 8] = (BYTE)(lengthBits & 0xff);
	
	uint32 w[64];
	for(uint64 m = 0; m < (maxlength >> 6); ++m)
	{
		for(int i = 0; i < 16; ++i)
			w[i] = ((uint32) msg0[(m << 6) + (i << 2)] << 24) | ((uint32) msg0[(m << 6) + (i << 2) + 1] << 16) | ((uint32) msg0[(m << 6) + (i << 2) + 2] << 8) | (uint32) msg0[(m << 6) + (i << 2) + 3];
		
		for(int i = 16; i < 64; ++i)
			w[i] = (uint32) (w[i - 16] + ext32_0(w[i - 15]) + w[i - 7] + ext32_1(w[i - 2]));
		
		
		a = hash[0];
		b = hash[1];
		c = hash[2];
		d = hash[3];
		e = hash[4];
		f = hash[5];
		g = hash[6];
		h = hash[7];
		
		
		for(int i = 0; i < 64; ++i)
		{
			t2 = (uint32) (sigma32_0(a) + maj(a, b, c));
			t1 = (uint32) (h + sigma32_1(e) + ch(e, f, g) + k32[i] + w[i]);
		
			h = g;
			g = f;
			f = e;
			e = (uint32) (d + t1);
			d = c;
			c = b;
			b = a;
			a = (uint32) (t1 + t2);
			
		}
		
		hash[0] += a;
		hash[1] += b;
		hash[2] += c;
		hash[3] += d;
		hash[4] += e;
		hash[5] += f;
		hash[6] += g;
		hash[7] += h;
	}
	CryptString ret;
	ret.str[0] = (char) (hash[0] >> 24) & 0xff;
	ret.str[1] = (char) (hash[0] >> 16) & 0xff;
	ret.str[2] = (char) (hash[0] >> 8) & 0xff;
	ret.str[3] = (char) (hash[0]) & 0xff;
	ret.str[4] = (char) (hash[1] >> 24) & 0xff;
	ret.str[5] = (char) (hash[1] >> 16) & 0xff;
	ret.str[6] = (char) (hash[1] >> 8) & 0xff;
	ret.str[7] = (char) (hash[1]) & 0xff;
	ret.str[8] = (char) (hash[2] >> 24) & 0xff;
	ret.str[9] = (char) (hash[2] >> 16) & 0xff;
	ret.str[10] = (char) (hash[2] >> 8) & 0xff;
	ret.str[11] = (char) (hash[2]) & 0xff;
	ret.str[12] = (char) (hash[3] >> 24) & 0xff;
	ret.str[13] = (char) (hash[3] >> 16) & 0xff;
	ret.str[14] = (char) (hash[3] >> 8) & 0xff;
	ret.str[15] = (char) (hash[3]) & 0xff;
	ret.str[16] = (char) (hash[4] >> 24) & 0xff;
	ret.str[17] = (char) (hash[4] >> 16) & 0xff;
	ret.str[18] = (char) (hash[4] >> 8) & 0xff;
	ret.str[19] = (char) (hash[4]) & 0xff;
	ret.str[20] = (char) (hash[5] >> 24) & 0xff;
	ret.str[21] = (char) (hash[5] >> 16) & 0xff;
	ret.str[22] = (char) (hash[5] >> 8) & 0xff;
	ret.str[23] = (char) (hash[5]) & 0xff;
	ret.str[24] = (char) (hash[6] >> 24) & 0xff;
	ret.str[25] = (char) (hash[6] >> 16) & 0xff;
	ret.str[26] = (char) (hash[6] >> 8) & 0xff;
	ret.str[27] = (char) (hash[6]) & 0xff;
	ret.str[28] = (char) (hash[7] >> 24) & 0xff;
	ret.str[29] = (char) (hash[7] >> 16) & 0xff;
	ret.str[30] = (char) (hash[7] >> 8) & 0xff;
	ret.str[31] = (char) (hash[7]) & 0xff;
	
	return ret;
}