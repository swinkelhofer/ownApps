#ifndef AES
#define AES
#include "CryptString.h"
using namespace std;

void FirstRound();
void inv_LastRound();
void inv_Combined();
void FastBlockEnc128();
void FastBlockDec128();
void FastBlockEnc192();
void FastBlockDec192();
void FastBlockEnc256();
void FastBlockDec256();
void KeyExpansion();
void FirstRound();
void LastRound();
void Combined();
void encrypt(const char *infile, const char *outfile, CryptString key, int keylength);
void decrypt(const char *infile, const char *outfile, CryptString key);
void selfDecrypt(const char *infile, const char *outfile, CryptString key);
#endif
