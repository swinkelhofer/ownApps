#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
typedef void (WINAPI *compr)(const char *, const char *);
typedef void (WINAPI *decompr)(const char *, const char *);
typedef void (*progcb)(void *);

struct _stati64 file;

int ProgressCallback(void *p, __int64 in, __int64 out)
{
	printf("%3d%%\r", (in*100)/file.st_size);
	return 0;
}

int main(int argc, char *argv[])
{
	if(argc < 4)
	{
		printf("Usage: Compress.exe <Infile> <Outfile> <c | d>\nc, d stands for compress or decompress\n"); 
		return 0;
	}
	HMODULE hmod = LoadLibrary(TEXT("LzmaLib.dll"));
	if(hmod == NULL)
	{
		printf("Error loading lib");
		return 0;
	}
	_stati64(argv[1], &file);
	compr CompressFile = (compr) GetProcAddress(hmod, "CompressFile");
	decompr DecompressFile = (decompr) GetProcAddress(hmod, "DecompressFile");
	progcb SetProgressCallback = (progcb) GetProcAddress(hmod, "SetProgressCallback");
	
	SetProgressCallback((void*)&ProgressCallback);
	
	int len = strlen(argv[1]);
	char infile[len + 5];
	for(int i = 0; i < len; ++i)
		infile[i] = argv[1][i];
	infile[len] = '.';
	infile[len+1] = 't';
	infile[len+2] = 'x';
	infile[len+3] = 't';
	infile[len+4] = '\0';
	float start = clock();
	if(argv[3][0] == 'c')
		CompressFile(argv[1], argv[2]);
	else if(argv[3][0] == 'd')
		DecompressFile(argv[1], argv[2]);
	float stop = clock();
	printf("\nFinished in %f secs", (stop - start) / 1000.0);
	FreeLibrary(hmod);
	return 0;
}