/****************************************************************************
** Meta object code from reading C++ file 'ScreenKeyboard.h'
**
** Created: Sun 16. Oct 16:48:46 2011
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../ScreenKeyboard.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ScreenKeyboard.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Key[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
       5,    4,    4,    4, 0x05,

 // slots: signature, parameters, type, tag, flags
      15,    4,    4,    4, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Key[] = {
    "Key\0\0clicked()\0sendMessageSlot()\0"
};

const QMetaObject Key::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Key,
      qt_meta_data_Key, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Key::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Key::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Key::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Key))
        return static_cast<void*>(const_cast< Key*>(this));
    return QWidget::qt_metacast(_clname);
}

int Key::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: clicked(); break;
        case 1: sendMessageSlot(); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void Key::clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_SpecialKey[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      22,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SpecialKey[] = {
    "SpecialKey\0\0clicked()\0sendMessageSlot()\0"
};

const QMetaObject SpecialKey::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_SpecialKey,
      qt_meta_data_SpecialKey, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SpecialKey::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SpecialKey::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SpecialKey::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SpecialKey))
        return static_cast<void*>(const_cast< SpecialKey*>(this));
    return QWidget::qt_metacast(_clname);
}

int SpecialKey::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: clicked(); break;
        case 1: sendMessageSlot(); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void SpecialKey::clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_ScreenKeyboard[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      39,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ScreenKeyboard[] = {
    "ScreenKeyboard\0\0checkForActiveWindow()\0"
    "upOrLow()\0"
};

const QMetaObject ScreenKeyboard::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ScreenKeyboard,
      qt_meta_data_ScreenKeyboard, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ScreenKeyboard::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ScreenKeyboard::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ScreenKeyboard::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ScreenKeyboard))
        return static_cast<void*>(const_cast< ScreenKeyboard*>(this));
    return QWidget::qt_metacast(_clname);
}

int ScreenKeyboard::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: checkForActiveWindow(); break;
        case 1: upOrLow(); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
