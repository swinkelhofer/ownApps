#include <QtGui>
#include <windows.h>
#include <ctype.h>
#include <stdio.h>
#include "ScreenKeyboard.h"
#include "Closer.h"

Key::Key(QRect form, QChar label, QWidget *parent):QWidget(parent)
{
	//hoverTimer = new QTimer;
	//hoverTimer->setInterval(0);
	//hovered = false;
	effect = new QGraphicsColorizeEffect;
	effect->setColor(QColor(221,218,244));
	effect->setStrength(0.5);
	setMouseTracking(true);
	setFocusPolicy(Qt::WheelFocus);
	mousePressed = false;
	label0 = label;
	setFixedSize(form.width(),form.height());
	form.translate(-form.x(), -form.y());
	form0 = form;
	connect(this, SIGNAL(clicked()), this, SLOT(sendMessageSlot()));
	//connect(hoverTimer, SIGNAL(timeout()), this, SLOT(updateHover()));
}

void Key::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setRenderHints(QPainter::Antialiasing);
	painter.setPen(QColor(186,186,218));
	QLinearGradient grad;
	grad.setCoordinateMode(QGradient::StretchToDeviceMode);
	grad.setStart(0.5, 0);
	grad.setFinalStop(0.5, 1);
	grad.setColorAt(0, QColor(77,78,97));
	grad.setColorAt(1, QColor(16,20,42));
	painter.setBrush(grad);
	painter.drawRoundedRect(form0, 6, 6, Qt::AbsoluteSize);
	painter.drawText(form0, Qt::AlignCenter, QString(label0));
}

void Key::changeUpLow(bool upper)
{
	if(!upper)
	{
		switch(label0.toAscii())
		{
			case '^':	label0 = QChar('�');
								break;
			case '1':	label0 = QChar('!');
								break;
			case '2':	label0 = QChar('\"');
								break;
			case '3':	label0 = QChar('�');
								break;
			case '4':	label0 = QChar('$');
								break;
			case '5':	label0 = QChar('%');
								break;
			case '6':	label0 = QChar('&');
								break;
			case '7':	label0 = QChar('/');
								break;
			case '8':	label0 = QChar('(');
								break;
			case '9':	label0 = QChar(')');
								break;
			case '0':	label0 = QChar('=');
								break;
			case '�':	label0 = QChar('?');
								break;
			case '�':	label0 = QChar('`');
								break;
			case '+':	label0 = QChar('*');
								break;
			case '#':	label0 = QChar('\'');
								break;
			case '<':	label0 = QChar('>');
								break;
			case ',':	label0 = QChar(';');
								break;
			case '.':	label0 = QChar(':');
								break;
			case '-':	label0 = QChar('_');
								break;
			case '|':	label0 = QChar('\\');
								break;
			case '~':	label0 = QChar('�');
								break;
			case '[':	label0 = QChar(']');
								break;
			case '{':	label0 = QChar('}');
								break;
			case '�':	label0 = QChar('�');
								break;
			case '�':	label0 = QChar('�');
								break;
			case '�':	label0 = QChar('�');
								break;
			default:	label0 = QChar(toupper(label0.toAscii()));
		}
		update();
	}
	else
	{
		switch(label0.toAscii())
		{
			case '�':	label0 = QChar('^');
								break;
			case '!':	label0 = QChar('1');
								break;
			case '\"':	label0 = QChar('2');
								break;
			case '�':	label0 = QChar('3');
								break;
			case '$':	label0 = QChar('4');
								break;
			case '%':	label0 = QChar('5');
								break;
			case '&':	label0 = QChar('6');
								break;
			case '/':	label0 = QChar('7');
								break;
			case '(':	label0 = QChar('8');
								break;
			case ')':	label0 = QChar('9');
								break;
			case '=':	label0 = QChar('0');
								break;
			case '?':	label0 = QChar('�');
								break;
			case '`':	label0 = QChar('�');
								break;
			case '*':	label0 = QChar('+');
								break;
			case '\'':	label0 = QChar('#');
								break;
			case '>':	label0 = QChar('<');
								break;
			case ';':	label0 = QChar(',');
								break;
			case ':':	label0 = QChar('.');
								break;
			case '_':	label0 = QChar('-');
								break;
			case '\\':	label0 = QChar('|');
								break;
			case '�':	label0 = QChar('~');
								break;
			case '}':	label0 = QChar('{');
								break;
			case ']':	label0 = QChar('[');
								break;
			case '�':	label0 = QChar('�');
								break;
			case '�':	label0 = QChar('�');
								break;
			case '�':	label0 = QChar('�');
								break;
			default:	label0 = QChar(tolower(label0.toAscii()));
		}
		update();
	}
}

void Key::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		mousePressed = true;
		effect->setEnabled(true);
		setGraphicsEffect(effect);
	}
	else
	{
		effect->setEnabled(false);
		setGraphicsEffect(effect);
		mousePressed = false;
	}
}


void Key::mouseReleaseEvent(QMouseEvent *event)
{
	if(mousePressed)
	{
		mousePressed = false;
		effect->setEnabled(false);
		setGraphicsEffect(effect);
		emit clicked();
	}
}
/*void Key::mouseMoveEvent(QMouseEvent *event)
{
	if(underMouse() && !hovered)
	{
		hoverTimer->start();
		QGraphicsColorizeEffect *effect2 = new QGraphicsColorizeEffect;
		effect2->setStrength(0.4);
		//effect->setOffset(QPoint(1,-1));
		effect2->setColor(QColor(45,82,142));
		//effect->setBlurRadius(1);
		setGraphicsEffect(effect2);
	}
	else
		event->ignore();
}

void Key::updateHover()
{
	if(!underMouse())
	{
		hovered = false;
		hoverTimer->stop();
		QGraphicsBlurEffect *effect2 = new QGraphicsBlurEffect;
		effect2->setEnabled(false);
		setGraphicsEffect(effect2);
	}
}*/

void Key::sendMessageSlot()
{
	AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(((ScreenKeyboard*)this->parent())->activeWindow, 0), true);
	SetFocus(((ScreenKeyboard*)this->parent())->activeWindow);
	if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xb0,  1);
		
	}
	else if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xa7,  1);
	}
	else if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xb4,  1);
		
	}
	else if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xc4,  1);
		
	}
	else if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xe4,  1);
		
	}
	else if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xd6,  1);
		
	}
	else if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xf6,  1);
		
	}
	else if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xdc,  1);
		
	}
	else if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xfc,  1);
		
	}
	else if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xdf,  1);
		
	}
	
	else if(label0 == '�')
	{
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, 0xb5,  1);
		
	}
	else
	{	
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_CHAR, (int)label0.toAscii(),  1);
	}
	AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(((ScreenKeyboard*)this->parent())->activeWindow, 0), false);
}

SpecialKey::SpecialKey(QRect form, QString label, QWidget *parent):QWidget(parent)
{
	//hoverTimer = new QTimer;
	//hoverTimer->setInterval(0);
	//hovered = false;
	effect = new QGraphicsColorizeEffect;
	effect->setColor(QColor(221,218,244));
	effect->setStrength(0.5);
	setMouseTracking(true);
	setFocusPolicy(Qt::WheelFocus);
	mousePressed = false;
	label0 = label;
	setFixedSize(form.width(),form.height());
	form.translate(-form.x(), -form.y());
	form0 = form;
	connect(this, SIGNAL(clicked()), this, SLOT(sendMessageSlot()));
	//connect(hoverTimer, SIGNAL(timeout()), this, SLOT(updateHover()));
}

void SpecialKey::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setRenderHints(QPainter::Antialiasing);
	painter.setPen(QColor(186,186,218));
	QLinearGradient grad;
	grad.setCoordinateMode(QGradient::StretchToDeviceMode);
	grad.setStart(0.5, 0);
	grad.setFinalStop(0.5, 1);
	grad.setColorAt(0, QColor(77,78,97));
	grad.setColorAt(1, QColor(16,20,42));
	painter.setBrush(grad);
	painter.drawRoundedRect(form0, 6, 6, Qt::AbsoluteSize);
	painter.setBrush(Qt::gray);
	if(label0 == "Del")
	{
		painter.drawLine(32,15, 55,15);
		painter.drawLine(32,13, 32,17);
		painter.drawLine(32,13, 28,15);
		painter.drawLine(32,17, 28,15);
	}
	else if(label0 == "Tab")
	{
		painter.drawLine(23, 18, 53, 18);
		painter.drawLine(53, 16, 53, 20);
		painter.drawLine(53, 20, 57, 18);
		painter.drawLine(53, 16, 57, 18);
		
		
		painter.drawLine(27, 12, 57, 12);
		painter.drawLine(27, 14, 27, 10);
		painter.drawLine(27, 14, 23, 12);
		painter.drawLine(27, 10, 23, 12);
		
		painter.drawLine(23, 10, 23, 14);
		painter.drawLine(57, 16, 57, 20);
	}
	else if(label0 == "Enter")
	{
		painter.drawLine(60, 20, 60, 40);
		painter.drawLine(60, 40, 40, 40);
		painter.drawLine(40, 38, 40, 42);
		painter.drawLine(40, 42, 36, 40);
		painter.drawLine(40, 38, 36, 40);
	}
	else if(label0 == "CapsLock")
	{
		painter.drawLine(48, 22, 52, 22);
		painter.drawLine(52, 22, 52, 17);
		painter.drawLine(48, 22, 48, 17);
		
		painter.drawLine(48, 17, 46, 17);
		painter.drawLine(52, 17, 54, 17);
		
		painter.drawLine(54, 17, 50, 12);
		painter.drawLine(46, 17, 50, 12);
	}
	else if(label0 == "UpArrow")
	{
		//painter.drawLine(17, 19, 23, 19);
		painter.drawLine(17, 17, 20, 11);
		painter.drawLine(23, 17, 20, 11);
	}
	else if(label0 == "DownArrow")
	{
		//painter.drawLine(17, 11, 23, 11);
		painter.drawLine(17, 13, 20, 19);
		painter.drawLine(23, 13, 20, 19);
	}
	else if(label0 == "LeftArrow")
	{
		//painter.drawLine(25, 12, 25, 18);
		painter.drawLine(23, 12, 17, 15);
		painter.drawLine(23, 18, 17, 15);
	}
	else if(label0 == "RightArrow")
	{
		//painter.drawLine(15, 12, 15, 18);
		painter.drawLine(17, 12, 23, 15);
		painter.drawLine(17, 18, 23, 15);
	}
	else
		painter.drawText(form0, Qt::AlignCenter, QString(label0));
	if(label0 == "Enter")
	{
		painter.setBrush(Qt::white);
		painter.drawRoundedRect(-20, 30, 40, 30, 30,30, Qt::RelativeSize);
	}
	
}

void SpecialKey::changeUpLow(bool upper)
{
	return;
}

void SpecialKey::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		effect->setEnabled(true);
		setGraphicsEffect(effect);
		mousePressed = true;
	}
	else
	{
		effect->setEnabled(false);
		setGraphicsEffect(effect);
		mousePressed = false;
	}
}

void SpecialKey::mouseReleaseEvent(QMouseEvent *event)
{
	if(mousePressed)
	{
		effect->setEnabled(false);
		setGraphicsEffect(effect);
		mousePressed = false;
		emit clicked();
	}
}

/*void SpecialKey::mouseMoveEvent(QMouseEvent *event)
{
	if(underMouse() && !hovered)
	{
		hoverTimer->start();
		QGraphicsColorizeEffect *effect2 = new QGraphicsColorizeEffect;
		effect2->setStrength(0.4);
		//effect->setOffset(QPoint(1,-1));
		effect2->setColor(QColor(45,82,142));
		//effect->setBlurRadius(1);
		setGraphicsEffect(effect2);
	}
	else
		event->ignore();
}

void SpecialKey::updateHover()
{
	if(!underMouse())
	{
		hovered = false;
		hoverTimer->stop();
		QGraphicsBlurEffect *effect2 = new QGraphicsBlurEffect;
		effect2->setEnabled(false);
		setGraphicsEffect(effect2);
	}
}*/

void SpecialKey::sendMessageSlot()
{
	AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(((ScreenKeyboard*)this->parent())->activeWindow, 0), true);
	SetFocus(((ScreenKeyboard*)this->parent())->activeWindow);
	if(label0 == QString("Enter"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_RETURN, 1);
	else if(label0 == QString("Esc"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_ESCAPE, 1);
	else if(label0 == QString("F1"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F1, 1);
	else if(label0 == QString("F2"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F2, 1);
	else if(label0 == QString("F3"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F3, 1);
	else if(label0 == QString("F4"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F4, 1);
	else if(label0 == QString("F5"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F5, 1);
	else if(label0 == QString("F6"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F6, 1);
	else if(label0 == QString("F7"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F7, 1);
	else if(label0 == QString("F8"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F8, 1);
	else if(label0 == QString("F9"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F9, 1);
	else if(label0 == QString("F10"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F10, 1);
	else if(label0 == QString("F11"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F11, 1);
	else if(label0 == QString("F12"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_F12, 1);
	else if(label0 == QString("Strg"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_CONTROL, 1);
	else if(label0 == QString("Alt"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_MENU, 1);
	else if(label0 == QString("Druck"))
		keybd_event(VK_SNAPSHOT, 0, KEYEVENTF_EXTENDEDKEY | 0, 0);
		//PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_SYSKEYDOWN, VK_SNAPSHOT, 1);
	else if(label0 == QString("Pause"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_SYSKEYDOWN, VK_PAUSE, 1);
	else if(label0 == QString("Einfg"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_INSERT, 1);
	else if(label0 == QString("Entf"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_DELETE, 1);
	else if(label0 == QString("Del"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_BACK, 1);
	else if(label0 == QString("Tab"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_TAB, 1);
	else if(label0 == QString("UpArrow"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_UP, 1);
	else if(label0 == QString("DownArrow"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_DOWN, 1);
	else if(label0 == QString("LeftArrow"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_LEFT, 1);
	else if(label0 == QString("RightArrow"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_KEYDOWN, VK_RIGHT, 1);
	/*else if(label0 == QString("Win"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_SYSKEYDOWN, VK_LWIN, 1);
	else if(label0 == QString("Menu"))
		PostMessage(((ScreenKeyboard*)this->parent())->activeWindow, WM_SYSKEYDOWN, VK_APPS, 1);*/
	AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(((ScreenKeyboard*)this->parent())->activeWindow, 0), false);
}

ScreenKeyboard::ScreenKeyboard(QWidget *parent):QWidget(parent)
{
	
	//SetWindowLong(NULL, GWL_EXSTYLE, WS_EX_NOACTIVATE);
	setWindowTitle("ScreenKeyboard v1.0");
	up = false;
	mousePressed = false;
	AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(GetForegroundWindow(), 0), true);
	activeWindow = GetFocus();
	AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(GetForegroundWindow(), 0), false);
	checkActive = new QTimer;
	checkActive->start(1000);
	connect(checkActive, SIGNAL(timeout()), this, SLOT(checkForActiveWindow()));
	setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
	setAttribute(Qt::WA_TranslucentBackground, true);
	setAttribute(Qt::WA_ShowWithoutActivating, true);
	setFixedSize(680,300);
	
	Closer *closeButton = new Closer(this);
	closeButton->move(size().width() - 23, 8);
	
	setFocusPolicy(Qt::WheelFocus);
	SpecialKey *F1 = new SpecialKey(QRect(0,0,40,25), QString("F1"), this);		F1->move(20,50);
	SpecialKey *F2 = new SpecialKey(QRect(0,0,40,25), QString("F2"), this);		F2->move(60,50);
	SpecialKey *F3 = new SpecialKey(QRect(0,0,40,25), QString("F3"), this);		F3->move(100,50);
	SpecialKey *F4 = new SpecialKey(QRect(0,0,40,25), QString("F4"), this);		F4->move(140,50);
	SpecialKey *F5 = new SpecialKey(QRect(0,0,40,25), QString("F5"), this);		F5->move(180,50);
	SpecialKey *F6 = new SpecialKey(QRect(0,0,40,25), QString("F6"), this);		F6->move(220,50);
	SpecialKey *F7 = new SpecialKey(QRect(0,0,40,25), QString("F7"), this);		F7->move(260,50);
	SpecialKey *F8 = new SpecialKey(QRect(0,0,40,25), QString("F8"), this);		F8->move(300,50);
	SpecialKey *F9 = new SpecialKey(QRect(0,0,40,25), QString("F9"), this);		F9->move(340,50);
	SpecialKey *F10 = new SpecialKey(QRect(0,0,40,25), QString("F10"), this);		F10->move(380,50);
	SpecialKey *F11 = new SpecialKey(QRect(0,0,40,25), QString("F11"), this);		F11->move(420,50);
	SpecialKey *F12 = new SpecialKey(QRect(0,0,40,25), QString("F12"), this);		F12->move(460,50);
	SpecialKey *Druck = new SpecialKey(QRect(0,0,40,25), QString("Druck"), this);	Druck->move(500,50);
	SpecialKey *Pause = new SpecialKey(QRect(0,0,40,25), QString("Pause"), this);	Pause->move(540,50);
	SpecialKey *Einfg = new SpecialKey(QRect(0,0,40,25), QString("Einfg"), this);	Einfg->move(580,50);
	SpecialKey *Entf = new SpecialKey(QRect(0,0,40,25), QString("Entf"), this);	Entf->move(620,50);
	
	SpecialKey *Esc = new SpecialKey(QRect(0,0,40,30), QString("Esc"), this);		Esc->move(20, 75);
	Key *Dach = new Key(QRect(0,0,40,30), QChar('^'), this);	Dach->move(60, 75);
	Key *eins = new Key(QRect(0,0,40,30), QChar('1'), this);	eins->move(100, 75);
	Key *zwei = new Key(QRect(0,0,40,30), QChar('2'), this);	zwei->move(140, 75);
	Key *drei = new Key(QRect(0,0,40,30), QChar('3'), this);	drei->move(180, 75);
	Key *vier = new Key(QRect(0,0,40,30), QChar('4'), this);	vier->move(220, 75);
	Key *fuenf = new Key(QRect(0,0,40,30), QChar('5'), this);	fuenf->move(260, 75);
	Key *sechs = new Key(QRect(0,0,40,30), QChar('6'), this);	sechs->move(300, 75);
	Key *sieben = new Key(QRect(0,0,40,30), QChar('7'), this);	sieben->move(340, 75);
	Key *acht = new Key(QRect(0,0,40,30), QChar('8'), this);	acht->move(380, 75);
	Key *neun = new Key(QRect(0,0,40,30), QChar('9'), this);	neun->move(420, 75);
	Key *null = new Key(QRect(0,0,40,30), QChar('0'), this);	null->move(460, 75);
	Key *sz = new Key(QRect(0,0,40,30), QChar('�'), this);		sz->move(500, 75);
	Key *Akzent = new Key(QRect(0,0,40,30), QChar('�'), this);	Akzent->move(540, 75);
	SpecialKey *Del = new SpecialKey(QRect(0,0,80,30), QString("Del"), this);		Del->move(580, 75);
	
	SpecialKey *Tab = new SpecialKey(QRect(0,0,80,30), QString("Tab"), this);		Tab->move(20, 105);
	Key *q = new Key(QRect(0,0,40,30), QChar('q'), this);		q->move(100, 105);
	Key *w = new Key(QRect(0,0,40,30), QChar('w'), this);		w->move(140, 105);
	Key *e = new Key(QRect(0,0,40,30), QChar('e'), this);		e->move(180, 105);
	Key *r = new Key(QRect(0,0,40,30), QChar('r'), this);		r->move(220, 105);
	Key *t = new Key(QRect(0,0,40,30), QChar('t'), this);		t->move(260, 105);
	Key *z = new Key(QRect(0,0,40,30), QChar('z'), this);		z->move(300, 105);
	Key *u = new Key(QRect(0,0,40,30), QChar('u'), this);		u->move(340, 105);
	Key *i = new Key(QRect(0,0,40,30), QChar('i'), this);		i->move(380, 105);
	Key *o = new Key(QRect(0,0,40,30), QChar('o'), this);		o->move(420, 105);
	Key *p = new Key(QRect(0,0,40,30), QChar('p'), this);		p->move(460, 105);
	Key *ue = new Key(QRect(0,0,40,30), QChar('�'), this);		ue->move(500, 105);
	Key *Plus = new Key(QRect(0,0,40,30), QChar('+'), this);	Plus->move(540, 105);
	SpecialKey *Enter = new SpecialKey(QRect(0,0,80,60), QString("Enter"), this);	Enter->move(580, 105);
	
	SpecialKey *CapsLock = new SpecialKey(QRect(0,0,100,30), QString("CapsLock"), this);	CapsLock->move(20, 135);
	Key *a = new Key(QRect(0,0,40,30), QChar('a'), this);			a->move(120, 135);
	Key *s = new Key(QRect(0,0,40,30), QChar('s'), this);			s->move(160, 135);
	Key *d = new Key(QRect(0,0,40,30), QChar('d'), this);			d->move(200, 135);
	Key *f = new Key(QRect(0,0,40,30), QChar('f'), this);			f->move(240, 135);
	Key *g = new Key(QRect(0,0,40,30), QChar('g'), this);			g->move(280, 135);
	Key *h = new Key(QRect(0,0,40,30), QChar('h'), this);			h->move(320, 135);
	Key *j = new Key(QRect(0,0,40,30), QChar('j'), this);			j->move(360, 135);
	Key *k = new Key(QRect(0,0,40,30), QChar('k'), this);			k->move(400, 135);
	Key *l = new Key(QRect(0,0,40,30), QChar('l'), this);			l->move(440, 135);
	Key *oe = new Key(QRect(0,0,40,30), QChar('�'), this);			oe->move(480, 135);
	Key *ae = new Key(QRect(0,0,40,30), QChar('�'), this);			ae->move(520, 135);
	Key *Raute = new Key(QRect(0,0,40,30), QChar('#'), this);		Raute->move(560, 135);
	
	//SpecialKey *CapsLeft = new SpecialKey(QRect(0,0,80,30), QString("Caps"), this);		CapsLeft->move(0,115);
	Key *Eckklammer = new Key(QRect(0,0,40,30), QChar('['), this);		Eckklammer->move(20, 165);
	Key *Schweifklammer = new Key(QRect(0,0,40,30), QChar('{'), this);	Schweifklammer->move(60, 165);
	Key *Spitzklammer = new Key(QRect(0,0,40,30), QChar('<'), this);	Spitzklammer->move(100, 165);
	Key *y = new Key(QRect(0,0,40,30), QChar('y'), this);				y->move(140, 165);
	Key *x = new Key(QRect(0,0,40,30), QChar('x'), this);				x->move(180, 165);
	Key *c = new Key(QRect(0,0,40,30), QChar('c'), this);				c->move(220, 165);
	Key *v = new Key(QRect(0,0,40,30), QChar('v'), this);				v->move(260, 165);
	Key *b = new Key(QRect(0,0,40,30), QChar('b'), this);				b->move(300, 165);
	Key *n = new Key(QRect(0,0,40,30), QChar('n'), this);				n->move(340, 165);
	Key *m = new Key(QRect(0,0,40,30), QChar('m'), this);				m->move(380, 165);
	Key *Komma = new Key(QRect(0,0,40,30), QChar(','), this);			Komma->move(420, 165);
	Key *Punkt = new Key(QRect(0,0,40,30), QChar('.'), this);			Punkt->move(460, 165);
	Key *Minus = new Key(QRect(0,0,40,30), QChar('-'), this);			Minus->move(500, 165);
	Key *ateur = new Key(QRect(0,0,40,30), QChar('@'), this);			ateur->move(540, 165);
	Key *strich = new Key(QRect(0,0,40,30), QChar('|'), this);			strich->move(580, 165);
	Key *Tilde = new Key(QRect(0,0,40,30), QChar('~'), this);			Tilde->move(620, 165);
	//SpecialKey *CapsRight = new SpecialKey(QRect(0,0,120,30), QString("Caps"), this);		CapsRight->move(520,115);
	
	//SpecialKey *StrgLeft = new SpecialKey(QRect(0,0,60,30), QString(""), this);	StrgLeft->move(20,165);
	//SpecialKey *Win = new SpecialKey(QRect(0,0,60,30), QString(""), this);		Win->move(80,165);
	//SpecialKey *Alt = new SpecialKey(QRect(0,0,60,30), QString(""), this);		Alt->move(140,165);
	Key *Space = new Key(QRect(0,0,280,30), QChar(' '), this);		Space->move(200,195);
	//SpecialKey *AltGr = new SpecialKey(QRect(0,0,60,30), QString(""), this);		AltGr->move(480,165);
	//SpecialKey *ScrollMenu = new SpecialKey(QRect(0,0,60,30), QString(""), this);	ScrollMenu->move(540,165);
	//SpecialKey *StrgRight = new SpecialKey(QRect(0,0,60,30), QString(""), this);	StrgRight->move(600,165);
	
	SpecialKey *upArrow = new SpecialKey(QRect(0,0,40,30), QString("UpArrow"), this);	upArrow->move(560, 210);
	SpecialKey *downArrow = new SpecialKey(QRect(0,0,40,30), QString("DownArrow"), this);	downArrow->move(560, 240);
	SpecialKey *leftArrow = new SpecialKey(QRect(0,0,40,30), QString("LeftArrow"), this);	leftArrow->move(520, 240);
	SpecialKey *rightArrow = new SpecialKey(QRect(0,0,40,30), QString("RightArrow"), this);	rightArrow->move(600, 240);

	connect(CapsLock, SIGNAL(clicked()), this, SLOT(upOrLow()));
	connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
	
}

void ScreenKeyboard::upOrLow()
{

	if(up)
	{
		for(int i = 0; i < findChildren<Key *>().count(); ++i)
			findChildren<Key *>().at(i)->changeUpLow(true);
		up = false;
	}
	else
	{
		for(int i = 0; i < findChildren<Key *>().count(); ++i)
			findChildren<Key *>().at(i)->changeUpLow(false);
		up = true;
	}
}

void ScreenKeyboard::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.setBrush(QColor(12,21,51,210));
	painter.setPen(Qt::NoPen);
	painter.drawRect(0,0,size().width(), size().height());
	QPixmap pix(":/img/Leiste.png");
	painter.drawPixmap(QRect(0,0,680,30), pix.scaled(QSize(680, 30), Qt::IgnoreAspectRatio,Qt::SmoothTransformation));
	QPixmap pix2(":/img/NSA.png");
	painter.drawPixmap(QRect(70, size().height() - 93, 80, 80), pix2.scaled(QSize(80,80),  Qt::IgnoreAspectRatio,Qt::SmoothTransformation));
	painter.drawEllipse(QRect(70, size().height() - 93, 80, 80));
	painter.setPen(QColor(22,31,59));
	painter.setFont(QFont("Arial", 9, QFont::Bold));
	painter.drawText(8,8,size().width() - 8, 22, Qt::AlignLeft, "ScreenKeyboard");
}

void ScreenKeyboard::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		currentGlobalPos = event->globalPos();
		mousePressed = true;
	}
	else
	{
		mousePressed = false;
	}
}

void ScreenKeyboard::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mousePressed)
	{
		mousePressed = false;
		move(pos() - (currentGlobalPos - event->globalPos()));
	}
}

void ScreenKeyboard::mouseMoveEvent(QMouseEvent *event)
{
	
	if(mousePressed)
	{
		move(pos() - (currentGlobalPos - event->globalPos()));
		currentGlobalPos = event->globalPos();
	}
}


void ScreenKeyboard::checkForActiveWindow()
{
	if(this->isActiveWindow() || this->hasFocus())
	{
		ownWindow = GetFocus();
		if(activeWindow == ownWindow)
		{
		}
		else
		{
			AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(activeWindow, 0), true);		
			SetFocus(activeWindow);
			AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(activeWindow, 0), false);
		}
	}
	else
	{
		AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(GetForegroundWindow(), 0), true);
		activeWindow = GetFocus();
		AttachThreadInput(GetCurrentThreadId(), GetWindowThreadProcessId(GetForegroundWindow(), 0), false);
	}
}
