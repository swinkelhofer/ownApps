#include <QMessageBox>
#include "main.h"
#include <windows.h>
#include "ScreenKeyboard.h"

MSGApplication::MSGApplication(int argc, char *argv[]):QApplication(argc, argv)
{
	mainWidget = NULL;
}

bool MSGApplication::winEventFilter(MSG *msg, long *result)
{
	if(msg->message == WM_MOUSEACTIVATE)
	{
		*result = MA_NOACTIVATE;
		return true;
	}
	if(msg->message == WM_ACTIVATE || msg->message == WM_ACTIVATEAPP || msg->message == WM_CHILDACTIVATE || msg->message == WM_NCACTIVATE || msg->message == WM_SETFOCUS)
	{
		*result = MA_NOACTIVATE;
		return true;
	}
	return QApplication::winEventFilter(msg, result);
}

void MSGApplication::setMainWidget(QWidget *mainWindow)
{
	mainWidget = mainWindow;
}

int main(int argc, char *argv[])
{
	MSGApplication app(argc, argv);
	ScreenKeyboard *window = new ScreenKeyboard;
	app.setMainWidget(window);
	window->show();
	
	SetWindowLong(NULL, GWL_EXSTYLE, WS_EX_NOACTIVATE);
	return app.exec();
	
}