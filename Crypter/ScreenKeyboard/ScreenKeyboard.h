#include <QWidget>
#include <windows.h>
#include <QCoreApplication>
class QRect;
class QChar;
class QTimer;
class QPoint;
class QGraphicsColorizeEffect;
class Key:public QWidget
{
	Q_OBJECT
public:
	Key(QRect form, QChar label, QWidget *parent);
	void changeUpLow(bool up);
protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	//void mouseMoveEvent(QMouseEvent *event);
private:
	QRect form0;
	QChar label0;
	bool mousePressed/*, hovered*/;
	QGraphicsColorizeEffect *effect;
	QTimer *hoverTimer;
private slots:
	void sendMessageSlot();
	//void updateHover();
signals:
	void clicked();
};

class SpecialKey:public QWidget
{
	Q_OBJECT
public:
	SpecialKey(QRect form, QString label, QWidget *parent);
	void changeUpLow(bool up);
protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	//void mouseMoveEvent(QMouseEvent *event);
private:
	QRect form0;
	QString label0;
	bool mousePressed/*, hovered*/;
	QGraphicsColorizeEffect *effect;
	//QTimer *hoverTimer;
private slots:
	void sendMessageSlot();
	//void updateHover();
signals:
	void clicked();
};


class ScreenKeyboard:public QWidget
{
	Q_OBJECT
public:
	ScreenKeyboard(QWidget *parent = 0);
	HWND activeWindow, ownWindow;
private:
	QTimer *checkActive;
	bool up, mousePressed;
	QPoint currentGlobalPos;
protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void paintEvent(QPaintEvent *event);
private slots:
	void checkForActiveWindow();
	void upOrLow();
};
