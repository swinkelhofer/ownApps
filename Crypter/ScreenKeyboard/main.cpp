#include <QApplication>
#include "ScreenKeyboard.h"
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	ScreenKeyboard *window = new ScreenKeyboard;
	//app.installEventFilter(new FocusEventFilter);
	//app.parent()->installEventFilter(new FocusEventFilter);
	//qApp->installEventFilter(new FocusEventFilter(argc, argv));
	//window->installEventFilter(new FocusEventFilter);
	//window->installEventFilter(new FocusEventFilter);
	//QObject::connect(&app, SIGNAL(focusChanged(QWidget *, QWidget *)), window, SLOT(setFocus(QWidget *, QWidget *)));
	window->move(QApplication::desktop()->availableGeometry().width()/2 - window->size().width()/2, QApplication::desktop()->availableGeometry().height() - window->size().height());
	window->show();
	
	//SetWindowLong((HWND)GetCurrentThread(), GWL_EXSTYLE, WS_EX_NOACTIVATE);
	return app.exec();
}