#include "Neuron.h"
#include <math.h>

#define ActivationFunction(x) (tanh(x))
#define dActivationFunction(x) (1 - tanh(x)*tanh(x))

Connection::Connection(Neuron *a, Neuron *b)
{
	in = a;
	in->Next = this;
	out = b;
	out->Previous = this;
	weight = 0.0;
}

Neuron::Neuron()
{
	Output = 0.0;
	net = 0.0;
	NeuronNumber = 0;
}

Neuron::Neuron(int NeuronNo)
{
	Output = 0.0;
	net = 0.0;
	NeuronNumber = NeuronNo;
}

Neuron::getOutput()
{
	net = 0.0;
	for(int i = Previous->size(); i > 0 ; --i)
	{
		net += Previous[i]->Weight * Previous[i]->in.Output;
	}
	Output = ActivationFunction(net);	
}

HiddenNeuron::HiddenNeuron():Neuron()
{
	Output = 0.0;
	net = 0.0;
	NeuronNumber = 0;
}

HiddenNeuron::HiddenNeuron(int NeuronNo):Neuron()
{
	Output = 0.0;
	net = 0.0;
	NeuronNumber = NeuronNo;
}

