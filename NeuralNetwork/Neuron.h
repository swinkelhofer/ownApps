#include <vector>
#include <stdio.h>

class Connection
{
public:
	Connection(Neuron *a, Neuron *b);
	Neuron *in, *out;
	double weight;
};

class Neuron
{
public:
	Neuron();
	Neuron(int NeuronNo);
	virtual double GetOutput();
	virtual void Backpropagate();

private:
	std::vector<Connection*> Previous;
	std::vector<Connection*> Next;
	double Output;
	double net;
	int NeuronNumber;
};

class HiddenNeuron:public Neuron
{
public:
	HiddenNeuron();
	HiddenNeuron(int NeuronNo);
	void Backpropagate();
};

class InputNeuron:public Neuron
{
public:
	InputNeuron();
	InputNeuron(int NeuronNo);
	double GetOutput();
	double SetInput(double value);
	void Backpropagate();
};

class OutputNeuron:public Neuron
{
public:
	OutputNeuron();
	OutputNeuron(int NeuronNo);
	void Backpropagate();
}