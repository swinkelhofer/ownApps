#ifndef AES
#define AES
class CryptString;

void KeyExpansion();
void Var_S_Box_Creation();
void Mixing_Table_Creation();

void KeyDependantMixing();
void Var_Substitution();
void Static_Substitution();
void AddRoundKey();
void Hurricane();
void BlockEncryption();
void BlockDecryption();


class SASCHAClass
{
public:
	enum BlockMode {CBC=0,ECB=1};
	//enum KeyLength {Low=256, Middle=384, High=512}
	//------CryptString Encrypt(CryptString plaintext, CryptString key, int keylength=512);
	//------CryptString Decrypt(CryptString chiffretext, CryptString key, int keylength=512);
	void EncryptFile(const char* infile, const char* outfile, CryptString key, int keylength=512, BlockMode = SASCHAClass::CBC);
	void DecryptFile(const char* infile, const char* outfile, CryptString key, int keylength=512, BlockMode = SASCHAClass::CBC);
	
private:
	void SelectNr();
};
#endif
