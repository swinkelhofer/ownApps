#include <stdio.h>
#include <math.h>

int main()
{	
	double k = 0;
	for(int i = 1; i <= 1000000; ++i)
	{
		if(i & 0x1)
			k -= pow(((double)i-1) / (double)i, (double)i);
		else
			k += pow(((double)i-1) / (double)i, (double)i);
		printf("%d:\t%8lf\n", i, k);
	}
	return 0;
}