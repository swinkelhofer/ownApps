#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	int ntimes;
	scanf("%d", &ntimes);
	int height[ntimes];
	long long int moves[ntimes];
	for (int i = 0; i < ntimes; ++i)
	{
		scanf("%d %lld", &height[i], &moves[i]);
	}
	int output[3][1000];
	
	int index0 = 0, index1 = 0, index2 = 0;
	for(int i = 0; i < ntimes; ++i)
	{
		for(int j = 0; j < 1000; ++j)
		{
			output[0][j] = 0;
			output[1][j] = 0;
			output[2][j] = 0;
		}
		index0 = index1 = index2 = 0;
		for(int j = height[i]; j > 0; --j)
		{
			int temp = (((moves[i] + 1) - (1 << (j - 1))) % ((3 << j)) >> j);
			printf("temp: %d\n", temp);
			if(temp < 0)
			{
				output[0][index0] = j;
				++index0;
			}
			if(temp == 2)
			{
				output[0][index0] = j;
				++index0;
			}
			else if((j & 0x1) ^ (height[i] & 0x1))
			{
				if(temp == 0)
				{
					output[1][index1] = j;
					++index1;
				}
				else if(temp == 1)
				{
					output[2][index2] = j;
					++index2;
				}
			}
			else
			{
				if(temp == 1)
				{
					output[1][index1] = j;
					++index1;
				}
				else if(temp == 0)
				{
					output[2][index2] = j;
					++index2;
				}
			}
		}
		for(int n = 0; n < 3; ++n)
		{
			for(int j = 0; j < height[n]; ++j)
			{
				printf("%d|", output[n][j]);
			}
			printf("\n");
		}
	}
	return 0;
}