#include <stdlib.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
	int times, temp;
	register int runners, counter;
	int array[40000];
	char text[1000];
	int i, j, k;
	scanf("%d", &times);
	if(times > 99)
		times = 99;
	int output[times];
	for(i = 0; i < times; ++i)
	{
		scanf("%d", &temp);
		runners = temp;
		if(runners <= 1)
			runners = 2;
		else if(runners >= 40000)
			runners = 40000;
		counter = 0;
		for(j = 0; j < runners; ++j)
		{
			scanf("%d", &array[j]);
			for(k = 0; k < j; ++k)
				if(array[k] > array[j])
					++counter;
		}
		output[i] = counter;
		for(j = runners-1; j >= 0; --j)
			scanf("%d", &temp);
	}
	for(i = 0; i < times; ++i)
		printf("at least %d overtaking(s)\n", output[i]);
	return 0;
}
