#include <QWidget>
class QLineEdit;
class QTextEdit;
class QPushButton;
class ComplexIterator: public QWidget
{
	Q_OBJECT
public:
	ComplexIterator(QWidget *parent = 0);
private:
	QLineEdit *z_real, *z_imag, *c_real, *c_imag, *iterations;
	QPushButton *start;
	QTextEdit *output;
private slots:
	void iterate();
};