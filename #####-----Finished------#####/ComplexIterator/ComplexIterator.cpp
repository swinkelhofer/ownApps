#include <QtGui>
#include "ComplexIterator.h"

ComplexIterator::ComplexIterator(QWidget *parent):QWidget(parent)
{
	move(QApplication::desktop()->screenGeometry().width()/2-size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2 - 10);
	
	z_real = new QLineEdit("0");
	z_real->setValidator(new QDoubleValidator());
	z_imag = new QLineEdit("0");
	z_imag->setValidator(new QDoubleValidator());
	c_real = new QLineEdit("0");
	c_real->setValidator(new QDoubleValidator());
	c_imag = new QLineEdit("0");
	c_imag->setValidator(new QDoubleValidator());
	iterations = new QLineEdit("100");
	iterations->setValidator(new QIntValidator());
	start = new QPushButton("Starte Iteration");
	output = new QTextEdit;
	output->setReadOnly(true);
	QHBoxLayout *top1 = new QHBoxLayout;
	top1->addWidget(new QLabel("<h3>z = </h3>"));
	top1->addWidget(z_real);
	top1->addWidget(new QLabel("<h3> + </h3>"));
	top1->addWidget(z_imag);
	top1->addWidget(new QLabel("<h3> i</h3>"));
	
	QHBoxLayout *top2 = new QHBoxLayout;
	top2->addWidget(new QLabel("<h3>c = </h3>"));
	top2->addWidget(c_real);
	top2->addWidget(new QLabel("<h3> + </h3>"));
	top2->addWidget(c_imag);
	top2->addWidget(new QLabel("<h3> i</h3>"));
	
	QHBoxLayout *top3 = new QHBoxLayout;
	top3->addWidget(new QLabel("<h4>Anzahl Iterationen: </h4>"));
	top3->addWidget(iterations);
	top3->addWidget(start);
	
	QVBoxLayout *main = new QVBoxLayout;
	main->addLayout(top1);
	main->addLayout(top2);
	main->addLayout(top3);
	main->addWidget(output);
	connect(start, SIGNAL(clicked()), this, SLOT(iterate()));
	setLayout(main);
}

void ComplexIterator::iterate()
{
	if(iterations->text().isEmpty())
		iterations->setText("100");
	if(z_real->text().isEmpty())
		z_real->setText("0");
	if(z_imag->text().isEmpty())
		z_imag->setText("0");
	if(c_real->text().isEmpty())
		c_real->setText("0");
	if(c_imag->text().isEmpty())
		c_imag->setText("0");
	double a1, a2, b1, b2, temp;
	int it = iterations->text().toInt();
	a1 = z_real->text().toDouble();
	a2 = z_imag->text().toDouble();
	b1 = c_real->text().toDouble();
	b2 = c_imag->text().toDouble();
	output->setHtml(QString("<h4>h<sub>0</sub> = ") + z_real->text() + QString(" + ") + z_imag->text() + QString(" i</h4><br/>"));
	for(int i = 1; i < it; ++i)
	{
		temp = a1*a1 - a2 * a2 + b1;
		a2 = 2 * a1 * a2 + b2;
		a1 = temp;
		output->append(QString("<h4>h<sub>") + QString::number(i) + QString("</sub> = ") + QString::number(a1, 'f', 3) + QString(" + ") + QString::number(a2, 'f', 3) + QString(" i</h4><br/>"));
		output->scroll(0, 30);
	}
}	
	
	