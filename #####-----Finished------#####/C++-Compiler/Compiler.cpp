#include <QtGui>
#include "Compiler.h"
#include <process.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

Compiler::Compiler(QWidget *parent):QMainWindow(parent)
{
	shown = new QDialog;
	shown->setModal(true);
	ed = new QLineEdit("C:\\MinGW\\bin\\g++ ");
	but = new QPushButton("OK");
	filecomp = new QPushButton("Durchsuchen");
	QHBoxLayout *lay =new QHBoxLayout;
	lay->addWidget(ed);
	lay->addWidget(filecomp);
	lay->addWidget(but);
	shown->setLayout(lay);
	compilerpath = "C:\\MinGW\\bin\\g++ ";
	setWindowTitle("C++-Compiler");
	QWidget *widg = new QWidget;
	list = new QListView();
	appName = new QLineEdit;
	appName->setDisabled(true);
	compile = new QPushButton("Projekt erstellen");
	remove = new QPushButton("Quelldatei entfernen");
	load = new QPushButton("Quelldatei hinzufügen");
	save = new QPushButton("Ziel suchen");
	label1 = new QLabel("Quelldateien");
	label2 = new QLabel("Name der Anwendung");
	model = new QStringListModel(this);
	QVBoxLayout *left = new QVBoxLayout;
	QHBoxLayout *down = new QHBoxLayout;
	QVBoxLayout *right = new QVBoxLayout;
	left->addWidget(label1);
	left->addWidget(list);
	left->addWidget(label2);
	down->addWidget(appName);
	down->addWidget(save);
	left->addLayout(down);
	right->addWidget(load);
	right->addWidget(remove);
	right->addStretch();
	right->addWidget(compile);
	QHBoxLayout *main = new QHBoxLayout;
	main->addLayout(left);
	main->addLayout(right);
	connect(load, SIGNAL(clicked()), this, SLOT(open()));
	connect(compile, SIGNAL(clicked()), this, SLOT(run()));
	connect(save, SIGNAL(clicked()), this, SLOT(saven()));
	connect(remove, SIGNAL(clicked()), this, SLOT(move()));
	connect(filecomp, SIGNAL(clicked()), this, SLOT(setfilecomp()));
	widg->setLayout(main);
	setCentralWidget(widg);
	QMenu *bar = new QMenu;
	bar = menuBar()->addMenu("Optionen");
	QAction *ac = new QAction("Compiler festlegen", this);
	connect(ac, SIGNAL(triggered()), this, SLOT(Comp()));
	bar->addAction(ac);
}

void Compiler::setfilecomp()
{
	QString str = QFileDialog::getOpenFileName(this, "Compiler", "C:\\MinGW\\bin", "Anwendung (*.exe)");
	if(!str.isEmpty())
	{
		ed->setText(str);
	}
	else
		ed->setText("C:\\MinGW\\bin\\g++ ");
}

void Compiler::Comp()
{
	
	shown->show();
	connect(but, SIGNAL(clicked()), this, SLOT(setComp()));
}

void Compiler::setComp()
{
	if(!ed->text().isEmpty())
	{
		compilerpath = ed->text();
	}
	else
		compilerpath = "C:\\MinGW\\bin\\g++ ";
	shown->close();
}

void Compiler::open()
{
	//arglist = QString("");
	fileName += QFileDialog::getOpenFileNames(this, "Quelldateien", "C:\\", "C++-Quelldateien (*.cpp *.h)");
	model->setStringList(fileName);
	list->setModel(model);
	/*for (int i = 0; i < fileName.size(); i++)
	{
		arglist = arglist + fileName[i] + QString(" ");
	}*/
}

void Compiler::run()
{
	for (int i = 0; i < model->stringList().size(); i++)
		{
			arglist = arglist + model->stringList()[i] + QString(" ");
		}
	if(arglist.isEmpty())
	{
		QMessageBox::warning(this, "C++-Compiler", "Bitte Quelldateien laden!", 0, 0, 1);
	}
	if(appName->text().isEmpty())
	{
		QMessageBox::warning(this, "C++-Compiler", "Bitte .exe-Datei als Ziel eingeben!", 0, 0, 1);
	}
	if(!appName->text().isEmpty() && !model->stringList().isEmpty() && appName->text().endsWith(".exe"))
	{
		QString neu = QString(compilerpath) + arglist + QString(" -o ") + appName->text() + QString(" -O") + QString(" -Wall");
		//system(neu.toStdString().c_str());


		/*FILE *chkdsk;
		chkdsk = _popen( neu.toStdString().c_str(), "rt" );
		QFile file;
		file.open(chkdsk, QIODevice::ReadWrite);
		QString end = file.readAll();
		QLabel *showen = new QLabel(end);
		showen->show();*/
		QProcess process;
		//QStringList neu = QStringList() << QString("C:\\MinGW\\bin\\g++") << arglist << QString("-o") << appName->text();
		process.setReadChannelMode(QProcess::MergedChannels);
		process.setReadChannel(QProcess::StandardOutput);
		process.start(neu);
		if (process.waitForFinished())
			qDebug() << process.errorString();
		else
			qDebug() << process.readAll();
		QWidget *widg = new QWidget;
		QPushButton *but = new QPushButton("Schließen");
		QLabel *over = new QLabel(QString("<h3 align=center>Output</h3>"));
		QLabel *lab = new QLabel(process.readAll());
		QVBoxLayout *lay =new QVBoxLayout;
		lay->addWidget(over);
		lay->addWidget(lab);
		lay->addWidget(but);
		widg->setLayout(lay);
		widg->setWindowModality(Qt::ApplicationModal);
		widg->setGeometry(this->geometry());
		connect(but, SIGNAL(clicked()), widg, SLOT(close()));
		widg->show();
		process.close();
		arglist.clear();
		fileName.clear();
		savefile.clear();
		//appName->setText("");
		//model->setStringList(fileName);
		//list->setModel(model);
	}
}

void Compiler::saven()
{
	savefile = QFileDialog::getSaveFileName(this, "Zielort", "C:\\", "Anwendung (*.exe)");
	if(!savefile.endsWith(".exe") && !savefile.isEmpty())
	{
		savefile = savefile + QString(".exe");
	}
	appName->setText(savefile);
}

void Compiler::move()
{
	model->removeRows(list->currentIndex().row(), 1);
	fileName.removeAt(fileName.indexOf(list->currentIndex().data().toString()));
}