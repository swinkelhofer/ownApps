#include <QMainWindow>
class QLineEdit;
class QPushButton;
class QListView;
class QLabel;
class QStringList;
class QStringListModel;
class QWidget;
class QDialog;
class Compiler : public QMainWindow
{
	Q_OBJECT
public:
	Compiler(QWidget *parent=0);
private:
	QLineEdit *appName, *ed;
	QPushButton *compile, *remove, *load, *save, *but, *filecomp;
	QListView *list;
	QLabel *label1, *label2;
	QStringList fileName;
	QStringListModel *model;
	QString arglist;
	QString savefile;
	QString compilerpath;
	QDialog *shown;
private slots:
	void open();
	void run();
	void saven();
	void move();
	void setComp();
	void Comp();
	void setfilecomp();
};