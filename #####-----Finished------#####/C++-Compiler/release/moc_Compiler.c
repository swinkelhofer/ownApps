/****************************************************************************
** Meta object code from reading C++ file 'Compiler.h'
**
** Created: Di 2. Nov 19:50:51 2010
**      by: The Qt Meta Object Compiler version 59 (Qt 4.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Compiler.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Compiler.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

static const uint qt_meta_data_Compiler[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      17,    9,    9,    9, 0x08,
      23,    9,    9,    9, 0x08,
      31,    9,    9,    9, 0x08,
      38,    9,    9,    9, 0x08,
      48,    9,    9,    9, 0x08,
      55,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Compiler[] = {
    "Compiler\0\0open()\0run()\0saven()\0move()\0setComp()\0Comp()\0"
    "setfilecomp()\0"
};

const QMetaObject Compiler::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Compiler,
      qt_meta_data_Compiler, 0 }
};

const QMetaObject *Compiler::metaObject() const
{
    return &staticMetaObject;
}

void *Compiler::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Compiler))
	return static_cast<void*>(const_cast<Compiler*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Compiler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: open(); break;
        case 1: run(); break;
        case 2: saven(); break;
        case 3: move(); break;
        case 4: setComp(); break;
        case 5: Comp(); break;
        case 6: setfilecomp(); break;
        }
        _id -= 7;
    }
    return _id;
}
