#include <QtGui>
#include "Sierpinski.h"
#include <math.h>
#include <sys/time.h>


Chaos::Chaos(QWidget *parent):QWidget(parent)
{
	srand(time(0));
	iterations0 = 100000;
	pix = QPixmap(600,600);
	painted = true;
	setFixedSize(600,600);
	pointL = QPoint(0,600);
	pointR = QPoint(600,600);
	pointM = QPoint(300,600-sqrt(600*600-300*300));
	updater = new QPushButton("Update", this);
	move(QApplication::desktop()->screenGeometry().width()/2-size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2 - 10);
	connect(updater, SIGNAL(clicked()), this, SLOT(updaterSlot()));
}

void Chaos::updaterSlot()
{
	painted = true;
	update();
}

void Chaos::paintEvent(QPaintEvent *event)
{
	QPainter glob(&pix);
	if(painted)
	{
		painted = false;
		glob.setWindow(0,0,600,600);
		glob.setPen(Qt::black);
		glob.setBrush(Qt::white);
		glob.drawRect(0,0,600,600);
		glob.setRenderHint(QPainter::Antialiasing);
		paintPoint(glob);
	}
	QPainter(this).drawPixmap(QRect(0,0,600,600),pix, QRect(0,0,600,600));
}

void Chaos::paintPoint(QPainter &painter)
{
		int x,y;
		x = rand() % 600;
		y = rand() % 600;
		newPoint = QPoint(x,y);
		point0 = newPoint;
		painter.setPen(QPen(Qt::black, 1));
		painter.setBrush(Qt::white);
		int z;
		//srand(time(0));
		for(int i = 0; i < iterations0; i++)
		{
			z = (int)((float)rand())%3;
			if(z == 0)
			{
				newPoint = (newPoint + pointL)/2;
				if(newPoint.x() < 600 && newPoint.y() < 600)
				{
					painter.drawPoint(newPoint);
				}	
			}
			else if(z == 1)
			{
				newPoint = (newPoint + pointM)/2;
				if(newPoint.x() < 600 && newPoint.y() < 600)
				{
					painter.drawPoint(newPoint);
				}
			}
			else
			{
				newPoint = (newPoint + pointR)/2;
				if(newPoint.x() < 600 && newPoint.y() < 600)
				{
					painter.drawPoint(newPoint);
				}
			}
		}
		painter.setPen(QPen(Qt::red, 4,Qt::SolidLine,Qt::RoundCap,Qt::RoundJoin));
		painter.setBrush(Qt::NoBrush);
		painter.drawPoint(point0);
}
	