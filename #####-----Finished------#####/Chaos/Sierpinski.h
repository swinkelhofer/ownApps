#include <QWidget>
#include <QThread>
class QPoint;
class QPixmap;
class QPushButton;
class ChaosThread;
class Chaos: public QWidget
{
	Q_OBJECT
public:
	Chaos(QWidget *parent = 0);
protected:
	void paintEvent(QPaintEvent *event);
private:
	void paintPoint(QPainter &painter);
	int iterations0;
	QPoint pointL, pointM, pointR,newPoint, oldPoint,point0;
	QPushButton *updater;
	bool painted;
	QPixmap pix;
private slots:
	void updaterSlot();
};
