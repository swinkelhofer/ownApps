#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <psapi.h>
#include <string.h>
#include <ctype.h>
#include <wchar.h>
#include <ddk/ntapi.h>
#define MAX_UNICODE_PATH    32767L
void SetDebugPrivilege() 
{
	HANDLE hProcess = GetCurrentProcess(), hToken;
	TOKEN_PRIVILEGES priv;
	LUID luid;

	OpenProcessToken(hProcess, TOKEN_ADJUST_PRIVILEGES, &hToken);
	LookupPrivilegeValue(0, "SeDebugPrivilege", &luid);
	
    priv.PrivilegeCount = 1;
	priv.Privileges[0].Luid = luid;
	priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	AdjustTokenPrivileges(hToken, false, &priv, 0, 0, 0);
	
	
	CloseHandle(hToken);
	CloseHandle(hProcess);
}


// Used in PEB struct
typedef ULONG smPPS_POST_PROCESS_INIT_ROUTINE;

// Used in PEB struct
typedef struct _smPEB_LDR_DATA {
	BYTE Reserved1[8];
	PVOID Reserved2[3];
	LIST_ENTRY InMemoryOrderModuleList;
} smPEB_LDR_DATA, *smPPEB_LDR_DATA;

// Used in PEB struct
typedef struct _smRTL_USER_PROCESS_PARAMETERS {
	BYTE Reserved1[16];
	PVOID Reserved2[10];
	UNICODE_STRING ImagePathName;
	UNICODE_STRING CommandLine;
} smRTL_USER_PROCESS_PARAMETERS, *smPRTL_USER_PROCESS_PARAMETERS;

// Used in PROCESS_BASIC_INFORMATION struct
typedef struct _smPEB {
	BYTE Reserved1[2];
	BYTE BeingDebugged;
	BYTE Reserved2[1];
	PVOID Reserved3[2];
	smPPEB_LDR_DATA Ldr;
	smPRTL_USER_PROCESS_PARAMETERS ProcessParameters;
	BYTE Reserved4[104];
	PVOID Reserved5[52];
	smPPS_POST_PROCESS_INIT_ROUTINE PostProcessInitRoutine;
	BYTE Reserved6[128];
	PVOID Reserved7[1];
	ULONG SessionId;
} smPEB, *smPPEB;

typedef struct _smPROCESS_BASIC_INFORMATION {
    LONG ExitStatus;
    smPPEB PebBaseAddress;
    ULONG_PTR AffinityMask;
    LONG BasePriority;
    ULONG_PTR UniqueProcessId;
    ULONG_PTR InheritedFromUniqueProcessId;
} smPROCESS_BASIC_INFORMATION, *smPPROCESS_BASIC_INFORMATION;

typedef struct _smPROCESSINFO
{
	DWORD   dwPID;
	DWORD   dwParentPID;
	DWORD   dwSessionID;
	DWORD   dwPEBBaseAddress;
	DWORD   dwAffinityMask;
	LONG    dwBasePriority;
	LONG    dwExitStatus;
	BYTE    cBeingDebugged;
	TCHAR   szImgPath[MAX_UNICODE_PATH];
	TCHAR   szCmdLine[MAX_UNICODE_PATH];
} smPROCESSINFO;


typedef NTSTATUS (*_NtQueryInformationProcess)(IN HANDLE ProcessHandle, IN PROCESSINFOCLASS ProcessInformationClass, OUT PVOID ProcessInformation,
    IN ULONG ProcessInformationLength, OUT PULONG ReturnLength);
_NtQueryInformationProcess QueryInformationProcess = (_NtQueryInformationProcess) GetProcAddress(GetModuleHandle("ntdll.dll"), "NtQueryInformationProcess");

BOOL sm_GetNtProcessInfo(const DWORD dwPID, smPROCESSINFO *ppi)
{
	BOOL  bReturnStatus                     = TRUE;
	DWORD dwSize                            = 0;
	DWORD dwSizeNeeded                      = 0;
	DWORD dwBytesRead                       = 0;
	DWORD dwBufferSize                      = 0;
	HANDLE hHeap                            = 0;
	WCHAR *pwszBuffer                       = NULL;

	smPROCESSINFO spi                       = {0};
	smPPROCESS_BASIC_INFORMATION pbi        = NULL;

	smPEB peb                               = {0};
	smPEB_LDR_DATA peb_ldr                  = {0};
	smRTL_USER_PROCESS_PARAMETERS peb_upp   = {0};

	ZeroMemory(&spi, sizeof(spi));
	ZeroMemory(&peb, sizeof(peb));
	ZeroMemory(&peb_ldr, sizeof(peb_ldr));
	ZeroMemory(&peb_upp, sizeof(peb_upp));

	spi.dwPID = dwPID;
 
	// Attempt to access process
	HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | 
								PROCESS_VM_READ, FALSE, dwPID);
	if(hProcess == INVALID_HANDLE_VALUE) {
		return FALSE;
	}

	// Try to allocate buffer 
	hHeap = GetProcessHeap();

	dwSize = sizeof(smPROCESS_BASIC_INFORMATION);

	pbi = (smPPROCESS_BASIC_INFORMATION)HeapAlloc(hHeap,
												HEAP_ZERO_MEMORY,
												dwSize);
	// Did we successfully allocate memory
	if(!pbi) {
		CloseHandle(hProcess);
		return FALSE;
	}
	
	// Attempt to get basic info on process
	NTSTATUS dwStatus = QueryInformationProcess(hProcess,
												ProcessBasicInformation,
												pbi,
												dwSize,
												&dwSizeNeeded);
	
	// If we had error and buffer was too small, try again
	// with larger buffer size (dwSizeNeeded)
	if(dwStatus >= 0 && dwSize < dwSizeNeeded)
	{
		if(pbi)
			HeapFree(hHeap, 0, pbi);
		pbi = (smPPROCESS_BASIC_INFORMATION)HeapAlloc(hHeap,
											HEAP_ZERO_MEMORY,
											dwSizeNeeded);
		if(!pbi) {
			CloseHandle(hProcess);
			return FALSE;
		}

		dwStatus = QueryInformationProcess(hProcess,
											ProcessBasicInformation,
											pbi, dwSizeNeeded, &dwSizeNeeded);
	}

	// Did we successfully get basic info on process
	if(dwStatus >= 0)
	{
		// Basic Info
	//        spi.dwPID          = (DWORD)pbi->UniqueProcessId;
		spi.dwParentPID      = (DWORD)pbi->InheritedFromUniqueProcessId;
		spi.dwBasePriority   = (LONG)pbi->BasePriority;
		spi.dwExitStatus     = (NTSTATUS)pbi->ExitStatus;
		spi.dwPEBBaseAddress = (DWORD)pbi->PebBaseAddress;
		spi.dwAffinityMask   = (DWORD)pbi->AffinityMask;
	
		// Read Process Environment Block (PEB)
		if(pbi->PebBaseAddress)
		{
			if(ReadProcessMemory(hProcess, pbi->PebBaseAddress, &peb, sizeof(peb), &dwBytesRead))
			{
				spi.dwSessionID    = (DWORD)peb.SessionId;
				spi.cBeingDebugged = (BYTE)peb.BeingDebugged;
	
				// Here we could access PEB_LDR_DATA, i.e., module list for process
	//              dwBytesRead = 0;
	//              if(ReadProcessMemory(hProcess,
	//                                   pbi->PebBaseAddress->Ldr,
	//                                   &peb_ldr,
	//                                   sizeof(peb_ldr),
	//                                   &dwBytesRead))
	//              {
					// get ldr
	//              }
	
				// if PEB read, try to read Process Parameters
				dwBytesRead = 0;
				if(ReadProcessMemory(hProcess,
									peb.ProcessParameters,
									&peb_upp,
									sizeof(smRTL_USER_PROCESS_PARAMETERS),
									&dwBytesRead))
				{
					// We got Process Parameters, is CommandLine filled in
					if(peb_upp.CommandLine.Length > 0) {
						// Yes, try to read CommandLine
						pwszBuffer = (WCHAR *)HeapAlloc(hHeap,
														HEAP_ZERO_MEMORY,
														peb_upp.CommandLine.Length);
						// If memory was allocated, continue
						if(pwszBuffer)
						{
							if(ReadProcessMemory(hProcess,
												peb_upp.CommandLine.Buffer,
												pwszBuffer,
												peb_upp.CommandLine.Length,
												&dwBytesRead))
							{
								// if commandline is larger than our variable, truncate
								if(peb_upp.CommandLine.Length >= sizeof(spi.szCmdLine)) 
									dwBufferSize = sizeof(spi.szCmdLine) - sizeof(TCHAR);
								else
									dwBufferSize = peb_upp.CommandLine.Length;
							
								// Copy CommandLine to our structure variable
	#if defined(UNICODE) || (_UNICODE)
								// Since core NT functions operate in Unicode
								// there is no conversion if application is
								// compiled for Unicode
								StringCbCopyN(spi.szCmdLine, sizeof(spi.szCmdLine),
											pwszBuffer, dwBufferSize);
	#else
								// Since core NT functions operate in Unicode
								// we must convert to Ansi since our application
								// is not compiled for Unicode
								WideCharToMultiByte(CP_ACP, 0, pwszBuffer,
													(int)(dwBufferSize / sizeof(WCHAR)),
													spi.szCmdLine, sizeof(spi.szCmdLine),
													NULL, NULL);
	#endif
							}
							if(!HeapFree(hHeap, 0, pwszBuffer)) {
								// failed to free memory
								bReturnStatus = FALSE;
								goto gnpiFreeMemFailed;
							}
						}
					}   // Read CommandLine in Process Parameters
	
					// We got Process Parameters, is ImagePath filled in
					if(peb_upp.ImagePathName.Length > 0) {
						// Yes, try to read ImagePath
						dwBytesRead = 0;
						pwszBuffer = (WCHAR *)HeapAlloc(hHeap,
														HEAP_ZERO_MEMORY,
														peb_upp.ImagePathName.Length);
						if(pwszBuffer)
						{
							if(ReadProcessMemory(hProcess,
												peb_upp.ImagePathName.Buffer,
												pwszBuffer,
												peb_upp.ImagePathName.Length,
												&dwBytesRead))
							{
								// if ImagePath is larger than our variable, truncate
								if(peb_upp.ImagePathName.Length >= sizeof(spi.szImgPath)) 
									dwBufferSize = sizeof(spi.szImgPath) - sizeof(TCHAR);
								else
									dwBufferSize = peb_upp.ImagePathName.Length;
	
								// Copy ImagePath to our structure
	#if defined(UNICODE) || (_UNICODE)
								StringCbCopyN(spi.szImgPath, sizeof(spi.szImgPath),
											pwszBuffer, dwBufferSize);
	#else
								WideCharToMultiByte(CP_ACP, 0, pwszBuffer,
													(int)(dwBufferSize / sizeof(WCHAR)),
													spi.szImgPath, sizeof(spi.szImgPath),
													NULL, NULL);
	#endif
							}
							if(!HeapFree(hHeap, 0, pwszBuffer)) {
								// failed to free memory
								bReturnStatus = FALSE;
								goto gnpiFreeMemFailed;
							}
						}
					}   // Read ImagePath in Process Parameters
				}   // Read Process Parameters
			}   // Read PEB 
		}   // Check for PEB
	
		// System process for WinXP and later is PID 4 and we cannot access
		// PEB, but we know it is aka ntoskrnl.exe so we will manually define it.
		// ntkrnlpa.exe if Physical Address Extension (PAE)
		// ntkrnlmp.exe if Symmetric MultiProcessing (SMP)
		// Actual filename is ntoskrnl.exe, but other name will be in
		// Original Filename field of version block.
		if(spi.dwPID == 4) {
			ExpandEnvironmentStrings("%SystemRoot%\\System32\\ntoskrnl.exe",
									spi.szImgPath, sizeof(spi.szImgPath));
		}
	}   // Read Basic Info
	
	gnpiFreeMemFailed:
	
	// Free memory if allocated
	if(pbi != NULL)
		if(!HeapFree(hHeap, 0, pbi)) {
			// failed to free memory
		}
	
	CloseHandle(hProcess);
	
	// Return filled in structure to caller
	*ppi = spi;
	
	return bReturnStatus;
}

void PrintProcessNameAndID( DWORD processID )
{
    HANDLE hProcess, AllocAdresse, hRemoteThread = NULL;
	
   

	TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");
	BOOL system = false;
	
	smPROCESSINFO info;
	memset(&info, 0, sizeof(info));
	sm_GetNtProcessInfo(processID, &info); 
	hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | 
								PROCESS_VM_READ | PROCESS_VM_WRITE, FALSE, processID);
	
	if(hProcess != NULL && processID != GetCurrentProcessId())
	{
		HMODULE hMod[1024], hPMod[1];
        DWORD cbNeeded, cPNeeded;
		DWORD out;
		
		EnumProcessModules(hProcess, hMod, sizeof(hMod), &cbNeeded);
		for (DWORD i = 0; i < (cbNeeded/sizeof(HMODULE)); ++i)
        {
			if(i == 0)
			{
				GetModuleBaseName(hProcess, *hMod, szProcessName, 
								sizeof(szProcessName)/sizeof(TCHAR));
				printf("\nPID: %4u\t%s \n", processID, szProcessName);
				printf("CommandLine:\t%s\n", info.szCmdLine);
				//HANDLE parentHandle = OpenProcess(PROCESS_QUERY_INFORMATION | 
				//				PROCESS_VM_READ | PROCESS_VM_WRITE, FALSE, info.dwParentPID);
				//EnumProcessModules(parentHandle, hPMod, sizeof(hPMod), &cPNeeded);
				//GetModuleBaseName(parentHandle, *hPMod, szProcessName, 
				//				sizeof(szProcessName)/sizeof(TCHAR));
				//printf("Parent Process:\t%s\n", szProcessName);
				printf("%d\n", info.dwParentPID);
			}
			else
			{
				GetModuleFileNameEx(hProcess, hMod[i], szProcessName,
                                      sizeof(szProcessName)/sizeof(TCHAR));
				
				printf("\t+++ %s \n", szProcessName);
				
			}
        }
	}
	CloseHandle(hProcess);
}

int main(int argc, char *argv[])
{
	SetDebugPrivilege();
	DWORD aProcesses[1024], cbNeeded, cProcesses;
    unsigned int i;

    if ( !EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) )
    {
        return 1;
    }


    // Calculate how many process identifiers were returned.

    cProcesses = cbNeeded / sizeof(DWORD);

    // Print the name and process identifier for each process.

    for ( i = 0; i < cProcesses; i++ )
    {
        if( aProcesses[i] != 0)
        {
            PrintProcessNameAndID( aProcesses[i] );
        }
    }
}