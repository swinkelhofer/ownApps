#include <QtGui>
#include "PassGen.h"
#include <sys/time.h>

PassGen::PassGen(QWidget *parent):QDialog(parent)
{
	QPalette pal;
	pal.setColor(QPalette::Window, Qt::white);
	setPalette(pal);
	setWindowFlags(Qt::FramelessWindowHint);
	pass = new QLineEdit;
	res = new QLabel;
	copy = new QPushButton("Kopieren");
	name = new QLabel("<font size=14pt>�2009, Sascha Winkelhofer</font>");
	saveword = new QPushButton("Sicheres Passwort?");
	exit = new QPushButton("Fenster schlie�en");
	create = new QPushButton("Passwort erstellen");
	lab1 = new QLabel("Bitte Wort/Satz eingeben:");
	QHBoxLayout *unten = new QHBoxLayout;
	unten->addWidget(res);
	unten->addWidget(copy);
	QHBoxLayout *gunten = new QHBoxLayout;
	gunten->addWidget(saveword);
	gunten->addWidget(exit);
	QVBoxLayout *main = new QVBoxLayout;
	main->addWidget(lab1);
	main->addWidget(pass);
	main->addWidget(create);
	main->addLayout(unten);
	main->addLayout(gunten);
	main->addWidget(name);
	setLayout(main);
	setWindowTitle("PassGen");
	connect(create, SIGNAL(clicked()), this, SLOT(make()));
	connect(copy, SIGNAL(clicked()), this, SLOT(clipbrd()));
	connect(saveword, SIGNAL(clicked()), this, SLOT(help()));
	connect(exit, SIGNAL(clicked()), this, SLOT(close()));
	setWindowOpacity(0.85);
}

void PassGen::make()
{
	QString pw = pass->text();
	for(int i = 0; i<pw.length(); i++)
	{
		pw[i] = change(pw[i]);
		res->setText(QString("<h1 color=red>") + pw + QString("</h1>"));
	}

}

void PassGen::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setPen(QPen(Qt::black));
	painter.drawRect(0,0,width()-1, height()-1);
	painter.drawPixmap(QRect(0,0,width()-1, height()-1), QPixmap(":/img/vista.bmp"));
}

void PassGen::clipbrd()
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setText(res->text().mid(14,res->text().length()-19));
}

void PassGen::help()
{
	QMessageBox::about(this, tr("Sicheres Passwort?"), tr("<font family=Arial><h2>Passw�rter</h2>"
		"<p>Copyright &copy; 2010 Sascha Winkelhofer <br /><br />"
		"Ein sehr sicheres Passwort besteht aus "
		"<b>mindestens 8 Zeichen</b>, von denen"
		"bestenfalls je 2 Gro�-, Kleinbuchstaben, "
		"Zahlen und Sonderzeichen sind."
		"<h3>Strategie f�r ein sicheres Passwort</h3>"
		"W�hlen Sie ein Wort, eine Aussage,..."
		"die Sie sich gut merken k�nnen. Eine gute "
		"L�sung ist es nun einzelne Zeichen durch "
		"andere darzustellen. z.B. wird <b>\"JAMES 007\"</b>"
		"so zu <b>\"J@m3$ oO7\"</b></font>"));
}

QChar PassGen::change(QChar a)
{
	a = a.toUpper();
	int i = 0;
	srand((unsigned)time(NULL)+rand());
	if(a == QChar('A'))
	{
		i = rand();
		char cur[] = {'@', 'A', 'a','4'};
		a = QChar(cur[i%4]);
	}
	if(a == QChar('B'))
	{
		i = rand();
		char cur[] = {'B', 'b', '8', '�'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('C'))
	{
		i = rand();
		char cur[] = {'C', 'c', '<'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('D'))
	{
		i = rand();
		char cur[] = {'D', 'd'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('E'))
	{
		i = rand();
		QChar cur[] = {'e', 'E', 0x20AC, '3'};
		a = cur[i%4];
	}
	if(a == QChar('F'))
	{
		i = rand();
		char cur[] = {'f', 'F'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('G'))
	{
		i = rand();
		char cur[] = {'g', 'G', '9'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('H'))
	{
		i = rand();
		char cur[] = {'h', 'H', '#'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('I'))
	{
		i = rand();
		char cur[] = {'i', 'I', '!', '1'};
		a = QChar(cur[i%4]);
	}
	if(a == QChar('J'))
	{
		i = rand();
		char cur[] = {'j', 'J'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('K'))
	{
		i = rand();
		char cur[] = {'k', 'K'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('L'))
	{
		i = rand();
		char cur[] = {'l', 'L', '1', '!'};
		a = QChar(cur[i%4]);
	}
	if(a == QChar('M'))
	{
		i = rand();
		char cur[] = {'m', 'M'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('N'))
	{
		i = rand();
		char cur[] = {'n', 'N', '�'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('O'))
	{
		i = rand();
		char cur[] = {'o', 'O', '0'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('P'))
	{
		i = rand();
		char cur[] = {'p', 'P'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('Q'))
	{
		i = rand();
		char cur[] = {'q', 'Q'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('R'))
	{
		i = rand();
		char cur[] = {'r', 'R'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('S'))
	{
		i = rand();
		char cur[] = {'s', 'S', '5', '$', '�'};
		a = QChar(cur[i%5]);
	}
	if(a == QChar('T'))
	{
		i = rand();
		char cur[] = {'t', 'T', '+', '7'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('U'))
	{
		i = rand();
		char cur[] = {'u', 'U'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('V'))
	{
		i = rand();
		char cur[] = {'v', 'V'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('W'))
	{
		i = rand();
		char cur[] = {'w', 'W'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('X'))
	{
		i = rand();
		char cur[] = {'x', 'X', '*'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('Y'))
	{
		i = rand();
		char cur[] = {'y', 'Y'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('Z'))
	{
		i = rand();
		char cur[] = {'z', 'Z', '2'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('0'))
	{
		i = rand();
		char cur[] = {'0', 'o', 'O'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('1'))
	{
		i = rand();
		char cur[] = {'1', '!', 'I'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('2'))
	{
		i = rand();
		char cur[] = {'z', 'Z', '2'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('3'))
	{
		i = rand();
		QChar cur[] = {'3', 'E', 0x20AC};
		a = cur[i%3];
	}
	if(a == QChar('4'))
	{
		i = rand();
		char cur[] = {'4', 'A'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('5'))
	{
		i = rand();
		char cur[] = {'5', 's', 'S', '$', '�'};
		a = QChar(cur[i%5]);
	}
	if(a == QChar('6'))
	{
		i = rand();
		char cur[] = {'6', 'b'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('7'))
	{
		i = rand();
		char cur[] = {'7', 'T'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('8'))
	{
		i = rand();
		char cur[] = {'8', 'B', '�'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('9'))
	{
		i = rand();
		char cur[] = {'9', 'g'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar(' '))
	{
		i = rand();
		char cur[] = {'.', ',', '-', ' '};
		a = QChar(cur[i%4]);
	}
	if(a == QChar('�'))
	{
		i = rand();
		char cur[] = {'n', '�'};
		a = QChar(cur[i%2]);
	}
	if(a == QChar('-'))
	{
		i = rand();
		char cur[] = {'-', ' '};
		a = QChar(cur[i%2]);
	}
	if(a == QChar(','))
	{
		i = rand();
		char cur[] = {',', ' ', '.'};
		a = QChar(cur[i%3]);
	}
	if(a == QChar('.'))
	{
		i = rand();
		char cur[] = {',', '.', ' '};
		a = QChar(cur[i%3]);
	}
	return a;
}
