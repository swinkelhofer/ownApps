#include <QDialog>
class QPushButton;
class QLabel;
class QChar;
class QLineEdit;
class PassGen:public QDialog
{
	Q_OBJECT
public:
	PassGen(QWidget *parent = 0);
private:
	QPushButton *create, *copy, *saveword, *exit;
	QLabel *res, *lab1, *name;
	QLineEdit *pass;
private slots:
	void make();
	void paintEvent(QPaintEvent *event);
	void clipbrd();
	void help();
	QChar change(QChar a);
};
