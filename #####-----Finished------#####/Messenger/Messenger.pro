######################################################################
# Automatically generated by qmake (2.01a) Do 2. Feb 11:03:55 2012
######################################################################

TEMPLATE = app
TARGET =
DEPENDPATH += . release
INCLUDEPATH += .

# Input
HEADERS += Messenger.h
SOURCES += main.cpp Messenger.cpp
RESOURCES += PassTest.qrc
RC_FILE = res.rc
QMAKE_CXXFLAGS_RELEASE -= -O1 -O2 -static
QMAKE_LFLAGS -= -Wl,-enable-runtime-pseudo-reloc