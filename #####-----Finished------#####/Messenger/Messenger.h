#include <QDialog>

class QPushButton;
class QTextEdit;
class QLabel;
class QLineEdit;
class QTimer;
class Messenger:public QDialog
{
	Q_OBJECT
public:
	Messenger(QWidget *parent = 0);
private:
	//QListWidget *namelist;
	//QListWidget *iplist;
	//QStringList messages;
	QLabel *status;
	QPushButton *send;
	QTextEdit *text;
	QLineEdit *newMessage;
	int socketID;
	QTimer *timer, *timer2;
	//void setDat();
	void init();
	//char BUFFER[1024];
private slots:
	//void setCurrentRow(int row);
	void establishConnection();
	void readConnection();
	void sendData();
};
