#include <QtGui>
#include "PassTest.h"
#include <math.h>

PassTest::PassTest(QWidget *parent):QDialog(parent)
{
	setWindowTitle("PassTest");
	QPalette pal;
	pal.setColor(QPalette::Window, Qt::white);
	setPalette(pal);
	//setWindowFlags(Qt::FramelessWindowHint);
	setWindowFlags(Qt::FramelessWindowHint);
	but = new QPushButton("Testen");
	bout = new QPushButton("sicheres Passwort?");
	clos = new QPushButton("Fenster schlie�en");
	pass = new QLineEdit;
	widg = new QWidget;
	widg->setFixedSize(100,20);
	name = new QLabel("<font size=\"8pt\">�2009, Sascha Winkelhofer</font>");
	strong = 0;
	QHBoxLayout *down = new QHBoxLayout;
	down->addWidget(bout);
	down->addWidget(clos);
	QVBoxLayout *all = new QVBoxLayout;
	all->addWidget(widg);
	all->addWidget(pass);
	all->addWidget(but);
	all->addLayout(down);
	all->addWidget(name);
	setLayout(all);
	connect(but, SIGNAL(clicked()), this, SLOT(test()));
	connect(clos, SIGNAL(clicked()), this, SLOT(close()));
	connect(bout, SIGNAL(clicked()), this, SLOT(help()));
	setWindowOpacity(0.85);

}

void PassTest::test()
{
	QString password = pass->text();
	if(!password.isEmpty())
	{
	int a = 0; 
	int A = 0; 
	int sonder = 0; 
	int zahl = 0;
	for (int i = 0; i < password.length(); i++)
	{
		if(password.at(i).isUpper())
			A = A+1;
		else if(password.at(i).isLower())
			a = a+1;
		else if(password.at(i).isNumber())
			zahl = zahl+1;
		else
			sonder = sonder+1;
	}
	int posibil = 0;
	if(A > 0)
	{
		posibil += A+29;
		//A=1;
	}
	if(a > 0)
	{
		posibil += a+29;
		//a=1;
	}
	if(sonder > 0)
	{
		posibil += sonder+32;
		//sonder=1;
	}
	if(zahl > 0)
	{
		posibil += zahl+10;
		//zahl=1;
	}
	double erg = pow(posibil, 8);
	erg = erg / 1000000;
	erg = erg / 86400 /365;
	strong = erg;
		strong = (int) erg*100/(700)+password.length()*2;
	if(strong > 100)
		strong = 100;
	this->update();
	}
	else
	{
		strong=0;
		this->update();
	}

}

void PassTest::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setPen(QPen(Qt::black));
	painter.drawRect(0,0,width()-1, height()-1);
	painter.drawPixmap(QRect(0,0,width()-1, height()-1), QPixmap(":/img/vista.bmp"));
	painter.setBrush(QBrush(Qt::white,Qt::SolidPattern));
	painter.drawRect(0,0,width()-1,20);
	if(strong >= 99)
	{
		QColor col(50,255,50,255);
		painter.setBrush(QBrush(col,Qt::SolidPattern));
		painter.drawRect(0,0,width()-1,20);
		painter.drawText(QRect(3,0, width()-1,20),Qt::AlignLeft|Qt::AlignVCenter, "Sehr sicher");
	}
	else if(strong <99 && strong >= 80)
	{
		painter.setBrush(QBrush(QColor(180,255,0,180),Qt::SolidPattern));
		painter.drawRect(0,0,width()-1,20);
		painter.drawText(QRect(3,0, width()-1,20),Qt::AlignLeft|Qt::AlignVCenter, "Sicher");


	}
	else if(strong < 80 && strong >= 60)
	{
		painter.setBrush(QBrush(Qt::yellow,Qt::SolidPattern));
		painter.drawRect(0,0,width()-1,20);
		painter.drawText(QRect(3,0, width()-1,20),Qt::AlignLeft|Qt::AlignVCenter, "Normal");


	}
	else if(strong < 60 && strong >= 10)
	{
		QColor col;
		col.setNamedColor("orange");
		painter.setBrush(QBrush(col,Qt::SolidPattern));
		painter.drawRect(0,0, width()-1,20);
		painter.drawText(QRect(3,0, width()-1,20),Qt::AlignLeft|Qt::AlignVCenter, "Unsicher");

	}
	else
	{
		painter.setBrush(QBrush(Qt::red,Qt::SolidPattern));
		painter.drawRect(0,0,width()-1,20);
		painter.drawText(QRect(3,0, width()-1,20),Qt::AlignLeft|Qt::AlignVCenter, "Sehr unsicher");

	}


}

void PassTest::help()
{
	QMessageBox::about(this, tr("Sicheres Passwort?"), tr("<font family=Arial><h2>Passw�rter</h2>"
		"<p>Copyright &copy; 2009 Sascha Winkelhofer <br /><br />"
		"Ein sehr sicheres Passwort besteht aus "
		"<b>mindestens 8 Zeichen</b>, von denen "
		"bestenfalls je 2 Gro�-, Kleinbuchstaben, "
		"Zahlen und Sonderzeichen sind."
		"<h3>Strategie f�r ein sicheres Passwort</h3>"
		"W�hlen Sie ein Wort, eine Aussage,..., "
		"welches Sie sich gut merken k�nnen. Eine gute "
		"L�sung ist es nun einzelne Zeichen durch "
		"andere darzustellen. z.B. wird <b>\"JAMES 007\"</b>"
		"so zu <b>\"J@m3$ oO7\"</b></font>"));
}