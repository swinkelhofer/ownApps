#include <QDialog>
class QLineEdit;
class QPushButton;
class QString;
class QWidget;
class QLabel;
class PassTest:public QDialog
{
	Q_OBJECT
public:
	PassTest(QWidget *parent = 0);
	QLineEdit *pass;
	QPushButton *but,*clos,*bout;
	QLabel *name;
protected:
	void paintEvent(QPaintEvent *event);
private:
	QWidget *widg;
	double strong;
private slots:
	void test();
	void help();
};