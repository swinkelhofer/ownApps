#include <QDialog>
class QLineEdit;
class QTextEdit;
class QPushButton;
class QProcess;
class QTimer;
class ReadPipe: public QDialog
{
	Q_OBJECT

public:
	ReadPipe(QWidget *parent = 0);
private:
	QLineEdit *path, *cmdLine;
	QPushButton *search, *start;
	QTextEdit *output;
	QProcess *proc;
	QTimer *timer;
private slots:
	void searchPath();
	void startApp();
	void checkState();
};
