#include <QtGui>
#include "ReadPipe.h"

ReadPipe::ReadPipe(QWidget *parent):QDialog(parent)
{
	search = new QPushButton("Durchsuchen");
	start = new QPushButton("Starte Programm");
	start->setDefault(true);
	path = new QLineEdit;
	path->setReadOnly(true);
	cmdLine = new QLineEdit;
	output = new QTextEdit;
	proc = new QProcess;
	proc->setProcessChannelMode(QProcess::MergedChannels);
	proc->setReadChannel(QProcess::StandardOutput);
	timer = new QTimer;
	QHBoxLayout *top = new QHBoxLayout;
	top->addWidget(path);
	top->addWidget(search);
	QVBoxLayout *main = new QVBoxLayout;
	main->addWidget(new QLabel("Programm:"));
	main->addLayout(top);
	main->addWidget(new QLabel("Optionale Startparameter:"));
	main->addWidget(cmdLine);
	main->addWidget(start);
	main->addWidget(new QLabel("Output:"));
	main->addWidget(output);
	connect(search, SIGNAL(clicked()), this, SLOT(searchPath()));
	connect(start, SIGNAL(clicked()), this, SLOT(startApp()));
	connect(timer, SIGNAL(timeout()), this, SLOT(checkState()));
	resize(400, 300);
	setLayout(main);
}

void ReadPipe::searchPath()
{
	QString exe = QFileDialog::getOpenFileName(this, "Suche Programm", 0, "Ausführbare Dateien (*.exe)");
	if(!exe.isEmpty())
		path->setText(exe);
}

void ReadPipe::startApp()
{
	if(start->text() == "Starte Programm" && !path->text().isEmpty())
	{
		if(!cmdLine->text().isEmpty())
			proc->start(path->text(), QStringList() << cmdLine->text());
		else
			proc->start(path->text(), QStringList());
		if(proc->waitForStarted(-1))
		//if(proc->state() == QProcess::Running || proc->state() == QProcess::Starting)
		{
			start->setText("Stoppe Programm");
			timer->start(1000);
			output->setText(proc->readAll());
		}
	
	}
	else if(start->text() == "Stoppe Programm" && proc->state() == QProcess::Running)
	{
		timer->stop();
		proc->kill();
		proc->waitForFinished();
		if(proc->state() == QProcess::NotRunning)
			start->setText("Starte Programm");
		output->append(proc->readAll());
	}
	else if(start->text() == "Stoppe Programm" && proc->state() == QProcess::NotRunning)
	{
		timer->stop();
		start->setText("Starte Programm");
		output->append(proc->readAll());
	}
}

void ReadPipe::checkState()
{
	if(proc->state() == QProcess::NotRunning)
	{
		timer->stop();
		start->setText("Starte Programm");
		output->append(proc->readAll());
	}
	else if(proc->state() == QProcess::Running)
	{
		output->append(proc->readAll());
	}
}