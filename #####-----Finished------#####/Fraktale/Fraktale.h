#include <QWidget>
class QLabel;
class QPushButton;
class Fraktale: public QWidget
{
	Q_OBJECT

public:
	Fraktale(QWidget *parent = 0);
	
private:
	QLabel *Pic1, *Pic2, *Pic3, *Pic4, *Pic5;
	QPushButton *But1, *But2, *But3, *But4, *But5;

private slots:
	void start1();
	void start2();
	void start3();
	void start4();
	void start5();
};
