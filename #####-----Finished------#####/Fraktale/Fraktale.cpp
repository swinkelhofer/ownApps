#include "Fraktale.h"
#include <QtGui>

Fraktale::Fraktale(QWidget *parent):QWidget(parent)
{
	move(0,0);
	Pic1 = new QLabel;
	Pic1->setPixmap(QPixmap(":/Chaos.png").scaled(64, 64, Qt::KeepAspectRatio, Qt::SmoothTransformation));
	Pic2 = new QLabel;
	Pic2->setPixmap(QPixmap(":/Koch.png").scaled(64, 64, Qt::KeepAspectRatio, Qt::SmoothTransformation));
	Pic3 = new QLabel;
	Pic3->setPixmap(QPixmap(":/Drachenkurve.png").scaled(64, 64, Qt::KeepAspectRatio, Qt::SmoothTransformation));
	Pic4 = new QLabel;
	Pic4->setPixmap(QPixmap(":/Farn.png").scaled(64, 64, Qt::KeepAspectRatio, Qt::SmoothTransformation));
	Pic5 = new QLabel;
	Pic5->setPixmap(QPixmap(":/Julia.png").scaled(64, 64, Qt::KeepAspectRatio, Qt::SmoothTransformation));
	
	But1 = new QPushButton("Sierpinski-Dreieck");
	But2 = new QPushButton("Kochsche Schneeflocke");
	But3 = new QPushButton("Drachenkurve");
	But4 = new QPushButton("Barnsley-Farn");
	But5 = new QPushButton("Julia-Mengen");
	
	QGridLayout *grid = new QGridLayout;
	grid->addWidget(Pic1, 0, 0);
	grid->addWidget(But1, 0, 1);
	grid->addWidget(Pic2, 1, 0);
	grid->addWidget(But2, 1, 1);
	grid->addWidget(Pic3, 2, 0);
	grid->addWidget(But3, 2, 1);
	grid->addWidget(Pic4, 3, 0);
	grid->addWidget(But4, 3, 1);
	grid->addWidget(Pic5, 4, 0);
	grid->addWidget(But5, 4, 1);
	connect(But1, SIGNAL(clicked()), this, SLOT(start1()));
	connect(But2, SIGNAL(clicked()), this, SLOT(start2()));
	connect(But3, SIGNAL(clicked()), this, SLOT(start3()));
	connect(But4, SIGNAL(clicked()), this, SLOT(start4()));
	connect(But5, SIGNAL(clicked()), this, SLOT(start5()));
	setLayout(grid);
}

void Fraktale::start1()
{
	QProcess::execute("Sierpinski.exe");
}

void Fraktale::start2()
{
	QProcess::execute("Koch.exe");
}

void Fraktale::start3()
{
	QProcess::execute("Drachenkurve.exe");
}

void Fraktale::start4()
{
	QProcess::execute("Farn.exe");
}

void Fraktale::start5()
{
	QProcess::execute("Julia.exe");
}