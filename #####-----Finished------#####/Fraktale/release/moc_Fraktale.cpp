/****************************************************************************
** Meta object code from reading C++ file 'Fraktale.h'
**
** Created: Mon 7. May 19:32:58 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Fraktale.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Fraktale.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Fraktale[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      19,    9,    9,    9, 0x08,
      28,    9,    9,    9, 0x08,
      37,    9,    9,    9, 0x08,
      46,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Fraktale[] = {
    "Fraktale\0\0start1()\0start2()\0start3()\0"
    "start4()\0start5()\0"
};

const QMetaObject Fraktale::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Fraktale,
      qt_meta_data_Fraktale, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Fraktale::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Fraktale::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Fraktale::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Fraktale))
        return static_cast<void*>(const_cast< Fraktale*>(this));
    return QWidget::qt_metacast(_clname);
}

int Fraktale::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: start1(); break;
        case 1: start2(); break;
        case 2: start3(); break;
        case 3: start4(); break;
        case 4: start5(); break;
        default: ;
        }
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
