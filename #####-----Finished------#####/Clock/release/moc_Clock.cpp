/****************************************************************************
** Meta object code from reading C++ file 'Clock.h'
**
** Created: Sun 19. Dec 17:45:48 2010
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Clock.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Clock.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Clock[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       7,    6,    6,    6, 0x08,
      19,    6,    6,    6, 0x08,
      27,    6,    6,    6, 0x08,
      39,    6,    6,    6, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Clock[] = {
    "Clock\0\0changeCol()\0exits()\0showFront()\0"
    "showBack()\0"
};

const QMetaObject Clock::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Clock,
      qt_meta_data_Clock, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Clock::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Clock::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Clock::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Clock))
        return static_cast<void*>(const_cast< Clock*>(this));
    return QWidget::qt_metacast(_clname);
}

int Clock::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: changeCol(); break;
        case 1: exits(); break;
        case 2: showFront(); break;
        case 3: showBack(); break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
