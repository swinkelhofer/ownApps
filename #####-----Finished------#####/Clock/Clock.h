#include <QWidget>
#ifndef CLOCK
#define CLOCK
class QColor;
class QToolButton;
class QComboBox;
class QLabel;
class Clock:public QWidget
{
	Q_OBJECT
public:
	Clock(QWidget *parent = 0);
	QToolButton *close;
	int wholeWidth;

protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);

private:
	QTimer *timer;
	int widgX,widgY;
	bool mousePressed, closeShown, contextMenuShown;
	QColor backCol, uhrCol;
	//QComboBox *box, *box2;
	QWidget *changeColor;
	QLabel *copyright;
	void createColorChanger();
private slots:
	void changeCol();
	void exits();
	void showFront();
	void showBack();

};
#endif

