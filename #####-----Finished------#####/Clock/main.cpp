#include <QApplication>
#include <QSettings>
#include "Clock.h"
int main(int argc, char *argv[])
{
	QSettings settings(QSettings::IniFormat, QSettings::UserScope, "$h@d0WFoXx","Clock");
	QApplication app(argc, argv);
	Clock *window = new Clock;
	window->setWindowFlags(Qt::SplashScreen | Qt::FramelessWindowHint);
	//window->setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);
	window->setGeometry(1024/2-200,768/2-200,200,200);
	window->setAttribute(Qt::WA_DeleteOnClose,true);
	//window->setWindowFlags(Qt::SplashScreen);
	//QPalette pal;
	//pal.setColor(QPalette::Window, Qt::color0);
	//window->setPalette(pal);
	window->setWindowOpacity(0.85);
	window->show();
	window->move(settings.value("Clock/X",window->wholeWidth-window->width()).toInt(),settings.value("Clock/Y",0).toInt());
	return app.exec();
}