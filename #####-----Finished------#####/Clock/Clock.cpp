#include <QtGui>
#include <math.h>
#include "Clock.h"
#define PI 3.14159265

QSettings settings(QSettings::IniFormat, QSettings::UserScope, "$h@d0WFoXx","Clock");

Clock::Clock(QWidget *parent):QWidget(parent)
{
	backCol = QColor(settings.value("Clock/backColor-r",0).toInt(), settings.value("Clock/backColor-g",0).toInt(), settings.value("Clock/backColor-b",0).toInt(), settings.value("Clock/backColor-a",255).toInt());
	uhrCol = QColor(settings.value("Clock/uhrColor-r",0xa0).toInt(), settings.value("Clock/uhrColor-g",0xa0).toInt(), settings.value("Clock/uhrColor-b",0xa4).toInt());
	QPalette pal;
	pal.setBrush(QPalette::Window,QBrush(Qt::white));
	pal.setBrush(QPalette::Base,QBrush(Qt::white));
	pal.setBrush(QPalette::WindowText,QBrush(Qt::black));
	pal.setBrush(QPalette::Button, QBrush(Qt::white));
	pal.setBrush(QPalette::ButtonText,QBrush(Qt::black));
	timer = new QTimer;
	timer->start(1000);
	close = new QToolButton(this);
	close->setPalette(pal);
	close->setIcon(QIcon(":/img/Close.png"));
	close->setMaximumHeight(16);
	close->setMaximumWidth(close->height());
	setAttribute(Qt::WA_NoSystemBackground, true);
	setAttribute(Qt::WA_TranslucentBackground,true);
	close->move(160,20);
	close->hide();
	connect(close, SIGNAL(clicked()), this , SLOT(exits()));
	connect(timer, SIGNAL(timeout()), this , SLOT(update()));
	setMouseTracking(true);
	mousePressed = false;
	closeShown = false;
	contextMenuShown = false;
	createColorChanger();
	QDesktopWidget *desktop = new QDesktopWidget;
	wholeWidth = desktop->screenGeometry().width();
	setWindowIcon(QIcon(":/img/Clock.png"));
	copyright = new QLabel(this);
	copyright->setText(QString("<font size=10pt color=white>") + QString(0xa9) + QString(" 2010, $h@d0\\/\\/FoXx"));
	copyright->move(55,160);
	copyright->hide();
}

void Clock::exits()
{
	exit(0);
}

void Clock::createColorChanger()
{

	changeColor = new QWidget;
	changeColor->setWindowModality(Qt::WindowModal);
	changeColor->setWindowFlags(Qt::Popup);
	QLabel *lab1 = new QLabel("Hintergrundfarbe:");
	QLabel *lab2 = new QLabel("Uhrfarbe:");
	QPushButton *OK = new QPushButton("OK");

	QPushButton *back_col = new QPushButton("�ndern");
	QPushButton *front_col = new QPushButton("�ndern");
	
	QVBoxLayout *main = new QVBoxLayout;
	main->addWidget(lab1);
	main->addWidget(back_col);
	main->addWidget(lab2);
	main->addWidget(front_col);
	main->addWidget(OK);
	changeColor->setLayout(main);
	QPalette pal;
	pal.setBrush(QPalette::Window,QBrush(Qt::red));
	changeColor->setPalette(pal);
	changeColor->setWindowOpacity(0.75);
	changeColor->hide();
	connect(back_col, SIGNAL(clicked()), this, SLOT(showBack()));
	connect(front_col, SIGNAL(clicked()), this, SLOT(showFront()));
	connect(OK, SIGNAL(clicked()), this, SLOT(changeCol()));
}

void Clock::showFront()
{
	QColor temp = QColorDialog::getColor(uhrCol);
	if(temp.isValid())
	{
		uhrCol = temp;
		settings.setValue("Clock/uhrColor-r", uhrCol.red());
		settings.setValue("Clock/uhrColor-g", uhrCol.green());
		settings.setValue("Clock/uhrColor-b", uhrCol.blue());
		update();
	}
	changeColor->show();
	contextMenuShown = true;
}

void Clock::showBack()
{
	QColor temp = QColorDialog::getColor(backCol, 0, "Hintergrundfarbe w�hlen", QColorDialog::ShowAlphaChannel);
	if(temp.isValid())
	{
		backCol = temp;
		settings.setValue("Clock/backColor-r", backCol.red());
		settings.setValue("Clock/backColor-g", backCol.green());
		settings.setValue("Clock/backColor-b", backCol.blue());
		settings.setValue("Clock/backColor-a", backCol.alpha());
		update();
	}
	changeColor->show();
	contextMenuShown = true;
}

void Clock::changeCol()
{
	changeColor->hide();
	contextMenuShown = false;
	
}

void Clock::paintEvent(QPaintEvent *event)
{
	QPen thick(palette().foreground(), 2.5);
	thick.setJoinStyle(Qt::RoundJoin);
	thick.setCapStyle(Qt::RoundCap);
	QPen thin(QColor(180,0,0), 0.5);
	thin.setJoinStyle(Qt::RoundJoin);
	thin.setCapStyle(Qt::RoundCap);
	QPen normal(palette().foreground(), 1.5);
	normal.setJoinStyle(Qt::RoundJoin);
	normal.setCapStyle(Qt::RoundCap);
	QPainter painter(this);
	painter.setBackgroundMode(Qt::TransparentMode);
	int side = qMin(width(), height());
	painter.setViewport((width() - side) /2, (height() - side) /2, side, side);
	painter.setWindow(-56, -56, 112, 112);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setFont(QFont("Segoe UI", 7, QFont::DemiBold));
	QConicalGradient grad1(0,0,90);
	grad1.setColorAt(0.0, Qt::white);
	grad1.setColorAt(0.1, backCol);
	grad1.setColorAt(0.8, backCol);
	grad1.setColorAt(1.0, Qt::white);
	painter.setBrush(grad1);
	painter.setPen(Qt::NoPen);
	if(backCol.alpha() == 0)
	{
		painter.setBrush(Qt::transparent);
	}
		
	painter.drawRect(-56, -56, 112, 112);

	QConicalGradient grad(0,0,-90.0);
	grad.setColorAt(0.0, Qt::lightGray);
	grad.setColorAt(0.2, uhrCol);
	grad.setColorAt(0.5, Qt::white);
	grad.setColorAt(0.75, uhrCol);
	grad.setColorAt(1.0, Qt::lightGray);
	painter.setPen(Qt::NoPen);
	painter.setBrush(grad);
	painter.drawEllipse(-48, -48, 96, 96);
	painter.setPen(normal);


	painter.drawText(QRectF(-7, -48, 14, 10), Qt::AlignVCenter | Qt::AlignHCenter,"12");
	painter.drawText(QRectF(-7, 38, 14, 10), Qt::AlignVCenter | Qt::AlignHCenter,"6");
	painter.drawText(QRectF(38, -5, 10, 10), Qt::AlignVCenter | Qt::AlignHCenter,"3");
	painter.drawText(QRectF(-48, -5, 10, 10), Qt::AlignVCenter | Qt::AlignHCenter,"9");
	QTime time;
	time = QTime::currentTime();
	int min = time.minute();
	int sek = time.second();
	int hour = time.hour();

	int alpha1 = min*6;
	int alpha2 = sek*6;
	double alpha3 = (hour%12)*30+min/2;
	double b1 = sin(alpha1*PI/180)*40;
	double b2 = sin(alpha2*PI/180)*45;
	double b3 = sin(alpha3*PI/180)*30;
	double a1 = sqrt(40*40-b1*b1);
	double a2 = sqrt(45*45-b2*b2);
	double a3 = sqrt(30*30-b3*b3);
	painter.setPen(normal);
	if(alpha1 >= 0 && alpha1 <= 90)
	{
		painter.drawLine(0,0, (int) b1, (int) -a1);
	}
	else if(alpha1 > 90 && alpha1 <= 180)
	{
		painter.drawLine(0,0, (int) b1, (int) a1);
	}
	else if(alpha1 	> 180 && alpha1 <= 270)
	{
		painter.drawLine(0,0, (int) b1, (int) a1);
	}
	else if(alpha1 > 270 && alpha1 < 360)
	{
		painter.drawLine(0,0, (int) b1, (int) -a1);
	}
	
	painter.setPen(thin);
	if(alpha2 >= 0 && alpha2 <= 90)
	{
		painter.drawLine(0,0, (int) b2, (int) -a2);
	}
	else if(alpha2 > 90 && alpha2 <= 180)
	{
		painter.drawLine(0,0, (int) b2, (int) a2);
	}
	else if(alpha2 	> 180 && alpha2 <= 270)
	{
		painter.drawLine(0,0, (int) b2, (int) a2);
	}
	else if(alpha2 > 270 && alpha2 < 360)
	{
		painter.drawLine(0,0, (int) b2, (int) -a2);
	}
	
	painter.setPen(thick);
	if(alpha3 >= 0 && alpha3 <= 90)
	{
		painter.drawLine(0,0, (int) b3, (int) -a3);
	}
	else if(alpha3 > 90 && alpha3 <= 180)
	{
		painter.drawLine(0,0, (int) b3, (int) a3);
	}
	else if(alpha3 	> 180 && alpha3 <= 270)
	{
		painter.drawLine(0,0, (int) b3, (int) a3);
	}
	else if(alpha3 > 270 && alpha3 < 360)
	{
		painter.drawLine(0,0, (int) b3, (int) -a3);
	}
	
	painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));
	painter.drawEllipse(-1,-1,2,2);
}

void Clock::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		widgX = event->x();
		widgY = event->y();
		this->setCursor(QCursor(Qt::SizeAllCursor));
		mousePressed = true;
		copyright->show();
	}
}

void Clock::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		int newGlobalX, newGlobalY;
		newGlobalX = event->globalX();
		newGlobalY = event->globalY();
		if(newGlobalX-widgX <= wholeWidth-200 && newGlobalY-widgY >= 0)
		{
			this->move((newGlobalX-widgX),(newGlobalY-widgY));
			settings.setValue("Clock/X", newGlobalX-widgX);
			settings.setValue("Clock/Y", newGlobalY-widgY);
		}
		else
		{
			this->move(wholeWidth-200,0);
			settings.setValue("Clock/X", wholeWidth-200);
			settings.setValue("Clock/Y", 0);
		}
		this->setCursor(QCursor(Qt::ArrowCursor));
		mousePressed = false;
		if(contextMenuShown)
		{
			changeColor->hide();
			contextMenuShown = false;
		}
		copyright->hide();
	}
	if(event->button() == Qt::RightButton)
	{
		changeColor->show();
		changeColor->move(event->globalX(),event->globalY());
		contextMenuShown = true;
	}
}

void Clock::mouseMoveEvent(QMouseEvent *event)
{
		if(mousePressed)
		{
			int newGlobalX, newGlobalY;
			newGlobalX = event->globalX();
			newGlobalY = event->globalY();
			this->move((newGlobalX-widgX),(newGlobalY-widgY));
		}
		else
		{
			if((event->x() > 140) && (event->y() < 60))
			{
				close->show();
				closeShown = true;
			}
			else
			{
				if(closeShown)
					close->hide();
			}
		}
}