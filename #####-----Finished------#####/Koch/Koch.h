#include <QWidget>
class QSpinBox;
class QPixmap;
class QImage;
class QPolygonF;
class Koch: public QWidget
{
	Q_OBJECT
public:
	Koch(QWidget *parent = 0);
private:
	int level0;
	long double PI;
	QSpinBox *spin;
	QPixmap image;
	bool drawn;
	QPolygonF poly;
protected:
	void paintEvent(QPaintEvent *event);
	void drawCurve(QPainter &g, long double x1, long double y1, long double angle1, long double sideLength, int level);
private slots:
	void setLevel(int level);
};
