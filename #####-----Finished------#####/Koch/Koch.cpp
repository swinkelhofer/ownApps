#include <QtGui>
#include "Koch.h"
#include <math.h>

Koch::Koch(QWidget *parent):QWidget(parent)
{
	image = QPixmap(800,800);
	drawn = true;
	level0 = 1;
	PI = 3.14159265358979323846;
	setFixedSize(800,800);
	spin =  new QSpinBox(this);
	spin->setRange(1,7);
	setAttribute(Qt::WA_OpaquePaintEvent,true);
	move(QApplication::desktop()->screenGeometry().width()/2-size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2 - 10);
	connect(spin, SIGNAL(valueChanged(int)), this, SLOT(setLevel(int)));
}

void Koch::setLevel(int level)
{
	level0 = level;
	drawn = true;
	update();
}

void Koch::paintEvent(QPaintEvent *event)
{
	QPainter painter(&image);
	if(drawn)
	{
		poly.clear();
		drawn = false;
		painter.setRenderHint(QPainter::Antialiasing, true);
		painter.setWindow(0,0,1000,1000);
		painter.setBrush(Qt::black);
		painter.setPen(Qt::NoPen);
		painter.drawRect(0,0,1000,1000);
		//painter.setPen(Qt::black);
		long double x1=100, y1=750, angle=-PI/3;
		for (int i=0; i<3; i++) 
		{
			drawCurve(painter,x1,y1,angle,800,level0);
			x1 += 800*cos(angle);
			y1 += 800*sin(angle);
			angle += 2*PI/3;
		}
		QRadialGradient radialGrad(QPointF(500, 500), 500);
		radialGrad.setColorAt(0, Qt::white);
		//radialGrad.setColorAt(0.5, QColor(100,100,255));
		//radialGrad.setColorAt(0.6, QColor(100,100,255));
		//radialGrad.setColorAt(0.6, Qt::blue);
		radialGrad.setColorAt(1, QColor(0,0,255));
		painter.setBrush(radialGrad);
		painter.setPen(Qt::NoPen);
		painter.drawPolygon(poly,Qt::WindingFill);
	}
	QPainter glob(this);
	glob.drawPixmap(QRect(0,0,800,800), image, QRect(0,0,800,800));
}

	

void Koch::drawCurve(QPainter &g, long double x1, long double y1, long double angle1, long double sideLength, int level)
{

	long double x2, y2, angle2, x3, y3, angle3, x4, y4;

	if (level>1) {
      // ‹bergebene Teilstrecke in vier neue Teilstrecken zerlegen

		sideLength /= 3;
		level -= 1;

      // erste Teilstrecke
		drawCurve(g, x1,y1, angle1, sideLength, level);

      // zweite Teilstrecke
		x2 = x1+sideLength*cos(angle1);
		y2 = y1+sideLength*sin(angle1);
		angle2 = angle1-PI/3;
		drawCurve(g, x2,y2, angle2, sideLength, level);

      // dritte Teilstrecke
		x3 = x2+sideLength*cos(angle2);
		y3 = y2+sideLength*sin(angle2);
		angle3 = angle1+PI/3;
		drawCurve(g, x3,y3, angle3, sideLength, level);

      // vierte Teilstrecke
		x4 = x3+sideLength*cos(angle3);
		y4 = y3+sideLength*sin(angle3);
      // angle4 = angle1
		drawCurve(g, x4,y4, angle1, sideLength, level);
    }
    else
	{
      // Teilstrecke zeichnen
		//g.drawLine((int)x1,(int)y1,(int)(x1+sideLength*cos(angle1)),(int)(y1+sideLength*sin(angle1)));
		poly << QPointF(x1, y1) << QPointF(x1+sideLength*cos(angle1), y1+sideLength*sin(angle1));
	}
}
