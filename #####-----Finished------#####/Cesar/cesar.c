#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	char string[100];
	scanf("%s", string);
	for(int i = 0; i < strlen(string); ++i)
	{
		string[i] = tolower(string[i]);
	}
	printf("%s\n", string);
	for(int i = 0; i < 26; ++i)
	{
		for(int j = 0; j < strlen(string); ++j)
			string[j] = (((string[j] - 97) + 1) % 26) + 97;
		printf("%s\n", string);
	}
	return 0;
}