/****************************************************************************
** Meta object code from reading C++ file 'Julia.h'
**
** Created: Mon 7. May 17:36:06 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Julia.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Julia.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_JuliaWidget[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      59,   13,   12,   12, 0x0a,
     178,  142,   12,   12, 0x2a,
     285,  256,   12,   12, 0x2a,
     374,  351,   12,   12, 0x2a,
     449,  428,   12,   12, 0x2a,
     510,  491,   12,   12, 0x2a,
     540,   12,   12,   12, 0x0a,
     545,   12,   12,   12, 0x0a,
     551,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_JuliaWidget[] = {
    "JuliaWidget\0\0real_c,imaginary_c,x,y,width,height,apfelmann\0"
    "calc(long double,long double,long double,long double,long double,long "
    "double,bool)\0"
    "real_c,imaginary_c,x,y,width,height\0"
    "calc(long double,long double,long double,long double,long double,long "
    "double)\0"
    "real_c,imaginary_c,x,y,width\0"
    "calc(long double,long double,long double,long double,long double)\0"
    "real_c,imaginary_c,x,y\0"
    "calc(long double,long double,long double,long double)\0"
    "real_c,imaginary_c,x\0"
    "calc(long double,long double,long double)\0"
    "real_c,imaginary_c\0calc(long double,long double)\0"
    "in()\0out()\0updater()\0"
};

const QMetaObject JuliaWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_JuliaWidget,
      qt_meta_data_JuliaWidget, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &JuliaWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *JuliaWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *JuliaWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_JuliaWidget))
        return static_cast<void*>(const_cast< JuliaWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int JuliaWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: calc((*reinterpret_cast< long double(*)>(_a[1])),(*reinterpret_cast< long double(*)>(_a[2])),(*reinterpret_cast< long double(*)>(_a[3])),(*reinterpret_cast< long double(*)>(_a[4])),(*reinterpret_cast< long double(*)>(_a[5])),(*reinterpret_cast< long double(*)>(_a[6])),(*reinterpret_cast< bool(*)>(_a[7]))); break;
        case 1: calc((*reinterpret_cast< long double(*)>(_a[1])),(*reinterpret_cast< long double(*)>(_a[2])),(*reinterpret_cast< long double(*)>(_a[3])),(*reinterpret_cast< long double(*)>(_a[4])),(*reinterpret_cast< long double(*)>(_a[5])),(*reinterpret_cast< long double(*)>(_a[6]))); break;
        case 2: calc((*reinterpret_cast< long double(*)>(_a[1])),(*reinterpret_cast< long double(*)>(_a[2])),(*reinterpret_cast< long double(*)>(_a[3])),(*reinterpret_cast< long double(*)>(_a[4])),(*reinterpret_cast< long double(*)>(_a[5]))); break;
        case 3: calc((*reinterpret_cast< long double(*)>(_a[1])),(*reinterpret_cast< long double(*)>(_a[2])),(*reinterpret_cast< long double(*)>(_a[3])),(*reinterpret_cast< long double(*)>(_a[4]))); break;
        case 4: calc((*reinterpret_cast< long double(*)>(_a[1])),(*reinterpret_cast< long double(*)>(_a[2])),(*reinterpret_cast< long double(*)>(_a[3]))); break;
        case 5: calc((*reinterpret_cast< long double(*)>(_a[1])),(*reinterpret_cast< long double(*)>(_a[2]))); break;
        case 6: in(); break;
        case 7: out(); break;
        case 8: updater(); break;
        default: ;
        }
        _id -= 9;
    }
    return _id;
}
static const uint qt_meta_data_Julia[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       7,    6,    6,    6, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Julia[] = {
    "Julia\0\0draw()\0"
};

const QMetaObject Julia::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Julia,
      qt_meta_data_Julia, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Julia::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Julia::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Julia::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Julia))
        return static_cast<void*>(const_cast< Julia*>(this));
    return QWidget::qt_metacast(_clname);
}

int Julia::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: draw(); break;
        default: ;
        }
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
