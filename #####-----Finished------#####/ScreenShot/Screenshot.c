#include <windows.h>
#include <stdlib.h>

void saveScreenShot(char* fileName)
{
	HWND window = GetDesktopWindow();
	HDC _dc = GetWindowDC(window);
	HDC dc = CreateCompatibleDC(0);

	RECT re;
	GetWindowRect(window, &re);
	int w = re.right, h = re.bottom;
    void* buf = new char[w*h*4];

	HBITMAP bm = CreateCompatibleBitmap(_dc, w, h);
	SelectObject(dc, bm);

	StretchBlt(dc, 0, 0, w, h, _dc, 0, 0, w, h, SRCCOPY);

	void* f = CreateFile(fileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, 0);

	BITMAPFILEHEADER bif;
	bif.bfType = 19778;
	bif.bfSize = w*h*4 + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	bif.bfReserved2 = 0;
	bif.bfReserved2 = 0;
	bif.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	BITMAPINFO infobmp;
	infobmp.bmiHeader.biSize = sizeof(infobmp.bmiHeader);
	infobmp.bmiHeader.biWidth = w;
	infobmp.bmiHeader.biHeight = h;
	infobmp.bmiHeader.biPlanes = 1;
	infobmp.bmiHeader.biBitCount = 32;
	infobmp.bmiHeader.biCompression = 0;
	infobmp.bmiHeader.biSizeImage = 0;
	infobmp.bmiHeader.biXPelsPerMeter = 0;
	infobmp.bmiHeader.biYPelsPerMeter = 0;
	infobmp.bmiHeader.biClrUsed = 0;
	infobmp.bmiHeader.biClrImportant = 0;

	CreateDIBSection(dc, &infobmp, DIB_RGB_COLORS, &buf, 0, 0);
	GetDIBits(dc, bm, 0, h, buf, &infobmp, DIB_RGB_COLORS);

	DWORD r;
	WriteFile(f, &bif, sizeof(bif), &r, NULL);
	WriteFile(f, &infobmp, sizeof(infobmp), &r, NULL);
	WriteFile(f, buf, w*h*4, &r, NULL);

	CloseHandle(f);
	DeleteDC(_dc);
	DeleteDC(dc);
}

int main(int argc, char *argv[])
{
	saveScreenShot((char*)"neu.png");
	return 0;
}