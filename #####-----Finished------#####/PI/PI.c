#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <time.h>
#include <signal.h>
#include <windows.h>
#define RAND_MAX_SQUARE (RAND_MAX*RAND_MAX)
#define PI 3.141592653589793

double montecarlo_pi(int tropfenzahl)
{
	register int innerhalb = 0;
	register double dotx, doty;
	srand(time(0));
	for(int i = 0; i < tropfenzahl; ++i)
	{  
		dotx = (double)rand();
		doty = (double)rand();
		if((double)(dotx*dotx + doty*doty) <= (double)RAND_MAX_SQUARE)
			++innerhalb;
	}
	return (4.0*(double)innerhalb/(double)tropfenzahl);
}

double bbp_pi(int iteration)		//Algorithmus nach Bailey-Borwein-Plouffe
{
	register long double pi = 0;
	register long double save = 0;
	if(iteration > 15)
		iteration = 15;	//Da 1 << 4*k ab k=16 überläuft!!!
	for(int k = 0; k < iteration; ++k)
	{
		save = (long double)(k<<3);
		pi += (((((save + 5.0)*(save + 6.0)*(save + 7.0)) - ((save + 1.0)*(save + 4.0)*(save + 5.5)))*2.0) / ((save + 1.0)*(save + 4.0)*(save + 5.0)*(save + 6.0)*(long double)((__int64)1 << (k<<2))));
	}
	return pi;
}

double fractional_pi(int iteration)
{
	if(iteration < 300000)
		iteration = 300000;
	bool sign = true;
	register long double pi = 0, save = (long double)(iteration + (iteration & 0x1));
	for(long double i = 3; i <= save; i += 2.0)
	{
		if(sign)
		{
			pi += 4.0/((i-1.0)*i*(i+1.0));
			sign = false;
		}
		else
		{
			pi -= 4.0/((i-1.0)*i*(i+1.0));
			sign = true;
		}
	}
	return (pi+3.0);
}

double leibniz_pi(int iteration)
{
	register long double pi = 0;
	bool sign = true;
	for(int i = 0; i < iteration; ++i)
	{
		if(sign)
		{
			pi += 1.0/(long double)((i<<1) + 1);
			sign = false;
		}
		else
		{
			pi -= 1.0/(long double)((i<<1) + 1);
			sign = true;
		}
	}
	return (4.0*pi);
}

double wallis_pi(int iteration)
{
	register long double pi = 2.0;
	int end = iteration + (iteration & 0x1);
	register __int64 save = 0;
	for(int i = 2; i <= end; i+=2)
	{
		save = (__int64)i * (__int64)i;
		pi *= (long double)save/(long double)(save-1);
	}
	return pi;
}

double flaechen_pi(int gitter)
{
	int in = 0;
	for(int i = -gitter; i < gitter; ++i)
	{
		for(int j = -gitter; j < gitter; ++j)
		{
			if((__int64)((__int64)i*(__int64)i + (__int64)j*(__int64)j) <= (__int64)gitter * (__int64)gitter)
				++in;
		}
	}
	return ((long double) in / ((long double) gitter*(long double)gitter));
}

double buffon_pi(int needles)
{
	int intersect = 0;
	double x,y;
	srand(time(0));
	for(int i = 0; i < needles; ++i)
	{
		x = (double)(rand() * 100) / (double)RAND_MAX;	//randomisierter Start-y-Wert der Nadel
		y = sin((double)(rand() % 360) * PI / 180.0);	//End-y-Wert der Nadel mit randomisiertem Winkel
		if((int)(x) != (int)(x+y))
			if(((x+y) >= x && (int)(x+y) & 0x1) || ((x+y) < x && (int)(x) & 0x1))	//wenn Schnittpunkt mit Linie mit 2*Nadellänge
				++intersect;
	}
	return ((double)needles/(double)intersect);
}

double euler(int iterations)
{
	long double b = 1.0 + 1.0/(long double)iterations;
	long double e = 1;
	while(iterations != 0)
	{
		while(!(iterations & 0x1))
		{
			iterations >>= 1;
			b *= b;
		}
		--iterations;
		e *= b;
	}
	return e;
}

double taylor_euler(int iterations)
{
	if(iterations > 65)
		iterations = 65;
	long double e = 1.0;
	__int64 save = 1;
	for(int i = 1; i <= iterations; ++i)
	{
		save *= i;
		e += 1/(long double)save;
	}
	return e;
}

void err(int signal)
{
	if(signal == SIGFPE)
		printf("\n\nCannot divide by zero\n\n");
	if(signal == SIGILL)
		printf("\n\nIllegal instruction occured\n\n");
	if(signal == SIGSEGV)
		printf("\n\nSegmentation fault\n\n");
	if(signal == SIGTERM)
		printf("\n\nProgramm wurde per SIGTERM gezwungen zu beenden\n\n");
	if(signal == SIGABRT)
		printf("\n\nProgramm wurde per SIGABRT gezwungen zu beenden\n\n");
	if(signal == SIGBREAK)
		printf("\n\nProgramm wurde per SIGBREAK gezwungen zu beenden\n\n");
	if(signal == SIGINT)
		printf("\n\nProgramm wurde per SIGINT gezwungen zu beenden\n\n");
}

int main(int argc, char *argv[])
{
	float start = clock();
	signal(SIGFPE, err);
	signal(SIGSEGV, err);
	signal(SIGILL, err);
	signal(SIGABRT, err);
	signal(SIGINT, err);
	signal(SIGTERM, err);
	signal(SIGBREAK, err);
	
	/*printf("%p\t", getenv(argv[1]));
	printf("%p\n\n%s\n\n", getenv(argv[2]), getenv(argv[2]));
	char *pointer = getenv(argv[2]);
	*(pointer) = 'D';
	printf("%p\n\n%s\n\n", pointer, pointer);
	printf("%p\n\n%s\n\n", getenv(argv[2]), getenv(argv[2]));*/
	printf("Die Zahl Montecarlo - PI entspricht in etwa: %.16f\n\n", montecarlo_pi(atoi(argv[1])));
	printf("Die Zahl bbp - PI entspricht in etwa: %.16f\n\n", bbp_pi(atoi(argv[1])));
	printf("Die Zahl frac - PI entspricht in etwa: %.16f\n\n", fractional_pi(atoi(argv[1])));
	printf("Die Zahl wallis - PI entspricht in etwa: %.16f\n\n", wallis_pi(atoi(argv[1])));
	printf("Die Zahl leibniz - PI entspricht in etwa: %.16f\n\n", leibniz_pi(atoi(argv[1])));
	printf("Die Zahl buffon - PI entspricht in etwa: %.16f\n\n", buffon_pi(atoi(argv[1])));
	printf("Die Zahl e entspricht in etwa: %.16f\n\n", euler(atoi(argv[1])));
	printf("Die Zahl taylor-e entspricht in etwa: %.16f\n\n\n\n", taylor_euler(atoi(argv[1])));
	//printf("%x\t", time(0));
	
	//float stop = clock();
	//printf("%x\t", GetCurrentTime());
	//printf("%x\n", clock());
	//int a = 1;
	//a = a/(a^1);
	return 0;
}