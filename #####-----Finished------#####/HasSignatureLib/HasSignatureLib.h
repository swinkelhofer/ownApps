/*
       ^     ^
      /^\   /^\
     // \\ // \\
    @           @
   |             |
   |   (o) (o)   |
    4           2
     \         /
    ___\ | | /___
     ___\| |/___
         (@)      $F0X><


The Function prologues are:

	BOOL HasSignature(const char *)
	BOOL ExportSignature(const char *)

	
You can use the SHA-2-Functions in the library the following way:
	
	HMODULE hmod = LoadLibrary("SHA2Lib.dll");
	HasSignatureFunction HasSignature = (HasSignatureFunction) GetProcAddress(hmod, "HasSignature");
	ExportSignatureFunction ExportSignature = (ExportSignatureFunction) GetProcAddress(hmod, "ExportSignature");
	HasSignature("Datei.exe")
	ExportSignature("Datei.exe")
	� 2011, Sascha Winkelhofer
*/

#include <windows.h>

typedef BOOL (WINAPI *HasSignatureFunction)(const char *);
typedef BOOL (WINAPI *ExportSignatureFunction)(const char *);