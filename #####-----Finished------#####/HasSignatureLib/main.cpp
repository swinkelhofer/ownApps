#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
typedef BOOL (WINAPI *IsSig)(const char *);
typedef BOOL (WINAPI *ExpSig)(const char *);
int main(int argc, char *argv[])
{
	HMODULE lib = LoadLibrary(TEXT("HasSignatureLib.dll"));
	if(lib == NULL)
	{
		fprintf(stderr, "Error while loading HasSignatureLib.dll");
		return 0;
	}
	IsSig proc = (IsSig)GetProcAddress(lib, "HasSignature");
	ExpSig Export = (ExpSig)GetProcAddress(lib, "ExportSignature");
	if(proc == NULL)
	{
		fprintf(stderr, "Error while loading Function HasSignature");
		return 0;
	}
	if(Export == NULL)
	{
		fprintf(stderr, "Error while loading Function ExportSignature");
		return 0;
	}
	if(proc(argv[1]))
		printf("%s ist signiert", argv[1]);
	else
		printf("%s ist nicht signiert", argv[1]);
	
	Export(argv[1]);
	
	FreeLibrary(lib);
	return 0;
}