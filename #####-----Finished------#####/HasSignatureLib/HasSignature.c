#include <stdlib.h>
#include <windows.h>
#include <stdio.h>
#include <string.h>

#define DllExport extern "C" __declspec(dllexport)

BOOL WINAPI DllMain(HINSTANCE hDllInst, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason)
	{
        case DLL_PROCESS_ATTACH:
            break;
        case DLL_PROCESS_DETACH:
            break;
		case DLL_THREAD_ATTACH:
			break;
		case DLL_THREAD_DETACH:
			break;
    }
    return TRUE;
}

DllExport BOOL HasSignature(const char *moduleName)
{
	FILE *in = fopen(moduleName, "rb");
	if(in == NULL)
	{
		fprintf(stderr, "Error while opening module. Maybe it does not exist.\n");
		exit(0);
	}
	short size_pe = 0, size_sig = 0;
	int pointer_sig_segment = 0, sig_pos = 0;
	fseek(in, 60, SEEK_SET);
	fread(&pointer_sig_segment, 2, 1, in);
	pointer_sig_segment += 152;
	fseek(in, pointer_sig_segment, SEEK_SET);
	fread(&sig_pos, 4, 1, in);
	fread(&size_pe, 2, 1, in);
	fseek(in, sig_pos, SEEK_SET);
	fread(&size_sig, 2, 1, in);
	fclose(in);
	if(size_sig != size_pe)
		return FALSE;
	else
		return TRUE;
}

DllExport BOOL ExportSignature(const char *moduleName)
{
	FILE *in = fopen(moduleName, "rb");
	if(in == NULL)
	{
		fprintf(stderr, "Error while opening module. Maybe it does not exist.\n");
		exit(0);
	}
	short size_pe = 0, size_sig = 0;
	int pointer_sig_segment = 0, sig_pos = 0;
	fseek(in, 60, SEEK_SET);
	fread(&pointer_sig_segment, 2, 1, in);
	pointer_sig_segment += 152;
	fseek(in, pointer_sig_segment, SEEK_SET);
	fread(&sig_pos, 4, 1, in);
	fread(&size_pe, 2, 1, in);
	fseek(in, sig_pos, SEEK_SET);
	fread(&size_sig, 2, 1, in);
	if(size_sig != size_pe)
		return FALSE;
	fseek(in, sig_pos + 0x9d, SEEK_SET);
	
	short word = 0;
	int pos = 0;
	fread(&word, 2, 1, in);
	while(word != 0xffff8230 && !feof(in))
	{
		word = 0;
		++pos;
		fseek(in, sig_pos + 0x9d + pos, SEEK_SET);
		fread(&word, 2, 1, in);
	}
	fseek(in, sig_pos + 0x9d + pos, SEEK_SET);
	unsigned char byte;
	int length = strlen(moduleName);
	char outfile[length + 5];
	for(int i = 0; i < length; ++i)
		outfile[i] = moduleName[i];
	outfile[length] = '.';
	outfile[length+1] = 'c';
	outfile[length+2] = 'r';
	outfile[length+3] = 't';
	outfile[length+4] = '\0';
	FILE *out = fopen(outfile, "w+b");
	if(out == NULL)
	{
		fprintf(stderr, "Error while opening output file.\n");
		exit(0);
	}
	
	for(int i = 0; i < size_sig - 0x9d - pos; ++i)
	{
		fread(&byte, 1, 1, in);
		fwrite(&byte, 1, 1, out);
	}
	fclose(in);
	fclose(out);
	return TRUE;
}