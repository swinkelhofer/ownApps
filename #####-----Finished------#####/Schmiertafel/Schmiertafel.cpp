#include <QtGui>
#include "Schmiertafel.h"
#include <windows.h>
#define SchwammSize_x 32
#define SchwammSize_y 24


Schmiertafel::Schmiertafel(QWidget *parent):QWidget(parent)
{
	pix = QPixmap(":/Schmiertafel.png").scaled(800, 600, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	original = QPixmap(":/Schmiertafel.png").scaled(800, 600, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	drawable = QRect(36, 36, 728, 528);
	setWindowFlags(Qt::FramelessWindowHint);
	setFixedSize(800,600);
	mouseButton = 0;
	isSchwamm = false;
	setMouseTracking(true);
	
	
	menu = new ToolBar;
	connect(menu, SIGNAL(movedAbout(int, int)), this, SLOT(moveTo(int, int)));
	connect(menu, SIGNAL(clr()), this, SLOT(clear()));
	connect(menu, SIGNAL(ld()), this, SLOT(load()));
	connect(menu, SIGNAL(saveImg()), this, SLOT(save()));
	connect(menu, SIGNAL(closed()), this, SLOT(close()));
	
	//menu->show();
	/*
		0:	MouseUp
		1:	LeftDown
		2:	RightDown
	*/
	//painter = new QPainter(this);
}

void Schmiertafel::clear()
{
	pix = original;
	update();
}

void Schmiertafel::save()
{
	QPixmap main = QPixmap::grabWidget(this);
	QString fileName = QFileDialog::getSaveFileName(this, "Speichern", 0, "PNG-Bild (*.png)");
	if(!fileName.isEmpty())
		main.save(fileName, "PNG", 100);
	else
		MessageBox(0, L"Fehler beim Speichern", L"Fehler", 0);
}

void Schmiertafel::load()
{
	QString fileName = QFileDialog::getOpenFileName(this, "Laden", 0, "PNG-Bild (*.png)");
	if(!fileName.isEmpty())
	{
		pix.load(fileName, "PNG", Qt::AutoColor);
		update();
	}
	else
		MessageBox(0, L"Fehler beim Laden", L"Fehler", 0);
}

void Schmiertafel::paintEvent(QPaintEvent *event)
{
	//QPainter painter(this);
	//painter.setBrush(QBrush(QColor(8, 40, 16)));
	//painter.setPen(Qt::NoPen);
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	
	painter.drawPixmap(0,0,800,600, pix, 0, 0, 800, 600);
}

void Schmiertafel::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		mouseButton = 1;
		if(drawable.contains(event->pos()))
		{
			QPainter painter(&pix);
			painter.setRenderHint(QPainter::Antialiasing);
			painter.setBrush(QBrush(Qt::white));
			painter.setPen(QPen(Qt::white, 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
			
			painter.drawPoint(event->pos());
			update();
		}
	}
	if(event->button() == Qt::RightButton)
	{
		mouseButton = 2;
		isSchwamm = true;
		//setCursor(QCursor(QPixmap(":/Schwamm.png").scaled((SchwammSize_x/2), (SchwammSize_y/2), Qt::IgnoreAspectRatio, Qt::SmoothTransformation)));
		if(drawable.contains(event->pos()))
		{
			QPainter painter(&pix);
			painter.setRenderHint(QPainter::Antialiasing);
			//painter.setBrush(QBrush(Qt::white));
			//painter.setPen(QPen(Qt::white, 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
			painter.drawPixmap(event->pos().x()-(SchwammSize_x/2), event->pos().y()-(SchwammSize_y/2), SchwammSize_x, SchwammSize_y, original, event->pos().x()-(SchwammSize_x/2), event->pos().y()-(SchwammSize_y/2), SchwammSize_x, SchwammSize_y);
			//painter.drawPoint(event->pos());
			update();
		}
	}
}
	
	
void Schmiertafel::mouseReleaseEvent(QMouseEvent *event)
{
	mouseButton = 0;
	if(isSchwamm)
	{
		isSchwamm = false;
		//setCursor(Qt::ArrowCursor);
	}		
}

void Schmiertafel::mouseMoveEvent(QMouseEvent *event)
{
	if(drawable.contains(event->pos()) && mouseButton == 1)
	{
		QPainter painter(&pix);
		painter.setRenderHint(QPainter::Antialiasing);
		painter.setBrush(QBrush(Qt::white));
		painter.setPen(QPen(Qt::white, 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		
		painter.drawPoint(event->pos());
		update();
	}
	else if(drawable.contains(event->pos()) && mouseButton == 2)
	{
		QPainter painter(&pix);
		painter.setRenderHint(QPainter::Antialiasing);
		//painter.setBrush(QBrush(Qt::white));
		//painter.setPen(QPen(Qt::white, 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.drawPixmap(event->pos().x()-(SchwammSize_x/2), event->pos().y()-(SchwammSize_y/2), SchwammSize_x, SchwammSize_y, original, event->pos().x()-(SchwammSize_x/2), event->pos().y()-(SchwammSize_y/2), SchwammSize_x, SchwammSize_y);
		//painter.drawPoint(event->pos());
		update();
	}
	if(QRect(0, 0, 800, 36).contains(event->pos()))
	{
		menu->show();
	}
}

void Schmiertafel::resizeEvent(QResizeEvent *event)
{
	menu->move(this->x(), this->y()-40);
}

void Schmiertafel::moveTo(int x, int y)
{
	move(this->x() + x, this->y() + y);
	menu->move(this->x(), this->y()-40);
}

ToolBar::ToolBar(QWidget *parent):QWidget(parent)
{
	mouseDown = false;
	clear = new QPushButton("Reinigen");
	save = new QPushButton("Speichern");
	load = new QPushButton("Laden");
	close = new QPushButton("x");
	close->setFixedWidth(30);
	QHBoxLayout *main = new QHBoxLayout;
	main->addWidget(clear);
	main->addWidget(save);
	main->addWidget(load);
	main->addWidget(close);
	setLayout(main);
	setWindowFlags(Qt::FramelessWindowHint | Qt::SubWindow);
	connect(save, SIGNAL(clicked()), this, SIGNAL(saveImg()));
	connect(clear, SIGNAL(clicked()), this, SIGNAL(clr()));
	connect(load, SIGNAL(clicked()), this, SIGNAL(ld()));
	connect(close, SIGNAL(clicked()), this, SIGNAL(closed()));
	setFixedSize(800,40);
	QTimer *timer = new QTimer;
	timer->start(2000);
	connect(timer, SIGNAL(timeout()), this, SLOT(check()));
}

void ToolBar::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QPixmap pix = QPixmap(":/ToolBar.png").scaled(800, 40, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	painter.drawPixmap(0,0, 800, 40, pix, 0, 0, 800, 40);
}

void ToolBar::check()
{
	if(!underMouse())
		hide();
}

void ToolBar::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		cur = event->globalPos();
		mouseDown = true;
	}
}


void ToolBar::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton && mouseDown)
	{
		emit movedAbout(event->globalPos().x() - cur.x(), event->globalPos().y() - cur.y());
	}
}

void ToolBar::mouseMoveEvent(QMouseEvent *event)
{
	if(mouseDown)
	{
		emit movedAbout(event->globalPos().x() - cur.x(), event->globalPos().y() - cur.y());
		cur = event->globalPos();
	}
}