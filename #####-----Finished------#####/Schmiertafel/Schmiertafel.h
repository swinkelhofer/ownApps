#include <QWidget>
class QPixmap;
class QRect;
class QPushButton;
class QPoint;
class ToolBar: public QWidget
{
	Q_OBJECT
public:
	ToolBar(QWidget *parent = 0);
private:
	QPushButton *save, *clear, *load, *close;
	QPoint cur;
	bool mouseDown;
signals:
	void saveImg();
	void clr();
	void ld();
	void closed();
	void movedAbout(int x, int y);
private slots:
	void check();
protected slots:
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void paintEvent(QPaintEvent *event);
};

class Schmiertafel: public QWidget
{
	Q_OBJECT
public:
	Schmiertafel(QWidget *parent = 0);
	//QPainter *painter;
protected slots:
	void paintEvent(QPaintEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void resizeEvent(QResizeEvent*event);
	void moveTo(int x, int y);
	void clear();
	void save();
	void load();
private:
	ToolBar *menu;
	QRect drawable;
	QPixmap pix, original;
	short mouseButton;
	bool isSchwamm;
};
