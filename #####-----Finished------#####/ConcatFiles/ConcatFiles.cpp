#include <stdio.h>
#include <stdlib.h>
#include <sys\stat.h>
#include <sys/time.h>

int main(int argc, char *argv[])
{
	float start, stop;
	if(argc < 3)
	{
		printf("Usage: ConcatFiles <OutDatei> <Datei 1> <Datei 2> [...<Datei n>]\n\n");
		exit(0);
	}
	FILE *in;
	FILE *out = fopen(argv[1], "w+b");
	if(out == NULL)
	{
		printf("Error occured in \"%s\"\n\n", argv[1]);
		exit(0);
	}
	start = clock();
	for(int i = 2; i < argc; ++i)
	{
		in = fopen(argv[i], "rb");
		fseek(in, 0, SEEK_SET);
		struct _stati64 attribut;
		_stati64(argv[i], &attribut);
		__int64 loop = (__int64) attribut.st_size / (__int64) 1024;
		int rest = (__int64) attribut.st_size % (__int64) 1024;
		if(in == NULL)
		{
			printf("Error occured in \"%s\"\n\n", argv[i]);
			exit(0);
		}
		if(attribut.st_size >= 1048576)
		{
			char BYTE[1048576];
			for(__int64 i = 0; i < loop; ++i)
			{
				fread((char*)BYTE, 1, 1048576, in);
				fwrite((char*)BYTE, 1, 1048576, out);
			}
		}
		else
		{
			char BYTE[1024];
			for(__int64 i = 0; i < loop; ++i)
			{
				fread((char*)BYTE, 1, 1024, in);
				fwrite((char*)BYTE, 1, 1024, out);
			}
		}
		for(int i = 0; i < rest; ++i)
		{
			char BYTE[2];
			fread((char*)BYTE, 1, 1, in);
			fwrite((char*)BYTE, 1, 1, out);
		}
		fclose(in);
	}
	stop = clock();
	struct _stati64 attribut;
	_stati64(argv[1], &attribut);
	int MB = (int)((__int64) attribut.st_size / (__int64) 1048576);
	printf("Files successful concatted in %f s with %lf MB/s", (stop - start)/1000, (double)((double)MB/(((double)stop - (double)start)/(double)1000)));
	return 0;
}