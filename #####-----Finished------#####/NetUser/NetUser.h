#include <QDialog>
class QLineEdit;
class QPushButton;
class QLabel;
class netuser:public QDialog
{
	Q_OBJECT
public:
	netuser(QWidget *parent = 0);
private:
	QLabel *lab1,*lab2, *lab3;
	QLineEdit *pw,*pwval, *user;
	QPushButton *create;
private slots:
	void account();
};