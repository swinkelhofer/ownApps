#include <QtGui>
#include "NetUser.h"
netuser::netuser(QWidget *parent):QDialog(parent)
{
	pw = new QLineEdit;
	user = new QLineEdit;
	pw->setEchoMode(QLineEdit::Password);
	pwval = new QLineEdit;
	pwval->setEchoMode(QLineEdit::Password);
	lab1 = new QLabel("Benutzername:");
	lab2 = new QLabel("Passwort:");
	lab3 = new QLabel("Passwort best�tigen:");
	create = new QPushButton("Erstellen");
	QGridLayout *main = new QGridLayout;
	main->addWidget(lab1,0,0);
	main->addWidget(lab2,1,0);
	main->addWidget(lab3,2,0);
	main->addWidget(user,0,1);
	main->addWidget(pw,1,1);
	main->addWidget(pwval,2,1);
	main->addWidget(create,3,1);
	setLayout(main);
	connect(create, SIGNAL(clicked()), this, SLOT(account()));
}

void netuser::account()
{
	QString pass = pw->text();
	QString passval = pwval->text();
	QString uname = user->text();
	if(passval != pass)
	{
		QMessageBox::warning(this, "NetUser",
                                    "Passw�rter stimmen nicht �berein!!!",
                                    QMessageBox::Ok, //| QMessageBox::Default,
                                    QMessageBox::NoButton); //| QMessageBox::Escape);
		return;
	}
	QStringList flags;
	flags << QString("user") << QString("/ADD") << uname << pass;
	QProcess *proc = new QProcess;
	proc->start("net", flags);
	if(!proc->waitForStarted())
		return;
	pw->setText("");
	user->setText("");
	pwval->setText("");
}