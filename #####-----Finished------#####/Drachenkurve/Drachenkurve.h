#include <QWidget>
class QPixmap;
class QString;
class Drachenkurve: public QWidget
{
	Q_OBJECT
public:
	Drachenkurve(QWidget *parent = 0);
	void iterate(int iterations);
private:
	QString curveString;
	QPixmap image;
	bool drawn;
protected:
	void paintEvent(QPaintEvent *event);
};
