#include "Drachenkurve.h"
#include <QtGui>

Drachenkurve::Drachenkurve(QWidget *parent):QWidget(parent)
{
	curveString = QString("");
	setFixedSize(800,800);
	drawn = true;
	move(QApplication::desktop()->screenGeometry().width()/2-size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2 - 10);
	image = QPixmap(800,800);
}

void Drachenkurve::iterate(int iterations)
{
	QString inv_curveString;
	for(int i = 0; i < iterations; i++)
	{
		for(int j = curveString.length()-1; j >= 0; j--)
		{
			if(curveString.at(j) == QChar('R'))
			{
				inv_curveString.append("L");
			}
			else if(curveString.at(j) == QChar('L'))
			{
				inv_curveString.append("R");
			}
		}
		curveString += QString("R");
		curveString += inv_curveString;
		inv_curveString = QString("");
	}
	drawn = true;
	update();
}

void Drachenkurve::paintEvent(QPaintEvent *event)
{
	if(drawn)
	{
		drawn = false;
		QPainter painter(&image);
		painter.setWindow(0, 0, 2000, 2000);
		painter.setBackgroundMode(Qt::TransparentMode);
		painter.setPen(Qt::NoPen);
		QRadialGradient radialGrad(QPointF(1000, 1000), 1000);
		radialGrad.setColorAt(0, Qt::darkRed);
		radialGrad.setColorAt(0.5, Qt::red);
		radialGrad.setColorAt(1, Qt::black);
	
		painter.setBrush(radialGrad);
		painter.drawRect(0,0,2000,2000);
		painter.translate(800, 550);
		painter.setPen(Qt::black);
		QPoint current(0,-1);
		int orientation = 0;
		/*
		0 = up
		1 = right
		2 = down
		3 = left
		*/
		painter.drawLine(QPoint(0,0), current);
		
		for(int i = 0; i < curveString.length(); i++)
		{
			if(orientation == 0)
			{
				if(curveString.at(i) == QChar('R'))
				{
					painter.drawLine(current, current + QPoint(1,0));
					orientation = 1;
					current = current + QPoint(1,0);
				}
				if(curveString.at(i) == QChar('L'))
				{
					painter.drawLine(current, current + QPoint(-1,0));
					orientation = 3;
					current = current + QPoint(-1,0);
				}
			}
			else if(orientation == 1)
			{
				if(curveString.at(i) == QChar('R'))
				{
					painter.drawLine(current, current + QPoint(0,1));
					orientation = 2;
					current = current + QPoint(0,1);
				}	
				if(curveString.at(i) == QChar('L'))
				{
					painter.drawLine(current, current + QPoint(0,-1));
					orientation = 0;
					current = current + QPoint(0,-1);
				}
			}
			else if(orientation == 2)
			{
				if(curveString.at(i) == QChar('R'))
				{
					painter.drawLine(current, current + QPoint(-1,0));
					orientation = 3;
					current = current + QPoint(-1,0);
				}
				if(curveString.at(i) == QChar('L'))
				{
					painter.drawLine(current, current + QPoint(1,0));
					orientation = 1;
					current = current + QPoint(1,0);
				}
			}
			else if(orientation == 3)
			{
				if(curveString.at(i) == QChar('R'))
				{
					painter.drawLine(current, current + QPoint(0,-1));
					orientation = 0;
					current = current + QPoint(0,-1);
				}
				if(curveString.at(i) == QChar('L'))
				{
					painter.drawLine(current, current + QPoint(0,1));
					orientation = 2;
					current = current + QPoint(0,1);
				}
			}
		}
	}
	QPainter glob(this);
	glob.drawPixmap(QRect(0,0,800,800), image, QRect(0,0,800,800));
}