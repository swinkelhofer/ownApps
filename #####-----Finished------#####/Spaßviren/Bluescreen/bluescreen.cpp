#include <QtGui>
#include "bluescreen.h"

bluescreen::bluescreen(QWidget *parent):QDialog(parent)
{
	QDesktopWidget *desktop = QApplication::desktop();
    width = desktop->width();
    height = desktop->height();
	setWindowFlags(Qt::FramelessWindowHint);
	setGeometry(0,0,width, height);
}

void bluescreen::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setBrush(QBrush(QColor(12,52,137)));
	painter.drawRect(0,0,width,height);
	painter.drawPixmap(QPoint(width-237,0),QPixmap(":/WindowsLogo.bmp"));
	painter.setFont(QFont("Lucida Console", 16));
	painter.setPen(QPen(Qt::white));
	painter.drawText(QRect(10,80,width, height-80),
 " Error appears in #40107a: $eax undefined Error \n\n \
  #401031:	31 c0                	xor    eax,eax \n \
  #401033:	89 44 24 04          	mov    DWORD PTR [esp+4],eax \n \
  #401037:	e8 64 67 00 00       	call   0x4077a0\n \
  #40103c:	83 f8 01             	cmp    eax,0x1\n \
  #40103f:	74 6c                	je     0x4010ad\n \
  #401041:	85 c0                	test   eax,eax\n \
  #401043:	74 2a                	je     0x40106f\n \
  #401045:	c7 04 24 08 00 00 00 	mov    DWORD PTR [esp],0x8\n \
  #40104c:	ff d0                	call   eax\n \
  #40104e:	bb ff ff ff ff       	mov    ebx,0xffffffff\n \
  #401053:	89 d8                	mov    eax,ebx\n \
  #401055:	8b 75 fc             	mov    esi,DWORD PTR [ebp-4]\n \
  #401058:	8b 5d f8             	mov    ebx,DWORD PTR [ebp-8]\n \
  #40105b:	89 ec                	mov    esp,ebp\n \
  #40105d:	5d                   	pop    ebp\n \
  #40105e:	c2 04 00             	ret    0x4\n \
  #401061:	3d 93 00 00 c0       	cmp    eax,0xc0000093\n \
  #401066:	74 bd                	je     0x401025\n \
  #401068:	3d 94 00 00 c0       	cmp    eax,0xc0000094\n \
  #40106d:	74 bb                	je     0x40102a\n \
  #40106f:	89 d8                	mov    eax,ebx\n \
  #401071:	8b 75 fc             	mov    esi,DWORD PTR [ebp-4]		\n \
  #401074:	8b 5d f8             	mov    ebx,DWORD PTR [ebp-8]\n \
  #401077:	89 ec                	mov    esp,ebp\n \
  #401079:	5d                   	pop    ebp\n \
  #40107a:	c2 04 00             	ret    0x4\n \
  #40107d:	8d 76 00             	lea    esi,[esi]\n \
  #401080:	3d 05 00 00 c0       	cmp    eax,0xc0000005\n \
  #401085:	75 e8                	jne    0x40106f\n \
  #401087:	c7 04 24 0b 00 00 00 	mov    DWORD PTR [esp],0xb\n \
  #40108e:	31 f6                	xor    esi,esi\n \
  #401090:	89 74 24 04          	mov    DWORD PTR [esp+4],esi\n \
  #401094:	e8 07 67 00 00       	call   0x4077a0\n \
  #401099:	83 f8 01             	cmp    eax,0x1\n \
  #40109c:	74 34                	je     0x4010d2						 \n \
  #40109e:	85 c0                	test   eax,eax\n \
  #4010a0:	74 cd                	je     0x40106f\n \
  #4010a2:	c7 04 24 0b 00 00 00 	mov    DWORD PTR [esp],0xb\n");
}