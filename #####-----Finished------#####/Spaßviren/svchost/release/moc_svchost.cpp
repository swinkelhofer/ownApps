/****************************************************************************
** Meta object code from reading C++ file 'svchost.h'
**
** Created: Di 2. Nov 19:53:25 2010
**      by: The Qt Meta Object Compiler version 59 (Qt 4.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../svchost.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'svchost.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

static const uint qt_meta_data_svchost[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      29,    9,    8,    8, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_svchost[] = {
    "svchost\0\0exitCode,exitStatus\0start(int,QProcess::ExitStatus)\0"
};

const QMetaObject svchost::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_svchost,
      qt_meta_data_svchost, 0 }
};

const QMetaObject *svchost::metaObject() const
{
    return &staticMetaObject;
}

void *svchost::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_svchost))
	return static_cast<void*>(const_cast<svchost*>(this));
    return QDialog::qt_metacast(_clname);
}

int svchost::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: start(*reinterpret_cast< int(*)>(_a[1]),*reinterpret_cast< QProcess::ExitStatus(*)>(_a[2])); break;
        }
        _id -= 1;
    }
    return _id;
}
