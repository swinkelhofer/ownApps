#include <QDialog>
class QLabel;
class QImage;
class QPushButton;
class QWidget;
class QTimer;
class Boo:public QDialog
{
	Q_OBJECT
public:
	Boo(QWidget *parent = 0);
private:
	QLabel *lab;
	int i;
	QImage *image;
	QPushButton *confirm;
	QTimer *shut;
	QTimer *open;
	QTimer *color;
	void mouseMoveEvent(QMouseEvent *event);
private slots:
	void opens();
	void closes();
	void startnew();
	void setColor();

};