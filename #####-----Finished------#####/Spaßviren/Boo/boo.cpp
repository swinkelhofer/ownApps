#include <QtGui>
#include "boo.h"
Boo::Boo(QWidget *parent):QDialog(parent)
{
	QTimer::singleShot(5000, this, SLOT(show()));
	shut = new QTimer(this);
	open = new QTimer(this);
	setWindowFlags(Qt::FramelessWindowHint);
	setWindowOpacity(0.7);
	confirm = new QPushButton("Beenden");
	lab = new QLabel("Boo");
	lab->setFont(QFont("Arial", 200));
	QPalette pal;
	pal.setColor(QPalette::Window, Qt::white);
	pal.setColor(QPalette::WindowText, Qt::blue);
	//lab->setPalette(pal);
	setPalette(pal);
	QHBoxLayout *down = new QHBoxLayout;
	down->addStretch();
	down->addWidget(confirm);
	down->addStretch();
	QVBoxLayout *main = new QVBoxLayout;
	main->addWidget(lab);
	main->addLayout(down);
	setLayout(main);
	setMouseTracking(true);
	//shut->start(300);
	//open->start(3000);
	i=1;
	shut->start(30000);
	color = new QTimer(this);
	color->start(500);
	connect(shut, SIGNAL(timeout()), this, SLOT(closes()));
	connect(open, SIGNAL(timeout()), this, SLOT(opens()));
	connect(color, SIGNAL(timeout()), this, SLOT(setColor()));
	hide();

}

void Boo::setColor()
{
	QPalette pal;
	int r = rand() % 256;
	int g = rand() % 256;
	int b = rand() % 256;
	pal.setColor(QPalette::Window, Qt::white);
	pal.setColor(QPalette::WindowText, QColor(r,g,b));
	setPalette(pal);
}

void Boo::mouseMoveEvent(QMouseEvent *event)
{	
	QTimer timer;
	if(i > 2)
	{
		hide();
		timer.stop();
	}
	else
	{
	if((event->globalX() <= x()+width()+100) && (event->globalX() >= x()) && (event->globalY() <= y()+height()) && (event->globalY() >= y()))
	{
		hide();
		timer.singleShot(3000, this, SLOT(show()));
		i++;
	}
	}
}


void Boo::opens()
{
	open->stop();
	show();
	color->start(500);
	shut->start(30000);
	i = 1;

}
void Boo::closes()
{
	shut->stop();
	hide();
	color->stop();
	open->start(300000);
}

void Boo::startnew()
{
	QProcess proc;
	proc.start("Boo.exe", NULL);
	if(!proc.waitForStarted())
		return;
}