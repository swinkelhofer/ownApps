#include <QWidget>
class QLineEdit;
class QPushButton;
class QListView;
class QStringList;
class QStringListModel;
class QDialog;
class QToolButton;
class Compiler : public QWidget
{
	Q_OBJECT
public:
	Compiler(QWidget *parent=0);
private:
	QLineEdit *appName, *ed, *cmdLine;
	QPushButton *compile, *remove, *load, *save, *but;//, *filecomp;
	QListView *list;
	QStringList fileName;
	QStringListModel *model;
	QString arglist;
	QString savefile;
	QString compilerpath;
	QToolButton *extButton;
	QWidget *extWidget;
	int height;
	//QDialog *shown;
private slots:
	void open();
	void run();
	void saven();
	void del();
	void startApp();
	void extensions();
protected:
	void resizeEvent(QResizeEvent *event);
	//void setComp();
	//void Comp();
	//void setfilecomp();
};