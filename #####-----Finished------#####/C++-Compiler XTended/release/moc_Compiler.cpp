/****************************************************************************
** Meta object code from reading C++ file 'Compiler.h'
**
** Created: Tue 15. May 17:45:27 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Compiler.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Compiler.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Compiler[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      17,    9,    9,    9, 0x08,
      23,    9,    9,    9, 0x08,
      31,    9,    9,    9, 0x08,
      37,    9,    9,    9, 0x08,
      48,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Compiler[] = {
    "Compiler\0\0open()\0run()\0saven()\0del()\0"
    "startApp()\0extensions()\0"
};

const QMetaObject Compiler::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Compiler,
      qt_meta_data_Compiler, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Compiler::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Compiler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Compiler::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Compiler))
        return static_cast<void*>(const_cast< Compiler*>(this));
    return QWidget::qt_metacast(_clname);
}

int Compiler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: open(); break;
        case 1: run(); break;
        case 2: saven(); break;
        case 3: del(); break;
        case 4: startApp(); break;
        case 5: extensions(); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
