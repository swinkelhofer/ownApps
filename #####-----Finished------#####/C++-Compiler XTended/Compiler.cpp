#include <QtGui>
#include "Compiler.h"
#include <process.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

Compiler::Compiler(QWidget *parent):QWidget(parent)
{
	/*shown = new QDialog;
	shown->setModal(true);
	ed = new QLineEdit("C:\\MinGW\\bin\\g++ ");
	but = new QPushButton("OK");
	//filecomp = new QPushButton("Durchsuchen");
	QHBoxLayout *lay =new QHBoxLayout;
	lay->addWidget(ed);
	//lay->addWidget(filecomp);
	lay->addWidget(but);
	shown->setLayout(lay);*/
	move(QApplication::desktop()->screenGeometry().width()/2-size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2 - 10);
	
	compilerpath = QDir::currentPath() + QString("/bin/bin/g++.exe");
	setWindowTitle("C++-Compiler");
	//QWidget *widg = new QWidget;
	list = new QListView();
	appName = new QLineEdit;
	appName->setDisabled(true);
	compile = new QPushButton("Projekt erstellen");
	compile->setFont(QFont(font().family(), font().pointSize() + 2, QFont::Bold, false));
	remove = new QPushButton("Quelldatei entfernen");
	load = new QPushButton("Quelldatei hinzufügen");
	save = new QPushButton("Ziel suchen");
	model = new QStringListModel(this);
	QVBoxLayout *left = new QVBoxLayout;
	QHBoxLayout *down = new QHBoxLayout;
	QVBoxLayout *right = new QVBoxLayout;
	left->addWidget(new QLabel("Quelldateien"));
	left->addWidget(list);
	left->addSpacing(10);
	left->addWidget(new QLabel("Name der Anwendung"));
	down->addWidget(appName);
	down->addWidget(save);
	//left->addLayout(down);
	right->addWidget(load);
	right->addWidget(remove);
	right->addStretch();
	//right->addWidget(compile);
	QHBoxLayout *main = new QHBoxLayout;
	main->addLayout(left);
	main->addLayout(right);
	extButton = new QToolButton;
	extButton->setArrowType(Qt::DownArrow);
	extButton->setText("Mehr");
	extButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	QVBoxLayout *mostMain = new QVBoxLayout;
	mostMain->addLayout(main);
	mostMain->addLayout(down);
	
	
	cmdLine = new QLineEdit;
	
	QHBoxLayout *extShower = new QHBoxLayout;
	extShower->addStretch();
	extShower->addWidget(extButton);
	QVBoxLayout *ext = new QVBoxLayout;
	//ext->addLayout(extShower);
	ext->addWidget(new QLabel("Benutzerdefinierte Kommandozeilen-Optionen:"));
	ext->addWidget(cmdLine);
	
	
	extWidget = new QWidget;
	extWidget->setLayout(ext);
	extWidget->hide();
	
	mostMain->addLayout(extShower);
	mostMain->addWidget(extWidget);
	mostMain->addSpacing(10);
	mostMain->addWidget(compile);
	connect(load, SIGNAL(clicked()), this, SLOT(open()));
	connect(compile, SIGNAL(clicked()), this, SLOT(run()));
	connect(save, SIGNAL(clicked()), this, SLOT(saven()));
	connect(remove, SIGNAL(clicked()), this, SLOT(del()));
	connect(extButton, SIGNAL(clicked()), this, SLOT(extensions()));
	//connect(filecomp, SIGNAL(clicked()), this, SLOT(setfilecomp()));
	setLayout(mostMain);
	//setCentralWidget(widg);
	//QMenu *bar = new QMenu;
	//bar = menuBar()->addMenu("Optionen");
	//QAction *ac = new QAction("Compiler festlegen", this);
	//connect(ac, SIGNAL(triggered()), this, SLOT(Comp()));
	//bar->addAction(ac);
}

void Compiler::extensions()
{
	if(extWidget->isVisible())
	{
		extWidget->hide();
		extButton->setText("Mehr");
		extButton->setArrowType(Qt::DownArrow);
		adjustSize();
	}
	else
	{
		extWidget->show();
		extButton->setText("Weniger");
		extButton->setArrowType(Qt::UpArrow);
		adjustSize();
	}
}

/*void Compiler::setfilecomp()
{
	QString str = QFileDialog::getOpenFileName(this, "Compiler", "C:\\MinGW\\bin", "Anwendung (*.exe)");
	if(!str.isEmpty())
	{
		ed->setText(str);
	}
	else
		ed->setText("C:\\MinGW\\bin\\g++ ");
}*/

/*void Compiler::Comp()
{
	
	shown->show();
	connect(but, SIGNAL(clicked()), this, SLOT(setComp()));
}

void Compiler::setComp()
{
	if(!ed->text().isEmpty())
	{
		compilerpath = ed->text();
	}
	else
		compilerpath = "C:\\MinGW\\bin\\g++ ";
	shown->close();
}*/
void Compiler::resizeEvent(QResizeEvent *event)
{
	appName->setFixedWidth(list->rect().width());
	save->setFixedWidth(load->rect().width());
	setFixedWidth(this->width());
	//move(QApplication::desktop()->screenGeometry().width()/2-size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2 - 10);
	list->setFixedHeight(list->height());
}

void Compiler::open()
{
	//arglist = QString("");
	fileName += QFileDialog::getOpenFileNames(this, "Quelldateien", 0, "C++-Quelldateien (*.cpp *.c *.cxx *.cc)");
	fileName.removeDuplicates();
	model->setStringList(fileName);
	list->setModel(model);
	/*for (int i = 0; i < fileName.size(); i++)
	{
		arglist = arglist + fileName[i] + QString(" ");
	}*/
}

void Compiler::run()
{
	for(int i = 0; i < model->stringList().size(); i++)
	{
		arglist += QString(" \"") + model->stringList()[i] + QString("\"");
	}
	if(arglist.isEmpty())
	{
		QMessageBox::warning(this, "C++-Compiler", "Bitte Quelldateien laden!", 0, 0, 1);
	}
	else if(appName->text().isEmpty())
	{
		QMessageBox::warning(this, "C++-Compiler", "Bitte .exe-Datei als Ziel eingeben!", 0, 0, 1);
	}
	if(!appName->text().isEmpty() && !model->stringList().isEmpty() && appName->text().endsWith(".exe"))
	{
		QString neu = compilerpath + arglist + QString(" -o \"") + appName->text() + QString("\" -static -O -Wall ") + cmdLine->text();
		//system(neu.toStdString().c_str());


		/*FILE *chkdsk;
		chkdsk = _popen( neu.toStdString().c_str(), "rt" );
		QFile file;
		file.open(chkdsk, QIODevice::ReadWrite);
		QString end = file.readAll();
		QLabel *showen = new QLabel(end);
		showen->show();*/
		QProcess process;
		//QStringList neu = QStringList() << QString("C:\\MinGW\\bin\\g++") << arglist << QString("-o") << appName->text();
		process.setReadChannelMode(QProcess::MergedChannels);
		process.setReadChannel(QProcess::StandardOutput);
		process.start(neu);
		if (process.waitForFinished())
			qDebug() << process.errorString();
		else
			qDebug() << process.readAll();
		QWidget *widg = new QWidget;
		widg->setFixedSize(this->rect().width(), this->rect().height());
		QPushButton *but = new QPushButton("Schließen");
		QLabel *over = new QLabel(QString("<h2 align=center>Output</h2>"));
		QString info = process.readAll();
		QTextEdit *lab = new QTextEdit(QString("<h3>") + info + QString("</h3>"));
		lab->setReadOnly(true);
		//lab->setWordWrap(true);
		QVBoxLayout *lay =new QVBoxLayout;
		lay->addWidget(over);
		lay->addWidget(lab);
		lay->addWidget(but);
		if(info.isEmpty())
		{
			lab->setText("<h3>No Errors occured</h3>");
			QPushButton *starten = new QPushButton("Neu erstelltes Programm starten");
			lay->addWidget(starten);
			connect(starten, SIGNAL(clicked()), this, SLOT(startApp()));
		}
		widg->setLayout(lay);
		widg->setWindowModality(Qt::ApplicationModal);
		widg->setGeometry(this->geometry());
		connect(but, SIGNAL(clicked()), widg, SLOT(close()));
		//widg->move(QApplication::desktop()->screenGeometry().width()/2-size().width()/2, QApplication::desktop()->screenGeometry().height()/2 - size().height()/2 - 10);
		widg->move(this->x(), this->y());
		widg->show();
		process.close();
		arglist.clear();
		fileName.clear();
		savefile.clear();
		//appName->setText("");
		//model->setStringList(fileName);
		//list->setModel(model);
	}
}

void Compiler::startApp()
{
	QProcess::startDetached(appName->text());
}

void Compiler::saven()
{
	savefile = QFileDialog::getSaveFileName(this, "Zielort", 0, "Anwendung (*.exe)");
	if(!savefile.endsWith(".exe") && !savefile.isEmpty())
	{
		savefile = savefile + QString(".exe");
	}
	appName->setText(savefile);
}

void Compiler::del()
{
	int num = list->currentIndex().row();
	//model->removeRows(list->currentIndex().row(), 1);
	//fileName.removeAt(fileName.indexOf(list->currentIndex().data().toString()));
	fileName.removeAt(num);
	fileName.removeDuplicates();
	model->setStringList(fileName);
	list->setModel(model);
	
}