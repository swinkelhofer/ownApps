/****************************************************************************
** Meta object code from reading C++ file 'colorizer.h'
**
** Created: Thu 23. Dec 18:29:27 2010
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../colorizer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'colorizer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_check[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
       9,    7,    6,    6, 0x05,
      25,    6,    6,    6, 0x05,
      35,    6,    6,    6, 0x05,
      50,   45,    6,    6, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_check[] = {
    "check\0\0y\0posChanged(int)\0onClick()\0"
    "changed()\0tick\0deleted(check*)\0"
};

const QMetaObject check::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_check,
      qt_meta_data_check, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &check::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *check::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *check::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_check))
        return static_cast<void*>(const_cast< check*>(this));
    return QWidget::qt_metacast(_clname);
}

int check::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: posChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: onClick(); break;
        case 2: changed(); break;
        case 3: deleted((*reinterpret_cast< check*(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void check::posChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void check::onClick()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void check::changed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void check::deleted(check * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
static const uint qt_meta_data_colorizer[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      21,   10,   10,   10, 0x08,
      36,   31,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_colorizer[] = {
    "colorizer\0\0changed()\0newTick()\0tick\0"
    "delTick(check*)\0"
};

const QMetaObject colorizer::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_colorizer,
      qt_meta_data_colorizer, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &colorizer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *colorizer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *colorizer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_colorizer))
        return static_cast<void*>(const_cast< colorizer*>(this));
    return QWidget::qt_metacast(_clname);
}

int colorizer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: changed(); break;
        case 1: newTick(); break;
        case 2: delTick((*reinterpret_cast< check*(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void colorizer::changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
