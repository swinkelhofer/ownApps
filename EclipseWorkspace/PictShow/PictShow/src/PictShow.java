import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;

public class PictShow extends JFrame implements MouseListener
{
	static final long serialVersionUID = 1;
	private JScrollPane scroll;
	private Bitmap bitmap;
	private Bitmap thumbnail;
	private JButton load, loadURL;
	private String fileName;
	public PictShow()
	{
		fileName = "C:\\ownApps\\Java\\Uni\\PictShow\\bin\\Julia.png";
		this.setLayout(new BorderLayout());
		load = new JButton("Bild aus Datei laden");
		load.addMouseListener(this);
		loadURL = new JButton("Bild aus URL laden");
		loadURL.addMouseListener(this);
		JPanel south = new JPanel();
		south.setLayout(new FlowLayout());
		south.add(load);
		south.add(loadURL);
		thumbnail = new Bitmap();
		thumbnail.setSize(133,this.getHeight());
		thumbnail.setThumbnail(true);
		bitmap = new Bitmap();
		bitmap.setSize(900,700);
		bitmap.setSize(800,600);
		thumbnail.changeBitmap(fileName);
		bitmap.changeBitmap(fileName);
		scroll = new JScrollPane(bitmap, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		//scroll.setSize(500, 400);
		this.add(thumbnail, BorderLayout.WEST);
		this.add(scroll, BorderLayout.CENTER);
		this.add(south, BorderLayout.SOUTH);
		this.setSize(1024, 768);
		this.setTitle("PictShow");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void mouseClicked(MouseEvent event)
	{
		if(event.getSource() == load)
		{
			try
			{
				JFileChooser imageFile = new JFileChooser();
				imageFile.setFocusable(true);
				imageFile.setVisible(true);
				imageFile.showOpenDialog(null);
				if(!imageFile.getSelectedFile().getAbsolutePath().equals(fileName) && !imageFile.getSelectedFile().getAbsolutePath().isEmpty())
				{
					bitmap.changeBitmap(imageFile.getSelectedFile().getAbsolutePath());
					thumbnail.changeBitmap(imageFile.getSelectedFile().getAbsolutePath());
				}
			}
			catch(NullPointerException e)
			{
				return;
			}
		}
		if(event.getSource() == loadURL)
		{
			String s = new String("");
			try
			{
				s = JOptionPane.showInputDialog(null, "URL eingeben");
				if(s.isEmpty())
					return;
			}
			catch(NullPointerException e)
			{
				return;
			}
			try
			{
				bitmap.changeBitmapURL(new URL(s));
				thumbnail.changeBitmapURL(new URL(s));
			}
			catch(MalformedURLException e)
			{
				JOptionPane.showMessageDialog(null, "Bitte eine korrekte URL eingeben");
				mouseClicked(event);
			}
			
		}
		
	}
	public void mouseReleased(MouseEvent event)
	{
	}
	public void mousePressed(MouseEvent event)
	{
	}
	public void mouseEntered(MouseEvent event)
	{
	}
	public void mouseExited(MouseEvent event)
	{
	}
	public static void main(String[] args)
	{
		PictShow win = new PictShow();
		win.setVisible(true);
		
	}
	
}
