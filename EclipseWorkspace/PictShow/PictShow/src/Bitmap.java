import javax.swing.*;
import java.io.*;
import java.awt.*;
import java.net.*;


public class Bitmap extends JPanel
{
	static final long serialVersionUID = 1;
	private Image img;
	boolean isThumbnail;
	public Bitmap()
	{
		isThumbnail = false;
		img = getToolkit().getImage("C:\\ownApps\\Java\\Uni\\PictShow\\bin\\Julia.png");
	}
	public void setThumbnail(boolean isThumbnail)
	{
		this.isThumbnail = isThumbnail;
	}
	public void changeBitmap(String FileName)
	{
		if(new File(FileName).exists())
			img = getToolkit().getImage(FileName);
		else
			img = null;
		this.repaint();
	}
	public void changeBitmapURL(URL url)
	{
		img = getToolkit().getImage(url);
		this.repaint();
	}
	public void paint(Graphics g)
	{
		if(img != null)
		{
			g.setColor(Color.decode("#eeeeee"));
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			int width = img.getWidth(this);
			int height = img.getHeight(this);
			int offsetX = 0, offsetY = 0;
			if(isThumbnail)
			{
				if(width < height)
				{
					width = width * 150 / height;
					height = 150;
					offsetX = (150 - width) / 2;
					offsetY = (this.getHeight() - height) / 2;
				}
				else
				{
					height = height * 150 / width;
					width = 150;
					offsetY = (this.getHeight() - height) / 2;
					offsetX = 5;
				}
				g.fillRect(0,0, this.getWidth()+5, this.getHeight());
			}
			img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			g.drawImage(img, offsetX, offsetY, width, height, this);
			this.getParent().repaint();
		}
		else
		{
			g.setFont(new Font("SansSerif", Font.BOLD, 28));
			g.drawString("Image not available", this.getParent().getWidth() / 2 - 70, this.getParent().getHeight() / 2 - 10);
		}
	}
	public Dimension getPreferredSize()
	{
		if(img == null)
			return new Dimension(0,0);
		if(!isThumbnail)
			return new Dimension(img.getWidth(this), img.getHeight(this));
		else return new Dimension(160,150);
	}
	public Dimension getMinimumSize()
	{
		if(img == null)
			return new Dimension(0,0);
		if(!isThumbnail)
			return new Dimension(img.getWidth(this), img.getHeight(this));
		else return new Dimension(160,150);
	}
}
