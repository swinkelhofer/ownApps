#include "BigInt.h"
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main(int argc, char *argv[])
{
	cout.unsetf(ios_base::dec);
	cout.setf(ios_base::dec | ios_base::uppercase | ios_base::fixed);
	cout.fill('0');
	BigInt big;
	
	//cout << (int)((a & (unsigned int)pow(2,31))!=0); //Maskieren eines einzelnen Bits!!!!!!*/
	//BigInt a(atoi(argv[1])),b, one(1),two(2), null(0);
	/*BigInt f;
	for(int i = 0; i < 32; i++)
		f.bigint[i] = 0xFFFFFFFF;
	f.length = 32;
	a.length = 1;
	b.length = 1;
	a.bigint[31] = 0x25;
	//a.bigint[30] = 0xFAAF0495;
	b.bigint[31] = 0x9;
	//b.bigint[30] = 0x0;
	//a = BigInt::modExponentation(a,b,f);
	a = BigInt::random(f);
	//a = a % b;
	//a = a % two;
	//a = a >> 1;
	for(int i = 0; i < 32; i++)
	{
		cout.width(8);
		cout << a.bigint[i] << " ";
	}
	*/
	//BigInt one(1);
	time_t start,stop;
	//cout << BigInt::random(two).bigint[31] << "\n";
	//cout << (BigInt(321) % BigInt(27)).bigint[31]<<"\n";
	BigInt a = BigInt::exponentation(BigInt(2), BigInt(521))-1;
	//a = 9403;
	//b = 2349;
	//BigInt a = BigInt(180) * (BigInt::exponentation(BigInt(2), BigInt(127)) - BigInt(1)) * (BigInt::exponentation(BigInt(2), BigInt(127)) - BigInt(1)) + BigInt(1);
	//a = (BigInt(180) * BigInt::exponentation(BigInt::exponentation(2,127) - one , 2)) + 1;
	//cout << a.toHex();
	//big.fromHex(argv[1]);
	/*start = clock();
	BigInt a = BigInt::createPrime(atoi(argv[1]));
	stop = clock();
	cout << a.toHex();*/
	/*big = a.karatsuba(b);
	cout << "\nKaratsuba:\n";
	big.printHex2Console();
	cout << "\nNormal:\n";
	big = a*b;
	
	big.printHex2Console();*/
	//cout << "\n\n";
	//cout << a.toHex();
	//stop = clock();
	/*for(int i = 0; i < INTCOUNT; i++)
	{
		cout.width(8);
		cout << a.bigint[i] << " ";
	}*/
	/*a.printHex2Console();
	start = clock();
	if(BigInt::isMillerRabinPrime(a))
		cout << "\nprim";
	else
		cout << "\nzusammengesetzt";
	stop = clock();
	cout.unsetf(ios_base::hex);
	cout << "\n" << stop - start << " Milliseconds";*/
	//BigInt *a(37);
	for(int i = 0x1; i < 0x64; i++)
	{
		//BigInt a = 37;
		cout << (2*i+1) << ":\t";
		//a.printHex2Console();
		if(BigInt::isMillerRabinPrime(2*i+1))
			cout << "prim\n";
		else
			cout << "zusammengesetzt\n";
	}
	
	usrand(time(0));
	srand(time(NULL));
	//BigInt a;
	//a.fromHex(argv[1]);
	start = clock();
	cout << BigInt::isMillerRabinPrime(a);
	//a = BigInt::createPrime(512);
	//a = atoi(argv[2]) % 16;
	stop = clock();
	cout << "\n";
	cout << a.toHex();
	a.printHex2Console();
	cout << "\n" << (float)(((float)stop - (float)start) / (float)1000.0) << " Seconds\n";
	
	start = clock();
	//cout << a.millerRabinFast();
	//a = BigInt::createPrime(512);
	//a = atoi(argv[2]) % 16;
	stop = clock();
	//BigInt::random(a).printHex2Console();
	//	cout << "\n" << (float)(((float)stop - (float)start) / (float)1000.0) << " Seconds\n";
	//cout.setf(
	/*cout.unsetf(ios_base::dec);
	cout.setf(ios_base::hex | ios_base::uppercase | ios_base::fixed);
	for(int i = 0; i< 50; i++)
	{
		cout.width(8);
		cout << urand() << "\n";
	}
	cout << __cplusplus<< "\n";
	cout << __DATE__ << "\t" << __TIME__;*/
	return 0;
}