/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/

#ifndef BIGINT
#define BIGINT
#include <string>
#define BITLENGTH 1280
#define INTCOUNT BITLENGTH / 32
using namespace std;


static unsigned int x_urand;
inline void usrand(unsigned int x)
{
	x_urand = x;
}
inline unsigned int urand()
{
	x_urand = 1664525L * x_urand + 1013904223L;
	return x_urand;
}
inline unsigned int COMP_CARRY(unsigned int a, unsigned int b, unsigned int c)
{
	//temp_carry = ((__int64) a + (__int64) b + (__int64) c) >> 32;
	return (unsigned int) ((__int64)((__int64) a + (__int64) b + (__int64) c) >> 32);
}


class BigInt
{
public:
/*################################################
			Constructors + Settings
################################################*/
	BigInt();
	BigInt(unsigned int a);

/*################################################
			Arithmetic Operators
################################################*/
	BigInt operator+(BigInt a);
	BigInt operator-(BigInt a);
	BigInt operator*(BigInt a);
	BigInt operator%(BigInt a);
	BigInt operator++();
	BigInt operator++(int);
	BigInt operator--();
	BigInt operator--(int);

	static BigInt exponentation(BigInt base, BigInt exponent);
	static BigInt modExponentation(BigInt base, BigInt exponent, BigInt modulus);
	static BigInt modAddition(BigInt add1, BigInt add2, BigInt modulus);
	static BigInt modMultiplication(BigInt factor1, BigInt factor2, BigInt modulus);

/*################################################
			Bitwise Operators
################################################*/
	BigInt operator<<(int shift);
	BigInt operator>>(int shift);
	BigInt operator|(BigInt a);
	BigInt operator&(BigInt a);
	BigInt operator^(BigInt a);
	BigInt operator~();

/*################################################
			Comparison Operators
################################################*/
	bool operator<(BigInt a);
	bool operator<=(BigInt a);
	bool operator>(BigInt a);
	bool operator>=(BigInt a);
	bool operator==(BigInt a);
	bool operator!=(BigInt a);


/*################################################
			RSA relevant Methods
################################################*/
	static BigInt random(BigInt max);
	static bool isMillerRabinPrime(BigInt n);		//!!! 0,1,2 ergeben falsche Werte, bzw. Endlosschleifen!!!
	static BigInt createPrime(int bits);

/*###############################################
			Conversion Methods
###############################################*/
	void printHex2Console();
	string toHex(bool uppercase = true);
	void fromHex(char *hex);

private:
/*###############################################
			Private Methods
###############################################*/
	//inline unsigned int COMP_CARRY(unsigned int a, unsigned int b, unsigned int c);	
	
/*################################################
			O= Operators
################################################*/
	BigInt operator<<=(int shift);
	BigInt operator>>=(int shift);
	inline BigInt operator+=(BigInt a);
	inline BigInt operator-=(BigInt a);
	BigInt operator*=(BigInt a);
	inline BigInt operator%=(BigInt a);

/*################################################
			Arithmetic Operators
################################################*/
	inline BigInt sub(BigInt a);
	inline BigInt modExpEq(BigInt exponent, BigInt modulus);	//Writes Value back to Calling Instance
/*###############################################
			Private Attributes
###############################################*/
	unsigned int bigint[INTCOUNT];
	int length;
};

#endif
