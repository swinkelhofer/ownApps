/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/

#include "BigInt.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <iostream>

using namespace std;


/*################################################
			Forward Definitions
################################################*/

unsigned int pow_2[] =  {0x00000001, 0x00000002, 0x00000004, 0x00000008, 0x00000010, 0x00000020, 0x00000040, 0x00000080,
	0x00000100, 0x00000200, 0x00000400, 0x00000800, 0x00001000, 0x00002000, 0x00004000, 0x00008000,
	0x00010000, 0x00020000, 0x00040000, 0x00080000, 0x00100000, 0x00200000, 0x00400000, 0x00800000,
	0x01000000, 0x02000000, 0x04000000, 0x08000000, 0x10000000, 0x20000000, 0x40000000, 0x80000000};
const BigInt *null = new BigInt;
const BigInt *one = new BigInt(1);
const BigInt *two = new BigInt(2);
BigInt a1, a2, ret, m, x;
unsigned int temp, carry;
int temp1;


/*################################################
			Private Methods
################################################*/



/*################################################
			Constructors + Settings
################################################*/
BigInt::BigInt()
{
	for(int i = 0; i < INTCOUNT; i++)
		bigint[i] = 0;
	length = 0;
}

BigInt::BigInt(unsigned int a)
{
	//*this = *null;
	/*for(int i = 0; i < INTCOUNT-1; i++)
		this->bigint[i] = 0;*/
	*this = *null;
	this->bigint[INTCOUNT - 1] = a;
	if(a != 0)
	{
		this->length = 1;
	}
	//else
	//	this->length = 0;
	/*for(int i = 0; i < INTCOUNT-1; i++)
		bigint[i] = 0;
	bigint[INTCOUNT - 1] = a;
	if(a != 0)
		length = 1;
	else
		length = 0;*/
}


/*################################################
			Arithmetic Operators
################################################*/
BigInt BigInt::operator+(BigInt a)
{
	carry = 0;
	register int temp1 = (this->length > a.length ? this->length : a.length);
	int i = INTCOUNT - 1;
	for(; i >= INTCOUNT - temp1 - 1; i--)
	{
		ret.bigint[i] = this->bigint[i] + a.bigint[i] + carry;
		carry = COMP_CARRY(this->bigint[i], a.bigint[i], carry);
	}
	if(ret.bigint[INTCOUNT - temp1-1] == 0)
		ret.length = temp1;
	else
		ret.length = temp1+1;
	return ret;
}

BigInt BigInt::operator-(BigInt a)
{
	if(a >= *this)
		return *null;
	ret = a;
	carry = 1;
	for(int i = INTCOUNT - 1;  i >= INTCOUNT - max(this->length, a.length)-1; i--)
	{
		temp = ~ret.bigint[i];
		ret.bigint[i] = this->bigint[i] + temp + carry;
		carry = COMP_CARRY(this->bigint[i], temp, carry);
	}
	temp = 0;
	while(!ret.bigint[temp] && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::sub(BigInt a)
{
	if(a >= *this)
		return *null;
	ret = a;
	carry = 1;
	for(register int i = INTCOUNT - 1;  i >= INTCOUNT - (this->length > a.length ? this->length : a.length)-1; i--)
	{
		temp = ~ret.bigint[i];
		ret.bigint[i] = this->bigint[i] + temp + carry;
		carry = COMP_CARRY(this->bigint[i], temp, carry);
	}
	temp = 0;
	while(!ret.bigint[temp] && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}


BigInt BigInt::operator*(BigInt a)
{
	//if(this->length + a.length < INTCOUNT)
	//{
		BigInt *ret = new BigInt;// = new BigInt();
		//*ret = *null;
		if(this->length <= a.length)
		{
			a1 = a;
			a2 = *this;
		}
		else
		{
			a1 = *this;
			a2 = a;
		}
		for(int i = INTCOUNT - 1; i >= INTCOUNT - a2.length; i--)
		{
			if(a2.bigint[i] & 0x1)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x2)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x4)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x8)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x10)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x20)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x40)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x80)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x100)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x200)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x400)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x800)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x1000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x2000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x4000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x8000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x10000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x20000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x40000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x80000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x100000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x200000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x400000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x800000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x1000000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x2000000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x4000000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x8000000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x10000000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x20000000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x40000000)
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[i] &  0x80000000)
				*ret += a1;
			a1 <<= 1;			
		}
		return *ret;
	//}
	//else
	//	return *null;
}

BigInt BigInt::operator%(BigInt a)
{
	if(a > *this)
		return *this;
	a1 = *this;
	int temp = ((this->length - a.length + 1) * 32);
	a <<= temp;
	for(int i = temp; i > 0; i--)
	{
		a >>= 1;
		if(a1 >= a)
			a1 -= a;
	}
	return a1;
}

BigInt BigInt::operator++()
{
	if(this->bigint[INTCOUNT - 1] < 0xFFFFFFFF)
	{
		this->bigint[INTCOUNT - 1]++;
		return *this;
	}
	*this += *one;
	return *this;
}

BigInt BigInt::operator++(int)
{
	BigInt ret = *this;
	if(this->bigint[INTCOUNT - 1] < 0xFFFFFFFF)
	{
		this->bigint[INTCOUNT - 1]++;
		return *this;
	}
	*this += *one;
	return ret;
}

BigInt BigInt::operator--()
{
	if(this->bigint[INTCOUNT - 1] > 1)
	{
		this->bigint[INTCOUNT - 1]--;
		return *this;
	}
	*this = *this - *one;
	return *this;// -= *one;
}

BigInt BigInt::operator--(int)
{
	BigInt ret = *this;
	if(this->bigint[INTCOUNT - 1] > 1)
	{
		this->bigint[INTCOUNT - 1]--;
		return *this;
	}
	*this = *this - *one;
	return ret;
}

BigInt BigInt::exponentation(BigInt base, BigInt exponent)
{
	BigInt a1 = base;
	BigInt z1 = exponent;
	BigInt *ret = new BigInt(1);
	while(z1.length != 0)
	{
		while(z1.bigint[INTCOUNT - 1] % 2 == 0)
		{
			z1 >>= 1;
			a1 *= a1;
		}
		--z1;
		*ret *= a1;
	}
	return *ret;
}

BigInt BigInt::modExponentation(BigInt base, BigInt exponent, BigInt modulus)			//ret = (a1 ^ z1) % modulus
{
	BigInt a1 = base; 
	BigInt z1 = exponent;
	BigInt *ret = new BigInt(1);
	while(z1.length != 0)
	{
		while(z1.bigint[INTCOUNT - 1] % 2 == 0)
		{
			z1 >>= 1;
			a1 *= a1;
			a1 %= modulus;
		}
		--z1;
		*ret *= a1;
		*ret %= modulus;
	}
	return *ret;	
}

BigInt BigInt::modExpEq(BigInt exponent, BigInt modulus)			//ret = (a1 ^ z1) % modulus
{
	BigInt a1 = *this; 
	BigInt z1 = exponent;
	//BigInt *ret = new BigInt(1);
	*this = *one;
	//while(z1 != *null)
	while(z1.length != 0)
	{
		while(z1.bigint[INTCOUNT - 1] % 2 == 0)
		{
			z1 >>= 1;
			a1 *= a1;
			a1 %= modulus;
		}
		--z1;
		*this *= a1;
		*this %= modulus;
	}
	return *this;	
}

BigInt BigInt::modAddition(BigInt add1, BigInt add2, BigInt modulus)
{
	return ((add1 % modulus) + (add2 % modulus)) % modulus;
}

BigInt BigInt::modMultiplication(BigInt factor1, BigInt factor2, BigInt modulus)
{
	return ((factor1 % modulus) * (factor2 % modulus)) % modulus;
}


/*################################################
			Bitwise Operators
################################################*/
BigInt BigInt::operator<<(int shift)
{
	ret = *this;
	if(shift == 1)
	{
		for(int i = INTCOUNT - ret.length - 1; i < INTCOUNT; i++)
		{
			ret.bigint[i] = (ret.bigint[i] << 1) + (ret.bigint[i+1] >> 31);
		}
		ret.bigint[0] = ret.bigint[0] << 1;
		if(ret.bigint[INTCOUNT - ret.length - 1] != 0)
			ret.length++;
		return ret;
	}
	int div = shift / 32;
	int mod = shift % 32;
	if(mod == 0 && div > 0)
	{
		for(int i = 0; i < INTCOUNT - div; i++)
			ret.bigint[i] = ret.bigint[i + temp];
		for(int i = INTCOUNT - div; i < INTCOUNT; i++)
			ret.bigint[i] = 0;
		ret.length += div;
		return ret;
	}
	if(shift >= INTCOUNT * 32)
		return *null;
	if(div != 0)
	{
		for(int i = 0; i < INTCOUNT - div; i++)
		{
			ret.bigint[i] = ret.bigint[i + div];
		}
		for(int i = 0; i < div; i++)
		{
			ret.bigint[INTCOUNT - 1 - i] = 0;
		}
	}
	if(mod != 0)
	{
		for(int i = 0; i < INTCOUNT-1; i++)
		{
			ret.bigint[i] = (ret.bigint[i] << (mod)) + (ret.bigint[i+1] >> (32-(mod)));
		}
		ret.bigint[INTCOUNT - 1] = ret.bigint[INTCOUNT - 1] << (mod);
	}
	/*
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;*/
	ret.length += div;
	if(ret.bigint[INTCOUNT - ret.length - 1] != 0)
		ret.length++;
	return ret;
}

BigInt BigInt::operator>>(int shift)
{
	ret = *this;
	if(shift == 1)
	{
		for(int i = INTCOUNT - 1; i >= INTCOUNT - ret.length; i--)
		{
			ret.bigint[i] = (ret.bigint[i] >> 1) + (ret.bigint[i-1] << 31);
		}
		ret.bigint[0] = ret.bigint[0] >> 1;
		if(ret.bigint[INTCOUNT - ret.length] == 0)
			ret.length--;
		return ret;
	}
	int div = shift / 32;
	int mod = shift % 32;
	if(shift >= INTCOUNT * 32)
		return *null;
	if(div != 0)
	{
		for(int i = INTCOUNT - 1; i >= div; i--)
		{
			ret.bigint[i] = ret.bigint[i - div];
		}
	
		for(int i = 0; i < div; i++)
		{
			ret.bigint[i] = 0;
		}
	}
	if(mod != 0)
	{
		for(int i = INTCOUNT - 1; i >= 1; i--)
		{
			ret.bigint[i] = (ret.bigint[i] >> (mod)) + (ret.bigint[i-1] << (32-(mod)));
		}
		ret.bigint[0] = ret.bigint[0] >> (mod);
	}
	/*temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;*/
	ret.length -= div;
	if(ret.bigint[INTCOUNT - ret.length] == 0)
		ret.length--;
	return ret;
}

BigInt BigInt::operator|(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] | a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator&(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] & a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator^(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] ^ a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator~()
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = ~this->bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}


/*################################################
			Comparison Operators
################################################*/
bool BigInt::operator<(BigInt a)
{
	if(this->length < a.length)
		return true;
	else if(this->length > a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; i++)
		{
			if(this->bigint[i] < a.bigint[i])
				return true;
			else if(this->bigint[i] > a.bigint[i])
				return false;
		}
		return false;
	}
}

bool BigInt::operator<=(BigInt a)
{
	if(this->length < a.length)
		return true;
	else if(this->length > a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; i++)
		{
			if(this->bigint[i] < a.bigint[i])
				return true;
			else if(this->bigint[i] > a.bigint[i])
				return false;
		}
		return true;
	}
}

bool BigInt::operator>(BigInt a)
{
	if(this->length > a.length)
		return true;
	else if(this->length < a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; i++)
		{
			if(this->bigint[i] > a.bigint[i])
				return true;
			else if(this->bigint[i] < a.bigint[i])
				return false;
		}
		return false;
	}
}

bool BigInt::operator>=(BigInt a)
{
	if(this->length > a.length)
		return true;
	else if(this->length < a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; i++)
		{
			if(this->bigint[i] > a.bigint[i])
				return true;
			else if(this->bigint[i] < a.bigint[i])
				return false;
		}
		return true;
	}
}

bool BigInt::operator==(BigInt a)
{
	if(this->length != a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; i++)
		{
			if(this->bigint[i] != a.bigint[i])
				return false;
		}
		return true;
	}
}

bool BigInt::operator!=(BigInt a)
{
	if(this->length != a.length)
		return true;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; i++)
		{
			if(this->bigint[i] != a.bigint[i])
				return true;
		}
		return false;
	}
}


/*################################################
			O= Operators
################################################*/
BigInt BigInt::operator<<=(int shift)
{
	if(shift == 1)
	{
		for(int i = 1; i < INTCOUNT; i++)
		{
			this->bigint[i] = (this->bigint[i] << 1) + (this->bigint[i+1] > 0x7FFFFFFF);
		}
		this->bigint[0] = this->bigint[0] << 1;
		if(this->bigint[INTCOUNT - this->length - 1] != 0)
			this->length++;
		return *this;
	}
	int div = shift/32;
	int mod = shift % 32;
	if(mod == 0 && div > 0)
	{
		for(int i = 0; i < INTCOUNT - div; i++)
			this->bigint[i] = this->bigint[i + div];
		for(int i = INTCOUNT - div; i < INTCOUNT; i++)
			this->bigint[i] = 0;
		this->length += (int) div;
		return *this;
	}
	if(shift >= INTCOUNT * 32)
		return *null;
	if(shift / 32 != 0)
	{
		for(int i = 0; i < INTCOUNT - div; i++)
		{
			this->bigint[i] = this->bigint[i + div];
		}
		for(int i = 0; i < div; i++)
		{
			this->bigint[INTCOUNT - 1 - i] = 0;
		}
	}
	if(mod != 0)
	{
		for(int i = 0; i < INTCOUNT-1; i++)
		{
			this->bigint[i] = (this->bigint[i] << (mod)) + (this->bigint[i+1] >> (32-(mod)));
		}
		this->bigint[INTCOUNT - 1] = this->bigint[INTCOUNT - 1] << (mod);
	}
	/*temp = 0;
	while(this->bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	this->length = INTCOUNT - temp;*/
	this->length += div;
	if(this->bigint[INTCOUNT - this->length - 1] != 0)
		this->length++;
	return *this;
}

BigInt BigInt::operator>>=(int shift)
{
	if(shift == 1)
	{
		for(int i = INTCOUNT - 1; i >= INTCOUNT - this->length; i--)
		{
			this->bigint[i] = (this->bigint[i] >> 1) + (this->bigint[i-1] << 31);
		}
		this->bigint[0] = this->bigint[0] >> 1;
		if(this->bigint[INTCOUNT - this->length] == 0)
			this->length--;
		return *this;
	}

	if(shift >= INTCOUNT * 32)
		return *null;
	int div = shift/32;
	int mod = shift % 32;
	if(div != 0)
	{
		for(int i = INTCOUNT - 1; i >= div; i--)
		{
			this->bigint[i] = this->bigint[i - div];
		}
	
		for(int i = 0; i < div; i++)
		{
			this->bigint[i] = 0;
		}
	}
	if(mod != 0)
	{
		for(int i = INTCOUNT - 1; i >= 1; i--)
		{
			this->bigint[i] = (this->bigint[i] >> (mod)) + (this->bigint[i-1] << (32-(mod)));
		}
		this->bigint[0] = this->bigint[0] >> (mod);
	}
	/*temp = 0;
	while(this->bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	this->length = INTCOUNT - temp;*/
	this->length -= div;
	if(this->bigint[INTCOUNT - this->length] == 0)
		this->length--;
	return *this;
}

BigInt BigInt::operator+=(BigInt a)
{
	temp1 = this->length > a.length ? this->length : a.length;
	carry = 0;
	for(register int i = INTCOUNT - 1; i >= INTCOUNT - temp1 - 1; i--)
	{
		temp = this->bigint[i];
		this->bigint[i] = this->bigint[i] + a.bigint[i] + carry;
		carry = COMP_CARRY(temp, a.bigint[i], carry);
	}
	if(this->bigint[INTCOUNT - temp1-1] == 0)
		this->length = temp1;
	else
		this->length = temp1+1;
	return *this;
}

BigInt BigInt::operator-=(BigInt a)
{
	if(a >= *this)
		return *null;
	carry = 1;
	for(register int i = INTCOUNT - 1;  i >= INTCOUNT - (this->length > a.length ? this->length : a.length)-1; i--)
	{
		temp = this->bigint[i];
		this->bigint[i] = this->bigint[i] + ~a.bigint[i] + carry;
		carry = COMP_CARRY(temp, ~a.bigint[i], carry);
	}
	temp = 0;
	while(!this->bigint[temp] && temp < INTCOUNT)
		temp++;
	this->length = INTCOUNT - temp;
	return *this;
}

BigInt BigInt::operator*=(BigInt a)
{
	//if(this->length + a.length < INTCOUNT)
	//{
		if(this->length <= a.length)
		{
			a1 = a;
			a2 = *this;
		}
		else
		{
			a1 = *this;
			a2 = a;
		}
		*this = *null;
		for(int i = INTCOUNT - 1; i >= INTCOUNT - a2.length; i--)
		{
			if(a2.bigint[i] & 0x1)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x2)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x4)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x8)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x10)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x20)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x40)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x80)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x100)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x200)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x400)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x800)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x1000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x2000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x4000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x8000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x10000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x20000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x40000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x80000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x100000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x200000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x400000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x800000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x1000000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x2000000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x4000000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x8000000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x10000000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x20000000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x40000000)
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[i] & 0x80000000)
				*this += a1;
			a1 <<= 1;			
		}
		return *this;
	//}
	//else
	//{
	//	*this = *null;
	//	return *this;
	//}
}

BigInt BigInt::operator%=(BigInt a)
{
	if(a > *this)
		return *this;
	int temp = (this->length - a.length + 1) * 32;
	a <<= temp;
	for(int i = temp; i > 0 ; i--)
	{
		a >>= 1;
		if(*this >= a)
			*this -= a;
	}
	return *this;
}


/*################################################
			RSA relevant Methods
################################################*/
BigInt BigInt::random(BigInt max)
{
	ret.bigint[INTCOUNT - max.length] = (unsigned int) (urand() % (max.bigint[INTCOUNT - max.length]-1))+1;
	for(int i = INTCOUNT - max.length + 1; i < INTCOUNT; i++)
	{
		ret.bigint[i] = urand();
	}
	ret.length = max.length;
	return ret;
}

bool BigInt::isMillerRabinPrime(BigInt n)
{
	/*for(int i = 0; i< 30; i++)
		if(!n.miller())
			return false;
	return true;*/
	/*if(n.length == 1 && n.bigint[INTCOUNT - 1] == 0x2)
		return true;
	if((n.length == 1 && n.bigint[INTCOUNT - 1] == 0x1) || !(n.bigint[INTCOUNT - 1] & 0x1))
		return false;*/
	bool ism;
	int l;
	//m = m.sub(n, *one);
	//m = n - *one;
	m = n.sub(*one);
	for(int i = 0; i < 30; i++)
	{
		l = 0;
		x = BigInt::random(m);
		//while(m.bigint[INTCOUNT - 1] % 2 == 0)
		while(!(m.bigint[INTCOUNT - 1] & 0x1))
		{
			l++;
			m >>= 1;
		}
		//x = BigInt::modExponentation(x,m,n);
		x.modExpEq(m,n);
		m = n.sub(*one);
		//m = n - *one;
		ism = false;
		if((x.length == 1 && x.bigint[INTCOUNT - 1] == 0x1) || x == m)
		{
			//ism = true;
			continue;
		}
		for(int j = l; j > 1; j--)
		{
			x *= x;
			x %= n;
			if(x == m)
			{
				ism = true;
				break;
			}
			if(x.length == 1 && x.bigint[INTCOUNT - 1] == 0x1)// == *one)
			{
				return false;
			}
		}
		if(!ism)
			return false;
	}
	return true;
}

BigInt BigInt::createPrime(int bits)
{
	bits--;
	srand(time(NULL));
	BigInt ret;
	int j = 0;
	while(true)
	{
		int temp = urand();
		if(temp % 2 == 0)
			temp++;
		ret.bigint[INTCOUNT - 1] = temp;
		for(int i = INTCOUNT - 2; i >= INTCOUNT - bits/32; i--)
		{
			ret.bigint[i] = urand();
		}
		if(bits % 32 != 0)
		{
			ret.bigint[INTCOUNT - bits/32 - 1] = (urand() % (int) pow_2[bits % 32]) + pow_2[bits % 32];
			ret.length = bits/32 + 1;
		}
		else
			ret.length = bits/32;
		while(true)
		{
			j++;
			if(BigInt::isMillerRabinPrime(ret))
			{
				cout << j << " Versuche!!!!\n";
				return ret;
			}
			else if(j == 1000);
			{
				break;
			}
			
		}
	}
}


/*###############################################
			Conversion Methods
###############################################*/
void BigInt::printHex2Console()
{
	cout.fill('0');
	cout.unsetf(ios_base::dec);
	cout.setf(ios_base::hex | ios_base::uppercase | ios_base::fixed);
	for(int i = 0; i < INTCOUNT; i++)
	{
		if(i % 8 == 0)
			cout << "\n";
		cout.width(8);
		cout << this->bigint[i] << " ";
	}
	cout.unsetf(ios_base::hex);
	cout.setf(ios_base::dec);
}

string BigInt::toHex(bool uppercase)
{
	string hex = "";
	char temp;
	BigInt sixteen(16), a = *this;
	while(a != *null)
	{
		switch((a % sixteen).bigint[INTCOUNT - 1])
		{
			case 0:		hex += '0';
						break;
			case 1:		hex += '1';
						break;
			case 2:		hex += '2';
						break;
			case 3:		hex += '3';
						break;
			case 4:		hex += '4';
						break;
			case 5:		hex += '5';
						break;
			case 6:		hex += '6';
						break;
			case 7:		hex += '7';
						break;
			case 8:		hex += '8';
						break;
			case 9:		hex += '9';
						break;
			case 10:	hex += 'A';
						break;
			case 11:	hex += 'B';
						break;
			case 12:	hex += 'C';
						break;
			case 13:	hex += 'D';
						break;
			case 14:	hex += 'E';
						break;
			case 15:	hex += 'F';
						break;
			default:	hex += '0';
						break;
		}
		a = a >> 4;
	}
	if(!uppercase)
	{
		for(int i = 0; i < (int) hex.length() / 2; i++)
		{
			temp = tolower(hex[i]);
			hex[i] = tolower(hex[hex.length() - 1 - i]);
			hex[hex.length() - 1 - i] = temp;
		}
	}
	else
	{
		for(int i = 0; i < (int) hex.length() / 2; i++)
		{
			temp = hex[i];
			hex[i] = hex[hex.length() - 1 - i];
			hex[hex.length() - 1 - i] = temp;
		}
	}
	if(hex == string(""))
		hex += '0';
	hex.insert(0,"0x ");
	return hex;
}

void BigInt::fromHex(char *hex)
{
	int hexlength = strlen(hex);
	int start = 0;
	int chiffre = 0;
	if(hex[0] == '0' && toupper(hex[1]) == 'X' && hex[2] == ' ')
	{
		hexlength -= 3;
		start = 2;
	}
	for(int i = 0; i < hexlength; i++)
	{
		switch(toupper(hex[start + i]))
		{
			case '0':	chiffre = 0;
						break;
			case '1':	chiffre = 1;
						break;
			case '2':	chiffre = 2;
						break;
			case '3':	chiffre = 3;
						break;
			case '4':	chiffre = 4;
						break;
			case '5':	chiffre = 5;
						break;
			case '6':	chiffre = 6;
						break;
			case '7':	chiffre = 7;
						break;
			case '8':	chiffre = 8;
						break;
			case '9':	chiffre = 9;
						break;
			case 'A':	chiffre = 10;
						break;
			case 'B':	chiffre = 11;
						break;
			case 'C':	chiffre = 12;
						break;
			case 'D':	chiffre = 13;
						break;
			case 'E':	chiffre = 14;
						break;
			case 'F':	chiffre = 15;
						break;
		}
		*this = *this << 4;
		*this = *this + BigInt(chiffre);
	}
}