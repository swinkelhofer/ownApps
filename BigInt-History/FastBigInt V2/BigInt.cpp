/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/

#include "BigInt.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <iostream>

using namespace std;


/*################################################
			Forward Definitions
################################################*/

unsigned int pow_2[] =  {0x00000001, 0x00000002, 0x00000004, 0x00000008, 0x00000010, 0x00000020, 0x00000040, 0x00000080,
	0x00000100, 0x00000200, 0x00000400, 0x00000800, 0x00001000, 0x00002000, 0x00004000, 0x00008000,
	0x00010000, 0x00020000, 0x00040000, 0x00080000, 0x00100000, 0x00200000, 0x00400000, 0x00800000,
	0x01000000, 0x02000000, 0x04000000, 0x08000000, 0x10000000, 0x20000000, 0x40000000, 0x80000000};
const BigInt *null = new BigInt(0);
const BigInt *one = new BigInt(1);
const BigInt *two = new BigInt(2);
BigInt a1, a2, ret, m, x;
unsigned int temp, carry;
__int64 temp_carry;


/*################################################
			Private Methods
################################################*/
inline unsigned int BigInt::COMP_CARRY(unsigned int a, unsigned int b, unsigned int c)
{
	temp_carry = ((__int64) a + (__int64) b + (__int64) c) >> 32;
	return (unsigned int) temp_carry;
}


/*################################################
			Constructors + Settings
################################################*/
BigInt::BigInt()
{
	*this = *null;
}

BigInt::BigInt(int a)
{
	for(int i = 0; i < INTCOUNT-1; i++)
		bigint[i] = 0;
	bigint[INTCOUNT-1] = a;
	length = 1;
}


/*################################################
			Arithmetic Operators
################################################*/
inline BigInt BigInt::operator+(BigInt a)
{
	carry = 0;
	int temp = this->length > a.length ? this->length : a.length;
	int i = INTCOUNT - 1;
	for(; i >= INTCOUNT - temp - 1; i--)
	{
		ret.bigint[i] = this->bigint[i] + a.bigint[i] + carry;
		carry = COMP_CARRY(this->bigint[i], a.bigint[i], carry);
	}
	if(ret.bigint[INTCOUNT - temp-1] == 0)
		ret.length = temp;
	else
		ret.length = temp+1;
	return ret;
}

inline BigInt BigInt::operator-(BigInt a)
{
	if(a >= *this)
		return *null;
	ret = a;
	carry = 1;
	for(int i = INTCOUNT - 1;  i >= INTCOUNT - max(this->length, a.length)-1; i--)
	{
		temp = ~ret.bigint[i];
		ret.bigint[i] = this->bigint[i] + temp + carry;
		carry = COMP_CARRY(this->bigint[i], temp, carry);
	}
	temp = 0;
	while(!ret.bigint[temp] && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator*(BigInt a)
{
	if(this->length + a.length < INTCOUNT)
	{
		BigInt *ret = new BigInt();
		if(this->length <= a.length)
		{
			a1 = a;
			a2 = *this;
		}
		else
		{
			a1 = *this;
			a2 = a;
		}
		for(int i = 1; i <= a2.length; i++)
		{
			if(a2.bigint[INTCOUNT - i] & pow_2[0])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[1])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[2])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[3])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[4])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[5])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[6])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[7])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[8])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[9])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[10])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[11])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[12])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[13])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[14])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[15])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[16])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[17])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[18])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[19])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[20])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[21])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[22])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[23])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[24])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[25])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[26])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[27])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[28])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[29])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[30])
				*ret += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[31])
				*ret += a1;
			a1 <<= 1;			
		}
		return *ret;
	}
	else
		return *null;
}

inline BigInt BigInt::operator%(BigInt a)
{
	if(a > *this)
		return *this;
	a1 = *this;
	int temp = ((this->length - a.length + 1) * 32);
	a <<= temp;
	for(int i = 0; i < temp; i++)
	{
		a >>= 1;
		if(a1 >= a)
			a1 -= a;
	}
	return a1;
}

BigInt BigInt::operator++()
{
	if(this->bigint[INTCOUNT - 1] < 0xFFFFFFFF)
	{
		this->bigint[INTCOUNT - 1]++;
		return *this;
	}
	*this = *this + *one;
	return *this;
}

BigInt BigInt::operator++(int)
{
	BigInt ret = *this;
	if(this->bigint[INTCOUNT - 1] < 0xFFFFFFFF)
	{
		this->bigint[INTCOUNT - 1]++;
		return *this;
	}
	*this = *this + *one;
	return ret;
}

BigInt BigInt::operator--()
{
	if(this->bigint[INTCOUNT - 1] > 1)
	{
		this->bigint[INTCOUNT - 1]--;
		return *this;
	}
	*this = *this - *one;
	return *this;
}

BigInt BigInt::operator--(int)
{
	BigInt ret = *this;
	if(this->bigint[INTCOUNT - 1] > 1)
	{
		this->bigint[INTCOUNT - 1]--;
		return *this;
	}
	*this = *this - *one;
	return ret;
}

inline BigInt BigInt::exponentation(BigInt base, BigInt exponent)
{
	BigInt a1 = base;
	BigInt z1 = exponent;
	BigInt *ret = new BigInt(1);
	while(z1 != *null)
	{
		while(z1.bigint[INTCOUNT - 1] % 2 == 0)
		{
			z1 >>= 1;
			a1 *= a1;
		}
		--z1;
		*ret *= a1;
	}
	return *ret;
}

inline BigInt BigInt::modExponentation(BigInt base, BigInt exponent, BigInt modulus)
{
	BigInt a1 = base; 
	BigInt z1 = exponent;
	BigInt *ret = new BigInt(1);
	while(z1 != *null)
	{
		while(z1.bigint[INTCOUNT - 1] % 2 == 0)
		{
			z1 >>= 1;
			a1 *= a1;
			a1 %= modulus;
		}
		--z1;
		*ret *= a1;
		*ret %= modulus;
	}
	return *ret;
}

BigInt BigInt::modAddition(BigInt add1, BigInt add2, BigInt modulus)
{
	return ((add1 % modulus) + (add2 % modulus)) % modulus;
}

BigInt BigInt::modMultiplication(BigInt factor1, BigInt factor2, BigInt modulus)
{
	return ((factor1 % modulus) * (factor2 % modulus)) % modulus;
}


/*################################################
			Bitwise Operators
################################################*/
BigInt BigInt::operator<<(int shift)
{
	ret = *this;
	if(shift == 1)
	{
		for(int i = 1; i < INTCOUNT; i++)
		{
			ret.bigint[i] = (ret.bigint[i] << 1) + (ret.bigint[i+1] >> 31);
		}
		ret.bigint[0] = ret.bigint[0] << 1;
		if(ret.bigint[INTCOUNT - ret.length - 1] != 0)
			ret.length++;
		return ret;
	}
	if(shift % 32 == 0 && shift/32 > 0)
	{
		int temp = shift/32;
		for(int i = 0; i < INTCOUNT - temp; i++)
			ret.bigint[i] = ret.bigint[i + temp];
		for(int i = INTCOUNT - temp; i < INTCOUNT; i++)
			ret.bigint[i] = 0;
		ret.length += temp;
		return ret;
	}
	if(shift >= INTCOUNT * 32)
		return *null;
	if(shift/32 != 0)
	{
		for(int i = 0; i < INTCOUNT - shift/32; i++)
		{
			ret.bigint[i] = ret.bigint[i + shift/32];
		}
		for(int i = 0; i < shift/32; i++)
		{
			ret.bigint[INTCOUNT - 1 - i] = 0;
		}
	}
	if(shift % 32 != 0)
	{
		for(int i = 0; i < INTCOUNT-1; i++)
		{
			ret.bigint[i] = (ret.bigint[i] << (shift % 32)) + (ret.bigint[i+1] >> (32-(shift%32)));
		}
		ret.bigint[INTCOUNT - 1] = ret.bigint[INTCOUNT - 1] << (shift % 32);
	}
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator>>(int shift)
{
	ret = *this;
	if(shift == 1)
	{
		for(int i = INTCOUNT - 1; i >= INTCOUNT - ret.length; i--)
		{
			ret.bigint[i] = (ret.bigint[i] >> 1) + (ret.bigint[i-1] << 31);
		}
		ret.bigint[0] = ret.bigint[0] >> 1;
		if(ret.bigint[INTCOUNT - ret.length] == 0)
			ret.length--;
		return ret;
	}

	if(shift >= INTCOUNT * 32)
		return *null;
	if(shift/32 != 0)
	{
		for(int i = INTCOUNT - 1; i >= shift/32; i--)
		{
			ret.bigint[i] = ret.bigint[i - shift/32];
		}
	
		for(int i = 0; i < shift/32; i++)
		{
			ret.bigint[i] = 0;
		}
	}
	if(shift % 32 != 0)
	{
		for(int i = INTCOUNT - 1; i >= 1; i--)
		{
			ret.bigint[i] = (ret.bigint[i] >> (shift % 32)) + (ret.bigint[i-1] << (32-(shift%32)));
		}
		ret.bigint[0] = ret.bigint[0] >> (shift % 32);
	}
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator|(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] | a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator&(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] & a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator^(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] ^ a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator~()
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = ~this->bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}


/*################################################
			Comparison Operators
################################################*/
bool BigInt::operator<(BigInt a)
{
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] < a.bigint[i])
				return true;
			else if(this->bigint[i] > a.bigint[i])
				return false;
		}
		return false;
}

bool BigInt::operator<=(BigInt a)
{
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] < a.bigint[i])
				return true;
			else if(this->bigint[i] > a.bigint[i])
				return false;
		}
		return true;
}

bool BigInt::operator>(BigInt a)
{
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] > a.bigint[i])
				return true;
			else if(this->bigint[i] < a.bigint[i])
				return false;
		}
		return false;
}

bool BigInt::operator>=(BigInt a)
{
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] > a.bigint[i])
				return true;
			else if(this->bigint[i] < a.bigint[i])
				return false;
		}
		return true;
}

bool BigInt::operator==(BigInt a)
{
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] != a.bigint[i])
				return false;
		}
		return true;
}

bool BigInt::operator!=(BigInt a)
{
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] != a.bigint[i])
				return true;
		}
		return false;
}


/*################################################
			O= Operators
################################################*/
BigInt BigInt::operator<<=(int shift)
{
	if(shift == 1)
	{
		for(int i = 1; i < INTCOUNT; i++)
		{
			this->bigint[i] = (this->bigint[i] << 1) + (this->bigint[i+1] >> 31);
		}
		this->bigint[0] = this->bigint[0] << 1;
		if(this->bigint[INTCOUNT - this->length - 1] != 0)
			this->length++;
		return *this;
	}
	int div = shift/32;
	int mod = shift % 32;
	if(mod == 0 && div > 0)
	{
		for(int i = 0; i < INTCOUNT - div; i++)
			this->bigint[i] = this->bigint[i + div];
		for(int i = INTCOUNT - div; i < INTCOUNT; i++)
			this->bigint[i] = 0;
		this->length += (int) div;
		return *this;
	}
	if(shift >= INTCOUNT * 32)
		return *null;
	if(shift / 32 != 0)
	{
		for(int i = 0; i < INTCOUNT - div; i++)
		{
			this->bigint[i] = this->bigint[i + div];
		}
		for(int i = 0; i < div; i++)
		{
			this->bigint[INTCOUNT - 1 - i] = 0;
		}
	}
	if(mod != 0)
	{
		for(int i = 0; i < INTCOUNT-1; i++)
		{
			this->bigint[i] = (this->bigint[i] << (mod)) + (this->bigint[i+1] >> (32-(mod)));
		}
		this->bigint[INTCOUNT - 1] = this->bigint[INTCOUNT - 1] << (mod);
	}
	temp = 0;
	while(this->bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	this->length = INTCOUNT - temp;
	return *this;
}

BigInt BigInt::operator>>=(int shift)
{
	if(shift == 1)
	{
		for(int i = INTCOUNT - 1; i >= INTCOUNT - this->length; i--)
		{
			this->bigint[i] = (this->bigint[i] >> 1) + (this->bigint[i-1] << 31);
		}
		this->bigint[0] = this->bigint[0] >> 1;
		if(this->bigint[INTCOUNT - this->length] == 0)
			this->length--;
		return *this;
	}

	if(shift >= INTCOUNT * 32)
		return *null;
	int div = shift/32;
	int mod = shift % 32;
	if(div != 0)
	{
		for(int i = INTCOUNT - 1; i >= div; i--)
		{
			this->bigint[i] = this->bigint[i - div];
		}
	
		for(int i = 0; i < div; i++)
		{
			this->bigint[i] = 0;
		}
	}
	if(mod != 0)
	{
		for(int i = INTCOUNT - 1; i >= 1; i--)
		{
			this->bigint[i] = (this->bigint[i] >> (mod)) + (this->bigint[i-1] << (32-(mod)));
		}
		this->bigint[0] = this->bigint[0] >> (mod);
	}
	temp = 0;
	while(this->bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	this->length = INTCOUNT - temp;
	return *this;
}

inline BigInt BigInt::operator+=(BigInt a)
{
	carry = 0;
	int temp1 = this->length > a.length ? this->length : a.length;
	int i = INTCOUNT - 1;
	for(; i >= INTCOUNT - temp1 - 1; i--)
	{
		temp = this->bigint[i];
		this->bigint[i] = this->bigint[i] + a.bigint[i] + carry;
		carry = COMP_CARRY(temp, a.bigint[i], carry);
	}
	if(this->bigint[INTCOUNT - temp1-1] == 0)
		this->length = temp1;
	else
		this->length = temp1+1;
	return *this;
}

inline BigInt BigInt::operator-=(BigInt a)
{
	if(a >= *this)
		return *null;
	carry = 1;
	for(int i = INTCOUNT - 1;  i >= INTCOUNT - max(this->length, a.length)-1; i--)
	{
		temp = this->bigint[i];
		this->bigint[i] = this->bigint[i] + ~a.bigint[i] + carry;
		carry = COMP_CARRY(temp, ~a.bigint[i], carry);
	}
	temp = 0;
	while(!this->bigint[temp] && temp < INTCOUNT)
		temp++;
	this->length = INTCOUNT - temp;
	return *this;
}

BigInt BigInt::operator*=(BigInt a)
{
	if(this->length + a.length < INTCOUNT)
	{
		if(this->length <= a.length)
		{
			a1 = a;
			a2 = *this;
		}
		else
		{
			a1 = *this;
			a2 = a;
		}
		*this = BigInt(0);
		for(int i = 1; i <= a2.length; i++)
		{
			if(a2.bigint[INTCOUNT - i] & pow_2[0])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[1])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[2])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[3])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[4])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[5])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[6])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[7])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[8])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[9])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[10])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[11])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[12])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[13])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[14])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[15])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[16])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[17])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[18])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[19])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[20])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[21])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[22])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[23])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[24])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[25])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[26])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[27])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[28])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[29])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[30])
				*this += a1;
			a1 <<= 1;
			if(a2.bigint[INTCOUNT - i] & pow_2[31])
				*this += a1;
			a1 <<= 1;			
		}
		return *this;
	}
	else
	{
		*this = *null;
		return *null;
	}
}

inline BigInt BigInt::operator%=(BigInt a)
{
	if(a > *this)
		return *this;
	int temp = (this->length - a.length + 1) * 32;
	a <<= temp;
	for(int i = 0; i < temp; i++)
	{
		a >>= 1;
		if(*this >= a)
			*this -= a;
	}
	return *this;
}


/*################################################
			RSA relevant Methods
################################################*/
BigInt BigInt::random(BigInt max)
{
	ret.bigint[INTCOUNT - max.length] = ((rand()*0xff37) % (max.bigint[INTCOUNT - max.length]-1))+1;
	for(int i = INTCOUNT - max.length + 1; i < INTCOUNT; i++)
	{
		ret.bigint[i] = (unsigned int) rand()*0xff31;
	}
	ret.length = max.length;
	return ret;
}

bool BigInt::isMillerRabinPrime(BigInt n)
{
	for(int i = 0; i< 30; i++)
		if(!n.miller())
			return false;
	return true;
}

bool BigInt::miller()
{
	if(*this == *two)
		return true;
	if(*this == *one || *this == *null || this->bigint[INTCOUNT - 1] % 2 == 0)
		return false;
	//BigInt m, x;
	int l = 0;
	m = *this - *one;
	while(m.bigint[INTCOUNT - 1] % 2 == 0)
	{
		l++;
		m >>= 1;
	}
	x = BigInt::random(*this);
	x = BigInt::modExponentation(x,m,*this);
	m = *this - *one;
	if(x == *one || x == m)
		return true;
	for(int j = 1; j < l; j++)
	{
		x *= x;
		x %= *this;
		if(x == m)
			return true;
		else if(x == *one)
		{
			return false;
		}
	}
	return false;
}

BigInt BigInt::createPrime(int bits)
{
	bits--;
	srand(time(0));
	BigInt ret;
	while(true)
	{
		int temp = rand()*0xff0b + 0xff0a7;
		if(temp % 2 == 0)
			temp++;
		ret.bigint[INTCOUNT - 1] = temp;
		for(int i = INTCOUNT - 2; i >= INTCOUNT - bits/32; i--)
		{
			ret.bigint[i] = rand()*0xff30;
		}
		if(bits % 32 != 0)
		{
			ret.bigint[INTCOUNT - bits/32 - 1] = (rand()*0xff29) % (int) pow(2, (bits % 32)) + pow(2, bits % 32);
			ret.length = bits/32 + 1;
		}
		else
			ret.length = bits/32;
		int i = 0;
		while(true)
		{
			if(BigInt::isMillerRabinPrime(ret))
				return ret;
			i++;
			if(i == 1000);
				break;
			
		}
	}
}


/*###############################################
			Conversion Methods
###############################################*/
void BigInt::printHex2Console()
{
	cout.fill('0');
	cout.unsetf(ios_base::dec);
	cout.setf(ios_base::hex | ios_base::uppercase | ios_base::fixed);
	for(int i = 0; i < INTCOUNT; i++)
	{
		if(i % 8 == 0)
			cout << "\n";
		cout.width(8);
		cout << this->bigint[i] << " ";
	}
	cout.unsetf(ios_base::hex);
	cout.setf(ios_base::dec);
}

string BigInt::toHex(bool uppercase)
{
	string hex = "";
	char temp;
	BigInt sixteen(16), a = *this;
	while(a != *null)
	{
		switch((a % sixteen).bigint[INTCOUNT - 1])
		{
			case 0:		hex += '0';
						break;
			case 1:		hex += '1';
						break;
			case 2:		hex += '2';
						break;
			case 3:		hex += '3';
						break;
			case 4:		hex += '4';
						break;
			case 5:		hex += '5';
						break;
			case 6:		hex += '6';
						break;
			case 7:		hex += '7';
						break;
			case 8:		hex += '8';
						break;
			case 9:		hex += '9';
						break;
			case 10:	hex += 'A';
						break;
			case 11:	hex += 'B';
						break;
			case 12:	hex += 'C';
						break;
			case 13:	hex += 'D';
						break;
			case 14:	hex += 'E';
						break;
			case 15:	hex += 'F';
						break;
			default:	break;
		}
		a = a >> 4;
	}
	if(!uppercase)
	{
		for(int i = 0; i < (int) hex.length() / 2; i++)
		{
			temp = tolower(hex[i]);
			hex[i] = tolower(hex[hex.length() - 1 - i]);
			hex[hex.length() - 1 - i] = temp;
		}
	}
	else
	{
		for(int i = 0; i < (int) hex.length() / 2; i++)
		{
			temp = hex[i];
			hex[i] = hex[hex.length() - 1 - i];
			hex[hex.length() - 1 - i] = temp;
		}
	}
	hex.insert(0,"0x ");
	return hex;
}

void BigInt::fromHex(char *hex)
{
	int hexlength = strlen(hex);
	int start = 0;
	int chiffre = 0;
	if(hex[0] == '0' && toupper(hex[1]) == 'X' && hex[2] == ' ')
	{
		hexlength -= 3;
		start = 2;
	}
	for(int i = 0; i < hexlength; i++)
	{
		switch(toupper(hex[start + i]))
		{
			case '0':	chiffre = 0;
						break;
			case '1':	chiffre = 1;
						break;
			case '2':	chiffre = 2;
						break;
			case '3':	chiffre = 3;
						break;
			case '4':	chiffre = 4;
						break;
			case '5':	chiffre = 5;
						break;
			case '6':	chiffre = 6;
						break;
			case '7':	chiffre = 7;
						break;
			case '8':	chiffre = 8;
						break;
			case '9':	chiffre = 9;
						break;
			case 'A':	chiffre = 10;
						break;
			case 'B':	chiffre = 11;
						break;
			case 'C':	chiffre = 12;
						break;
			case 'D':	chiffre = 13;
						break;
			case 'E':	chiffre = 14;
						break;
			case 'F':	chiffre = 15;
						break;
		}
		*this = *this << 4;
		*this = *this + BigInt(chiffre);
	}
}