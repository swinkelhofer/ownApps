/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/

#ifndef BIGINT
#define BIGINT
#include <string>
#define BITLENGTH 1280
#define INTCOUNT BITLENGTH / 32
using namespace std;

class BigInt
{
public:
/*################################################
			Constructors + Settings
################################################*/
	BigInt();
	BigInt(int a);

/*################################################
			Arithmetic Operators
################################################*/
	inline BigInt operator+(BigInt a);
	inline BigInt operator-(BigInt a);
	BigInt operator*(BigInt a);
	inline BigInt operator%(BigInt a);
	BigInt operator++();
	BigInt operator++(int);
	BigInt operator--();
	BigInt operator--(int);

	inline static BigInt exponentation(BigInt base, BigInt exponent);
	inline static BigInt modExponentation(BigInt base, BigInt exponent, BigInt modulus);
	static BigInt modAddition(BigInt add1, BigInt add2, BigInt modulus);
	static BigInt modMultiplication(BigInt factor1, BigInt factor2, BigInt modulus);

/*################################################
			Bitwise Operators
################################################*/
	BigInt operator<<(int shift);
	BigInt operator>>(int shift);
	BigInt operator|(BigInt a);
	BigInt operator&(BigInt a);
	BigInt operator^(BigInt a);
	BigInt operator~();

/*################################################
			Comparison Operators
################################################*/
	bool operator<(BigInt a);
	bool operator<=(BigInt a);
	bool operator>(BigInt a);
	bool operator>=(BigInt a);
	bool operator==(BigInt a);
	bool operator!=(BigInt a);
	
/*################################################
			O= Operators
################################################*/
	BigInt operator<<=(int shift);
	BigInt operator>>=(int shift);
	inline BigInt operator+=(BigInt a);
	inline BigInt operator-=(BigInt a);
	BigInt operator*=(BigInt a);
	inline BigInt operator%=(BigInt a);

/*################################################
			RSA relevant Methods
################################################*/
	static BigInt random(BigInt max);
	static bool isMillerRabinPrime(BigInt n);
	static BigInt createPrime(int bits);
	bool miller();

/*###############################################
			Conversion Methods
###############################################*/
	void printHex2Console();
	string toHex(bool uppercase = true);
	void fromHex(char *hex);

	int length;
private:
/*###############################################
			Private Methods
###############################################*/
	inline unsigned int COMP_CARRY(unsigned int a, unsigned int b, unsigned int c);

/*###############################################
			Private Attributes
###############################################*/
	unsigned int bigint[INTCOUNT];
};

#endif
