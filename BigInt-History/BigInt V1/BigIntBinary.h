/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/


#ifndef BIG_INT
#define BIG_INT
#define BITLENGTH 1024
#include <string>
using namespace std;
typedef char* hexstring;
class BigIntBin
{
public:
	/*################################################
				Constructors + Settings
	################################################*/
	BigIntBin();
	BigIntBin(const char *bitfield);
	BigIntBin(int n);
	
	
	/*################################################
				Arithmetic Operators
	################################################*/
	BigIntBin operator+(BigIntBin a);
	BigIntBin operator-(BigIntBin a);
	BigIntBin operator*(BigIntBin a);
	BigIntBin operator/(BigIntBin a);
	BigIntBin operator%(BigIntBin a);
	BigIntBin operator++(int);
	BigIntBin operator++();
	BigIntBin operator--(int);
	BigIntBin operator--();
	BigIntBin div(BigIntBin a);
	BigIntBin mod(BigIntBin a);
	
	static BigIntBin exponentation(BigIntBin base, BigIntBin exponent);
	static BigIntBin modExponentation(BigIntBin base, BigIntBin exponent, BigIntBin modulus);
	static BigIntBin modAddition(BigIntBin add1, BigIntBin add2, BigIntBin modulus);
	static BigIntBin modMultiplication(BigIntBin factor1, BigIntBin factor2, BigIntBin modulus);
	
	
	/*################################################
				Bitwise Operators
	################################################*/
	BigIntBin operator<<(int shift);
	BigIntBin operator>>(int shift);
	BigIntBin operator&(BigIntBin a);
	BigIntBin operator|(BigIntBin a);
	BigIntBin operator^(BigIntBin a);	
	BigIntBin operator!();	
	
	
	/*################################################
				Comparison Operators
	################################################*/
	bool operator<(BigIntBin a);
	bool operator<=(BigIntBin a);
	bool operator>(BigIntBin a);
	bool operator>=(BigIntBin a);
	bool operator==(BigIntBin a);
	bool operator!=(BigIntBin a);
	
	
	/*###############################################
				Conversion Methods
	###############################################*/
	string toBin();
	void fromBin(const char *bitfield);
	string toHex();
	void fromHex(const char *hex);
	string toDec();																		//Dringend überarbeiten!!!!
	void fromDec(const char *dec);
	string toDecMod();
	
	
	/*################################################
				RSA relevant Methods
	################################################*/
	static BigIntBin random(BigIntBin max);
	static bool isMillerRabinPrime(BigIntBin n);     									//Miller-Rabin 30 Checks...
	static BigIntBin createPrime(int bits);
	//static BigIntBin extendedEuclidianAlgorithm(BigIntBin a1, BigIntBin a2);   		//Modulares Inverses  --> RSA: e*d + k*phi(N) = 1 (ggT(e,phi(N))   mit phi(N) = (p-1)(q-1)   (a1 = e; a2 = phi(N))
	//static BigIntBin euclidianAlgorithm(BigIntBin a1, BigIntBin a2);					//ggT(a1, a2) zur Teilerfremdheit, oder e = 65537 = 2^16+1

	int getLength();

private:
	/*###############################################
				Private Attributes
	###############################################*/
	bool bitfield0[BITLENGTH];
	int length;
	
	/*###############################################
				Private Methods
	###############################################*/
	BigIntBin(bool bitfield[BITLENGTH]);
	static bool millerRabinTest(BigIntBin n);
};
#endif
