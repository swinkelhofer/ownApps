/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/

#include "BigIntBinary.h"
#include <string.h>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <iostream>
#include <ctype.h>
#include <time.h>
#define BITLENGTH 1024
using namespace std;


const BigIntBin null("0");
const BigIntBin one("1");
const BigIntBin two("10");

/*################################################
			Constructors + Settings
################################################*/
	
BigIntBin::BigIntBin()
{
	for(int i = 0; i < BITLENGTH; i++)
		bitfield0[i] = false;
	length = 0;
}

BigIntBin::BigIntBin(int n)
{
	length = 0;
}

BigIntBin::BigIntBin(const char *bitfield)
{
	if(strlen(bitfield) <= BITLENGTH)
	{
		for(int i = 0; i < BITLENGTH; i++)
			bitfield0[i] = false;
		for(unsigned int i = 0; i < strlen(bitfield); i++)
			if(bitfield[i] == '1')
				bitfield0[BITLENGTH - strlen(bitfield) + i] = true;
		int i = 0;
		while(!this->bitfield0[i] && i < BITLENGTH)
			i++;
		length = BITLENGTH - i;
	}
}

inline BigIntBin::BigIntBin(bool bitfield[BITLENGTH])
{
	for(int i = 0; i < BITLENGTH; i++)
		bitfield0[i] = bitfield[i];
	int i = 0;
	while(!this->bitfield0[i] && i < BITLENGTH)
		i++;
	length = BITLENGTH - i;
}

/*################################################
			Arithmetic Operators
################################################*/

BigIntBin BigIntBin::operator+(BigIntBin a)
{
	BigIntBin ret;
	bool carry = false;
	if(a.length >= this->length)
		ret.length = a.length;
	else
		ret.length = this->length;
	for(int i = BITLENGTH - 1; i >= BITLENGTH - ret.length - 1; i--)
	{
		ret.bitfield0[i] = this->bitfield0[i] ^ a.bitfield0[i] ^ carry;
		carry = (this->bitfield0[i] & a.bitfield0[i]) | (a.bitfield0[i] & carry) | (this->bitfield0[i] & carry);
	}
	int i = 0;
	while(!ret.bitfield0[i])
		i++;
	ret.length = BITLENGTH - i;
	return ret;
}

BigIntBin BigIntBin::operator-(BigIntBin a)
{
	if(a < *this)
	{
		BigIntBin a1(this->bitfield0), a2(a.bitfield0);
		for(int i = BITLENGTH - a1.length; i < BITLENGTH; i++)
		{
			a2.bitfield0[i] = !a2.bitfield0[i];
		}
		a2.length = a1.length;
		a2 = a2 + one;
		a1 = a2 + a1;
		a1.bitfield0[BITLENGTH - a1.length] = false;
		int i = 0;
		while(!a1.bitfield0[i])
			i++;
		a1.length = BITLENGTH - i;
		return a1;
	}
	else
		return BigIntBin("0");
}

BigIntBin BigIntBin::operator*(BigIntBin a)
{
	if(a.length + this->length < BITLENGTH)
	{
		BigIntBin a1(0), a2(0), ret;
		if(this->length >= a.length)
		{
			/*for(int i = 0; i < BITLENGTH; i++)
			{
				a1.bitfield0[i] = this->bitfield0[i];
				a2.bitfield0[i] = a.bitfield0[i];
			}
			a1.length = this->length;
			a2.length = a.length;*/
			a1 = *this;
			a2 = a;
		}
		else
		{
			/*for(int i = 0; i < BITLENGTH; i++)
			{
				a1.bitfield0[i] = a.bitfield0[i];
				a2.bitfield0[i] = this->bitfield0[i];
			}
			a1.length = a.length;
			a2.length = this->length;*/
			a1 = a;
			a2 = *this;
		}
		for(int i = 0; i < a2.length; i++)
		{
			if(a2.bitfield0[BITLENGTH-1-i])
				ret = ret + a1;
			a1 = a1 << 1;
		}
		return ret;
	}
	else
		return BigIntBin("0");
}	

BigIntBin BigIntBin::operator/(BigIntBin a)
{
	if(*this < a)
		return null;
	if(*this == a)
		return one;
	bool B[BITLENGTH + 1], inv_B[BITLENGTH + 1];
	bool RA[2*BITLENGTH + 1];
	int shift = BITLENGTH -  a.length;
	int quotientenbit[BITLENGTH];
	B[0] = false;
	RA[0] = false;
	inv_B[0] = true;
	for(int i = 1; i < BITLENGTH+1; i++)
	{
		if(i < BITLENGTH + 1 - shift)
			B[i] = a.bitfield0[i-1+shift];
		else
			B[i] = false;
		inv_B[i]= !B[i];
		RA[i] = false;
		RA[i + BITLENGTH] = this->bitfield0[i-1];
	}
	bool carry = true;
	int i = BITLENGTH;
	bool temp = false;
	while(carry && i >= 0)
	{
		temp = inv_B[i];
		inv_B[i] = inv_B[i] ^ carry;
		carry = temp & carry;
		i--;
	}
	for(int i = 0; i < 2 * BITLENGTH + 1; i++)
	{
		if(i < 2*BITLENGTH + 1 - shift)
			RA[i] = RA[i+shift];
		else
			RA[i] = false;
	}
	for(int i = 0; i < BITLENGTH; i++)
	{
		if((RA[0] & RA[1] & RA[2]) || !(RA[0] | RA[1] | RA[2]))
		{
			quotientenbit[i] = 0;
			for(int j = 0; j < 2*BITLENGTH; j++)
				RA[j] = RA[j+1];
			RA[2*BITLENGTH] = false;
		}
		else if(!RA[0])
		{
			quotientenbit[i] = 1;
			for(int j = 0; j < 2*BITLENGTH; j++)
				RA[j] = RA[j+1];
			RA[2*BITLENGTH] = false;
			carry = false;
			for(int j = BITLENGTH; j >= 0; j--)
			{
				temp = RA[j];
				RA[j] = RA[j] ^ inv_B[j] ^ carry;
				carry = (temp & inv_B[j]) | (temp & carry) | (inv_B[j] & carry);
			}
		}
		else if(RA[0])
		{
			quotientenbit[i] = -1;
			for(int j = 0; j < 2*BITLENGTH; j++)
				RA[j] = RA[j+1];
			RA[2*BITLENGTH] = false;
			carry = false;
			for(int j = BITLENGTH; j >= 0; j--)
			{
				temp = RA[j];
				RA[j] = RA[j] ^ B[j] ^ carry;
				carry = (temp & B[j]) | (temp & carry) | (B[j] & carry);
			}
		}
	}
	for(int i = 0; i < BITLENGTH; i++)
	{
		if(quotientenbit[i] == 0)
		{
			B[i] = false;
			inv_B[i] = true;
		}
		else if(quotientenbit[i] == 1)
		{
			B[i] = true;
			inv_B[i] = true;
		}
		else
		{
			B[i] = false;
			inv_B[i] = false;
		}
	}
	i = BITLENGTH - 1;
	carry = true;
	while(carry && i >= 0)
	{
		temp = inv_B[i];
		inv_B[i] = inv_B[i] ^ carry;
		carry = temp & carry;
		i--;
	}
	carry = false;
	for(int i = BITLENGTH - 1; i >= 0; i--)
	{
		temp = B[i];
		B[i] = B[i] ^ inv_B[i] ^ carry;
		carry = (temp & inv_B[i]) | (temp & carry) | (inv_B[i] & carry);
	}
	if(RA[0])
	{
		carry = false;
		for(int i = BITLENGTH - 1; i >= 0; i--)
		{
			temp = B[i];
			B[i] = B[i] ^ carry ^ true;
			carry = (temp & true) | (temp & carry) | (true & carry);
		}
	}
	BigIntBin ret(0);
	for(int i = 0; i < BITLENGTH; i++)
		ret.bitfield0[i] = B[i];
	i = 0;
	while(!ret.bitfield0[i] && i < BITLENGTH)
		i++;
	ret.length = BITLENGTH - i;
	return ret;
}

BigIntBin BigIntBin::operator%(BigIntBin a)
{
	if(*this < a)
		return *this;
	bool B[BITLENGTH + 1], inv_B[BITLENGTH + 1];
	bool RA[2*BITLENGTH + 1];
	int shift = BITLENGTH -  a.length;
	B[0] = false;
	RA[0] = false;
	inv_B[0] = true;
	for(int i = 1; i < BITLENGTH+1; i++)
	{
		if(i < BITLENGTH + 1 - shift)
			B[i] = a.bitfield0[i-1+shift];
		else
			B[i] = false;
		inv_B[i]= !B[i];
		RA[i] = false;
		RA[i + BITLENGTH] = this->bitfield0[i-1];
	}
	bool carry = true;
	int i = BITLENGTH;
	bool temp = false;
	while(carry && i >= 0)
	{
		temp = inv_B[i];
		inv_B[i] = inv_B[i] ^ carry;
		carry = temp & carry;
		i--;
	}
	for(int i = 0; i < 2 * BITLENGTH + 1; i++)
	{
		if(i < 2*BITLENGTH + 1 - shift)
			RA[i] = RA[i+shift];
		else
			RA[i] = false;
	}
	for(int i = 0; i < BITLENGTH; i++)
	{
		if((RA[0] & RA[1] & RA[2]) || !(RA[0] | RA[1] | RA[2]))
		{
			for(int j = 0; j < 2*BITLENGTH; j++)
				RA[j] = RA[j+1];
			RA[2*BITLENGTH] = false;
		}
		else if(!RA[0])
		{
			for(int j = 0; j < 2*BITLENGTH; j++)
				RA[j] = RA[j+1];
			RA[2*BITLENGTH] = false;
			carry = false;
			for(int j = BITLENGTH; j >= 0; j--)
			{
				temp = RA[j];
				RA[j] = RA[j] ^ inv_B[j] ^ carry;
				carry = (temp & inv_B[j]) | (temp & carry) | (inv_B[j] & carry);
			}
		}
		else if(RA[0])
		{
			for(int j = 0; j < 2*BITLENGTH; j++)
				RA[j] = RA[j+1];
			RA[2*BITLENGTH] = false;
			carry = false;
			for(int j = BITLENGTH; j >= 0; j--)
			{
				temp = RA[j];
				RA[j] = RA[j] ^ B[j] ^ carry;
				carry = (temp & B[j]) | (temp & carry) | (B[j] & carry);
			}
		}
	}
	if(RA[0])
	{
		carry = false;
		for(int j = BITLENGTH; j >= 0; j--)
		{
			temp = RA[j];
			RA[j] = RA[j] ^ B[j] ^ carry;
			carry = (temp & B[j]) | (temp & carry) | (B[j] & carry);
		}
	}
	for(int i = BITLENGTH+1;i >= 0; i--)
	{
		if(i >= shift)
			RA[i] = RA[i-shift];
		else
			RA[i] = false;
	}
		RA[i] = RA[i-shift];
	BigIntBin ret(0);
	for(int i = 0; i < BITLENGTH; i++)
		ret.bitfield0[i] = RA[i+1];
	i = 0;
	while(!ret.bitfield0[i] && i < BITLENGTH)
		i++;
	ret.length = BITLENGTH - i;
	return ret;
}

BigIntBin BigIntBin::operator++()
{
	bool carry = true, temp = false;
	int i = BITLENGTH-1;
	while(carry && i >= 0)
	{
		temp = this->bitfield0[i];
		this->bitfield0[i] = this->bitfield0[i] ^ carry;
		carry = temp & carry;
		i--;
	}
	i = 0;
	while(i < BITLENGTH && !this->bitfield0[i])
		i++;
	this->length = BITLENGTH - i;
	return *this;
}

BigIntBin BigIntBin::operator++(int)
{
	BigIntBin ret(this->bitfield0);
	bool carry = true, temp = false;
	int i = BITLENGTH-1;
	while(carry && i >= 0)
	{
		temp = this->bitfield0[i];
		this->bitfield0[i] = this->bitfield0[i] ^ carry;
		carry = temp & carry;
		i--;
	}
	i = 0;
	while(i < BITLENGTH && !this->bitfield0[i])
		i++;
	this->length = BITLENGTH - i;
	return ret;
}

BigIntBin BigIntBin::operator--()
{
	bool carry = false, temp;
	for(int i = BITLENGTH - 1; i >= 0; i--)
	{
		temp = this->bitfield0[i];
		this->bitfield0[i] = this->bitfield0[i] ^ carry ^ true;
		carry = (temp & true) | (temp & carry) | (true & carry);
	}
	int i = 0;
	while(!this->bitfield0[i] && i >= 0)
		i++;
	this->length = BITLENGTH - i;
	return *this;
}

BigIntBin BigIntBin::operator--(int)
{
	BigIntBin ret = *this;
	bool carry = false, temp;
	for(int i = BITLENGTH - 1; i >= 0; i--)
	{
		temp = this->bitfield0[i];
		this->bitfield0[i] = this->bitfield0[i] ^ carry ^ true;
		carry = (temp & true) | (temp & carry) | (true & carry);
	}
	int i = 0;
	while(!this->bitfield0[i] && i >= 0)
		i++;
	this->length = BITLENGTH - i;
	return *this;
	return ret;
}

BigIntBin BigIntBin::div(BigIntBin a)
{
	BigIntBin a1 = *this, a2 = a, ret;
	if(a1.length < a2.length)
		return null;
	int lengthsave = a2.length;
	a2 = a2 << (a1.length - a2.length);
	if(a1 < a2)
	{
		ret.length = a1.length - lengthsave;
		a2 = a2 >> 1;
		a1 = a1 - a2;
		ret.bitfield0[BITLENGTH - ret.length] = true;
	}
	else
	{
		ret.length = a1.length - lengthsave + 1;
		a1 = a1 - a2;
		ret.bitfield0[BITLENGTH - ret.length] = true;
	}
	for(int i = 1; i < ret.length; i++)
	{
		a2 = a2 >> 1;
		if(a2 > a1)
		{
			ret.bitfield0[BITLENGTH - ret.length + i] = false;
		}
		else
		{
			a1 = a1 - a2;
			ret.bitfield0[BITLENGTH - ret.length + i] = true;
		}
	}
	return ret;
}

BigIntBin BigIntBin::mod(BigIntBin a)
{
	BigIntBin a1 = *this, a2 = a, ret;
	if(a1 < a2)
		return a1;
	int lengthsave = a2.length;
	a2 = a2 << (a1.length - lengthsave);
	if(a1 < a2)
	{
		lengthsave = a1.length - lengthsave - 1;
		a2 = a2 >> 1;
		a1 = a1 - a2;
	}
	else
	{
		lengthsave = a1.length - lengthsave;
		a1 = a1 - a2;
	}
	for(int i = 0; i < lengthsave;)
	{
		/*if(a1.length < a2.length)
		{
			i = i + a2.length - a1.length;
			a2 = a2 >> (a2.length - a1.length);
		}*/
		
		a2 = a2 >> 1;
		i++;
	
		if(a1 >= a2)
			a1 = a1 - a2;
		//i++;
		//while(a1 < a2 && i < lengthsave)
		//{
		//	a2 = a2 >> 1;
		//	i++;
		//}
		//1 = a1 - a2;
		//else if(a1 >= a2)
		//{
		//}
	}
	return a1;
}

BigIntBin BigIntBin::exponentation(BigIntBin base, BigIntBin exponent)
{
	BigIntBin ret = one, a1 = base;
	int i = 0;
	while(i < exponent.length)
	{
		if(exponent.bitfield0[BITLENGTH - 1 - i])
			ret = ret * a1;
		a1 = a1 * a1;
		i++;
	}
	return ret;
}

BigIntBin BigIntBin::modExponentation(BigIntBin base, BigIntBin exponent, BigIntBin modulus)
{
	BigIntBin ret = one, a1 = base;
	int i = 0;
	while(i < exponent.length)
	{
		if(exponent.bitfield0[BITLENGTH - 1 - i])
			ret = (ret * a1).mod(modulus);
		a1 = (a1* a1).mod(modulus);
		i++;
	}
	return ret;
}

BigIntBin BigIntBin::modAddition(BigIntBin add1, BigIntBin add2, BigIntBin modulus)
{
	return (add1+add2)%modulus;
}

BigIntBin BigIntBin::modMultiplication(BigIntBin factor1, BigIntBin factor2, BigIntBin modulus)
{
	return (factor1*factor2)%modulus;
}

/*################################################
			Bitwise Operators
################################################*/

BigIntBin BigIntBin::operator<<(int shift)
{
	BigIntBin ret(this->bitfield0);
	if(ret.length + shift <= BITLENGTH)
	{
		for(int i = BITLENGTH - ret.length; i < BITLENGTH; i++)
			ret.bitfield0[i-shift] = ret.bitfield0[i];
		for(int i = 1; i <= shift; i++)
			ret.bitfield0[BITLENGTH - i] = false;
		ret.length = ret.length + shift;
	}
	else
	{
		for(int i = 0; i < BITLENGTH-shift; i++)
			ret.bitfield0[i] = ret.bitfield0[i+shift];
		for(int i = BITLENGTH - shift; i < BITLENGTH; i++)
			ret.bitfield0[i] = false;
	}
	return ret;
}

BigIntBin BigIntBin::operator>>(int shift)
{
	if(shift <= this->length)
	{
		BigIntBin ret(this->bitfield0);
		for(int i = BITLENGTH-1; i >= BITLENGTH - ret.length; i--)
		{
			ret.bitfield0[i] = ret.bitfield0[i-shift];
		}
		for(int i = 0; i < shift; i++)
			ret.bitfield0[BITLENGTH - ret.length + i] = false;
		ret.length = ret.length - shift;
		return ret;
	}
	else
	{
		return BigIntBin("0");
	}
}

BigIntBin BigIntBin::operator&(BigIntBin a)
{
	BigIntBin ret(this->bitfield0);
	for(int i = 0; i < BITLENGTH; i++)
		ret.bitfield0[i] = ret.bitfield0[i] & a.bitfield0[i];
	return ret;
}

BigIntBin BigIntBin::operator|(BigIntBin a)
{
	BigIntBin ret(this->bitfield0);
	for(int i = 0; i < BITLENGTH; i++)
		ret.bitfield0[i] = ret.bitfield0[i] | a.bitfield0[i];
	return ret;
}

BigIntBin BigIntBin::operator^(BigIntBin a)
{
	BigIntBin ret(this->bitfield0);
	for(int i = 0; i < BITLENGTH; i++)
		ret.bitfield0[i] = ret.bitfield0[i] ^ a.bitfield0[i];
	return ret;
}

BigIntBin BigIntBin::operator!()
{
	BigIntBin ret(this->bitfield0);
	for(int i = 0; i < BITLENGTH; i++)
		ret.bitfield0[i] = !ret.bitfield0[i];
	return ret;
}

/*################################################
			Comparison Operators
################################################*/

bool BigIntBin::operator<(BigIntBin a)
{
	if(this->length < a.length)
		return true;
	else if(this->length > a.length)
		return false;
	else if(this->length == a.length)
	{
		for(int i = BITLENGTH - a.length; i < BITLENGTH; i++)
		{
			if(!a.bitfield0[i] && this->bitfield0[i])
				return false;
			else if(a.bitfield0[i] && !this->bitfield0[i])
				return true;
		}
		return false;
	}
}

bool BigIntBin::operator<=(BigIntBin a)
{
	if(this->length < a.length)
		return true;
	else if(this->length > a.length)
		return false;
	else if(this->length == a.length)
	{
		for(int i = BITLENGTH - a.length; i < BITLENGTH; i++)
		{
			if(!a.bitfield0[i] && this->bitfield0[i])
				return false;
			else if(a.bitfield0[i] && !this->bitfield0[i])
				return true;
		}
		return true;
	}
}

bool BigIntBin::operator>(BigIntBin a)
{
	if(this->length > a.length)
		return true;
	else if(this->length < a.length)
		return false;
	else if(this->length == a.length)
	{
		for(int i = BITLENGTH - a.length; i < BITLENGTH; i++)
		{
			if(!a.bitfield0[i] && this->bitfield0[i])
				return true;
			else if(a.bitfield0[i] && !this->bitfield0[i])
				return false;
		}
		return false;
	}
}

bool BigIntBin::operator>=(BigIntBin a)
{
	if(this->length > a.length)
		return true;
	else if(this->length < a.length)
		return false;
	else if(this->length == a.length)
	{
		for(int i = BITLENGTH - a.length; i < BITLENGTH; i++)
		{
			if(!a.bitfield0[i] && this->bitfield0[i])
				return true;
			else if(a.bitfield0[i] && !this->bitfield0[i])
				return false;
		}
		return true;
	}
}

bool BigIntBin::operator==(BigIntBin a)
{
	if(this->length != a.length)
		return false;
	else
	{
		for(int i = BITLENGTH - a.length; i < BITLENGTH; i++)
		{
			if(a.bitfield0[i] ^ this->bitfield0[i])
				return false;
		}
		return true;
	}
}

bool BigIntBin::operator!=(BigIntBin a)
{
	if(this->length != a.length)
		return true;
	else
	{
		for(int i = BITLENGTH - a.length; i < BITLENGTH; i++)
		{
			if(a.bitfield0[i] ^ this->bitfield0[i])
				return true;
		}
		return false;
	}
}

/*###############################################
			Conversion Methods
###############################################*/

string BigIntBin::toBin()
{
	string ret = "0b ";
	int quadro = this->length;
	while(quadro % 4 != 0)
		quadro++;
	for(int i = BITLENGTH - quadro; i < BITLENGTH; i++)
	{
		if(bitfield0[i])
			ret += "1";
		else
			ret += "0";
		if((i+1) % 4 == 0)
			ret += " ";
	}
	return ret;
}

void BigIntBin::fromBin(const char *bitfield)
{
	if(strlen(bitfield) <= BITLENGTH)
	{
		for(int i = 0; i < BITLENGTH; i++)
			bitfield0[i] = false;
		for(unsigned int i = 0; i < strlen(bitfield); i++)
			if(bitfield[i] == '1')
				bitfield0[BITLENGTH - strlen(bitfield) + i] = true;
		int i = 0;
		while(!bitfield0[i])
		{
			if(i == BITLENGTH)
				break;
			i++;
		}
		length = BITLENGTH - i;
	}
}

string BigIntBin::toHex()
{
	if(this->length != 0)
	{
		int quatro = this->length;
		string ret = "0x ";
		int chiffre;
		while(quatro % 4 != 0)
			quatro++;
		if(quatro % 8 != 0)
			ret += "0";
		for(int i = 0; i < quatro / 4; i++)
		{
			chiffre = 0;
			for(int j = 0; j < 4; j++)
			{
				if(this->bitfield0[BITLENGTH - quatro + i * 4 + j])
					chiffre = chiffre + pow(2,3-j);
			}
			switch(chiffre)
			{
				case 0: ret = ret + '0';
						break;
				case 1: ret = ret + '1';
						break;
				case 2: ret = ret + '2';
						break;
				case 3: ret = ret + '3';
						break;
				case 4: ret = ret + '4';
						break;
				case 5: ret = ret + '5';
						break;
				case 6: ret = ret + '6';
						break;
				case 7: ret = ret + '7';
						break;
				case 8: ret = ret + '8';
						break;
				case 9: ret = ret + '9';
						break;
				case 10: ret = ret + 'A';
						break;
				case 11: ret = ret + 'B';
						break;
				case 12: ret = ret + 'C';
						break;
				case 13: ret = ret + 'D';
						break;
				case 14: ret = ret + 'E';
						break;
				case 15: ret = ret + 'F';
						break;
				default: ret = ret + '0';
						break;
			}
			if((i+((quatro +4)%8)/4) % 2 == 0)
				ret += " ";
		}
		return ret;
	}
	else
	{
		string ret = "0x 0";
		return ret;
	}
}

void BigIntBin::fromHex(const char *hex)
{
	if(strlen(hex) * 4 <= BITLENGTH)
	{
		for(int i = 0; i < BITLENGTH; i++)
			this->bitfield0[i]=false;
		this->length = strlen(hex) * 4;
		for(int i = BITLENGTH - this->length; i < BITLENGTH; i+=4)
		{
			switch(toupper(hex[(i-BITLENGTH+this->length)/4]))
			{
				case '0':
					break;
				case '1':
					this->bitfield0[i+3] = true;
					break;
				case '2':
					this->bitfield0[i+2] = true;
					break;
				case '3':
					this->bitfield0[i+2] = true;
					this->bitfield0[i+3] = true;
					break;
				case '4':
					this->bitfield0[i+1] = true;
					break;
				case '5':
					this->bitfield0[i+1] = true;
					this->bitfield0[i+3] = true;
					break;
				case '6':
					this->bitfield0[i+1] = true;
					this->bitfield0[i+2] = true;
					break;
				case '7':
					this->bitfield0[i+1] = true;
					this->bitfield0[i+2] = true;
					this->bitfield0[i+3] = true;
					break;
				case '8':
					this->bitfield0[i] = true;
					break;
				case '9':
					this->bitfield0[i] = true;
					this->bitfield0[i+3] = true;
					break;
				case 'A':
					this->bitfield0[i] = true;
					this->bitfield0[i+2] = true;
					break;
				case 'B':
					this->bitfield0[i] = true;
					this->bitfield0[i+2] = true;
					this->bitfield0[i+3] = true;
					break;
				case 'C':
					this->bitfield0[i] = true;
					this->bitfield0[i+1] = true;
					break;
				case 'D':
					this->bitfield0[i] = true;
					this->bitfield0[i+1] = true;
					this->bitfield0[i+3] = true;
					break;
				case 'E':
					this->bitfield0[i] = true;
					this->bitfield0[i+1] = true;
					this->bitfield0[i+2] = true;
					break;
				case 'F':
					this->bitfield0[i] = true;
					this->bitfield0[i+1] = true;
					this->bitfield0[i+2] = true;
					this->bitfield0[i+3] = true;
					break;
				default:
					break;
			}
		}
		int count = 0;
		while(!this->bitfield0[count] && count < BITLENGTH)
			count++;
		this->length = BITLENGTH - count;
	}
}

string BigIntBin::toDec()
{
	string dec = "";
	BigIntBin a1(this->bitfield0), mod, ten("1010");
	int chiffre = 0;
	while(a1 > BigIntBin("0"))
	{
		chiffre = 0;
		mod = a1 % ten;
		a1 = a1 / ten;
		for(int i = 0; i < 4; i++)
			if(mod.bitfield0[BITLENGTH - 1 - i])
				chiffre = chiffre + pow(2, i);
		dec += (char) chiffre + 48;
	}
	char temp;
	for(int i = 0; i < dec.length() /2; i++)
	{
		temp = dec[i];
		dec[i] = dec[dec.length() - 1 - i];
		dec[dec.length() - 1 - i] = temp;
	}
	if(dec == "")
		dec = "0";
	dec.insert(0,"0d ");
	return dec;
}

string BigIntBin::toDecMod()
{
	string dec = "";
	BigIntBin a1(this->bitfield0), mod, ten("1010");
	int chiffre = 0;
	while(a1 > BigIntBin("0"))
	{
		chiffre = 0;
		mod = a1.mod(ten);
		a1 = a1 / ten;
		for(int i = 0; i < 4; i++)
			if(mod.bitfield0[BITLENGTH - 1 - i])
				chiffre = chiffre + pow(2, i);
		dec += (char) chiffre + 48;
	}
	char temp;
	for(int i = 0; i < dec.length() /2; i++)
	{
		temp = dec[i];
		dec[i] = dec[dec.length() - 1 - i];
		dec[dec.length() - 1 - i] = temp;
	}
	if(dec == "")
		dec = "0";
	dec.insert(0,"0d ");
	return dec;
}

void BigIntBin::fromDec(const char *dec)
{
	BigIntBin ten("1010");
	BigIntBin temp;
	BigIntBin ret;
	int count = 0;
	int charlength = strlen(dec);
	while(dec[count] == '0' && count < charlength)
		count++;
	charlength -= count;
	for(int i = charlength - 1; i >= 0; i--)
	{
		temp.fromBin("0");
		switch(dec[i])
		{
			case '0':	temp.fromBin("0000");
						break;
			case '1':	temp.fromBin("0001");
						break;
			case '2':	temp.fromBin("0010");
						break;
			case '3':	temp.fromBin("0011");
						break;
			case '4':	temp.fromBin("0100");
						break;
			case '5':	temp.fromBin("0101");
						break;
			case '6':	temp.fromBin("0110");
						break;
			case '7':	temp.fromBin("0111");
						break;
			case '8':	temp.fromBin("1000");
						break;
			case '9':	temp.fromBin("1001");
						break;
			default:	temp.fromBin("0");
						break;
		}
		for(int j = 0; j < charlength - 1 - i; j++)
			temp = temp * ten;
		ret = ret + temp;
	}
	*this = ret;
}

BigIntBin BigIntBin::random(BigIntBin max)
{
	BigIntBin ret;
	cout << "BEGIN random\n";
	srand((unsigned)time(NULL));
	for(int i = 0; i < BITLENGTH; i++)
		ret.bitfield0[i] = rand() % 2;
	int i=0;
	while(!ret.bitfield0[i] && i < BITLENGTH)
		i++;
	ret.length = BITLENGTH-i;
	cout << "END random\n";
	return ret%max;
}

bool BigIntBin::isMillerRabinPrime(BigIntBin n)
{
	cout << "BEGIN isMillerRabinPrime\n";
	for(int i = 0; i < 30; i++)
	{
		if(!millerRabinTest(n))
		{
			cout << "END isMillerRabinPrime\n\n";
			return false;
		}
	}
	cout << "END isMillerRabinPrime\n\n";
	return true;
}

bool BigIntBin::millerRabinTest(BigIntBin n)
{
	cout << "BEGIN millerRabinTest\n";
	if(!n.bitfield0[BITLENGTH-1])
	{
		cout << "END millerRabinTest\n";
		return false;
	}
	BigIntBin m = n;
	m--;
	int l0 = 0;
	while(!m.bitfield0[BITLENGTH - 1 - l0] && l0 < BITLENGTH)
	{
		l0++;
	}
	int i = 0;
	int temp = l0;
	/*while(l0 > 0)
	{
		l.bitfield0[BITLENGTH - 1 - i] = l0 % 2;
		l0 = l0 / 2;
		i++;
	}*/
	//l.length = BITLENGTH - i;
	BigIntBin bigtemp = n >> l0;
	//bigtemp = n / (BigIntBin::exponentation(two, l));
	BigIntBin x = BigIntBin::random(n - one);
	if(x == null)
		x++;
		
	cout << "BEGIN x = x^m mod n\n";
	x = BigIntBin::modExponentation(x, bigtemp, n);

	cout << "END x = x^m mod n\n";
	
	if(x == one || x == m)
	{
		cout << "END millerRabinTest\n";
		return true;
	}
	for(int i = 1; i < temp; i++)
	{
		cout << "BEGIN x = x^2 mod n\n";
		x = ((x) * (x)) % n;
		cout << "END x = x^2 mod n\n";
		if(x.length == 1 && x.bitfield0[BITLENGTH - 1])
		{
			cout << "END millerRabinTest\n";
			return false;
		}
		else if(x == m)
		{
			cout << "END millerRabinTest\n";
			return true;
		}
		cout << i << ":\n";
	}
	cout << "END millerRabinTest\n";
	return false;
}

BigIntBin BigIntBin::createPrime(int bits)
{
	BigIntBin ret;
	ret.length = bits;
	BigIntBin two("10");
	ret.bitfield0[BITLENGTH - bits] = true;
	srand(time(NULL));
	for(int i = 2; i < bits - 1; i++)
		ret.bitfield0[BITLENGTH - i] = rand() % 2;
	ret.bitfield0[BITLENGTH - 1] = true;
	int i = 0;
	while(!ret.isMillerRabinPrime(ret))
	{
		ret = ret + two;
		i++;
	}
	return ret;
}

int BigIntBin::getLength()
{
	return this->length;
}
