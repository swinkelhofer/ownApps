#include "BigInt.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define BITLENGTH 1024
#define INTCOUNT BITLENGTH / 32

BigInt::BigInt()
{
	for(int i = 0; i < INTCOUNT; i++)
		bigint[i] = 0;
	length = 0;
}

BigInt BigInt::operator+(BigInt a)
{
	unsigned int carry = 0;
	unsigned int temp;
	BigInt ret;
	for(int i = INTCOUNT - 1; i >= 0; i--)
	{
		ret.bigint[i] = this->bigint[i] + a.bigint[i] + carry;
		carry = (unsigned int) ((this->bigint[i] + a.bigint[i] + carry < a.bigint[i]) || (this->bigint[i] + a.bigint[i] + carry < this->bigint[i]));
	}
	return ret;
}
