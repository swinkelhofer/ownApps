#define BITLENGTH 1024
#define INTCOUNT BITLENGTH / 32

class BigInt
{
public:
	BigInt();
	BigInt operator+(BigInt a);
	unsigned int bigint[INTCOUNT];
private:
	int length;
};