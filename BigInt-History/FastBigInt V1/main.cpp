#include "BigInt.h"
#include <iostream>
using namespace std;
int main()
{
	cout.unsetf(ios_base::dec);
	cout.setf(ios_base::hex | ios_base::uppercase | ios_base::fixed);
	BigInt big;
	big.bigint[31] = 0xFA09DA41;
	big.bigint[30] = 0x0FFDA302;
	big.bigint[29] = 0x0FFDA302;
	big.bigint[28] = 0x0FFDA302;
	big.bigint[27] = 0x0FFDA302;
	big.bigint[26] = 0x0FFDA302;
	big.bigint[25] = 0x0FFDA344;
	big.bigint[24] = 0x0FFDA302;
	big.bigint[23] = 0x0F345302;
	big.bigint[22] = 0x0FFD3535;
	BigInt big2;
	big2.bigint[31] = 0xF00439AA;
	big2.bigint[30] = 0x0090A0F7;
	big2.bigint[29] = 0x0090A0F7;
	big2.bigint[28] = 0x0090A0F0;
	big2.bigint[27] = 0x0090A0F7;
	big2.bigint[26] = 0x00907669;
	big2.bigint[25] = 0x0060A0F7;
	big2.bigint[24] = 0x0090A0F7;
	big2.bigint[23] = 0x0090A0F3;
	big2.bigint[22] = 0x4390A023;
	big = big + big2;
	cout.fill('0');
	cout << "0x ";
	cout.setf(ios_base::left);
	cout.width(8);
	cout << big.bigint[22];
	cout.setf(ios_base::left);
	cout.width(8);
	cout << big.bigint[23];
	cout.setf(ios_base::left);
	cout.width(8);
	cout << big.bigint[24];
	cout.setf(ios_base::left);
	cout.width(8);
	cout << big.bigint[25];
	cout.setf(ios_base::left);
	cout.width(8);
	cout << big.bigint[26];
	cout.setf(ios_base::left);
	cout.width(8);
	cout << big.bigint[27];
	cout.setf(ios_base::left);
	cout.width(8);
	cout << big.bigint[28];
	cout.setf(ios_base::left);
	cout.width(8);
	cout << big.bigint[29];
	cout.setf(ios_base::left);
	cout.width(8);
	cout << big.bigint[30];
	cout.setf(ios_base::left);
	cout.width(8);
	cout << big.bigint[31];
	return 0;
}