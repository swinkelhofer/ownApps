#include <QMainWindow>

class QPushButton;
class QSlider;

class DanceDJ:public QMainWindow
{
	Q_OBJECT
public:
	DanceDJ(QWidget *parent = 0);
private:
	QPushButton *play1, *play2;
	QSlider *vol1, *vol2;
	QSlider *cross;
};

	