#include <QtGui>
#include "DanceDJ.h"

DanceDJ::DanceDJ(QWidget *parent):QMainWindow(parent)
{
	vol1 = new QSlider(Qt::Vertical);
	vol1->setRange(0,100);
	vol2 = new QSlider(Qt::Vertical);
	vol2->setRange(0,100);
	cross = new QSlider;
	cross->setRange(0,100);
	play1 = new QPushButton("Play");
	play2 = new QPushButton("Play");
	
	QVBoxLayout *left = new QVBoxLayout;
	left->addWidget(vol1);
	left->addWidget(play1);
	
	QVBoxLayout *right = new QVBoxLayout;
	right->addWidget(play2);
	right->addWidget(vol2);
	
	QHBoxLayout *main = new QHBoxLayout;
	main->addLayout(left);
	main->addWidget(cross);
	main->addLayout(right);
	
	setLayout(main);
	setWindowTitle("DanceDJ");
}
