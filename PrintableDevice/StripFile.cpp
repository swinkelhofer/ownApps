#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sys/stat.h>
#include <ctype.h>
#include <string.h>
using namespace std;
bool isPossibleWord(const char *buffer)
{
	int len = strlen(buffer);
	for(int i = len-3; i >= 0; --i)
	{
		if((!isalpha(buffer[i]) && !isspace(buffer[i]) && !ispunct(buffer[i])) || (!islower(buffer[i]) && i > 0))
			return false;
	}
	return true;
}

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		printf("StripFile <Filename>\n");
		exit(0);
	}
	FILE *in, *out;
	in = fopen(argv[1], "rb");
	char buffer[1024];
	out = fopen("stripped.txt", "wb");
	if(in == NULL || out == NULL)
	{
		printf("Error opening files\n");
		exit(0);
	}
	while(!feof(in))
	{
		fgets(buffer, 1024, in);
		if(strlen(buffer) > 5 && isPossibleWord(buffer))
			fputs(buffer, out);
	}
}
