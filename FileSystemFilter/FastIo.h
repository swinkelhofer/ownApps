#include <ntifs.h>
#include <wdm.h>
BOOLEAN FsFilterFastIoQueryBasicInfo(__in PFILE_OBJECT FileObject, __in BOOLEAN Wait, __out PFILE_BASIC_INFORMATION Buffer, __out PIO_STATUS_BLOCK IoStatus, __in PDEVICE_OBJECT DeviceObject);
VOID FsFilterFastIoDetachDevice(__in PDEVICE_OBJECT SourceDevice, __in PDEVICE_OBJECT TargetDevice);
