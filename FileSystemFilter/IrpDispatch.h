#include <ntifs.h>
#include <wdm.h>
NTSTATUS FsFilterDispatchPassThrough(__in PDEVICE_OBJECT DeviceObject, __in PIRP Irp);
NTSTATUS FsFilterDispatchCreate(__in PDEVICE_OBJECT DeviceObject, __in PIRP Irp);
