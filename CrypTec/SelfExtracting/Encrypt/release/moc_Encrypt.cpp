/****************************************************************************
** Meta object code from reading C++ file 'Encrypt.h'
**
** Created: Fr 12. Nov 23:40:12 2010
**      by: The Qt Meta Object Compiler version 59 (Qt 4.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Encrypt.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Encrypt.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

static const uint qt_meta_data_EncryptWidget[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      20,   15,   14,   14, 0x08,
      39,   14,   14,   14, 0x08,
      49,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_EncryptWidget[] = {
    "EncryptWidget\0\0text\0changeOut(QString)\0Encrypt()\0change()\0"
};

const QMetaObject EncryptWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_EncryptWidget,
      qt_meta_data_EncryptWidget, 0 }
};

const QMetaObject *EncryptWidget::metaObject() const
{
    return &staticMetaObject;
}

void *EncryptWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EncryptWidget))
	return static_cast<void*>(const_cast<EncryptWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int EncryptWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: changeOut(*reinterpret_cast< const QString(*)>(_a[1])); break;
        case 1: Encrypt(); break;
        case 2: change(); break;
        }
        _id -= 3;
    }
    return _id;
}
