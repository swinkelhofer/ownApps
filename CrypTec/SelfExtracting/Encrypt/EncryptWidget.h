#include <QWidget>
#include "CryptString.h"
#ifndef ENCRYPT_WIDGET
#define ENCRYPT_WIDGET
class QPushButton;
class QLineEdit;
class QLabel;
class QCheckBox;
/*class CryptString;*/
class AESClass;
class EncryptWidget:public QWidget
{
	Q_OBJECT
public:
	EncryptWidget(QWidget *parent = 0);
private:
	QLabel *lab1, *lab2, *lab3;
	QLineEdit *In, *Out, *PW;
	QPushButton *ChangeIn, *OK;
	QCheckBox *del;
	CryptString key;
private slots:
	void changeOut(const QString &text);
	void Encrypt();
	void change();
};
#endif