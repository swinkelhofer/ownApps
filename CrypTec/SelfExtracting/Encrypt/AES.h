#ifndef AES
#define AES
class CryptString;

void FastBlockEnc256();
void KeyExpansion();
void FirstRound();
//void ShiftRows_Subst();
void LastRound();
//void inv_ShiftRows_Subst();
//void MixColumns();
//void inv_MixColumns();
void Combined();
bool SaveDeletion(const char *file);


class AESClass
{
public:
	bool EncryptFile(const char* infile, const char* outfile, CryptString key);
};
#endif
