#include "AES.h"
#include "CryptString.h"
#include <stdlib.h>
#include <sys/stat.h>
#include <math.h>
#include <time.h>
#include <QtGui>
using namespace std;

unsigned char s_box[256] =  
		{0x63 ,0x7c ,0x77 ,0x7b ,0xf2 ,0x6b ,0x6f ,0xc5 ,0x30 ,0x01 ,0x67 ,0x2b ,0xfe ,0xd7 ,0xab ,0x76	
		,0xca ,0x82 ,0xc9 ,0x7d ,0xfa ,0x59 ,0x47 ,0xf0 ,0xad ,0xd4 ,0xa2 ,0xaf ,0x9c ,0xa4 ,0x72 ,0xc0
		,0xb7 ,0xfd ,0x93 ,0x26 ,0x36 ,0x3f ,0xf7 ,0xcc ,0x34 ,0xa5 ,0xe5 ,0xf1 ,0x71 ,0xd8 ,0x31 ,0x15
		,0x04 ,0xc7 ,0x23 ,0xc3 ,0x18 ,0x96 ,0x05 ,0x9a ,0x07 ,0x12 ,0x80 ,0xe2 ,0xeb ,0x27 ,0xb2 ,0x75
		,0x09 ,0x83 ,0x2c ,0x1a ,0x1b ,0x6e ,0x5a ,0xa0 ,0x52 ,0x3b ,0xd6 ,0xb3 ,0x29 ,0xe3 ,0x2f ,0x84
		,0x53 ,0xd1 ,0x00 ,0xed ,0x20 ,0xfc ,0xb1 ,0x5b ,0x6a ,0xcb ,0xbe ,0x39 ,0x4a ,0x4c ,0x58 ,0xcf
		,0xd0 ,0xef ,0xaa ,0xfb ,0x43 ,0x4d ,0x33 ,0x85 ,0x45 ,0xf9 ,0x02 ,0x7f ,0x50 ,0x3c ,0x9f ,0xa8
		,0x51 ,0xa3 ,0x40 ,0x8f ,0x92 ,0x9d ,0x38 ,0xf5 ,0xbc ,0xb6 ,0xda ,0x21 ,0x10 ,0xff ,0xf3 ,0xd2
		,0xcd ,0x0c ,0x13 ,0xec ,0x5f ,0x97 ,0x44 ,0x17 ,0xc4 ,0xa7 ,0x7e ,0x3d ,0x64 ,0x5d ,0x19 ,0x73
		,0x60 ,0x81 ,0x4f ,0xdc ,0x22 ,0x2a ,0x90 ,0x88 ,0x46 ,0xee ,0xb8 ,0x14 ,0xde ,0x5e ,0x0b ,0xdb
		,0xe0 ,0x32 ,0x3a ,0x0a ,0x49 ,0x06 ,0x24 ,0x5c ,0xc2 ,0xd3 ,0xac ,0x62 ,0x91 ,0x95 ,0xe4 ,0x79
		,0xe7 ,0xc8 ,0x37 ,0x6d ,0x8d ,0xd5 ,0x4e ,0xa9 ,0x6c ,0x56 ,0xf4 ,0xea ,0x65 ,0x7a ,0xae ,0x08
		,0xba ,0x78 ,0x25 ,0x2e ,0x1c ,0xa6 ,0xb4 ,0xc6 ,0xe8 ,0xdd ,0x74 ,0x1f ,0x4b ,0xbd ,0x8b ,0x8a
		,0x70 ,0x3e ,0xb5 ,0x66 ,0x48 ,0x03 ,0xf6 ,0x0e ,0x61 ,0x35 ,0x57 ,0xb9 ,0x86 ,0xc1 ,0x1d ,0x9e
		,0xe1 ,0xf8 ,0x98 ,0x11 ,0x69 ,0xd9 ,0x8e ,0x94 ,0x9b ,0x1e ,0x87 ,0xe9 ,0xce ,0x55 ,0x28 ,0xdf
		,0x8c ,0xa1 ,0x89 ,0x0d ,0xbf ,0xe6 ,0x42 ,0x68 ,0x41 ,0x99 ,0x2d ,0x0f ,0xb0 ,0x54 ,0xbb ,0x16};
		
		
unsigned char rcon[255] = {  
		0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a,   
		0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39,   
		0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a,   
		0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8,   
		0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef,   
		0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc,   
		0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b,   
		0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3,   
		0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94,   
		0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20,   
		0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35,   
		0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f,   
		0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04,   
		0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63,   
		0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd,   
		0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb};

unsigned char xtime[256] = {0x0, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 
		0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 0x3c, 0x3e, 
		0x40, 0x42, 0x44, 0x46, 0x48, 0x4a, 0x4c, 0x4e, 0x50, 0x52, 0x54, 0x56, 0x58, 0x5a, 0x5c, 0x5e, 
		0x60, 0x62, 0x64, 0x66, 0x68, 0x6a, 0x6c, 0x6e, 0x70, 0x72, 0x74, 0x76, 0x78, 0x7a, 0x7c, 0x7e, 
		0x80, 0x82, 0x84, 0x86, 0x88, 0x8a, 0x8c, 0x8e, 0x90, 0x92, 0x94, 0x96, 0x98, 0x9a, 0x9c, 0x9e, 
		0xa0, 0xa2, 0xa4, 0xa6, 0xa8, 0xaa, 0xac, 0xae, 0xb0, 0xb2, 0xb4, 0xb6, 0xb8, 0xba, 0xbc, 0xbe, 
		0xc0, 0xc2, 0xc4, 0xc6, 0xc8, 0xca, 0xcc, 0xce, 0xd0, 0xd2, 0xd4, 0xd6, 0xd8, 0xda, 0xdc, 0xde, 
		0xe0, 0xe2, 0xe4, 0xe6, 0xe8, 0xea, 0xec, 0xee, 0xf0, 0xf2, 0xf4, 0xf6, 0xf8, 0xfa, 0xfc, 0xfe, 
		0x1b, 0x19, 0x1f, 0x1d, 0x13, 0x11, 0x17, 0x15, 0xb, 0x9, 0xf, 0xd, 0x3, 0x1, 0x7, 0x5, 
		0x3b, 0x39, 0x3f, 0x3d, 0x33, 0x31, 0x37, 0x35, 0x2b, 0x29, 0x2f, 0x2d, 0x23, 0x21, 0x27, 0x25, 
		0x5b, 0x59, 0x5f, 0x5d, 0x53, 0x51, 0x57, 0x55, 0x4b, 0x49, 0x4f, 0x4d, 0x43, 0x41, 0x47, 0x45, 
		0x7b, 0x79, 0x7f, 0x7d, 0x73, 0x71, 0x77, 0x75, 0x6b, 0x69, 0x6f, 0x6d, 0x63, 0x61, 0x67, 0x65, 
		0x9b, 0x99, 0x9f, 0x9d, 0x93, 0x91, 0x97, 0x95, 0x8b, 0x89, 0x8f, 0x8d, 0x83, 0x81, 0x87, 0x85, 
		0xbb, 0xb9, 0xbf, 0xbd, 0xb3, 0xb1, 0xb7, 0xb5, 0xab, 0xa9, 0xaf, 0xad, 0xa3, 0xa1, 0xa7, 0xa5, 
		0xdb, 0xd9, 0xdf, 0xdd, 0xd3, 0xd1, 0xd7, 0xd5, 0xcb, 0xc9, 0xcf, 0xcd, 0xc3, 0xc1, 0xc7, 0xc5, 
		0xfb, 0xf9, 0xff, 0xfd, 0xf3, 0xf1, 0xf7, 0xf5, 0xeb, 0xe9, 0xef, 0xed, 0xe3, 0xe1, 0xe7, 0xe5};



unsigned char KeyBlock[240];
unsigned char State[16], temp[16], temp2[16];
unsigned char a,b,c,d;
int R, Nr, Nk;
unsigned int counter;

bool AESClass::EncryptFile(const char* infile, const char* outfile, CryptString key)
{
	QWidget *cryptstat = new QWidget;
	QLabel *lab = new QLabel("Verschl�ssle mit sicherem AES256 im CBC-Mode:");
	QProgressBar *bar = new QProgressBar;
	bar->setOrientation(Qt::Horizontal);
	bar->setTextVisible(true);
	bar->setRange(0,100);
	bar->show();
	bar->setValue(0);
	QVBoxLayout *main = new QVBoxLayout;
	main->addWidget(lab);
	main->addWidget(bar);
	cryptstat->setLayout(main);
	cryptstat->show();
	
	
	
	Nk = 8;
	Nr = 14;
	for(int i = 0; i < 32; i++)
		KeyBlock[i] = key.str[i];
	
	
	KeyExpansion();
	
	
	
	
	FILE *in;
	in = fopen(infile, "r+b");
	struct stat datei;
	stat(infile,&datei);
	unsigned int byte = (unsigned int) datei.st_size;
	register unsigned int  rounds = (unsigned int) byte/16;
	int carry = byte % 16;
	
	if(in == NULL)
	{
		return false;
	}
	FILE *out;
	out = fopen(outfile, "a+b");
	if(out == NULL)
	{
		return false;
	}
	
	
	//IV: 0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f, 0x10
	for(int l = 1; l <= 16; l++)
		State[l-1] = (unsigned char) l;
	//#IV
	clock_t start,stop;
	start = clock();
	
	register unsigned int range = 0;
	for(counter = 0; counter < rounds; counter++)
	{
		if(counter*100/rounds == range)
		{
			bar->setValue(range++);
		}
		fread(&temp, 1, 16, in);
		State[0] = State[0] ^ temp[0];
		State[1] = State[1] ^ temp[1];
		State[2] = State[2] ^ temp[2];
		State[3] = State[3] ^ temp[3];
		State[4] = State[4] ^ temp[4];
		State[5] = State[5] ^ temp[5];
		State[6] = State[6] ^ temp[6];
		State[7] = State[7] ^ temp[7];
		State[8] = State[8] ^ temp[8];
		State[9] = State[9] ^ temp[9];
		State[10] = State[10] ^ temp[10];
		State[11] = State[11] ^ temp[11];
		State[12] = State[12] ^ temp[12];
		State[13] = State[13] ^ temp[13];
		State[14] = State[14] ^ temp[14];
		State[15] = State[15] ^ temp[15];
		FastBlockEnc256();
		fwrite(&State, 1, 16, out);	
	}
	if(carry != 0)
	{
		bar->setValue(100);
		fread(&temp, 1, 16, in);
		for(int i = carry; i < 16; i++)
		{
			temp[i] = 0x00;
		}
		State[0] = State[0] ^ temp[0];
		State[1] = State[1] ^ temp[1];
		State[2] = State[2] ^ temp[2];
		State[3] = State[3] ^ temp[3];
		State[4] = State[4] ^ temp[4];
		State[5] = State[5] ^ temp[5];
		State[6] = State[6] ^ temp[6];
		State[7] = State[7] ^ temp[7];
		State[8] = State[8] ^ temp[8];
		State[9] = State[9] ^ temp[9];
		State[10] = State[10] ^ temp[10];
		State[11] = State[11] ^ temp[11];
		State[12] = State[12] ^ temp[12];
		State[13] = State[13] ^ temp[13];
		State[14] = State[14] ^ temp[14];
		State[15] = State[15] ^ temp[15];
		FastBlockEnc256();
		fwrite(&State, 1, 16, out);
	}
	fputc(carry,out);
	for(int i = 3; i >= 0; i--)
	{
		fputc((byte / (unsigned int ) pow(256, i)) % 256, out);
		
	}
	stop = clock();
	float time = (float)(stop-start);
	QLabel *timer = new QLabel(QString::number(time/1000) + QString(" Secs"));
	timer->show();
	
	for(int i = 0; i < 240; i++)
		KeyBlock[i] = 0x01;
	fclose(in);
	fclose(out);
	delete(cryptstat);
	//delete(&key);
	return true;
}

bool SaveDeletion(const char *file)
{
	QWidget *delstat = new QWidget;
	QProgressBar *delbar = new QProgressBar;
	delbar->setOrientation(Qt::Horizontal);
	delbar->setTextVisible(true);
	delbar->setRange(0,100);
	delbar->setValue(0);
	QLabel *lab = new QLabel("L�sche Ursprungs-Datei sicher durch dreimaliges �berschreiben:");
	QVBoxLayout *main = new QVBoxLayout;
	main->addWidget(lab);
	main->addWidget(delbar);
	delstat->setLayout(main);
	delstat->show();
	struct stat datei;
	stat(file,&datei);
	int byte = datei.st_size;
	
	
	
	register int rounds = byte/16;
	int rest = byte % 16;
	char randomOut[16];
	char random;
	int cur;
	for(int times = 0; times < 3; times++)
	{
		FILE *over;
		over = fopen(file, "w+b");
		cur=0;
		srand(time(0));
		fseek(over, 0, SEEK_SET);
		for(int i = 0; i < rounds; i++)
		{
			
			if(cur == i*100/rounds)
			{
				delbar->setValue(cur++);
			}
			randomOut[0] = (char) rand() % 256;
			randomOut[1] = (char) rand() % 256;
			randomOut[2] = (char) rand() % 256;
			randomOut[3] = (char) rand() % 256;
			randomOut[4] = (char) rand() % 256;
			randomOut[5] = (char) rand() % 256;
			randomOut[6] = (char) rand() % 256;
			randomOut[7] = (char) rand() % 256;
			randomOut[8] = (char) rand() % 256;
			randomOut[9] = (char) rand() % 256;
			randomOut[10] = (char) rand() % 256;
			randomOut[11] = (char) rand() % 256;
			randomOut[12] = (char) rand() % 256;
			randomOut[13] = (char) rand() % 256;
			randomOut[14] = (char) rand() % 256;
			randomOut[15] = (char) rand() % 256;
			fwrite(&randomOut, 1,16,over);
		}
		for(int i = 0; i < rest; i++)
		{
			random = (unsigned char) rand()%256;
			fwrite(&random, 1,1, over);
		}
		fclose(over);
	}
	delbar->setValue(100);
	delstat->hide();
	delete(delstat);
	return true;
}

inline void FastBlockEnc256()
{
	FirstRound();
	R = 1;
	Combined();
	R = 2;
	Combined();
	R = 3;
	Combined();
	R = 4;
	Combined();
	R = 5;
	Combined();
	R = 6;
	Combined();
	R = 7;
	Combined();
	R = 8;
	Combined();
	R = 9;
	Combined();
	R = 10;
	Combined();
	R = 11;
	Combined();
	R = 12;
	Combined();
	R = 13;
	Combined();
	R = 14;
	LastRound();
}

void KeyExpansion()
{
	unsigned char temp[4];
	unsigned char a;
	for(int roundNr = Nk; roundNr < (Nr+1)*4; roundNr++)
	{
		//temp
		for(int i = 0; i < 4; i++)
		{
			temp[i] = KeyBlock[(roundNr-1)*4 + i];
		}
		//RotWord()
		if(roundNr % Nk == 0)
		{
			a = temp[0];
			temp[0] = temp[1];
			temp[1] = temp[2];
			temp[2] = temp[3];
			temp[3] = a;
		}
		//SubWord() f�r Nk 4 & 6
		if(roundNr % Nk == 0 && Nk <= 6)
		{
			temp[0] = s_box[temp[0]];
			temp[1] = s_box[temp[1]];
			temp[2] = s_box[temp[2]];
			temp[3] = s_box[temp[3]];
		}
		//SubWord() f�r Nk 8
		if(roundNr % 4 == 0 && Nk == 8)
		{
			temp[0] = s_box[temp[0]];
			temp[1] = s_box[temp[1]];
			temp[2] = s_box[temp[2]];
			temp[3] = s_box[temp[3]];
		}
		//XOR rcon
		if(roundNr % Nk == 0)
		{
			temp[0] = temp[0] ^ rcon[roundNr / Nk];
		}
		//XOR Word[roundNr-Nk]
		for(int i = 0; i < 4; i++)
		{
			temp[i] = temp[i] ^ KeyBlock[(roundNr-Nk)*4+i];
		}
		//Word in KeyBlock
		for(int i = 0; i < 4; i++)
		{
			KeyBlock[roundNr*4+i]=temp[i];
		}
	}
}

inline void FirstRound()
{
	State[0] = State[0] ^ KeyBlock[0];
	State[1] = State[1] ^ KeyBlock[1];
	State[2] = State[2] ^ KeyBlock[2];
	State[3] = State[3] ^ KeyBlock[3];
	State[4] = State[4] ^ KeyBlock[4];
	State[5] = State[5] ^ KeyBlock[5];
	State[6] = State[6] ^ KeyBlock[6];
	State[7] = State[7] ^ KeyBlock[7];
	State[8] = State[8] ^ KeyBlock[8];
	State[9] = State[9] ^ KeyBlock[9];
	State[10] = State[10] ^ KeyBlock[10];
	State[11] = State[11] ^ KeyBlock[11];
	State[12] = State[12] ^ KeyBlock[12];
	State[13] = State[13] ^ KeyBlock[13];
	State[14] = State[14] ^ KeyBlock[14];
	State[15] = State[15] ^ KeyBlock[15];
}

inline void LastRound()
{
	State[0]=s_box[State[0]] ^ KeyBlock[R*16 + 0];
	State[4]=s_box[State[4]] ^ KeyBlock[R*16 + 4];
	State[8]=s_box[State[8]] ^ KeyBlock[R*16 + 8];
	State[12]=s_box[State[12]] ^ KeyBlock[R*16 + 12];
	
	a = s_box[State[1]];
	State[1]=s_box[State[5]] ^ KeyBlock[R*16 + 1];
	State[5]=s_box[State[9]] ^ KeyBlock[R*16 + 5];
	State[9]=s_box[State[13]] ^ KeyBlock[R*16 + 9];
	State[13]=a ^ KeyBlock[R*16 + 13];
	
	a = s_box[State[2]];
	b = s_box[State[6]];
	State[2]=s_box[State[10]] ^ KeyBlock[R*16 + 2];
	State[6]=s_box[State[14]] ^ KeyBlock[R*16 + 6];
	State[10]=a ^ KeyBlock[R*16 + 10];
	State[14]=b ^ KeyBlock[R*16 + 14];
	
	a = s_box[State[15]];	
	State[15]=s_box[State[11]] ^ KeyBlock[R*16 + 15];
	State[11]=s_box[State[7]] ^ KeyBlock[R*16 + 11];
	State[7]=s_box[State[3]] ^ KeyBlock[R*16 + 7];
	State[3]=a ^ KeyBlock[R*16 + 3];
}

inline void Combined()
{
	//shiftrows + subst
	a = State[1];
	State[1] = s_box[State[5]];
	State[5] = s_box[State[9]];
	State[9] = s_box[State[13]];
	State[13] = s_box[a];
	a = State[2];
	b = State[6];
	State[2] = s_box[State[10]];
	State[6] = s_box[State[14]];
	State[10] = s_box[a];
	State[14] = s_box[b];
	a = State[15];
	State[15] = s_box[State[11]];
	State[11] = s_box[State[7]];
	State[7] = s_box[State[3]];
	State[3] = s_box[a];
	//addround + mixcols
	b = s_box[State[0]];  
    a = b ^ State[1] ^ State[2] ^ State[3];
	State[0] = b ^ xtime[b ^ State[1]] ^ a ^ KeyBlock[R*16 + 0];
	State[1] = State[1] ^ xtime[State[1] ^ State[2]] ^ a ^ KeyBlock[R*16 + 1];
	State[2] = State[2] ^ xtime[State[2] ^ State[3]] ^ a ^ KeyBlock[R*16 + 2];
	State[3] = State[3] ^ xtime[State[3] ^ b] ^ a ^ KeyBlock[R*16 + 3];
	b = s_box[State[4]];  
    a = b ^ State[5] ^ State[6] ^ State[7];
	State[4] = b ^ xtime[b ^ State[5]] ^ a ^ KeyBlock[R*16 + 4];
	State[5] = State[5] ^ xtime[State[5] ^ State[6]] ^ a ^ KeyBlock[R*16 + 5];
	State[6] = State[6] ^ xtime[State[6] ^ State[7]] ^ a ^ KeyBlock[R*16 + 6];
	State[7] = State[7] ^ xtime[State[7] ^ b] ^ a ^ KeyBlock[R*16 + 7];
	b = s_box[State[8]];  
    a = b ^ State[9] ^ State[10] ^ State[11];
	State[8] = b ^ xtime[b ^ State[9]] ^ a ^ KeyBlock[R*16 + 8];
	State[9] = State[9] ^ xtime[State[9] ^ State[10]] ^ a ^ KeyBlock[R*16 + 9];
	State[10] = State[10] ^ xtime[State[10] ^ State[11]] ^ a ^ KeyBlock[R*16 + 10];
	State[11] = State[11] ^ xtime[State[11] ^ b] ^ a ^ KeyBlock[R*16 + 11];
	b = s_box[State[12]];  
    a = b ^ State[13] ^ State[14] ^ State[15];
	State[12] = b ^ xtime[b ^ State[13]] ^ a ^ KeyBlock[R*16 + 12];
	State[13] = State[13] ^ xtime[State[13] ^ State[14]] ^ a ^ KeyBlock[R*16 + 13];
	State[14] = State[14] ^ xtime[State[14] ^ State[15]] ^ a ^ KeyBlock[R*16 + 14];
	State[15] = State[15] ^ xtime[State[15] ^ b] ^ a ^ KeyBlock[R*16 + 15];
}
