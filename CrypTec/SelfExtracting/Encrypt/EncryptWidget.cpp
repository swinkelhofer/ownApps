#include <QtGui>
#include "EncryptWidget.h"
#include "AES.h"
#include "CryptString.h"

EncryptWidget::EncryptWidget(QWidget *parent):QWidget(parent)
{
	lab1 = new QLabel("Zu verschlüsselnde Datei:");
	lab2 = new QLabel("Selbstenschlüsselnde .exe-Datei:");
	lab3 = new QLabel("Passwort:");
	QFont font("Arial");
	font.setItalic(true);
	In = new QLineEdit;
	Out = new QLineEdit;
	In->setFont(font);
	Out->setFont(font);
	In->setDisabled(true);
	Out->setDisabled(true);
	PW = new QLineEdit;
	PW->setEchoMode(QLineEdit::Password);

	
	OK = new QPushButton("Verschlüsseln");
	font.setBold(true);
	font.setItalic(false);
	OK->setFont(font);
	ChangeIn = new QPushButton("Ändern");
	del = new QCheckBox("Ursprungs-Datei sicher löschen");
	QGridLayout *top = new QGridLayout;
	top->setSizeConstraint(QLayout::SetMinimumSize);
	top->addWidget(lab1, 0,0);
	top->addWidget(In, 1, 0);
	top->addWidget(lab2, 2, 0);
	top->addWidget(Out, 3, 0);
	//top->addWidget(lab3, 5, 0);
	//top->addWidget(PW, 6, 0);
	top->addWidget(del, 4, 0);
	top->addWidget(ChangeIn, 1,1);
	//top->addWidget(OK, 6, 1);
	
	
	QHBoxLayout *mid = new QHBoxLayout;
	mid->setSizeConstraint(QLayout::SetMinimumSize);
	mid->addWidget(PW);
	mid->addStretch();
	
	
	QVBoxLayout *main = new QVBoxLayout;
	main->setSizeConstraint(QLayout::SetMinimumSize);
	main->addLayout(top);
	main->addSpacing(20);
	main->addWidget(lab3);
	main->addLayout(mid);
	main->addSpacing(20);
	main->addWidget(OK);
	setLayout(main);
	
	
	
	ChangeIn->setFocus(Qt::MouseFocusReason);
	setTabOrder(ChangeIn, del);
	setTabOrder(del,PW);
	setTabOrder(PW, OK);
	connect(In, SIGNAL(textChanged(QString)), this, SLOT(changeOut(QString)));
	connect(OK, SIGNAL(clicked()), this, SLOT(Encrypt()));
	connect(ChangeIn, SIGNAL(clicked()), this, SLOT(change()));
	
	setWindowFlags(Qt::MSWindowsFixedSizeDialogHint | Qt::WindowTitleHint | Qt::WindowSystemMenuHint);
	//setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
}

void EncryptWidget::Encrypt()
{
	OK->setDisabled(true);
	QFile infile("Decrypt.exe");
	//QFileInfo info(Out->text());
	//QDir::setCurrent(info.path());
	infile.copy(Out->text());
	AESClass aes;
	
	if(PW->text().isEmpty())
	{
		OK->setDisabled(false);
		return;
	}
	key.set(PW->text().toStdString().c_str());
	if(!aes.EncryptFile(In->text().toStdString().c_str(), Out->text().toStdString().c_str(), key))
	{
		OK->setDisabled(false);
		return;
	}
	if(del->checkState() == Qt::Checked)
	{
		SaveDeletion(In->text().toStdString().c_str());
		QFile deleteFile(In->text());
		deleteFile.remove();
	}
	for(int i = 0; i < 32; i++)
	{
		key.str[i] = 0x01;
	}
	PW->setText(QString("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
	key.set(PW->text().toStdString().c_str());
	PW->clear();
	
	OK->setDisabled(false);
	
}

void EncryptWidget::changeOut(const QString &text)
{
	Out->setText(text + QString(".exe"));
}

void EncryptWidget::change()
{
	In->setText(QFileDialog::getOpenFileName(
                    this,
                    "Choose a file to encrypt",
                    "C:\\",
                    "All Files (*.*)"));
}
