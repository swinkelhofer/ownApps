#ifndef AES
#define AES
#include <QThread>
#include "CryptString.h"
#include <string.h>
using namespace std;

void FastBlockDec256();
void KeyExpansion();
void FirstRound();
void inv_LastRound();
void inv_Combined();


class AESThread: public QThread
{
	Q_OBJECT
public:
	void setParams(string setInFile, string setOutFile, string setKey);
	void stop();
protected:
	void run();
signals:
	void percentageChanged(int value);
private:
	FILE *in, *out;
	string inFile, outFile;
	CryptString key;
};
#endif
