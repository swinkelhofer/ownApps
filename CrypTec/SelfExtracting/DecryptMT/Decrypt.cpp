#include "Decrypt.h"
#include "AES.h"
#include "CryptString.h"
#include <QtGui>

DecryptWidget::DecryptWidget(char *thisFile, QWidget *parent):QWidget(parent)
{
	decry = new QWidget;
	QLabel *declab = new QLabel("Entschlüssle mit AES256 im CBC-Mode verschlüsselte Datei:");
	bar = new QProgressBar;
	bar->setOrientation(Qt::Horizontal);
	bar->setRange(0,100);
	bar->setValue(0);
	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(declab);
	mainLayout->addWidget(bar);
	decry->setLayout(mainLayout);
	decry->hide();


	thisExe = thisFile;
	lab1 = new QLabel("Passwort: ");
	PW = new QLineEdit;
	PW->setEchoMode(QLineEdit::Password);
	OK = new QPushButton("Entschlüsseln");
	QHBoxLayout *main = new QHBoxLayout;
	main->addWidget(lab1);
	main->addWidget(PW);
	main->addWidget(OK);
	setLayout(main);
	connect(OK, SIGNAL(clicked()), this, SLOT(Decrypt()));
	setAttribute(Qt::WA_DeleteOnClose);
}

void DecryptWidget::closeEvent(QCloseEvent *event)
{
	if(aesThread.isRunning())
		aesThread.stop();
	event->accept();
}

void DecryptWidget::Decrypt()
{
	if(OK->text() == QString("Abbrechen"))
	{
		close();
	}
	else if(OK->text() == QString("Entschlüsseln"))
	{
		if(PW->text().trimmed().isEmpty())
			return;
		OK->setText("Abbrechen");
		QString inFile(thisExe.c_str());
		inFile.chop(4);
		inFile += QString(".dcu");
		QFile file(thisExe.c_str());
		file.copy(inFile);
		QString outFile(thisExe.c_str());
		outFile.chop(4);
		aesThread.setParams(inFile.toStdString(), outFile.toStdString(), PW->text().toStdString());
		aesThread.start();
	
		connect(&aesThread, SIGNAL(percentageChanged(int)), bar, SLOT(setValue(int)));
		connect(&aesThread, SIGNAL(finished()), this, SLOT(cryptFinished()));
		decry->show();
		PW->clear();
	}
}

void DecryptWidget::cryptFinished()
{
	QFile file;
	QString inFile = QString(thisExe.c_str());
	inFile.chop(4);
	inFile += QString(".dcu");
	file.setFileName(inFile);
	file.remove();
	delete(decry);
	close();
}