#include <QWidget>
#include "CryptString.h"
#include "AES.h"
#include <string.h>
using namespace std;

class QPushButton;
class QLabel;
class QLineEdit;
class QWidget;
class QProgressBar;
class DecryptWidget: public QWidget
{
	Q_OBJECT
public:
	DecryptWidget(char *thisfile, QWidget *parent = 0);
private:
	QLabel *lab1;
	QLineEdit *PW;
	QPushButton *OK;
	QWidget *decry;
	QProgressBar *bar;
	string thisExe;
	CryptString key;
	AESThread aesThread;
private slots:
	void Decrypt();
	void cryptFinished();
protected:
	void closeEvent(QCloseEvent *event);
};
