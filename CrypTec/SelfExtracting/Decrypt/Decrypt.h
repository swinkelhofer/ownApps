#include <QWidget>
#include "CryptString.h"
class QPushButton;
class QLabel;
class QLineEdit;
class DecryptWidget: public QWidget
{
	Q_OBJECT
public:
	DecryptWidget(char *thisfile, QWidget *parent = 0);
private:
	QLabel *lab1;
	QLineEdit *pw;
	QPushButton *OK;
	char *thisexe;
	CryptString key;
private slots:
	void Decrypt();
};
