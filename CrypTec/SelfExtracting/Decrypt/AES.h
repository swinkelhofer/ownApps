#ifndef AES
#define AES
class CryptString;

void FastBlockDec256();
void KeyExpansion();
void FirstRound();
//void ShiftRows_Subst();
//void inv_ShiftRows_Subst();
void inv_LastRound();
//void MixColumns();
//void inv_MixColumns();
void inv_Combined();



class AESClass
{
public:
	bool DecryptFile(const char* infile, const char* outfile, CryptString key);
};
#endif
