#include "Decrypt.h"
#include "AES.h"
#include "CryptString.h"
#include <QtGui>

DecryptWidget::DecryptWidget(char *thisfile, QWidget *parent):QWidget(parent)
{
	thisexe = thisfile;
	lab1 = new QLabel("Passwort: ");
	pw = new QLineEdit;
	pw->setEchoMode(QLineEdit::Password);
	OK = new QPushButton("Entschlüsseln");
	QHBoxLayout *main = new QHBoxLayout;
	main->addWidget(lab1);
	main->addWidget(pw);
	main->addWidget(OK);
	setLayout(main);
	connect(OK, SIGNAL(clicked()), this, SLOT(Decrypt()));
	setAttribute(Qt::WA_DeleteOnClose);
}

void DecryptWidget::Decrypt()
{
	AESClass aes;
	if(pw->text().trimmed().isEmpty())
		return;
	
	key.set(pw->text().toStdString().c_str());
	QString infile(thisexe);
	infile.chop(4);
	infile += QString(".dcu");
	QFile file(thisexe);
	file.copy(infile);
	QString outfile(thisexe);
	outfile.chop(4);
	if(!aes.DecryptFile(infile.toStdString().c_str(), outfile.toStdString().c_str(), key))
		return;
	file.setFileName(infile);
	file.remove();
	pw->setText("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
	key.set(pw->text().toStdString().c_str());
	pw->clear();
	close();
	//exit(0);
	
	//destroy();
}