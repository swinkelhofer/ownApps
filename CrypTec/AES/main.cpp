#include "AES.h"
#include "CryptString.h"
#include <sys/time.h>
#include <sys/stat.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
int main(int argc, char *argv[])
{
	AESClass aes25;
	int keylength = 128;
	AESClass::BlockMode block = AESClass::CBC;
	cout << "\n\n##############################################\nWelcome to AES-File-Encryption and -Decryption\n##############################################\n\n\n";
	switch(argc)
	{
		case 1: cout << "Please argument a file to de- and encrypt: AES <filename> [keylength BlockMode]\nor type AES --help to get info\n";
				exit(0);
		case 2: keylength = 256;
				block = AESClass::CBC;
				if(string(argv[1]) == string("--help"))
				{
					cout << "AES <filename> [keylength BlockMode]\n\n\tkeylength = 128 or 192 or (Standard) 256\n\tBlockMode = ECB (less safe) or (Standard) CBC (save)\n\n";
					exit(0);
				}
				cout << "Start with Parameters: AES" << keylength << ", BlockMode: CBC\n\n";
				break;
		case 3: keylength = atoi(argv[2]);
				block = AESClass::CBC;
				if(keylength != 128 && keylength != 192 && keylength != 256)
				{
					cout << "Use valid AES-Keylength (128, 192 or 256 Bit)";
					exit(0);
				}
				cout << "Start with Parameters: AES" << keylength << ", BlockMode: CBC\n\n";
				break;
		case 4: keylength = atoi(argv[2]);
				if(keylength != 128 && keylength != 192 && keylength != 256)
				{
					cout << "Use valid AES-Keylength (128, 192 or 256 Bit)\n\n";
					exit(0);
				}
				if(string(argv[3]) == string("CBC"))
				{
					block = AESClass::CBC;
					cout << "Start with Parameters: AES" << keylength << ", BlockMode: CBC\n\n";
				}
				else if(string(argv[3]) == string("ECB"))
				{
					block = AESClass::ECB;
					cout << "Start with Parameters: AES" << keylength << ", BlockMode: ECB\n\n";
				}
				else
				{
					block = AESClass::CBC;
					cout << "Start with Parameters: AES" << keylength << ", BlockMode: CBC ("<< argv[3] << " not known)\n\n";
				}
				
				break;
	}

	CryptString key;
	key.set("\x2b\x7e\x15\x16\x28\xae\xd2\xa6\xab\xf7\x15\x88\x09\xcf\x4f\x3c");
	
	string infile = argv[1];
	string outfile = string("entschl ") + infile;
	string dcu = infile + string(".dcu");
	aes25.EncryptFile(infile.c_str(), dcu.c_str(),key,keylength,block);
	
	aes25.DecryptFile(dcu.c_str(), outfile.c_str(),key,keylength,block);
	getchar();
	exit(0);
	return 0;
}


