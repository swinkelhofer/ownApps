#define xtime(x)   ((x<<1) ^ (((x>>7) & 1) * 0x1b))
#define Multiply(x,y) (((y & 1) * x) ^ ((y>>1 & 1) * xtime(x)) ^ ((y>>2 & 1) * xtime(xtime(x))) ^ ((y>>3 & 1) * xtime(xtime(xtime(x)))) ^ ((y>>4 & 1) * xtime(xtime(xtime(xtime(x))))))  
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <sys/stat.h>
using namespace std;
int main()
{
	/*�cout << "unsigned char xtime[256] = {";
	for(int i = 0; i < 256; i++)
	{
		if(i % 16 == 0 && i != 0)
			cout << "\n\t\t";
		printf("0x%x", (unsigned char) xtime(i));
		if(i != 255)
			cout << ", ";
	}
	cout << "};";*/
	FILE *in;
	in = fopen("bt.iso","r+b");
	FILE *out;
	out = fopen("bt.out.iso", "w+b");
	unsigned char State[16];
	
	ifstream dat_in;
	dat_in.open("bt.iso" ,ios_base::in | ios_base::binary);
	
	ofstream dat_out;
	dat_out.open("bt.outer.iso", ios_base::out | ios_base::binary);
	
	struct stat datei;
	stat("bt.iso",&datei);
	float size = datei.st_size/1024/1024;
	std::cout.precision(3);
	clock_t start,stop;
	start = clock();
	for(int i = 0; i < datei.st_size/16; i++)
	{
		fread(&State, 1, 16, in);
		fwrite(&State, 1, 16, out);
	}
	stop = clock();
	std::cout << "FILE*: " << (float)size*1000/(float)(stop-start) << " MB/Sec\t\t"<< (stop-start)/1000 <<" Sec\n\n";


	start = clock();
	for(int i = 0; i < datei.st_size/16; i++)
	{
		dat_in.read((char*)State,16); //Block einlesen
		dat_out.write((char*)State,16);
	}
	stop = clock();
	std::cout << "fstream: " << (float)size*1000/(float)(stop-start) << " MB/Sec\t\t"<< (stop-start)/1000 <<" Sec\n\n";


}