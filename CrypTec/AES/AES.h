#ifndef AES
#define AES
class CryptString;

void FastBlockEnc128();
void FastBlockDec128();
void FastBlockEnc192();
void FastBlockDec192();
void FastBlockEnc256();
void FastBlockDec256();
void KeyExpansion();
void FirstRound();
//void ShiftRows_Subst();
void LastRound();
//void inv_ShiftRows_Subst();
void inv_LastRound();
//void MixColumns();
//void inv_MixColumns();
void Combined();
void inv_Combined();
void printAES();
void printKey();


class AESClass
{
public:
	enum BlockMode {CBC=0,ECB=1};
	//------CryptString Encrypt(CryptString plaintext, CryptString key, int keylength=256);
	//------CryptString Decrypt(CryptString chiffretext, CryptString key, int keylength=256);
	//AESClass::CBC is the much saver Cipher Block Chaining Mode, it is recommended to use this mode, although it's a bit slower (about 15%)
	void EncryptFile(const char* infile, const char* outfile, CryptString key, int keylength=256, BlockMode = AESClass::CBC);
	void DecryptFile(const char* infile, const char* outfile, CryptString key, int keylength=256, BlockMode = AESClass::CBC);
	
private:
	void SelectNr();
	//void EncryptFile(char* infile, char* outfile, char* key);
	//void DecryptFile(char* infile, char* outfile, char* key);
	//AESBlockCryption aes;
};
#endif
