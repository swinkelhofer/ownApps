#include "VTabBar.h"
#include <QApplication>
#include <QtGui>
int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	QPlastiqueStyle *style = new QPlastiqueStyle;
	app.setStyle(style);
	QPalette pal;
	pal.setColor(QPalette::Active, QPalette::Window, Qt::black);
	pal.setColor(QPalette::Active, QPalette::WindowText, Qt::darkGray);
	pal.setColor(QPalette::Active, QPalette::Base, Qt::lightGray);
	pal.setColor(QPalette::Active, QPalette::Text, Qt::black);
	pal.setColor(QPalette::Active, QPalette::Button, QColor(30,30,30));
	pal.setColor(QPalette::Active, QPalette::ButtonText, Qt::gray);
	pal.setColor(QPalette::Active, QPalette::Highlight, QColor(150,150,150));
	pal.setColor(QPalette::Active, QPalette::HighlightedText, Qt::black);
	pal.setColor(QPalette::Active, QPalette::BrightText, Qt::white);
	
	pal.setColor(QPalette::Disabled, QPalette::Window, Qt::black);
	pal.setColor(QPalette::Disabled, QPalette::WindowText, Qt::lightGray);
	pal.setColor(QPalette::Disabled, QPalette::Base, Qt::darkGray);
	pal.setColor(QPalette::Disabled, QPalette::Text, QColor(20,20,20));
	pal.setColor(QPalette::Disabled, QPalette::Button, Qt::black);
	pal.setColor(QPalette::Disabled, QPalette::ButtonText, Qt::darkGray);
	pal.setColor(QPalette::Disabled, QPalette::Highlight, QColor(150,150,150));
	pal.setColor(QPalette::Disabled, QPalette::HighlightedText, Qt::black);
	pal.setColor(QPalette::Disabled, QPalette::BrightText, Qt::white);
	
	pal.setColor(QPalette::Inactive, QPalette::Window, Qt::black);
	pal.setColor(QPalette::Inactive, QPalette::WindowText, Qt::darkGray);
	pal.setColor(QPalette::Inactive, QPalette::Base, Qt::lightGray);
	pal.setColor(QPalette::Inactive, QPalette::Text, Qt::black);
	pal.setColor(QPalette::Inactive, QPalette::Button, QColor(30,30,30));
	pal.setColor(QPalette::Inactive, QPalette::ButtonText, Qt::gray);
	pal.setColor(QPalette::Inactive, QPalette::Highlight, QColor(150,150,150));
	pal.setColor(QPalette::Inactive, QPalette::HighlightedText, Qt::black);
	pal.setColor(QPalette::Inactive, QPalette::BrightText, Qt::white);
	app.setPalette(pal);
	
	QLabel *lab1 = new QLabel("Seite1");
	QLabel *lab2 = new QLabel("Seite2");
	QLabel *lab3 = new QLabel("Seite3");
	
	
	VTabBar *window = new VTabBar;
	window->addTab(lab1, "Erster Tab");
	window->addTab(lab2, "Zweiter Tab");
	window->addTab(lab3, "Dritter Tab");
	window->setCurrentIndex(0);
	
	
	window->show();
	return app.exec();
}