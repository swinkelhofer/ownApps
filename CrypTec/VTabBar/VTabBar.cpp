#include "VTabBar.h"
#include <QtGui>

VTabBar::VTabBar(QWidget *parent):QWidget(parent)
{
	stack = new QStackedLayout;
	tabs = new QVBoxLayout;
	tabs->setSpacing(0);
	QHBoxLayout *main = new QHBoxLayout;
	main->addLayout(tabs);
	main->addLayout(stack);
	setLayout(main);
	setMouseTracking(true);
	connect(this, SIGNAL(currentChanged(int)), this, SLOT(setCurrentIndex(int)));
	current = 0;
	highlightedNr = -1;
}

void VTabBar::addTab(QWidget *widget, const QString &text)
{
	stack->addWidget(widget);
	widget->setMouseTracking(true);
	list.append(new Tab(text, false));
	tabs->addWidget(list.at(list.count()-1));
}

void VTabBar::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::LeftButton)
	{
		if(list.indexOf(this->childAt(event->x(), event->y())) != -1)
		{
			this->findChildren<Tab *>().at(current)->current = false;
			this->findChildren<Tab *>().at(current)->highlighted = false;
			this->findChildren<Tab *>().at(current)->setPalette(this->findChildren<Tab *>().at(current)->temp);
			this->findChildren<Tab *>().at(current)->update();
			emit currentChanged(list.indexOf(this->childAt(event->x(), event->y())));
			current = list.indexOf(this->childAt(event->x(), event->y()));
			this->findChildren<Tab *>().at(list.indexOf(this->childAt(event->x(), event->y())))->current = true;
			this->findChildren<Tab *>().at(list.indexOf(this->childAt(event->x(), event->y())))->highlighted = false;
			this->findChildren<Tab *>().at(list.indexOf(this->childAt(event->x(), event->y())))->setPalette(this->findChildren<Tab *>().at(list.indexOf(this->childAt(event->x(), event->y())))->temp);
			this->findChildren<Tab *>().at(list.indexOf(this->childAt(event->x(), event->y())))->update();
		}
	}
}

void VTabBar::mouseMoveEvent(QMouseEvent *event)
{
	if(list.indexOf(this->childAt(event->x(), event->y())) != -1)
	{
		if(highlightedNr != -1)
		{
			this->findChildren<Tab *>().at(highlightedNr)->highlighted = false;
			this->findChildren<Tab *>().at(highlightedNr)->update();
		}
		highlightedNr = list.indexOf(this->childAt(event->x(), event->y()));
		this->findChildren<Tab *>().at(highlightedNr)->highlighted = true;
		this->findChildren<Tab *>().at(highlightedNr)->update();
	}
	else
	{
		if(highlightedNr != -1)
		{
			this->findChildren<Tab *>().at(highlightedNr)->highlighted = false;
			this->findChildren<Tab *>().at(highlightedNr)->update();
			highlightedNr = -1;
		}
	}
}

void VTabBar::setCurrentIndex(int index)
{
	stack->setCurrentIndex(index);
	this->findChildren<Tab *>().at(index)->current = true;
	this->findChildren<Tab *>().at(index)->update();
}

Tab::Tab(QWidget *parent):QWidget(parent)
{
	current = false;
}

Tab::Tab(const QString &text, bool foreground, QWidget *parent):QWidget(parent)
{
	current = foreground;
	label = text;
	temp = this->palette();
	highlighted = false;
	setMouseTracking(true);
	setMinimumWidth((int)((float)fontMetrics().boundingRect(text).width()*(float)1.5));
	setMinimumHeight(fontMetrics().boundingRect(text).height()+30);
}

void Tab::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	if(!current)
	{
		if(highlighted)
		{
		/*QRadialGradient radialGrad(QPointF(this->width()/2, this->height()/2), this->height());
        radialGrad.setColorAt(0, QColor(20,20,20));
        radialGrad.setColorAt(0.7, this->palette().window().color());
        radialGrad.setColorAt(1, this->palette().window().color());
		
		painter.setPen(Qt::NoPen);
		painter.setBrush(QBrush(radialGrad));
		painter.drawRect(0,0,this->width()-1, this->height()-1);*/
		QFont font;
		font.setBold(true);
		painter.setFont(font);
		}
		else
		{
		painter.setPen(Qt::NoPen);
		painter.setBrush(this->palette().window());
		painter.drawRect(0,0,this->width()-1, this->height()-1);
		}
		painter.setPen(QPen(this->palette().windowText(), 2));
		painter.drawLine(0, this->height()-1, this->width()-11, this->height()-1);
		painter.drawLine(this->width()-1, this->height()-11, this->width()-1, 0);
		//painter.setPen(QPen(this->palette().window(),1));
		//painter.setBrush(QColor(20,20,20));
		//painter.drawRect(0,0,this->width()-1, this->height()-1);
		//painter.setPen(QPen(this->palette().windowText(), 1));
		painter.drawArc(this->width()-21, this->height()-21, 20, 20, 0, -4*360);
		painter.drawText(0,0,this->width(), this->height(), Qt::AlignVCenter, label);
	}
	else
	{
		QPalette pal;
		pal.setColor(QPalette::WindowText, QColor(temp.windowText().color().red()+60, temp.windowText().color().green()+60, temp.windowText().color().blue()+60));
		//painter.setPen(QPen(pal.windowText(),4));
		
		
		//painter.drawLine(this->width()-1, this->height()-1, this->width()-1, 0);
		
		painter.setPen(QPen(pal.windowText(),4));
		painter.setBrush(Qt::NoBrush);
		painter.drawLine(0, this->height()-1, this->width()-1, this->height()-1);
		QFont font;
		font.setPixelSize(this->fontInfo().pixelSize()+2);
		font.setBold(true);
		painter.setFont(font);
		painter.drawText(0,0,this->width(), this->height(), Qt::AlignVCenter, label);
	}
		
}

void Tab::setText(QString &text)
{
	label = text;
	update();
}

/*void Tab::mouseMoveEvent(QMouseEvent *event)
{
	QRect rect(8,8,this->width()-16, this->height()-16);
	if(rect.contains(event->pos()) && !current)
	{
		QPalette pal;
		pal.setColor(QPalette::WindowText, QColor(temp.windowText().color().red()+80, temp.windowText().color().green()+80, temp.windowText().color().blue()+80));
		setPalette(pal);
		highlighted = true;
		update();
	}
	if(!rect.contains(event->pos()) && !current)
	{
		setPalette(temp);
		highlighted = false;
		update();
	}
}*/