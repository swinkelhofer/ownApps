#include <QApplication>
#include <QtGui>
#include "CryptString.h"
#include "AES.h"
#include <iostream>
#include <string>
using namespace std;

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	if(argc < 4)
		cout << "Usage: FileCrypt <File> <enc OR dec> <Key>";
	else
	{
		if(string(argv[2]) == string("enc"))
		{
			CryptString key;
			key.set(argv[3]);
			AESClass aes;
			QString outfile = QString(argv[1]);
			if(outfile.endsWith(".dcu"))
			{
				cout << "Already encrypted!!!";
				exit(0);
			}
			outfile += QString(".dcu");
			if(!aes.EncryptFile(argv[1], outfile.toStdString().c_str(), key))
			{
				cout << "Could not encrypt!!!";
				exit(0);
			}
		}
		else if(string(argv[2]) == string("dec"))
		{
			CryptString key;
			key.set(argv[3]);
			AESClass aes;
			QString outfile = QString(argv[1]);
			if(!outfile.endsWith(".dcu"))
			{
				cout << "No encrypted file!!!";
				exit(0);
			}
			outfile.chop(4);
			if(!aes.DecryptFile(argv[1], outfile.toStdString().c_str(), key))
			{
				cout << "Could not decrypt!!!";
				exit(0);
			}
		}
	}
	exit(0);
	return app.exec();
}
