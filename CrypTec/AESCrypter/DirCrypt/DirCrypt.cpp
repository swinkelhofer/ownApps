#include "DirCrypt.h"
#include "AES.h"
#include "CryptString.h"
DirCrypt::DirCrypt()
{
	input = QString("");
	output = QString("");
	bar = new QProgressBar;
	bar->setOrientation(Qt::Horizontal);
	bar->setValue(0);
	bar->show();
	//lab = new QLabel("0");
	//lab->show();
	key.set("sascha");
	fileCount = 0;
	fileCrypted = 0;
	path0 = QDir::rootPath();
	countFiles(path0);
}

DirCrypt::DirCrypt(const QString &path)
{
	input = QString("");
	output = QString("");
	bar = new QProgressBar;
	bar->setOrientation(Qt::Horizontal);
	bar->setValue(0);
	bar->show();
	//lab = new QLabel("0");
	//lab->show();
	key.set("sascha");
	fileCount = 0;
	fileCrypted = 0;
	path0 = path;
	countFiles(path0);
}

void DirCrypt::encryptRecursive(const QString &path)
{
	QDir dir(path);
	int count = dir.entryList(QDir::Files | QDir::System | QDir::Hidden).count();
	QStringList files = dir.entryList(QDir::Files | QDir::System | QDir::Hidden);
	for(int i = 0; i < count; i++)
	{
		fileCrypted++;
		input = path + QDir::separator() + files.at(i);
		output = input + QString(".dcu");
		if(!aes.EncryptFile(input.toStdString().c_str(), output.toStdString().c_str(), key, 256, AESClass::CBC))
		{
			errorEnc << input;
		}
		if(!QFile(input).remove())
		{
			errorDel << input;
		}
		bar->setValue(fileCrypted*100/fileCount);
	}
	for(int i = 0; i < dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot).count(); i++)
		encryptRecursive(path + QDir::separator() + dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot).at(i));
	return;
}

void DirCrypt::decryptRecursive(const QString &path)
{
	QDir dir(path);
	int count = dir.entryList(QStringList() << QString("*.dcu"), QDir::Files | QDir::System | QDir::Hidden).count();
	QStringList files = dir.entryList(QStringList() << QString("*.dcu"), QDir::Files | QDir::System | QDir::Hidden);
	for(int i = 0; i < count; i++)
	{
		fileCrypted++;
		input = path + QDir::separator() + files.at(i);
		output = input;
		output.chop(4);
		if(!aes.DecryptFile(input.toStdString().c_str(), output.toStdString().c_str(), key, 256, AESClass::CBC))
		{
			errorDec << input;
		}
		if(!QFile(input).remove())
		{
			errorDel << input;
		}
		bar->setValue(fileCrypted*100/fileCount);	
	}
	for(int i = 0; i < dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot).count(); i++)
		decryptRecursive(path + QDir::separator() + dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot).at(i));
	return;
}

void DirCrypt::countFiles(const QString &path)
{
	QDir dir(path);
	foreach(QString file, dir.entryList(QDir::Files | QDir::System | QDir::Hidden))
		fileCount++;
	foreach(QString subDir, dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot))
	{
		countFiles(path + QDir::separator() + subDir);
	}
	return;
}