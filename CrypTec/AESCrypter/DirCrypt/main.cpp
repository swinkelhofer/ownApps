#include <QApplication>
#include "DirCrypt.h"
#include <string.h>
#include <iostream>
using namespace std;
int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	if(argc < 3)
		cerr << "Usage: DirCrypt <Directory's Path> <enc OR dec>";
	else
	{
	if(std::string(argv[2]) == std::string("dec"))
	{
		DirCrypt *dir = new DirCrypt(QString(argv[1]));
		dir->decryptRecursive(QString(argv[1]));
	}
		
	else if(std::string(argv[2]) == std::string("enc"))
	{
		DirCrypt *dir = new DirCrypt(QString(argv[1]));
		dir->encryptRecursive(QString(argv[1]));
	}
	}
	exit(0);
	return app.exec();
}