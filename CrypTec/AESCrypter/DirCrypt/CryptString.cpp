#include "CryptString.h"
#include <iostream>
#include <string.h>
#include <stdlib.h>
using namespace std;
CryptString::CryptString()
{
	blocks = 2;
	tempblocks = 0;
	resize();
}

void CryptString::set(char *val)
{
	int length = strlen(val);
	for(int i = 0; i < 32; i++)
		this->str[i] = 0x01;
	for(int i = 0; i < length; i++)
		this->str[i] = (unsigned char) val[i];
}

void CryptString::resize()
{
	unsigned char *temp = (unsigned char *)malloc(tempblocks*16);
	for(int i = 0; i < tempblocks*16; i++)
		temp[i] = str[i];
	free(str);
	str = (unsigned char *) malloc(blocks*16);
	if(tempblocks < blocks)
	{
		for(int i = 0; i < blocks*16; i++)
			str[i] = 0x00;
		for(int i = 0; i < tempblocks*16; i++)
			str[i] = temp[i];
	}
	else
	{
		for(int i = 0; i < blocks*16; i++)
			str[i] = temp[i];
	}
	tempblocks = blocks;
	free(temp);
}

CryptString CryptString::operator+(CryptString crypt)
{
	if(crypt.blocks == 0)
	{
		return *this;
	}
	else if(this->blocks == 0)
	{
		return crypt;
	}
	else
	{
		CryptString crypt1;
		crypt1.blocks = this->blocks + crypt.blocks;
		crypt1.resize();
		for(int i = 0; i < this->blocks*16; i++)
			crypt1.str[i] = this->str[i];
		int leng = this->blocks*16;
		
		for(int i = leng; i < crypt1.blocks*16; i++)
			crypt1.str[i] = crypt.str[i-leng];
		return crypt1;
	}
}

CryptString CryptString::operator=(CryptString crypt)
{
	this->blocks = crypt.blocks;
	this->resize();
	for(int i = 0; i < this->blocks*16; i++)
		this->str[i]=crypt.str[i];
	return *this;
}

void CryptString::print()
{
	for(int i = 0; i < this->blocks*16; i++)
		cout << this->str[i];
}
