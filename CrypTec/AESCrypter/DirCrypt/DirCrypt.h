#include <QtGui>
#include "AES.h"
#include "CryptString.h"
class CryptString;
class AESClass;
class DirCrypt
{
public:
	DirCrypt();
	DirCrypt(const QString &path);
	QString path0;
	void encryptRecursive(const QString &path);
	void decryptRecursive(const QString &path);
	
private:
	//QLabel *lab;
	int fileCount;
	int fileCrypted;
	void countFiles(const QString &path);
	CryptString key;
	AESClass aes;
	QString input,output;
	QStringList errorEnc, errorDec, errorDel;
	QProgressBar *bar;
};
