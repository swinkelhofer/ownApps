#include <QWidget>
#include <QList>
class QString;
class QStringList;
class QStackedLayout;
class QVBoxLayout;
class QLabel;
class Tab;
class QPalette;
//class QList<QWidget *>;

class VTabBar: public QWidget
{
	Q_OBJECT
public:
	VTabBar(QWidget *parent = 0);
	void addTab(QWidget *widget, const QString &text);
private:
	//QStringList tabLabels;
	QStackedLayout *stack;
	QVBoxLayout *tabs;
	QList<QWidget *> list;
	int current;
	int highlightedNr;
	//void update();
public slots:
	void setCurrentIndex(int index);
signals:
	void currentChanged(int index);
protected:
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
};

class Tab: public QWidget
{
	Q_OBJECT
public:
	Tab(QWidget *parent = 0);
	Tab(const QString &text, bool foreground = false, QWidget *parent = 0);
	void setText(QString &text);
	bool current;
	bool highlighted;
	QPalette temp;
protected:
	void paintEvent(QPaintEvent *event);
	//void mouseMoveEvent(QMouseEvent *event);
private:
	QString label;
};
