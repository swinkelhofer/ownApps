#ifndef AES
#define AES
#include "CryptString.h"
#include <string.h>
#include <QThread>
using namespace std;

void FastBlockEnc256();
void KeyExpansion();
void FirstRound();
void LastRound();
void Combined();

class AESThread: public QThread
{
	Q_OBJECT

public:
	void setParams(string setInfile, string setOutfile, string setKey);
	void stop();
	float time;
signals:
	void percentageChanged(int value);
protected:
	void run();
private:
	CryptString key;
	string inFile, outFile;
	FILE *in, *out;
};

class DelThread: public QThread
{
	Q_OBJECT
public:
	void setParams(string setDelFile);
	void stop();
protected:
	void run();
private:
	string delFile;
	FILE *over;
signals:
	void percentageChanged(int value);
};
#endif
