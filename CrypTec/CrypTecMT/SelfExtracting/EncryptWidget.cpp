#include <QtGui>
#include "EncryptWidget.h"

EncryptWidget::EncryptWidget(QWidget *parent):QWidget(parent)
{
	//BEGIN GUIze
	lab1 = new QLabel("Zu verschlüsselnde Datei:");
	lab2 = new QLabel("Selbstenschlüsselnde .exe-Datei:");
	lab3 = new QLabel("Passwort:");
	QFont font1("Arial"), font2("Arial");
	font1.setItalic(true);
	font2.setBold(true);
	In = new QLineEdit;
	In->setDisabled(true);
	In->setFont(font1);
	Out = new QLineEdit;
	Out->setDisabled(true);
	Out->setFont(font1);
	PW = new QLineEdit;
	PW->setEchoMode(QLineEdit::Password);
	delCheckBox = new QCheckBox("Ursprungs-Datei sicher löschen");
	OK = new QPushButton("Verschlüsseln");
	OK->setFont(font2);
	ChangeIn = new QPushButton("Ändern");
	ChangeIn->setFocus(Qt::MouseFocusReason);
	//END GUIze
	
	//BEGIN Layoutzis
	QGridLayout *top = new QGridLayout;
	top->setSizeConstraint(QLayout::SetMinimumSize);
	top->addWidget(lab1, 0,0);
	top->addWidget(In, 1, 0);
	top->addWidget(lab2, 2, 0);
	top->addWidget(Out, 3, 0);
	top->addWidget(delCheckBox, 4, 0);
	top->addWidget(ChangeIn, 1,1);
	
	QHBoxLayout *mid = new QHBoxLayout;
	mid->setSizeConstraint(QLayout::SetMinimumSize);
	mid->addWidget(PW);
	mid->addStretch();
	
	QVBoxLayout *main = new QVBoxLayout;
	main->setSizeConstraint(QLayout::SetMinimumSize);
	main->addLayout(top);
	main->addSpacing(20);
	main->addWidget(lab3);
	main->addLayout(mid);
	main->addSpacing(20);
	main->addWidget(OK);
	setLayout(main);
	
	setWindowFlags(Qt::MSWindowsFixedSizeDialogHint | Qt::WindowTitleHint | Qt::WindowSystemMenuHint);
	
	setTabOrder(ChangeIn, delCheckBox);
	setTabOrder(delCheckBox,PW);
	setTabOrder(PW, OK);
	//END Layoutzis

	//BEGIN Connectis
	connect(In, SIGNAL(textChanged(QString)), this, SLOT(changeOut(QString)));
	connect(OK, SIGNAL(clicked()), this, SLOT(encrypt()));
	connect(ChangeIn, SIGNAL(clicked()), this, SLOT(changeIn()));
	//END Connectis
	
	//BEGIN Verschlüsselungs-Status
	cryptStat = new QWidget;
	cryptStat->hide();
	QLabel *lab = new QLabel("Verschlüssle mit sicherem aesThread256 im CBC-Mode:");
	cryptBar = new QProgressBar;
	cryptBar->setOrientation(Qt::Horizontal);
	cryptBar->setTextVisible(true);
	cryptBar->setRange(0,100);
	cryptBar->show();
	cryptBar->setValue(0);
	QVBoxLayout *Statmain = new QVBoxLayout;
	Statmain->addWidget(lab);
	Statmain->addWidget(cryptBar);
	cryptStat->setLayout(Statmain);
	//END Verschlüsselungs-Status
	
	//BEGIN Lösch-Status
	delStat = new QWidget;
	delStat->hide();
	delBar = new QProgressBar;
	delBar->setOrientation(Qt::Horizontal);
	delBar->setTextVisible(true);
	delBar->setRange(0,100);
	delBar->setValue(0);
	QLabel *dellab = new QLabel("Lösche Ursprungs-Datei sicher durch dreimaliges Überschreiben:");
	QVBoxLayout *delmain = new QVBoxLayout;
	delmain->addWidget(dellab);
	delmain->addWidget(delBar);
	delStat->setLayout(delmain);
	//END Lösch-Status
}

void EncryptWidget::closeEvent(QCloseEvent *event)
{
	if(aesThread.isRunning())
	{
		aesThread.stop();
		aesThread.wait();
	}
	if(delThread.isRunning())
	{
		delThread.stop();
		delThread.wait();
	}
	delete(delStat);
	delete(cryptStat);
	event->accept();
}

void EncryptWidget::setDelFinished()
{
	delBar->setValue(100);
	delStat->hide();
	delBar->setValue(0);
	if(OK->text() == QString("Abbrechen"))
		OK->setText("Verschlüsseln");
}

void EncryptWidget::setCryptFinished()
{
	cryptBar->setValue(100);
	cryptStat->hide();
	cryptBar->setValue(0);
	if(OK->text() == QString("Abbrechen"))
		OK->setText("Verschlüsseln");
	QLabel *usedTime = new QLabel(QString::number(aesThread.time));
	usedTime->show();
}

void EncryptWidget::encrypt()
{
	if((aesThread.isRunning() || delThread.isRunning()) && OK->text() == QString("Abbrechen"))
	{
		if(aesThread.isRunning())
		{
			aesThread.stop();
			cryptStat->hide();
		}
		else if(delThread.isRunning())
		{
			delThread.stop();
			delStat->hide();
		}
		OK->setText("Verschlüsseln");
	}
	else if(OK->text() == QString("Verschlüsseln"))
	{
		if(PW->text().isEmpty())
		{
			OK->setDisabled(false);
			return;
		}
		OK->setDisabled(true);
		QFile infile("Decrypt.exe");
		QFile fileCheck(Out->text());
		{
			if(fileCheck.exists())
			{
				fileCheck.remove();
			}
		}
		infile.copy(Out->text());
		OK->setText("Abbrechen");
		cryptStat->show();
		
		//BEGIN Verschlüsselungs-Connectis
		connect(&aesThread, SIGNAL(percentageChanged(int)), cryptBar, SLOT(setValue(int)));
		connect(&aesThread, SIGNAL(finished()), this, SLOT(setCryptFinished()));
		//END Verschlüsselungs-Connectis
		
		aesThread.setParams(In->text().toStdString(), Out->text().toStdString(), PW->text().toStdString());
		aesThread.start(QThread::TimeCriticalPriority);
		
		if(delCheckBox->checkState() == Qt::Checked)
		{
			delThread.setParams(In->text().toStdString());
			
			//BEGIN Lösch-Connectis
			connect(&aesThread, SIGNAL(finished()), this, SLOT(startDelThread()));
			connect(&delThread, SIGNAL(percentageChanged(int)), delBar, SLOT(setValue(int)));
			connect(&delThread, SIGNAL(finished()), this, SLOT(setDelFinished()));
			//END Lösch-Connectis
		}
		PW->clear();
		OK->setDisabled(false);
	}
	
}

void EncryptWidget::startDelThread()
{
	delStat->show();
	delThread.start(QThread::TimeCriticalPriority);
}

void EncryptWidget::changeOut(const QString &text)
{
	Out->setText(text + QString(".exe"));
}

void EncryptWidget::changeIn()
{
	In->setText(QFileDialog::getOpenFileName(this, "Choose a file to encrypt", "C:\\", "All Files (*.*)"));
}
