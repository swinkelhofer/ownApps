#ifndef ENCRYPT_WIDGET
#define ENCRYPT_WIDGET
#include <QWidget>
#include "CryptString.h"
#include "AES.h"
class QPushButton;
class QLineEdit;
class QLabel;
class QCheckBox;
class QWidget;
class QProgressBar;
class EncryptWidget:public QWidget
{
	Q_OBJECT
public:
	EncryptWidget(QWidget *parent = 0);
private:
	QLabel *lab1, *lab2, *lab3;
	QLineEdit *In, *Out, *PW;
	QPushButton *ChangeIn, *OK;
	QCheckBox *delCheckBox;
	QWidget *cryptStat, *delStat;
	QProgressBar *cryptBar, *delBar;
	AESThread aesThread;
	DelThread delThread;
protected:
	void closeEvent(QCloseEvent *event);
private slots:
	void changeOut(const QString &text);
	void changeIn();
	void encrypt();
	void setCryptFinished();
	void setDelFinished();
	void startDelThread();
};
#endif