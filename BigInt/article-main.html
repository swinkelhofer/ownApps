<html><head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
<title>C/C++ Users Journal - November 2001</title></head>
<body text="#000000" background="" bgcolor="#FFFFFF">
<h2><img src="article-main-Dateien/Toc.gif" alt=" " width="54" height="54"><font color="#FF0000">   Engineering and Numerical Programming</font></h2>

<hr>
<h3>This article first appeared in the November 2001 issue
of <i>C/C++ Users Journal</i>. The article is reprinted with
permission of <i>C/C++ Users Journal</i> Copyright � 2001.
</h3>

<hr>
<h2 align="center"><font color="#800000">MAPM, A Portable Arbitrary Precision Math Library in C</font></h2>
<h3 align="center"><font color="#800000">Michael C. Ring</font></h3>

<blockquote>
<p>Frustrated by the finiteness of fixed-size arithmetic? This math library gives you the precision you need.</p>
</blockquote>

<hr>
<blockquote>
<p>Sometime ago I had to solve what is a fairly common problem in 
numerical and scientific programming. I needed to curve-fit a set of x,y
 data samples to a polynomial equation. This problem eventually led me 
to write my own arbitrary precision math library. Of course I knew at 
the time that other libraries existed, but they were lacking some 
features I considered important. This article describes some of the 
added features of MAPM, and some of the implementation details that 
readers probably don't think about every day; such as how to multiply 
two numbers together in less than O(N<sup>2</sup>) time. I hope that 
readers will find MAPM interesting and useful for their own 
applications. In particular, thanks to a C++ wrapper class contributed 
by Orion Sky Lawlor, I believe MAPM is very easy to use.</p>

<h3><font color="#000080">Why We Need Arbitrary Precision</font></h3>

<p>The project that inspired MAPM had specific requirements related to 
the maximum error allowed at each data point. In theory, by increasing 
the order of the polynomial used in the curve fit, it is possible to 
minimize the error of the data samples. After viewing a plot of the data
 samples, it became obvious that a high-order polynomial curve fit would
 be required. I used a least-squares curve-fitting algorithm. For a 
10th-order polynomial, the algorithm required a summation of the x data 
samples raised to the 20th power. In general, for an Nth order 
polynomial, least-squares will require computing data samples to the 2N 
power.</p>
<p>At first, everything was working as expected. As I increased the 
order of the curve fit, the error in the fit compared to the raw data 
became smaller. This was true until I modeled an 18th-order polynomial. 
At this point, the solution did not improve. Higher order curve fits 
failed to yield improvement as well. I then tried the same input data on
 three different computers (with different operating systems and 
compilers) and generated three very different solutions. This is when I 
realized that a more fundamental problem was coming into play.</p>
<p>As you have probably already guessed, the accumulation of the 
round-off errors in the floating-point math was the culprit. I had 
reached the limits of Standard C's <b>double</b> data type, at least as 
implemented on the machines that were available to me. I realized I 
would need an arbitrary precision math package to complete my 
calculations. An arbitrary precision math package allows math to be 
performed to any desired level of precision.</p>
<p>For example, consider the two numbers <b>N1 = 98237307.398797975997</b> and <b>N2 = 87733164872.98273499749</b>. <b>N1</b> has 20 significant digits; <b>N2</b> has 22. If you assign these to C <b>double</b>s, each variable will maintain only 15-16 digits of precision <a href="#1">[1]</a>. If you then multiply these numbers, the result will be precise only to the 15-16 most significant digits.</p>
<p>The <i>true</i> multiplication result would contain 42 significant 
digits. An arbitrary precision math library will do the precise math and
 save the full precision of the multiplication. Addition and subtraction
 errors are also eliminated in an arbitrary precision math library. If 
you add <b>1.0E+100</b> to <b>1.0E-100</b> in normal C (or any other 
language), the small fractional number is lost. In arbitrary precision 
math, the full precision is maintained (~200 significant digits in this 
example).</p>
<p>After converting the curve-fitting algorithm to use arbitrary 
precision math, the three different computers all computed byte-for-byte
 identical results. This held true to well beyond 30th-order 
polynomials. In case you are curious, my accuracy requirements were 
satisfied with a 24th-order polynomial.</p>

<h3><font color="#000080">MAPM Feature Set</font></h3>

<p>When I searched for an arbitrary precision math library to use, I 
noticed some common traits among the available libraries. First, most of
 them seemed to have a preference for integer-only math. And second, 
none of the arbitrary precision C libraries could perform the math 
functions typically found in <b>math.h</b>, such as <b>sqrt</b>, <b>cos</b>, <b>log</b>, etc.</p>
<p>It was at this point that I decided to write my own library. The 
basic requirements for my library were that it provide natural support 
for floating-point numbers and that it perform the most common functions
 found in <b>math.h</b>.</p>
<p>MAPM will perform the following functions to any desired precision level: <b>Sqrt</b>, <b>Cbrt</b> (cube root), <b>Sin</b>, <b>Cos</b>, <b>Tan</b>, <b>Arc-sin</b>, <b>Arc-cos</b>, <b>Arc-tan</b>, <b>Arc-tan2</b>, <b>Log</b>, <b>Log10</b>, <b>Exp</b>, <b>Pow</b>, <b>Sinh</b>, <b>Cosh</b>, <b>Tanh</b>, <b>Arc-sinh</b>, <b>Arc-cosh</b>, <b>Arc-tanh</b>, and also Factorial. The full <b>math.h</b>
 is not duplicated, though I think the functions listed above are most 
of the important ones. (My definition of what's important is what I've 
actually used in a real application.) MAPM also has a random number 
generator with of period of <b>1.0E+15</b>.</p>
<p>MAPM has proven to be very portable. It has been compiled and tested 
under x86 Linux, HP-UX, and Sun Solaris using the GCC compiler. It has 
also been compiled and tested under DOS/Windows NT/Windows 9x using GCC 
for DOS as well as (old) 16- and 32-bit compilers from Borland and 
Microsoft. Makefiles are included in the online source archive 
(available at &lt;www.cuj.com/code&gt;) for the most common compilers 
under various operating systems.</p>

<h3><font color="#000080">The MAPM C++ Wrapper Class</font></h3>

<p>Orion Sky Lawlor (<b>olawlor@acm.org</b>) has added a very nice C++ wrapper class to MAPM.</p>
<p>Using the C++ wrapper allows you to do things such as the following:</p>

<pre>// Compute the factorial of the integer n

MAPM factorial(MAPM n)
{
   MAPM i;
   MAPM product = 1;

   for (i=2; i &lt;= n; i++)
      product *= i;

   return product;
}
</pre>

<p>The syntax is the same as if you were just writing normal code, but 
all the computations will be performed with the high precision math 
library, using the new data type <b>MAPM</b>.</p>
<p>See <a href="http://www.tc.umn.edu/%7Eringx004/listing1.html">Listing 1</a>
 for another C++ sample. Note the use of literal character strings as 
constants. This allows the user to specify constants that cannot be 
represented by a standard C data type, such as a number with 200 digits 
or a number with a very large or small exponent (e.g., <b>6.21E-3714</b>).</p>

<h3><font color="#000080">Algorithms for Implementing MAPM</font></h3>

<p>Since the MAPM contains over 30 functions, it is not practical to 
discuss all the algorithms used in the library. I will, however, discuss
 the more interesting ones.</p>

<h4><font color="#000080">Multiplication</font></h4>

<p>In an arbitrary precision math library, the multiplication function is the most critical. Multiplication is normally an <b>O(N<sup>2</sup>)</b>
 operation. When you learned to multiply in grade school, you multiplied
 each digit of each number and did the final addition after all the 
multiplies were completed. To visualize this, multiply by hand a 
four-digit number by a four-digit number. The intermediate math requires
 16 multiplies (4<sup>2</sup>). This method works and is very easy to 
implement; however it becomes prohibitively slow when the number of 
digits becomes large. So a 10,000-digit multiply will require 100 
million individual multiplications!</p>

<h4><font color="#000080">Faster Multiplication</font></h4>

<p>The next multiplication algorithm discussed is commonly referred to as the divide-and-conquer algorithm.</p>
<p>Assume we have two numbers (<b>a</b> and <b>b</b>) each containing <b>2N</b> digits:</p>

<pre>let: a = (2<sup>N</sup>)*A<sub>1</sub> + A<sub>0</sub>, b = (2<sup>N</sup>)*B<sub>1</sub> + B<sub>0</sub>
</pre>

<p>where <b>A<sub>1</sub></b> is the "most significant half" of <b>a</b> and <b>A<sub>0</sub></b> is the "least significant half" of <b>a</b>. The same applies for <b>B<sub>1</sub></b> and <b>B<sub>0</sub></b>.</p>

<p>Now use the identity:</p>

<pre>ab = (2<sup>2N</sup> + 2<sup>N</sup>)A<sub>1</sub>B<sub>1</sub> + 2<sup>N</sup>(A<sub>1</sub>-A<sub>0</sub>)(B<sub>0</sub>-B<sub>1</sub>) + (2<sup>N</sup> + 1)A<sub>0</sub>B<sub>0</sub>
</pre>

<p>The original problem of multiplying two <b>2N</b>-digit numbers has been reduced to three multiplications of <b>N</b>-digit numbers plus some additions, subtractions, and shifts.</p>
<p>This multiplication algorithm can be used in a recursive process. The divide-and-conquer method results in approximately <b>O(N<sup>1.585</sup>)</b> growth in computing time as <b>N</b>
 increases. So a 10,000-digit multiply will result in only approximately
 2.188 million multiplies. This represents a considerable improvement 
over the generic <b>O(N<sup>2</sup>)</b> method.</p>

<h4><font color="#000080">Really Fast Multiplication</font></h4>

<p>The final multiplication algorithm discussed utilizes the FFT (Fast 
Fourier Transform). An FFT multiplication algorithm grows only at <b>O(N*Log<sub>2</sub>(N))</b>. This growth is far less than the normal <b>N<sup>2</sup></b> or the divide-and-conquer's <b>N<sup>1.585</sup></b>. An FFT tutorial is beyond the scope of this article, but the basic methodology can be discussed.</p>
<p>First, perform a forward Fourier transform on both numbers, where the
 digits of each number are regarded as the samples being input to the 
FFT. This yields two sets of coefficients in the "frequency" domain. 
Each set of coefficients will contain as many samples as the number of 
digits in the original number. (Also, each coefficient will be a complex
 number.) Second, multiply the two sets of coefficients together. That 
is, if the numbers being multiplied are <b>a</b> and <b>b</b>, perform <b>c<sub>a0</sub>*c<sub>b0</sub></b>, <b>c<sub>a1</sub>*c<sub>b1</sub></b>, etc., where <b>c<sub>an</sub></b>, <b>c<sub>bn</sub></b> are the frequency-domain coefficients of <b>a</b> and <b>b</b>.
 This pair-wise multiplication in the frequency domain is equivalent to 
convolution in the "time" or sample, domain. This yields the correct 
result, because when you multiply two <b>N</b>-digit numbers together, 
you are really performing a form of convolution across their digits. 
Third, compute the inverse transform on the product sequence. Lastly, 
convert the real part of the inverse FFT to integers and also release 
all your carries in the process.</p>
<p>The FFT used in MAPM is from Takuya Ooura. This FFT is fast, portable, and freely distributable.</p>
<p>To really see the differences in these three multiplication 
algorithms, compare these run times for multiplying two 1,000,000-digit 
numbers:</p>

<pre>FFT Method          : 40 seconds
Divide-and-Conquer  : 1 hr, 50 min
Ordinary N<sup>2</sup>         : 23.9 days*

*projected!
</pre>

<p>The MAPM library uses all three multiplication algorithms. For small input numbers, the normal <b>O(N<sup>2</sup>)</b>
 algorithm is used. (After all, you don't need a special algorithm to 
multiply 43 x 87.) Next, the FFT algorithm is used. FFT-based algorithms
 do reach a point where the floating-point math will overflow, so at 
this point the divide-and-conquer algorithm is used. Once the 
divide-and-conquer algorithm has divided down to the point where the FFT
 can handle it, the FFT will finish up.</p>

<h4><font color="#000080">Division</font></h4>

<p>Two division functions are used.</p>
<p>For dividing numbers less than 250 digits long, I used Knuth's division algorithm from <i>The Art of Computer Programming, Volume 2</i> <a href="#2">[2]</a> with a slight modification. I determine right in step D3 whether <b>q-hat</b>
 is too big, so step D6 is unnecessary. I use the first three (base 100)
 digits of the numerator and the first two digits of the denominator to 
determine the trial divisor, instead of Knuth's use of two and one 
digits, respectively.</p>
<p>For dividing numbers longer than 250 digits, I find <b>a/b</b> by first computing the reciprocal of <b>b</b> and then multiplying by <b>a</b>. The reciprocal <b>y = 1/x</b> is computed by using an iterative method such that:</p>

<pre>y<sub>n+1</sub> = y<sub>n</sub>(2 - xy<sub>n</sub>)
</pre>

<p>where <b>y<sub>n</sub></b> is the current best estimate for <b>1/x</b>. This calculation is performed repeatedly until <b>y<sub>n+1</sub></b> stops changing to within a predetermined tolerance.</p>

<h4><font color="#000080">Exponential, Sine, and Cosine</font></h4>

<p>These three functions are all computed by using a series expansion. 
Translations of the input are required to efficiently calculate these 
quantities. For more detail, see the accompanying sidebar, <a href="http://www.tc.umn.edu/%7Eringx004/sidebar.html">"Practical Series Expansions for Sine, Cosine, and Exponentials"</a></p>

<h4><font color="#000080">Random Number Generator</font></h4>

<p>The random number generator is also taken from Knuth <a href="#3">[3]</a>. Assuming the random number is <b>X</b>, compute (using all integer math):</p>

<pre>X = (a*X + c) MOD m
</pre>

<p>where <b>x MOD y</b> represents <b>x modulo y</b>. From Knuth, <b>m</b> should be large, at least 2<sup>30</sup>. MAPM uses <b>1.0E+15</b>. The <b>a</b> coefficient should be between <b>0.01*m</b> and <b>0.99*m</b> and not have a simple pattern of digits. The <b>a</b> coefficient should not have any large factors in common with <b>m</b> and (since <b>m</b> is a power of 10 in this case) if <b>a MOD 200 = 21</b>, then all <b>m</b> different possible values will be generated before <b>X</b> starts to repeat. MAPM uses <b>a = 716805947629621</b>, so <b>a MOD 200</b> does equal 21, and <b>a</b> is also a prime number. There are few restrictions on <b>c</b>, except that <b>c</b> can have no factor in common with <b>m</b>, hence I have set <b>c = a</b>. On the first call, the system time is used to initialize <b>X</b>.</p>

<h4><font color="#000080">Newton's Method (Also Known as Newton-Raphson)</font></h4>

<p>Unlike sine and cosine, which have convenient series expansions, not 
all math functions can be computed with a closed-form equation. Instead,
 they must be computed iteratively. These functions in MAPM (reciprocal,
 square root, cube root, logarithm, arc sine, and arc cosine) all use 
Newton's method to calculate the solution. (Although there are series 
expansions for logarithm, arc sine, and arc cosine, they all converge 
very slowly, so they are not practical for calculating these functions.)
 In general, the use of Newton's method in this application depends on 
the existence of an inverse for the function being computed <a href="#4">[4]</a>. An initial guess is made for <b>f(x)</b>, where <b>f</b> is the function being computed and <b>x</b> is the input value. Call this initial guess <b>y</b>. This initial guess is then plugged into the equation:</p>

<pre>y<sub>n+1</sub> = y<sub>n</sub> - (g(y<sub>n</sub>) - x)/g'(y<sub>n</sub>)
</pre>

<p>where:</p>

<pre>x is the input value
y<sub>n</sub> = current "guess" for the solution to
     f(x)
g(y<sub>n</sub>) = inverse function of f(x) 
        evaluated at y<sub>n</sub>
g'(y<sub>n</sub>) = first derivative of the inverse
         of f(x) evaluated at y<sub>n</sub>
y<sub>n+1</sub> = refined estimate of f(x)
</pre>

<p>This calculation is iterated until <b>y<sub>n+1</sub></b> stops changing to within a certain tolerance.</p>
<p>Newton's method is quadratically convergent. This results in doubling
 the number of significant digits that must be maintained during each 
iteration. MAPM uses the corresponding Standard C run-time library 
function to provide the initial guess.</p>
<p>As an example, I will demonstrate how to implement the square root 
function using Newton's method. Newton's method essentially asks: if the
 current guess for <b>sqrt(x)</b> is <b>y</b>, how close does squaring <b>y</b> come to producing <b>x</b>?</p>
<p>The equation for the inverse function <b>g(y)</b> is derived as follows:</p>

<pre>f(x) = y = sqrt(x)
g(y) = y<sup>2</sup> = x, the inverse function of
            f(x)
g'(y) = 2y, the derivative of the
        inverse function
</pre>

<p>Set up the iteration:</p>

<pre>y<sub>n+1</sub> = y<sub>n</sub> - (g(y<sub>n</sub>) - x)/g'(y<sub>n</sub>)
y<sub>n+1</sub> = y<sub>n</sub> - (y<sub>n</sub><sup>2</sup> - x)/2y<sub>n</sub>
</pre>

<p>and after a little algebra:</p>

<pre>y<sub>n+1</sub> = 0.5*[y<sub>n</sub> + x/y<sub>n</sub>]
</pre>

<p>To calculate the square root of 8.3, suppose I start with an initial 
guess of 1.0. (In actual use, I would generate a better initial guess.)</p>

<pre>y<sub>n+1</sub> = 0.5*[y<sub>n</sub> + 8.3/y<sub>n</sub>]
</pre>

<p>The iterations of <b>y<sub>n</sub></b> are as follows:</p>

<pre>iteration              y
------------------------------------
0          1.00000000000000000000000
1          4.65000000000000000000000
2          3.21747311827956989247311
3          2.89856862531072654536764
4          2.88102547060539111310782
5          2.88097205867270336382209
6          2.88097205817758669914416
7          2.88097205817758669910162
8          2.88097205817758669910162
</pre>

<p>As you can see, the iteration is converging to a constant value, the 
square root of 8.3. Similar iteration loops are used to compute the 
reciprocal, cube-root, logarithm, arc sine, and arc cosine functions. 
The library actually uses a better <b>sqrt</b> iteration:</p>

<pre>y<sub>n+1</sub> = 0.5*y<sub>n</sub>*[3 - x*y<sub>n</sub><sup>2</sup>]
</pre>

<p>This iteration actually finds <b>1/sqrt(x)</b>. It is preferable 
since there is no division operation. Division is slower than 
multiplication and is best avoided when possible.</p>

<h3><font color="#000080">Summary</font></h3>

<p>MAPM is a portable arbitrary math library that is easy to use, 
especially when used with the C++ wrapper class. It supports most of the
 math functions encountered in a typical application, so it can be used 
in many applications that require arbitrary precision. MAPM uses an 
FFT-based algorithm for multiplication, which is typically the 
performance bottleneck for arbitrary precision math libraries.</p>

<h3><font color="#000080">Notes and References</font></h3>

<p><a name="1"></a>[1]  This assumes that <b>double</b>s are implemented as 64-bit words, which is the case for most general purpose computers available today.</p>
<p><a name="2"></a>[2]  Donald E. Knuth. <i>The Art of Computer Programming, Volume 2, Seminumerical Algorithms, Third Edition</i> (Addison-Wesley, 1998), pages 270-273.</p>
<p><a name="3"></a>[3]  ibid., pages 184-185.</p>
<p><a name="4"></a>[4]  The reciprocal function is its own inverse, 
which might seem to pose a problem in using Newton's method. 
Fortunately, it can be factored out so that it does not actually appear 
in the algorithm.</p>

<h3><font color="#000080">Further Reading</font></h3>

<p>Bjarne Stroustrup. <i>The C++ Programming Language, Third Edition</i> (Addison-Wesley, 1997).</p>
<p>Takuya Ooura. "A General Purpose FFT (Fast Fourier/Cosine/Sine Transform) Package", 1996-1999.</p>
<p>D. H. Bailey. "Multiprecision Translation and Execution of Fortran Programs", <i>ACM Transactions on Mathematical Software</i>, September 1993, p. 288-319.</p>
<p>Press, Teukolsky, Vetterling, and Flannery. <i>Numerical Recipes in C: The Art of Scientific Computing, Second Edition</i> (Cambridge University Press, 1988-1992).</p>
<p>William H. Beyer. <i>CRC Standard Mathematical Tables, 25th Edition</i> (CRC Press, 1978).</p>
<p>Johnson and Riess. <i>Numerical Analysis</i> (Addison-Wesley, 1977).</p>
<p>Rule, Finkenaur, and Patrick. <i>FORTRAN IV Programming</i> (Prindle, Weber, and Schmidt, 1973). </p>


<p><i><b>Michael C. Ring</b> has a B.S. in Electrical Engineering from 
the University of Minnesota. He is currently a Principal Systems 
Engineer at Honeywell working system design and test on inertial 
navigation systems. He can be reached at <b>michael.ring@honeywell.com</b>.</i></p>

</blockquote>
<br><h5>The views and opinions expressed in this page are strictly those of the page author.<br>The contents of this page have not been reviewed or approved by the University of Minnesota.</h5></body></html>