class BigInt
{
public:
	BigInt(int base = 10);
	BigInt(int &value, int base = 10);
	BigInt(long &value, int base = 10);
	BigInt(long long &value, int base = 10);
	
	void SetValue(BigInt &a, int base = 10);
	void SetValue(int &a, int base = 10);
	void SetValue(long &a, int base = 10);
	void SetValue(long long &a, int base = 10);
	
	BigInt operator=(BigInt &a);
	BigInt operator=(int &a);
	BigInt operator=(long &a);
	BigInt operator=(long long &a);
	
	BigInt operator+(BigInt &a);
	BigInt operator+(int &a);
	BigInt operator+(long &a);
	BigInt operator+(long long &a);
	
	BigInt operator-(BigInt &a);
	BigInt operator-(int &a);
	BigInt operator-(long &a);
	BigInt operator-(long long &a);
	
	BigInt operator*(BigInt &a);
	BigInt operator*(int &a);
	BigInt operator*(long &a);
	BigInt operator*(long long &a);
	
	BigInt operator/(BigInt &a);
	BigInt operator/(int &a);
	BigInt operator/(long &a);
	BigInt operator/(long long &a);
	
	BigInt operator%(BigInt &a);
	BigInt operator%(int &a);
	BigInt operator%(long &a);
	BigInt operator%(long long &a);
	
	void ChangeBase(int newBase);
	
	//static functions
	static BigInt Pow(BigInt &base, BigInt &exponent);
	static BigInt Pow(BigInt &base, int &exponent);
	static BigInt Pow(BigInt &base, long &exponent);
	static BigInt Pow(BigInt &base, long long &exponent);
	
	static BigInt ModularExponentation(BigInt &base, BigInt &exponent, BigInt &modulus);
	static BigInt ModularExponentation(BigInt &base, int &exponent, BigInt &modulus);
	static BigInt ModularExponentation(BigInt &base, long &exponent, BigInt &modulus);
	static BigInt ModularExponentation(BigInt &base, long long &exponent, BigInt &modulus);
	
	static BigInt RandomPrime(int bits, int base = 10);
	
	static bool IsPrime(BigInt &value); //Miller-Rabin, 30 passes --> 4^30 ~= 10^18 numbers with one false true
private:
	int base0, digits0;
	char *value0;
	bool *binary0;
};
