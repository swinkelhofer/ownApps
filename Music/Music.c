#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include "bass.h"
#include "bass_fx.h"


/*typedef HSTREAM (*BASS_StreamCreateFile)(BOOL, void *, QWORD, QWORD, DWORD);
typedef BOOL (*BASS_Init)(int, DWORD, DWORD, HWND, GUID *);
typedef BOOL (*BASS_ChannelPlay)(DWORD, BOOL);
typedef BOOL (*BASS_ChannelPause)(DWORD, BOOL);
typedef BOOL (*BASS_ChannelSetAttribute)(DWORD, DWORD, float);*/

void CALLBACK SwapDSP(HDSP handle, DWORD channel, void *buffer, DWORD length, void *user)
{
    //short *s=(short*)buffer;
    //for (; length; length-=4, s+=2) {
    //    short temp=s[0];
    //    s[0]=0;
     //   s[1]=temp;
    //}
	int *s = (int*) buffer;
	for(int i = length - 20; i >= 0; i--)
	{
		s[length - 20 - i] = s[length - 20 - (i >> 1)];
	}
}


int main(int argc, char *argv[])
{
	/*HMODULE hmod = LoadLibrary(TEXT("bass.dll"));
	if(hmod == NULL)
	{
		return 0;
	}
	BASS_StreamCreateFile StreamCreateFile = (BASS_StreamCreateFile) GetProcAddress(hmod, "BASS_StreamCreateFile");
	BASS_Init Init = (BASS_Init) GetProcAddress(hmod, "BASS_Init");
	BASS_ChannelPlay ChannelPlay = (BASS_ChannelPlay) GetProcAddress(hmod, "BASS_ChannelPlay");
	//BASS_ChannelPause ChannelPause = (BASS_ChannelPause) GetProcAddress(hmod, "BASS_ChannelPause");
	//BASS_ChannelSetAttribute ChannelSetAttribute = (BASS_ChannelSetAttribute) GetProcAddress(hmod, "BASS_ChannelSetAttribute");
	
	if(StreamCreateFile == NULL || Init == NULL || ChannelPlay == NULL)// || ChannelPause == NULL)
	{
		printf("Cannot load Functions from Lib");
		return 0;
	}*/
	if (!BASS_Init(-1, 44100, BASS_DEVICE_FREQ, 0, NULL))
		printf("Can't initialize device\n");
		/*
	BASS_DX8_REVERB dist;
	dist.fInGain = 0;
    dist.fReverbMix = 0;
    dist.fReverbTime = 3000;
    dist.fHighFreqRTRatio = 0.001;
	*/
	
	
	HSTREAM str, str2;
	if(!(str = BASS_StreamCreateFile(FALSE, argv[1], 0, 0, BASS_STREAM_DECODE)) || !(str2 = BASS_StreamCreateFile(FALSE, argv[2], 0, 0, 0)))
		printf("Can't open stream\n");
	//if(!BASS_ChannelSetAttribute(str, BASS_ATTRIB_MUSIC_SPEED,250))
	//	printf("Error in setting Volume\n");
	
	BASS_PluginLoad("bass_fx.dll", 0);
	HSTREAM tempo = BASS_FX_TempoCreate(str,0);

	if(!BASS_ChannelPlay(tempo, FALSE)) // play the stream (continue from current position)
		printf("Can't play stream\n");
	
	if(!BASS_ChannelSetAttribute(tempo, BASS_ATTRIB_TEMPO, -10.0f))
		printf("no tempo change\n");
	//BASS_ChannelSetDSP(str, &SwapDSP, NULL, 10);


	while(true)
	{
		float value;
		char get = getchar();
		switch(get)
		{
			case 'p':	BASS_ChannelPause(tempo);
						break;
			case 'q':	BASS_ChannelPause(str2);
						break;
			case 'o':	BASS_ChannelPlay(tempo, FALSE);
						break;
			case 'w':	BASS_ChannelPlay(str2, FALSE);
						break;
			case 'i':	BASS_ChannelPlay(tempo, TRUE);
						break;
			case 'e':	BASS_ChannelPlay(str2, TRUE);
						break;
			case '+':	BASS_ChannelGetAttribute(tempo, BASS_ATTRIB_VOL, &value);
						if(value <= 0.9)
							value += 0.1;
						BASS_ChannelSetAttribute(tempo, BASS_ATTRIB_VOL, value);
						break;
			case '-':	BASS_ChannelGetAttribute(tempo, BASS_ATTRIB_VOL, &value);
						if(value >= 0.1)
							value -= 0.1;
						BASS_ChannelSetAttribute(tempo, BASS_ATTRIB_VOL, value);
						break;
			
			case 'y':	BASS_ChannelGetAttribute(str2, BASS_ATTRIB_VOL, &value);
						if(value <= 0.9)
							value += 0.1;
						BASS_ChannelSetAttribute(str2, BASS_ATTRIB_VOL, value);
						break;
			case 'x':	BASS_ChannelGetAttribute(str2, BASS_ATTRIB_VOL, &value);
						if(value >= 0.1)
							value -= 0.1;
						BASS_ChannelSetAttribute(str2, BASS_ATTRIB_VOL, value);
						break;
		}
	}
	return 0;
	
}