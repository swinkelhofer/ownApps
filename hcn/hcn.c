#include <stdio.h>
#include <stdlib.h>

int divisors(int n)
{
	int divisors = 1;
	while(n & 0x1 == 0)
	{
		++divisors;
		n >>= 1;
	}
	for(int i = 3; i <= n; ++i)
		if(n % i == 0)
			++divisors;
	return divisors;
}

int main()
{
	int i = 0;
	int divisorCount = 0;
	int divs = 0;
	while(true)
	{
		i += 2;
		divisorCount = divisors(i);
		if(divisorCount > divs)
		{
			divs = divisorCount;
			printf("%d, ", i);
		}
	}
	return 0;
}