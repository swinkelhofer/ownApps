/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/

#include "BigInt.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <iostream>

using namespace std;


/*################################################
			Forward Definitions
################################################*/

uint32 pow_2[] =  {0x00000001, 0x00000002, 0x00000004, 0x00000008, 0x00000010, 0x00000020, 0x00000040, 0x00000080,
	0x00000100, 0x00000200, 0x00000400, 0x00000800, 0x00001000, 0x00002000, 0x00004000, 0x00008000,
	0x00010000, 0x00020000, 0x00040000, 0x00080000, 0x00100000, 0x00200000, 0x00400000, 0x00800000,
	0x01000000, 0x02000000, 0x04000000, 0x08000000, 0x10000000, 0x20000000, 0x40000000, 0x80000000};
const BigInt *null = new BigInt(0);
const BigInt *one = new BigInt(1);
const BigInt *two = new BigInt(2);

BigInt a1, a2, ret, m, x, b;
uint32 temp;
int temp1;


/*################################################
			Constructors + Settings
################################################*/
BigInt::BigInt()
{
	*this = *null;
}

BigInt::BigInt(uint32 a)
{
	/*for(int i = 0; i < INTCOUNT_1; i+=2)
	{
		this->bigint[i] = this->bigint[i+1] = 0;
	}*/
	memset(&this->bigint, 0, INTCOUNT_1x4);
	this->bigint[INTCOUNT_1] = a;
	this->length = (int)(a != 0);
}/*################################################
			Arithmetic Operators
################################################*/
BigInt BigInt::operator+(BigInt a)
{
	register uint64 carry = 0;
	register int temp1 = (this->length > a.length ? this->length : a.length);
	
	for(int i = INTCOUNT_1; i >= INTCOUNT_1 - temp1; --i)
	{
		carry = (uint64)this->bigint[i] + (uint64)a.bigint[i] + (carry >> 32);
		ret.bigint[i] = (uint32) carry;
	}
	if(ret.bigint[INTCOUNT_1 - (ret.length = temp1)] != 0)
		++ret.length;
	return ret;
}

BigInt BigInt::operator-(BigInt a)
{
	if(a >= *this)
		return *null;
	ret = a;
	register uint64 carry = 0x100000000ULL;
	for(register int i = INTCOUNT_1;  i >= INTCOUNT_1 - this->length; --i)
	{
		ret.bigint[i] = (uint32) (carry = (uint64)this->bigint[i] + (uint64)~ret.bigint[i] + (carry >> 32));
	}
	temp = INTCOUNT - this->length;
	while(!ret.bigint[temp] && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator*(BigInt a)
{
	BigInt *ret = new BigInt(0);
	uint64 digits;
	register int thislength = this->length;
	register int alength = a.length;
	for(int i = 0; i < alength; i++)
	{
		digits = 0;
		for(int j = 0; j < thislength; j++)
		{
			digits = ret->bigint[INTCOUNT_1 - i - j] + (digits >> 32) + (uint64) a.bigint[INTCOUNT_1 - i] * (uint64) this->bigint[INTCOUNT_1 - j];
			ret->bigint[INTCOUNT_1 - i - j] = (uint32) digits;
		}
		ret->bigint[INTCOUNT_1 - i - thislength] = (digits >> 32);
	}
	if(!ret->bigint[INTCOUNT - (ret->length = alength + thislength)])
		--ret->length;
	return *ret;
}

BigInt BigInt::operator/(BigInt a)
{
	BigInt *ret = new BigInt(0);
	ret->length = 1;
	if(a > *this)
		return *null;
	a1 = *this;
	int temp = ((this->length - a.length + 1) * 32);
	a <<= temp;
	for(; temp > 0; --temp)
	{
		a.shiftOneRightEq();
		ret->shiftOneLeftEq();
		if(a1 >= a)
		{
			a1 -= a;
			ret->bigint[INTCOUNT_1] += 1;
		}
	}
	return *ret;
}

BigInt BigInt::operator%(BigInt b)
{
	if(*this < b)
		return *this;
	BigInt a = *this, temp, temp2;
	register uint32 factor, pos, l;
	register int blength = b.length;
	while(a >= b)
	{
		for(int i = INTCOUNT - blength; i < INTCOUNT; ++i)
		{
			temp.bigint[i - a.length + blength] = temp2.bigint[i - a.length + blength] = b.bigint[i];
		}
		pos = INTCOUNT - (temp.length = temp2.length = a.length);
		if(a < temp)
		{
			for(int i = pos + blength; i > pos; --i)
			{
				temp.bigint[i] = temp.bigint[i-1];
				temp2.bigint[i] = temp2.bigint[i-1];
			}
			temp.bigint[pos] = temp2.bigint[pos] = 0;
			--temp.length;
			--temp2.length;
		}
		l = 0;
		factor = a.bigint[pos];
		while(factor < 0x80000000)
		{
			factor <<= 1;
			++l;
		}
		factor = (uint32)((long double)(((uint64) factor << 32) + ((uint64) a.bigint[pos+1] << l) + ((uint64) a.bigint[pos+2] >> (32-l))) / (long double)(((uint64) temp.bigint[pos] << (32 + l)) + ((uint64) temp.bigint[pos+1] << l) + ((uint64) temp.bigint[pos+2] >> (32-l))));
		
		temp.multEq(factor);
		while(temp > a)
			temp.subEq(temp2, blength);
		a.subEq(temp, blength);
	}
	return a;
}

BigInt BigInt::operator++()
{
	if(this->bigint[INTCOUNT_1] < 0xFFFFFFFF)
	{
		++this->bigint[INTCOUNT_1];
		return *this;
	}
	return (*this += *one);
}

BigInt BigInt::operator++(int)
{
	BigInt ret = *this;
	if(this->bigint[INTCOUNT_1] < 0xFFFFFFFF)
	{
		++this->bigint[INTCOUNT_1];
		return ret;
	}
	*this += *one;
	return ret;
}

BigInt BigInt::operator--()
{
	if(this->bigint[INTCOUNT_1] > 0x1)
	{
		--this->bigint[INTCOUNT_1];
		return *this;
	}
	else
		return (*this -= *one);
}

BigInt BigInt::operator--(int)
{
	BigInt ret = *this;
	if(this->bigint[INTCOUNT_1] > 0x1)
	{
		--this->bigint[INTCOUNT_1];
		return *this;
	}
	else
	{
		*this -= *one;
		return ret;
	}
}

BigInt BigInt::exponentation(BigInt base, BigInt exponent)		//ret = b ^ e
{
	BigInt b = base;
	BigInt e = exponent;
	BigInt *back = new BigInt(1);
	while(e.length != 0)
	{
		while(!(e.bigint[INTCOUNT_1] & 0x1))
		{
			e >>= 1;
			b *= b;
		}
		e -= *one;
		*back *= b;
	}
	return *back;
}

BigInt BigInt::modExponentation(BigInt base, BigInt exponent, BigInt modulus)			//ret = (b ^ e) % modulus
{
	BigInt b = base; 
	BigInt e = exponent;
	BigInt *back = new BigInt(1);
	while(e.length != 0)
	{
		while(!(e.bigint[INTCOUNT_1] & 0x1))
		{
			e.shiftOneRightEq();
			b.squareEq();
			b %= modulus;
		}
		--e;
		*back *= b;
		*back %= modulus;
	}
	return *back;	
}

/*################################################
			Bitwise Operators
################################################*/
BigInt BigInt::operator<<(int shift)
{
	ret = *this;
	register int div = shift >> 5;
	shift &= 0x1f;
	register int i = INTCOUNT - ret.length - div;
	if(div)
	{
		for(; i < INTCOUNT - div; ++i)
			ret.bigint[i] = ret.bigint[i + div];
		while(i < INTCOUNT)
		{
			ret.bigint[i] = 0;
			++i;
		}
	}
	ret.length += div;
	if(shift)
	{
		for(int i = INTCOUNT_1 - ret.length; i < INTCOUNT_1; i++)
			ret.bigint[i] = (ret.bigint[i] << shift) + (ret.bigint[i+1] >> (32-shift));
		ret.bigint[INTCOUNT_1] <<= shift;
	}
	if(ret.bigint[INTCOUNT_1 - ret.length])
		ret.length++;
	return ret;
}

BigInt BigInt::operator>>(int shift)
{
	ret = *this;
	register int div = shift >> 5;
	shift &= 0x1f;
	if(!div && !shift)
		return ret;
	
	if(div > ret.length)
		return *null;
	register int h = INTCOUNT - ret.length + div;
	if(div)
	{
		for(int i = INTCOUNT_1; i >= h; --i)
		{
			ret.bigint[i] = ret.bigint[i - div];
		}
	
		for(int i = h - div; i < h; ++i)
		{
			ret.bigint[i] = 0;
		}
	}
	if(!(ret.length -= div))
		return *null;
	if(shift)
	{
		for(int i = INTCOUNT_1; i >= INTCOUNT - ret.length + 1; --i)
		{
			ret.bigint[i] = (ret.bigint[i] >> shift) + (ret.bigint[i-1] << (32-shift));
		}
		ret.bigint[INTCOUNT - ret.length] >>= shift;
	}
	if(!ret.bigint[INTCOUNT - ret.length])
		--ret.length;
	return ret;
}

BigInt BigInt::operator|(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] | a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator&(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] & a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator^(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] ^ a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator~()
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = ~this->bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}


/*################################################
			Comparison Operators
################################################*/
bool BigInt::operator<(BigInt a)
{
	if(this->length != a.length)
	{
		if(this->length < a.length)
			return true;
		else// if(this->length > a.length)
			return false;
	}
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] < a.bigint[i])
				return true;
			else if(this->bigint[i] > a.bigint[i])
				return false;
		}
		return false;
	}
}

bool BigInt::operator<=(BigInt a)
{
	if(this->length != a.length)
	{
		if(this->length < a.length)
			return true;
		else// if(this->length > a.length)
			return false;
	}
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] < a.bigint[i])
				return true;
			else if(this->bigint[i] > a.bigint[i])
				return false;
		}
		return true;
	}
}

bool BigInt::operator>(BigInt a)
{
	if(this->length != a.length)
	{
		if(this->length > a.length)
			return true;
		else// if(this->length < a.length)
			return false;
	}
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] > a.bigint[i])
				return true;
			else if(this->bigint[i] < a.bigint[i])
				return false;
		}
		return false;
	}
}

bool BigInt::operator>=(BigInt a)
{
	if(this->length != a.length)
	{
		if(this->length > a.length)
			return true;
		else// if(this->length < a.length)
			return false;
	}
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] > a.bigint[i])
				return true;
			else if(this->bigint[i] < a.bigint[i])
				return false;
		}
		return true;
	}
}

bool BigInt::operator==(BigInt a)
{
	/*if(this->length != a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] != a.bigint[i])
				return false;
		}
		return true;
	}*/
	if(!memcmp(&this->bigint, &a.bigint, INTCOUNTx4) && this->length == a.length)
		return true;
	else
		return false;
}

bool BigInt::operator!=(BigInt a)
{
	/*if(this->length != a.length)
		return true;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] != a.bigint[i])
				return true;
		}
		return false;
	}*/
	if(this->length != a.length || memcmp(&this->bigint, &a.bigint, INTCOUNTx4) == -1)
		return true;
	else
		return false;
}

/*################################################
			RSA relevant Methods
################################################*/
BigInt BigInt::random(BigInt max)
{
	register int maxlength = max.length;
	if(max.bigint[INTCOUNT - maxlength]-1 > 1)
		ret.bigint[INTCOUNT - maxlength] = (uint32) (urand() % (max.bigint[INTCOUNT - maxlength]-1))+1;
	else
		ret.bigint[INTCOUNT - maxlength] = 1;
	for(int i = INTCOUNT - maxlength + 1; i < INTCOUNT; ++i)
	{
		ret.bigint[i] = urand();
	}
	ret.length = maxlength;
	return ret;
}

bool BigInt::isMillerRabinPrime(BigInt n)
{
	if(n.length == 1 && n.bigint[INTCOUNT_1] == 2)
	{
		return true;
	}
	else if((n.length == 1 && n.bigint[INTCOUNT_1] == 1) || !(n.bigint[INTCOUNT_1] & 0x1))
	{
		return false;
	}
	bool ism = false;
	m = n.subOne();
	for(int i = 0; i < 30; ++i)
	{
		int l = 0;
		x = BigInt::random(m);
		while(!(m.bigint[INTCOUNT_1] & 0x1))
		{
			++l;
			m.shiftOneRightEq();
		}
		x.modExpEq(m,n);
		m = n.subOne();
		ism = false;
		if((x.length == 1 && x.bigint[INTCOUNT_1] == 1) || x == m)
		{
			continue;
		}
		for(; l > 1; --l)
		{
			x.squareEq();
			x %= n;
			if(x.length == 1 && x.bigint[INTCOUNT_1] == 0x1)
			{
				return false;
			}
			else if(x == m)
			{
				ism = true;
				break;
			}
		}
		if(!ism)
		{
			return false;
		}
	}
	return true;
}

BigInt BigInt::createPrime(int bits)
{
	--bits;
	BigInt ret;
	short i3, i5, i7, i11, j, b = 0;
	register uint32 tests = 0.7 * (bits << 1);
	while(true)
	{
		j = 0;
		for(int i = INTCOUNT_1; i >= INTCOUNT - (bits >> 5); --i)
		{
			ret.bigint[i] = urand();
		}
		if(!(ret.bigint[INTCOUNT_1] & 0x1))
			ret.bigint[INTCOUNT_1] ^= 0x1;
		if(bits & 0x1f != 0)
		{
			ret.bigint[INTCOUNT_1 - (bits >> 5)] = (urand() % (int) pow_2[bits & 0x1f]) + pow_2[bits & 0x1f];
			ret.length = (bits >> 5) + 1;
		}
		else
			ret.length = (bits >> 5);
		//cout << "start: switche" << endl;
		//cout << "start: mod3" << endl;
		switch((ret % BigInt(3)).bigint[INTCOUNT_1])
		{
			case 0:		i3 = 0;
						break;
			case 1:		i3 = 2;
						break;
			case 2:		i3 = 1;
						break;
			default:	exit(0);
		}
		//cout << "stop: mod3" << endl;
		//cout << "start: mod5" << endl;
		
		switch((ret % BigInt(5)).bigint[INTCOUNT_1])
		{
			case 0:		i5 = 0;
						break;
			case 1:		i5 = 3;
						break;
			case 2:		i5 = 1;
						break;
			case 3:		i5 = 4;
						break;
			case 4:		i5 = 2;
						break;
			default:	exit(0);
		}
		//cout << "stop: mod5" << endl;
		//cout << "start: mod7" << endl;
		
		switch((ret % BigInt(7)).bigint[INTCOUNT_1])
		{
			case 0:		i7 = 0;
						break;
			case 1:		i7 = 4;
						break;
			case 2:		i7 = 1;
						break;
			case 3:		i7 = 5;
						break;
			case 4:		i7 = 2;
						break;
			case 5:		i7 = 6;
						break;
			case 6:		i7 = 3;
						break;
			default:	exit(0);
		}
		//cout << "stop: mod7" << endl;
		//cout << "start: mod11" << endl;
		
		switch((ret % BigInt(11)).bigint[INTCOUNT_1])
		{
			case 0:		i11 = 0;
						break;
			case 1:		i11 = 6;
						break;
			case 2:		i11 = 1;
						break;
			case 3:		i11 = 7;
						break;
			case 4:		i11 = 2;
						break;
			case 5:		i11 = 8;
						break;
			case 6:		i11 = 3;
						break;
			case 7:		i11 = 9;
						break;
			case 8:		i11 = 4;
						break;
			case 9:		i11 = 10;
						break;
			case 10:	i11 = 5;
						break;
			default:	exit(0);
		}
		//cout << "stop: mod11" << endl;
		//cout << "stop: switche" << endl;
		while(true)
		{
			//cout << j << " ";
			if(++j >= tests)
			{
				++b;
				//cout << "exit wegen > tests";
				break;
				//exit(0);
			}
			if((i3 %= 3) == 0 || (i5 %= 5) == 0 || (i7 %= 7) == 0 || (i11 %= 11) == 0)
			{
				++i3;
				++i5;
				++i7;
				++i11;
				ret.bigint[INTCOUNT_1] += 2;
				continue;
			}
			if(BigInt::isMillerRabinPrime(ret))
			{
				cout.fill(' ');
				cout.width(3);
				cout << b*tests + j;
				return ret;
			}
			ret.bigint[INTCOUNT_1] += 2;
			++i3;
			++i5;
			++i7;
			++i11;
		}
	}
}

BigInt BigInt::createSavePrime(int bits)
{
	bits -= 2;
	BigInt ret;
	short i3, i5, i7, i11, j;
	register uint32 tests = 0.7 * (bits << 1);
	while(true)
	{
		j = 0;
		for(int i = INTCOUNT_1; i >= INTCOUNT - bits/32; --i)
		{
			ret.bigint[i] = urand();
		}
		if(!(ret.bigint[INTCOUNT_1] & 0x1))
			++ret.bigint[INTCOUNT_1];
		if(bits % 32 != 0)
		{
			ret.bigint[INTCOUNT_1 - bits/32] = (urand() % (int) pow_2[bits % 32]) + pow_2[bits % 32];
			ret.length = bits/32 + 1;
		}
		else
			ret.length = bits/32;
		switch((ret % BigInt(3)).bigint[INTCOUNT_1])
		{
			case 0:		i3 = 0;
						break;
			case 1:		i3 = 2;
						break;
			case 2:		i3 = 1;
						break;
			default:	exit(0);
		}
		
		switch((ret % BigInt(5)).bigint[INTCOUNT_1])
		{
			case 0:		i5 = 0;
						break;
			case 1:		i5 = 3;
						break;
			case 2:		i5 = 1;
						break;
			case 3:		i5 = 4;
						break;
			case 4:		i5 = 2;
						break;
			default:	exit(0);
		}
		
		switch((ret % BigInt(7)).bigint[INTCOUNT_1])
		{
			case 0:		i7 = 0;
						break;
			case 1:		i7 = 4;
						break;
			case 2:		i7 = 1;
						break;
			case 3:		i7 = 5;
						break;
			case 4:		i7 = 2;
						break;
			case 5:		i7 = 6;
						break;
			case 6:		i7 = 3;
						break;
			default:	exit(0);
		}
		
		switch((ret % BigInt(11)).bigint[INTCOUNT_1])
		{
			case 0:		i11 = 0;
						break;
			case 1:		i11 = 6;
						break;
			case 2:		i11 = 1;
						break;
			case 3:		i11 = 7;
						break;
			case 4:		i11 = 2;
						break;
			case 5:		i11 = 8;
						break;
			case 6:		i11 = 3;
						break;
			case 7:		i11 = 9;
						break;
			case 8:		i11 = 4;
						break;
			case 9:		i11 = 10;
						break;
			case 10:	i11 = 5;
						break;
			default:	exit(0);
		}
		while(true)
		{
			if(++j >= tests)
				break;
			
			if((i3 %= 3) == 0 || (i5 %= 5) == 0 || (i7 %= 7) == 0 || (i11 %= 11) == 0)
			{
				++i3;
				++i5;
				++i7;
				++i11;
				ret.bigint[INTCOUNT_1] += 2;
				continue;
			}
			if(BigInt::isMillerRabinPrime(ret))
			{
				if(BigInt::isMillerRabinPrime(((ret << 1) + *one)))
				{
					cout.fill(' ');
					cout.width(3);
					cout << j;
					return ((ret << 1) + *one);
				}
			}
			ret.bigint[INTCOUNT_1] += 2;
			++i3;
			++i5;
			++i7;
			++i11;
		}
	}
}

BigInt BigInt::euclidianAlgorithm(BigInt a, BigInt b)
{
	BigInt h;
	while(b != *null)
	{
		h = a % b;
		a = b;
		b = h;
	}
	return a;
}

BigInt BigInt::modInverse(BigInt a, BigInt modulus)
{
	BigInt mod = modulus;
	BigInt q,r;
	BigInt u=1;
	BigInt s=0;
	bool q_n = 0, r_n = 0, u_n = 0, s_n = 0, a_n = 0, mod_n = 0;
	while(modulus>0 && !mod_n)
	{
		q=a/modulus;
		q_n = a_n ^ mod_n;
		if(a >= q*modulus)
		{
			if(a_n ^ q_n ^ mod_n)
			{
				r = a + q*modulus;
				r_n = a_n;
			}
			else
			{
				r = a - q*modulus;
				r_n = q_n ^ mod_n;
			}
		}
		else
		{
			if(a_n ^ q_n ^ mod_n)
			{
				r = a + q*modulus;
				r_n = a_n;
			}
			else
			{
				r = q*modulus - a;
				r_n = !a_n;
			}
		}
		a=modulus;
		a_n = mod_n;
		modulus=r;
		mod_n = r_n;
		if(u >= q*s)
		{
			if(u_n ^ q_n ^ s_n)
			{
				r = u + q*s;
				r_n = u_n;
			}
			else
			{
				r = u - q*s;
				r_n = q_n ^ s_n;
			}
		}
		else
		{
			if(u_n ^ q_n ^ s_n)
			{
				r = u + q*s;
				r_n = u_n;
			}
			else
			{
				r = q*s - u;
				r_n = !u_n;
			}
		}
		u=s;
		u_n = s_n;
		s=r;
		s_n = r_n;
	}
	if(a != *one && !a_n)
		return *null;
	if(u_n)
		return (mod - u);
	else
		return u;
}

/*###############################################
			Conversion Methods
###############################################*/
void BigInt::printHex2Console()
{
	cout.fill('0');
	cout.unsetf(ios_base::dec);
	cout.setf(ios_base::hex | ios_base::uppercase | ios_base::fixed);
	for(int i = 0; i < INTCOUNT; i++)
	{
		if(i % 8 == 0)
			cout << "\n";
		cout.width(8);
		cout << this->bigint[i] << " ";
	}
	cout.unsetf(ios_base::hex);
	cout.setf(ios_base::dec);
}

string BigInt::toHex(bool uppercase)
{
	string hex = "";
	char temp;
	BigInt a = *this;
	while(a != *null)
	{
		switch(a.bigint[INTCOUNT_1] & 0xf)
		{
			case 0:		hex += '0';
						break;
			case 1:		hex += '1';
						break;
			case 2:		hex += '2';
						break;
			case 3:		hex += '3';
						break;
			case 4:		hex += '4';
						break;
			case 5:		hex += '5';
						break;
			case 6:		hex += '6';
						break;
			case 7:		hex += '7';
						break;
			case 8:		hex += '8';
						break;
			case 9:		hex += '9';
						break;
			case 10:	hex += 'A';
						break;
			case 11:	hex += 'B';
						break;
			case 12:	hex += 'C';
						break;
			case 13:	hex += 'D';
						break;
			case 14:	hex += 'E';
						break;
			case 15:	hex += 'F';
						break;
		}
		a >>= 4;
	}
	if(!uppercase)
	{
		for(int i = 0; i < (int) hex.length() / 2; i++)
		{
			temp = tolower(hex[i]);
			hex[i] = tolower(hex[hex.length() - 1 - i]);
			hex[hex.length() - 1 - i] = temp;
		}
	}
	else
	{
		for(int i = 0; i < (int) hex.length() / 2; i++)
		{
			temp = hex[i];
			hex[i] = hex[hex.length() - 1 - i];
			hex[hex.length() - 1 - i] = temp;
		}
	}
	if(hex == string(""))
		hex += '0';
	hex.insert(0,"0x");
	return hex;
}

string BigInt::toDec()
{
	string dec = "";
	char temp;
	BigInt ten(10), a = *this;
	while(a != *null)
	{
		switch((a % ten).bigint[INTCOUNT_1])
		{
			case 0:		dec += '0';
						break;
			case 1:		dec += '1';
						break;
			case 2:		dec += '2';
						break;
			case 3:		dec += '3';
						break;
			case 4:		dec += '4';
						break;
			case 5:		dec += '5';
						break;
			case 6:		dec += '6';
						break;
			case 7:		dec += '7';
						break;
			case 8:		dec += '8';
						break;
			case 9:		dec += '9';
						break;
		}
		a = a / ten;
	}
	for(int i = 0; i < (int) dec.length() / 2; i++)
	{
		temp = dec[i];
		dec[i] = dec[dec.length() - 1 - i];
		dec[dec.length() - 1 - i] = temp;
	}
	
	if(dec == string(""))
		dec += '0';
	return dec;
}

void BigInt::fromHex(char *hex)
{
	*this = *null;
	int hexlength = strlen(hex);
	int start = 0;
	int chiffre = 0;
	if(hex[0] == '0' && toupper(hex[1]) == 'X' && hex[2] == ' ')
		start = 2;
	else if(hex[0] == '0' && toupper(hex[1]) == 'X')
		start = 1;
	for(int i = start; i < hexlength; ++i)
	{
		switch(toupper(hex[i]))
		{
			case '0':	chiffre = 0;
						break;
			case '1':	chiffre = 1;
						break;
			case '2':	chiffre = 2;
						break;
			case '3':	chiffre = 3;
						break;
			case '4':	chiffre = 4;
						break;
			case '5':	chiffre = 5;
						break;
			case '6':	chiffre = 6;
						break;
			case '7':	chiffre = 7;
						break;
			case '8':	chiffre = 8;
						break;
			case '9':	chiffre = 9;
						break;
			case 'A':	chiffre = 10;
						break;
			case 'B':	chiffre = 11;
						break;
			case 'C':	chiffre = 12;
						break;
			case 'D':	chiffre = 13;
						break;
			case 'E':	chiffre = 14;
						break;
			case 'F':	chiffre = 15;
						break;
		}
		*this <<= 4;
		*this += BigInt(chiffre);
	}
}

void BigInt::fromDec(char *dec)
{
	int declength = strlen(dec);
	BigInt ten(10);
	uint32 chiffre = 0;
	for(int i = declength - 1; i >= 0; i--)
	{
		switch(dec[i])
		{
			case '0':	chiffre = 0;
						break;
			case '1':	chiffre = 1;
						break;
			case '2':	chiffre = 2;
						break;
			case '3':	chiffre = 3;
						break;
			case '4':	chiffre = 4;
						break;
			case '5':	chiffre = 5;
						break;
			case '6':	chiffre = 6;
						break;
			case '7':	chiffre = 7;
						break;
			case '8':	chiffre = 8;
						break;
			case '9':	chiffre = 9;
						break;
		}
		*this += BigInt(chiffre) * BigInt::exponentation(ten, BigInt((uint32) declength - 1 - i));
	}
}

/*################################################
			Private O= Operators
################################################*/
BigInt BigInt::operator<<=(int shift)
{
	register int div = shift >> 5;
	shift &= 0x1f;
	register int i = INTCOUNT - this->length - div;
	if(div)
	{
		for(; i < INTCOUNT-div; ++i)
			this->bigint[i] = this->bigint[i+div];
		while(i < INTCOUNT)
		{
			this->bigint[i] = 0;
			++i;
		}
	}
	this->length += div;
	if(shift)
	{
		for(i = INTCOUNT_1 - this->length; i < INTCOUNT_1; ++i)
			this->bigint[i] = (this->bigint[i] << shift) + (this->bigint[i+1] >> (32-shift));
		this->bigint[INTCOUNT_1] <<= shift;
	}
	if(this->bigint[INTCOUNT_1 - this->length])
		++this->length;
	return *this;
	/*if(shift < INTCOUNTx4)
	{
		BigInt a = *this;
		memcpy(&this->bigint, (void*)((int)&a.bigint + shift), INTCOUNTx4 - shift);
		memset((void*)((int)&this->bigint + INTCOUNTx4 - shift), 0, shift); 
		this->length += shift / 32;
		if(this->length >= INTCOUNT)
			this->length = INTCOUNT;
		if(!this->bigint[0])
			return (*this = *null);
		if(this->bigint[INTCOUNT_1 - this->length])
			++this->length;
		return *this;
	}
	else
		return (*this = *null);*/
}

BigInt BigInt::operator>>=(int shift)
{
	register int div = shift >> 5;
	shift &= 0x1f;
	register int i = INTCOUNT_1;
	if(div > this->length)
		return *null;
	register int h = INTCOUNT - this->length + div;
	if(div)
	{
		for(; i >= h; --i)
			this->bigint[i] = this->bigint[i - div];
		/*while(i >= (h - div))
		{
			this->bigint[i] = 0;
			--i;
		}*/
		memset((void*)((int)&this->bigint + h - div), 0, div);
	}
	if((this->length -= div) <= 0)
		return *null;
	if(shift)
	{
		for(h = INTCOUNT_1; h >= INTCOUNT - this->length + 1; --h)
			this->bigint[h] = (this->bigint[h] >> shift) + (this->bigint[h-1] << (32-shift));
		this->bigint[INTCOUNT - this->length] >>= shift;
	}
	if(!this->bigint[INTCOUNT - this->length])
		--this->length;
	return *this;
	/*if(shift < INTCOUNTx4)
	{
		BigInt a = *this;
		memcpy((void*)((int)&this->bigint + shift), &a.bigint, INTCOUNTx4 - shift);
		memset(&this->bigint, 0, shift); 
		this->length -= shift / 32;
		if(this->length <= 0)
			return (*this = *null);
		if(!this->bigint[INTCOUNT - this->length])
			--this->length;
		if(this->length <= 0)
			return (*this = *null);
		return *this;
	}
	else
		return (*this = *null);*/
}

BigInt BigInt::operator+=(BigInt a)
{
	register int newlength = this->length > a.length ? this->length : a.length;
	register uint64 carry = 0;
	for(register int i = INTCOUNT_1; i >= INTCOUNT_1 - newlength; --i)
	{
		carry = (uint64)this->bigint[i] + (uint64)a.bigint[i] + (carry >> 32);
		this->bigint[i] = (uint32) carry;
	}
	if(this->bigint[INTCOUNT_1 - (this->length = newlength)] != 0)
		++this->length;
	return *this;
}

BigInt BigInt::operator-=(BigInt a)
{
	if(a >= *this)
	{
		*this = *null;
		return *this;
	}
	register uint64 carry = 0x100000000ULL;
	for(register int i = INTCOUNT_1;  i >= INTCOUNT_1 - this->length; --i)
	{
		this->bigint[i] = (uint32) (carry = (uint64)this->bigint[i] + (uint64)~a.bigint[i] + (carry >> 32));
	}
	temp = INTCOUNT - this->length;
	while(!this->bigint[temp] && temp < INTCOUNT)
		++temp;
	this->length = INTCOUNT - temp;
	return *this;
}

BigInt BigInt::operator*=(BigInt a)
{
	b.length = this->length;
	for(int i = INTCOUNT_1 - this->length; i < INTCOUNT; ++i)
	{
		b.bigint[i] = this->bigint[i];
		this->bigint[i] = 0;
	}
	register uint64 digits;
	register int i2;
	for(int i = INTCOUNT_1; i >= INTCOUNT - b.length; --i)
	{
		digits = 0;
		for(int j = 0; j < a.length; ++j)
		{
			i2 = i - j;
			this->bigint[i2] = (uint32) (digits = this->bigint[i2] + (digits >> 32) + (uint64) b.bigint[i] * (uint64) a.bigint[INTCOUNT_1 - j]);
		}
		this->bigint[i - a.length] = (digits >> 32);
	}
	if(!this->bigint[INTCOUNT - (this->length = a.length + b.length)])
		--this->length;
	return *this;
}

BigInt BigInt::operator/=(BigInt a)
{
	BigInt *ret = new BigInt(0);
	ret->length = 1;
	if(a > *this)
		return *null;
	a1 = *this;
	int temp = ((this->length - a.length + 1) * 32);
	a <<= temp;
	for(; temp > 0; --temp)
	{
		a.shiftOneRightEq();
		ret->shiftOneLeftEq();
		if(a1 >= a)
		{
			a1 -= a;
			ret->bigint[INTCOUNT_1] += 1;
		}
	}
	*this = *ret;
	return *this;
}

BigInt BigInt::operator%=(BigInt b)
{
	if(*this < b)
		return *this;
	BigInt temp, temp2;
	register uint32 factor, pos, l;
	//register uint64 carry;
	register int blength = b.length;
	while(*this >= b)
	{
		factor = this->length - blength;
		/*for(int i = INTCOUNT - this->length; i < INTCOUNT - factor; ++i)
		{
			temp.bigint[i] = b.bigint[i+factor];
		}*/
		//memcpy((void*)((int)&temp.bigint+((INTCOUNT - this->length)<<2)), (void*)((int)&b.bigint + ((INTCOUNT - blength)<<2)), blength<<2);
		memcpy((void*)((int)&temp.bigint), (void*)((int)&b.bigint+(factor<<2)), (INTCOUNT - factor)<<2);
		pos = INTCOUNT - (temp.length = this->length);
		if(*this < temp)
		{
			for(int i = INTCOUNT - factor; i > pos; --i)
				temp.bigint[i] = temp.bigint[i-1];
			temp.bigint[pos] = 0;
			--temp.length;
		}
		l = 0;
		factor = this->bigint[pos];
		while(factor < 0x80000000)
		{
			factor <<= 1;
			++l;
		}
		//memcpy((void*)&temp2.bigint, (void*)&temp.bigint, INTCOUNTx4);
		//temp2.length = temp.length;
		//printf("%8x\t%8x\n", ((temp.bigint[pos] << l) + (temp.bigint[pos+1] >> (32 - l))), 
		//((((uint64) temp.bigint[pos] << (32 + l)) + ((uint64) temp.bigint[pos+1] << l) + ((uint64) temp.bigint[pos+2] >> (32-l))) >> 32));
		//printf("%x\t%8x\t%8x\n", factor, factor + (uint32)(((uint64)this->bigint[pos+1] >> (32-l))), 
		//((((uint64) factor << 32) + ((uint64) this->bigint[pos+1] << l) + ((uint64) this->bigint[pos+2] >> (32-l))) >> 32));
		//printf("%u\t%u\n", (uint32)((factor + (uint32)((uint64)this->bigint[pos+1] >> (32 - l))) / ((uint32)(temp.bigint[pos] << l) + (uint32)((uint64)temp.bigint[pos+1] >> (32 - l)))),
		//(uint32)((((uint64) factor << 32) + ((uint64) this->bigint[pos+1] << l) + ((uint64) this->bigint[pos+2] >> (32-l))) / (((uint64) temp.bigint[pos] << (32 + l)) + ((uint64) temp.bigint[pos+1] << l) + ((uint64) temp.bigint[pos+2] >> (32-l)))));
		
		temp2 = temp;
		temp.multEq(((((uint64) factor << 32) + ((uint64) this->bigint[pos+1] << l) + ((uint64) this->bigint[pos+2] >> (32-l))) / (((uint64) temp.bigint[pos] << (32 + l)) + ((uint64) temp.bigint[pos+1] << l) + ((uint64) temp.bigint[pos+2] >> (32-l)))));
		
		if(temp > *this)
			temp.subEq(temp2, blength);
		this->subEq(temp, blength);
	}
	return *this;
	
}

/*################################################
			Private Operators
################################################*/
void BigInt::multEq(uint64 a)
{
	uint64 carry = 0;
	for(int i = INTCOUNT_1; i >= INTCOUNT - this->length; --i)
	{
		this->bigint[i] = (uint32) (carry = (uint64) this->bigint[i] * a + (carry >> 32));
	}
	if(carry >>= 32)
	{
		this->bigint[INTCOUNT_1 - this->length] = (uint32)carry;
		++this->length;
	}
}	

void BigInt::subEq(BigInt a, int len)	//fast sub for numbers with large null-ending blocks (len)
{
	register uint64 carry = 0x100000000ULL;
	register int thislength = this->length;
	for(int i = INTCOUNT - thislength + len;  i >= INTCOUNT_1 - thislength; --i)
	{
		this->bigint[i] = (uint32)(carry = (uint64)this->bigint[i] + (uint64)~a.bigint[i] + (carry >> 32));
	}
	thislength = INTCOUNT - thislength;
	while(!this->bigint[thislength] && thislength < INTCOUNT)
		++thislength;
	this->length = INTCOUNT - thislength;
}

void BigInt::squareEq()
{
	for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
	{
		b.bigint[i] = this->bigint[i];
		this->bigint[i] = 0;
	}
	b.length = this->length;
	register uint64 digits, digits1, digits2, carry;
	for(int i = INTCOUNT_1; i >= INTCOUNT - b.length; --i)
	{
		carry = 0;
		for(int j = 0; j < INTCOUNT_1 - i; ++j)
		{
			this->bigint[i - j] = (uint32)((digits1 = (uint64) this->bigint[i - j] + carry) + ((digits2 = (uint64) b.bigint[INTCOUNT_1 - j] * (uint64) b.bigint[i]) << 1));
			carry = (digits2 + (digits1 >> 1)) >> 31;
		}
		this->bigint[i + i - INTCOUNT_1] = (uint32) (digits = (uint64) b.bigint[i] * (uint64) b.bigint[i] + carry);
		this->bigint[i + i - INTCOUNT] = (uint32)(digits >> 32);
	}
	if(!this->bigint[INTCOUNT - (this->length = (b.length + b.length))])
		--this->length;
}

void BigInt::shiftOneLeftEq()
{
	for(int i = INTCOUNT_1 - this->length; i < INTCOUNT_1; ++i)
		this->bigint[i] = (this->bigint[i] << 1) + (this->bigint[i+1] > 0x7FFFFFFF);
	this->bigint[INTCOUNT_1] <<= 1;
	if(this->bigint[INTCOUNT_1 - this->length])
		++this->length;
}

void BigInt::shiftOneRightEq()
{
	for(int i = INTCOUNT_1; i >= INTCOUNT - this->length; --i)
		this->bigint[i] = (this->bigint[i] >> 1) + (this->bigint[i-1] << 31);
	if(!this->bigint[INTCOUNT - this->length])
		--this->length;
}

BigInt BigInt::subOne()
{
	if(this->bigint[INTCOUNT_1] > 0x1)
	{
		ret = *this;
		--ret.bigint[INTCOUNT_1];
		return ret;
	}
	else
		return (*this - *one);
}

void BigInt::modExpEq(BigInt exponent, BigInt modulus)			//this = (b ^ e) % modulus
{
	BigInt b = *this;
	*this = *one;
	while(exponent.length)
	{
		while(!(exponent.bigint[INTCOUNT_1] & 0x1))
		{
			exponent.shiftOneRightEq();
			b.squareEq();
			b %= modulus;
		}
		--exponent;
		*this *= b;
		*this %= modulus;
	}
}