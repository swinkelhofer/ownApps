/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/

#define BITLENGTH 1280
#define INTCOUNT BITLENGTH / 32

class BigInt
{
public:

	/*################################################
				Constructors + Settings
	################################################*/
	BigInt();
	BigInt(int a);
	
	
	/*################################################
				Arithmetic Operators
	################################################*/
	BigInt operator+(BigInt a);
	BigInt operator-(BigInt a);
	BigInt operator*(BigInt a);
	BigInt operator%(BigInt a);
	
	static BigInt exponentation(BigInt base, BigInt exponent);
	static BigInt modExponentation(BigInt base, BigInt exponent, BigInt modulus);
	
	
	/*################################################
				Bitwise Operators
	################################################*/
	BigInt operator|(BigInt a);
	BigInt operator&(BigInt a);
	BigInt operator^(BigInt a);
	BigInt operator~();
	BigInt operator<<(int shift);
	BigInt operator>>(int shift);
	
	
	/*################################################
				Comparison Operators
	################################################*/
	bool operator<(BigInt a);
	bool operator<=(BigInt a);
	bool operator>(BigInt a);
	bool operator>=(BigInt a);
	bool operator==(BigInt a);
	bool operator!=(BigInt a);
	
	
	/*################################################
				RSA relevant Methods
	################################################*/
	static BigInt random(BigInt max);
	static bool isMillerRabinPrime(BigInt n);
	static BigInt createPrime(int bits);
	
	
	unsigned int bigint[INTCOUNT];
private:
	/*###############################################
				Private Attributes
	###############################################*/
	int length;
};