#include "BigInt.h"
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <windows.h>
using namespace std;
extern int tries_count;
#define endl "\n"

int main(int argc, char *argv[])
{
	cout.unsetf(ios_base::dec);
	cout.setf(ios_base::dec | ios_base::uppercase | ios_base::fixed);
	//cout.fill('0');
	BigInt big;
	
	//cout << (int)((a & (unsigned int)pow(2,31))!=0); //Maskieren eines einzelnen Bits!!!!!!*/
	//BigInt a(atoi(argv[1])),b, one(1),two(2), null(0);
	/*BigInt f;
	for(int i = 0; i < 32; i++)
		f.bigint[i] = 0xFFFFFFFF;
	f.length = 32;
	a.length = 1;
	b.length = 1;
	a.bigint[31] = 0x25;
	//a.bigint[30] = 0xFAAF0495;
	b.bigint[31] = 0x9;
	//b.bigint[30] = 0x0;
	//a = BigInt::modExponentation(a,b,f);
	a = BigInt::random(f);
	//a = a % b;
	//a = a % two;
	//a = a >> 1;
	for(int i = 0; i < 32; i++)
	{
		cout.width(8);
		cout << a.bigint[i] << " ";
	}
	*/
	//BigInt one(1);
	time_t start,stop;
	//BigInt a;
	//cout << BigInt::random(two).bigint[31] << "\n";
	//cout << (BigInt(321) % BigInt(27)).bigint[31]<<"\n";

	//BigInt a = BigInt::exponentation(BigInt(2), BigInt(521))-1;
	//BigInt b = BigInt::exponentation(BigInt(2), BigInt(521))-14;
	//a = 9403;
	//b = 2349;
	//BigInt a = BigInt(180) * (BigInt::exponentation(BigInt(2), BigInt(127)) - BigInt(1)) * (BigInt::exponentation(BigInt(2), BigInt(127)) - BigInt(1)) + BigInt(1);
	//a = (BigInt(180) * BigInt::exponentation(BigInt::exponentation(2,127) - one , 2)) + 1;
	//cout << a.toHex();
	//big.fromHex(argv[1]);
	/*start = clock();
	BigInt a = BigInt::createPrime(atoi(argv[1]));
	stop = clock();
	cout << a.toHex();*/
	/*big = a.karatsuba(b);
	cout << "\nKaratsuba:\n";
	big.printHex2Console();
	cout << "\nNormal:\n";
	big = a*b;
	
	big.printHex2Console();*/
	//cout << "\n\n";
	//cout << a.toHex();
	//stop = clock();
	/*for(int i = 0; i < INTCOUNT; i++)
	{
		cout.width(8);
		cout << a.bigint[i] << " ";
	}*/
	/*a.printHex2Console();
	start = clock();
	if(BigInt::isMillerRabinPrime(a))
		cout << "\nprim";
	else
		cout << "\nzusammengesetzt";
	stop = clock();
	cout.unsetf(ios_base::hex);
	cout << "\n" << stop - start << " Milliseconds";*/
	//BigInt *a(37);
	/*for(int i = 0x1; i < 0x64; i++)
	{
		//BigInt a = 37;
		cout << (2*i+1) << ":\t";
		//a.printHex2Console();
		if(BigInt::isMillerRabinPrime(2*i+1))
			cout << "prim\n";
		else
			cout << "zusammengesetzt\n";
	}
	*/
	//BigInt a = 15015;
	//BigInt b = 95095;
	//cout << (a/b).toDec();
	usrand(time(0));
	/*a = BigInt::exponentation(2,1024)-1;
	BigInt b = BigInt::exponentation(2,1024) - 19203;
	
	for(int i = 0; i < 100000;i++)
		a * b;
	
	
	cout << stop-start << " ms\n";	*/
	/*a = BigInt::exponentation(2,1024)-1;
	//BigInt b= BigInt::exponentation(2,1024)-14903;
	BigInt c = BigInt::exponentation(2, 1024) / 3;
	start = clock();
	int fail= 0;
	for(int i= 0; i < 10000; i++)
	{
		a = BigInt::random(a);
		c = BigInt::random(c);
		if(a.divide(c) != (a/c))
		{
			fail++;
		}
	}
	stop = clock();
	cout << fail << " "  <<stop-start << " ms\n";
	
	start = clock();*/
	/*for(int i= 0; i < 10000; i++)
	{
		a.squareMod(c);
	}
	stop = clock();
	cout << stop-start << " ms\n";*/
	/*
	a = BigInt::exponentation(BigInt(2), BigInt(128))-1;
	start = clock();
	for(int i = 0; i < 100000; ++i)
		a*a;
	stop = clock();
	cout << "128:\t" << stop - start << " ms\n";
	
	a = BigInt::exponentation(BigInt(2), BigInt(256))-1;
	start = clock();
	for(int i = 0; i < 100000; ++i)
		a*a;
	stop = clock();
	cout << "256:\t" << stop - start << " ms\n";
	
	a = BigInt::exponentation(BigInt(2), BigInt(512))-1;
	start = clock();
	for(int i = 0; i < 100000; ++i)
		a*a;
	stop = clock();
	cout << "512:\t" << stop - start << " ms\n";
	
	
	a = BigInt::exponentation(BigInt(2), BigInt(1024))-1;
	start = clock();
	for(int i = 0; i < 100000; ++i)
		a*a;
	stop = clock();
	cout << "1024:\t" << stop - start << " ms\n";
	
	a = BigInt::exponentation(BigInt(2), BigInt(2048))-1;
	start = clock();
	for(int i = 0; i < 100000; ++i)
		a*a;
	stop = clock();
	cout << "2048:\t" << stop - start << " ms\n";
	
	
	a = BigInt::exponentation(BigInt(2), BigInt(1024))-1;
	start = clock();
	for(int i = 0; i < 100000; ++i)
		a.square();
	stop = clock();
	cout << "sq1024:\t" << stop - start << " ms\n";
	
	a = BigInt::exponentation(BigInt(2), BigInt(2048))-1;
	start = clock();
	for(int i = 0; i < 100000; ++i)
		a.square();
	stop = clock();
	cout << "sq2048:\t" << stop - start << " ms\n";
	/*a = BigInt::exponentation(BigInt(2), BigInt(4096))-1;
	start = clock();
	for(int i = 0; i < 100000; ++i)
		a*a;
	stop = clock();
	cout << "4096:\t" << stop - start << " ms\n";*/
	
	/*BigInt kilo = BigInt::exponentation(2,1024);
	BigInt kilo1 = BigInt::random(kilo);
	kilo = BigInt::exponentation(2,512);
	BigInt kilo2 = BigInt::random(kilo);
	start = clock();
	for(int i = 0; i< 100000; ++i)
	{
		(kilo1%kilo2);
	}
	stop = clock();
	cout << "Bench:\t" << stop - start << " ms\n";*/
	
	//if(a*a != a*a)
	//	cout << "QUADRATIC ERROR!!!!!!\n";
	//cout << "a*a:\t" << (a*a).toHex() << "\n";
	//cout << "a.sqr:\t" << a*a.toHex() << "\n";
	//cout << BigInt(0x80000000).square().toHex() << "\n";
	
	BigInt a = BigInt::exponentation(BigInt(2), BigInt(1279))-1;
	start = clock();
	cout << BigInt::isMillerRabinPrime(a);
	stop = clock();
	cout << "\n" << (stop - start) << " ms\n";
	
	//return 0;
	//cout << "\n\n";
	//SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),BACKGROUND_INTENSITY|BACKGROUND_BLUE | BACKGROUND_GREEN | FOREGROUND_INTENSITY | FOREGROUND_RED); 
	//cout << BigInt::euclidianAlgorithm(a,b).toDec();
	//cout << "\n" << (float)(((float)stop - (float)start) / (float)1000.0) << " Seconds\n";

	//srand(time(NULL));
	//BigInt a;
	//a.fromHex(argv[1]);
	//cout << (BigInt(atoi(argv[1])) % 3).toHex() << "\n";
	float time_counter = 0.0;
	int counter = 0;
	for(int i = 1; i >= 1; i++)
	{
		start = clock();
		//cout << BigInt::isMillerRabinPrime(a);
		a = BigInt::createPrime(512);
		//a = atoi(argv[2]) % 16;
		stop = clock();
		//cout << "\n";
		//cout << a.toHex();
		//a.printHex2Console();
		cout.precision(3);
		cout.width(6);
		++counter;
		time_counter += (stop-start);
		cout << " \t" << (stop - start) <<  " ms\t" << "avg: " << time_counter/counter << " ms\t" <<  a.toHex() << "\n";
		//cout << (float)(((float)stop - (float)start) / (float)1000.0) << " Seconds\t";
		//cout << a.toHex() << "\n";
	}
	//start = clock();
	//cout << BigInt::isMillerRabinPrime(a);
	//a = BigInt::createPrime(512);
	//a = atoi(argv[2]) % 16;
	//stop = clock();
	//BigInt::random(a).printHex2Console();
	//cout << "\n" << (float)(((float)stop - (float)start) / (float)1000.0) << " Seconds\n";
	//cout.setf(
	/*cout.unsetf(ios_base::dec);
	cout.setf(ios_base::hex | ios_base::uppercase | ios_base::fixed);
	for(int i = 0; i< 50; i++)
	{
		cout.width(8);
		cout << urand() << "\n";
	}
	cout << __cplusplus<< "\n";
	cout << __DATE__ << "\t" << __TIME__;*/
	/*SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY | FOREGROUND_GREEN); 
	BigInt a512, a256, a128;
	a512 = BigInt::exponentation(2,513) - 1;
	a256 = BigInt::exponentation(2,257) - 1;
	a128 = BigInt::exponentation(2,129) -1;
	cout << "\n" << a512.toHex() << "\n" << a256.toHex() << "\n" << a128.toHex();
	start = clock();
	for(int i = 0; i < 100000; i++)
	{
		b=a512 * a512;
	}
	stop = clock();
	cout << "\na512: " <<(stop - start)/(float)1000.0 << " s\n";
	start = clock();
	for(int i = 0; i < 100000; i++)
	{
		b=a256 * a256;
	}
	stop = clock();
	cout << "\na256: " <<(stop - start)/(float)1000.0 << " s\n";
	start = clock();
	for(int i = 0; i < 100000; i++)
	{
		b=a128 * a128;
	
	stop = clock();
	cout << "\na128: " <<(stop - start)/(float)1000.0 << " s\n";*/
	//a = BigInt::exponentation(2,1024)-1;
	//b = BigInt::exponentation(2,1024)-140343;
	//b = BigInt::exponentation(2,1024)-140343;
	a = BigInt::exponentation(BigInt(2), BigInt(521))-1;
	start = clock();
	cout << BigInt::isMillerRabinPrime(a);
	stop = clock();
	cout << "\n" <<(stop - start)/(float)1000.0 << " s\n";
	//for(int i = 0; i < 1000; i++)
	//{
		
	
		//if(BigInt::mult(eins, zwei) != (eins *= zwei))
		//{
		//	cout << "Fail\n";
		//}
		//a*b;
	//}
	
	
	
	//getchar();
	//a = 0xCDADCFAD;
	//a = a * 0xFFFFFFFF;
	//a >>= 35;
	//cout << a.toHex()  << "  "<< a.length << "\n";
	//cout << (a <<56).toHex() << "  "<< a.length << "\n";
	//cout << a.toHex() << "  " << a.length;
	//cout << ((BigInt) 0xFACD >> 32).toHex();
	//cout << BigInt::modInverse(atoi(argv[1]), atoi(argv[2])).toDec()<<"\n";
	//cout << (((BigInt)atoi(argv[1]) * (BigInt)atoi(argv[2]))).toDec();
	return 0;
}
