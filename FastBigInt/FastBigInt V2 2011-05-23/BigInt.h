/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/

#ifndef BIGINT
#define BIGINT
#include <string>
#include <time.h>
#define BITLENGTH 2600
#define INTCOUNT (BITLENGTH + 64) / 32
#define INTCOUNT_1	(INTCOUNT - 1)
#define INTCOUNT_2	(INTCOUNT - 2)
using namespace std;


typedef unsigned long long uint64;
typedef unsigned int uint32;
static unsigned int x_urand = time(NULL);
inline void usrand(unsigned int x)
{
	x_urand = x;
}
inline unsigned int urand()
{
	//x_urand = 1664525L * x_urand + 1013904223L;
	//return x_urand;
	return (x_urand = 1664525L * x_urand + 1013904223L);
}

/*inline unsigned int COMP_CARRY(unsigned int a, unsigned int b, unsigned int c)
{
	//temp_carry = ((__int64) a + (__int64) b + (__int64) c) >> 32;
	return (unsigned int) ((__int64)((__int64) a + (__int64) b + (__int64) c) >> 32);
}*/


class BigInt
{
public:
/*################################################
			Constructors + Settings
################################################*/
	BigInt();
	BigInt(uint32 a);

	BigInt karatsuba1024(BigInt x, BigInt y);

/*################################################
			Arithmetic Operators
################################################*/
	BigInt operator+(BigInt a);
	BigInt operator-(BigInt a);
	BigInt operator*(BigInt a);
	BigInt operator/(BigInt a);
	BigInt operator%(BigInt b);
	BigInt operator++();
	BigInt operator++(int);
	BigInt operator--();
	BigInt operator--(int);
	
	void multEq(uint32 a);
	

	static BigInt exponentation(BigInt base, BigInt exponent);
	static BigInt modExponentation(BigInt base, BigInt exponent, BigInt modulus);

/*################################################
			Bitwise Operators
################################################*/
	BigInt operator<<(uint32 shift);
	BigInt operator>>(uint32 shift);
	BigInt operator|(BigInt a);
	BigInt operator&(BigInt a);
	BigInt operator^(BigInt a);
	BigInt operator~();

/*################################################
			Comparison Operators
################################################*/
	bool operator<(BigInt a);
	bool operator<=(BigInt a);
	bool operator>(BigInt a);
	bool operator>=(BigInt a);
	bool operator==(BigInt a);
	bool operator!=(BigInt a);

/*################################################
			RSA relevant Methods
################################################*/
	static BigInt random(BigInt max);
	static bool isMillerRabinPrime(BigInt n, uint32 witnesses = 30);
	static BigInt createPrime(int bits);
	static BigInt euclidianAlgorithm(BigInt a, BigInt b);			//ggT(a,n)
	static BigInt modInverse(BigInt a, BigInt modulus);				//x = a^-1 mod b
	
	BigInt square();
	void squareEq();

/*###############################################
			Conversion Methods
###############################################*/
	void printHex2Console();
	string toHex(bool uppercase = true);
	string toDec();
	void fromHex(char *hex);
	void fromDec(char *dec);

private:
/*################################################
			Private O= Operators
################################################*/
	BigInt operator<<=(uint32 shift);
	BigInt operator>>=(uint32 shift);
	inline BigInt operator+=(BigInt a);
	inline BigInt operator-=(BigInt a);
	BigInt operator*=(BigInt right);
	inline BigInt operator/=(BigInt a);
	inline BigInt operator%=(BigInt a);

/*################################################
			Private Operators
################################################*/
	inline void shiftOneLeftEq();
	inline void shiftOneRightEq();
	inline BigInt subOne();
	inline void modExpEq(BigInt exponent, BigInt modulus);

/*###############################################
			Private Attributes
###############################################*/
	unsigned int bigint[INTCOUNT];
	int length;
};

#endif
