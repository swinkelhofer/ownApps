/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/

#include "BigInt.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <iostream>

using namespace std;


/*################################################
			Forward Definitions
################################################*/

uint32 pow_2[] =  {0x00000001, 0x00000002, 0x00000004, 0x00000008, 0x00000010, 0x00000020, 0x00000040, 0x00000080,
	0x00000100, 0x00000200, 0x00000400, 0x00000800, 0x00001000, 0x00002000, 0x00004000, 0x00008000,
	0x00010000, 0x00020000, 0x00040000, 0x00080000, 0x00100000, 0x00200000, 0x00400000, 0x00800000,
	0x01000000, 0x02000000, 0x04000000, 0x08000000, 0x10000000, 0x20000000, 0x40000000, 0x80000000};

static const BigInt *null = new BigInt;
static const BigInt *one = new BigInt(1);
static const BigInt *two = new BigInt(2);

BigInt ret, m, x;
uint32 temp;


/*################################################
			Constructors + Settings
################################################*/
BigInt::BigInt()
{
	for(register int i = 0; i < INTCOUNT; ++i)
		this->bigint[i] = 0;
	this->length = 0;
}

BigInt::BigInt(uint32 a)
{
	*this = *null;
	this->bigint[INTCOUNT_1] = a;
	if(a)
	{
		this->length = 1;
	}
}


/*################################################
			Arithmetic Operators
################################################*/
BigInt BigInt::operator+(BigInt a)
{
	register uint32 carry = 0;
	register int temp = (this->length > a.length ? this->length : a.length);
	
	for(int i = INTCOUNT_1; i >= INTCOUNT_1 - temp; --i)
	{
		ret.bigint[i] = this->bigint[i] + a.bigint[i] + carry;
		carry = (uint32) (((uint64) this->bigint[i] + (uint64) a.bigint[i] + (uint64) carry) > 0xFFFFFFFF);//COMP_CARRY(this->bigint[i], a.bigint[i], carry);
	}
	if(ret.bigint[INTCOUNT_1 - (ret.length = temp)] != 0)
		++ret.length;
	return ret;
}

BigInt BigInt::operator-(BigInt a)
{
	if(a >= *this)
		return *null;
	ret = a;
	register uint32 carry = 1;
	for(register int i = INTCOUNT_1;  i >= INTCOUNT_1 - this->length; --i)
	{
		temp = ~ret.bigint[i];
		ret.bigint[i] = this->bigint[i] + temp + carry;
		carry = (uint32) (((uint64) this->bigint[i] + (uint64) temp + (uint64) carry) > 0xFFFFFFFF);// COMP_CARRY(this->bigint[i], temp, carry);
	}
	temp = INTCOUNT - this->length;
	while(!ret.bigint[temp] && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator*(BigInt a)
{
	/*register uint32 carry = 0;
	register int v;
	BigInt *ret = new BigInt;
	if(this->length <= a.length)
	{
		a1 = a;
		a2 = *this;
	}
	else
	{
		a1 = *this;
		a2 = a;
	}
	for(int i = INTCOUNT_1, j = 32; i >= INTCOUNT - a2.length; i--, j = 32)
	{
		a2_save = a2.bigint[i];
		//int j = 32;
		while(a2_save)
		{
			if(a2_save & 0x1)
			{
				//register uint32 carry = 0;
				//register int temp1 = (this->length > a1.length ? this->length : a1.length);
				carry = 0;
				for(v = INTCOUNT_1; v >= INTCOUNT_1 - a1.length /*temp1 ; v--)
				{
					temp = ret->bigint[v];
					ret->bigint[v] += a1.bigint[v] + carry;
					carry = (uint32) (((uint64) temp + (uint64) a1.bigint[v] + (uint64) carry) >> 32);//COMP_CARRY(this->bigint[v], a1.bigint[v], carry);
				}
				if(ret->bigint[INTCOUNT_1 - (ret->length = a1.length /*temp1 )] != 0)
					++ret->length;
				//*ret += a1;
			}
			a1 <<= 1;
			a2_save >>= 1;
			--j;
		}
		
		a1 <<= j;
	}
	return *ret;*/
	//BigInt *ret = new BigInt();
	ret = *null;
	uint64 digits;
	uint32 carry;
	register int thislength = this->length;
	register int alength = a.length;
	for(int i = 0; i < alength; ++i)
	{
		carry = 0;
		for(int j = 0; j < thislength; ++j)
		{
			digits = ret.bigint[INTCOUNT_1 - i - j] + (uint64) carry + (uint64) a.bigint[INTCOUNT_1 - i] * (uint64) this->bigint[INTCOUNT_1 - j];
			ret.bigint[INTCOUNT_1 - i - j] = (uint32) digits;
			carry = (uint32)(digits >> 32);
		}
		ret.bigint[INTCOUNT_1 - (i + thislength)] = carry;
	}
	if(!ret.bigint[INTCOUNT - (ret.length = alength + thislength)])
		--ret.length;
	return ret;
}

BigInt BigInt::operator/(BigInt a)
{
	BigInt *ret = new BigInt(0);
	ret->length = 1;
	if(a > *this)
		return *null;
	BigInt a1 = *this;
	int temp = ((this->length - a.length + 1) * 32);
	a <<= temp;
	for(; temp > 0; --temp)
	{
		a.shiftOneRightEq();
		ret->shiftOneLeftEq();
		if(a1 >= a)
		{
			a1 -= a;
			ret->bigint[INTCOUNT_1] += 1;
		}
	}
	return *ret;
}

BigInt BigInt::operator%(BigInt b)
{
	/*if(a > *this)
		return *this;
	a1 = *this;
	int temp = ((this->length - a.length + 1) * 32);
	a <<= temp;
	for(; temp > 0; --temp)
	{
		a.shiftOneRightEq();
		if(a1 >= a)
			a1 -= a;
	}
	return a1;*/
	BigInt a = *this,  temp, temp2;
	register uint32 factor, pos, l;
	register int blength = b.length;
	while(a >= b)
	{
		for(int i = INTCOUNT - b.length; i < INTCOUNT; ++i)
			temp.bigint[i - a.length + blength] = b.bigint[i];
		//temp.length = a.length;
		pos = INTCOUNT - (temp.length = a.length);
		if(a < temp)
		{
			//temp >>= 32;
			for(int i = INTCOUNT_1; i > INTCOUNT - temp.length; --i)
			{
				temp.bigint[i] = temp.bigint[i-1];
			}
			temp.bigint[pos] = 0;
			--temp.length;
			/*l = temp.bigint[pos] = 0;
			--temp.length;
			//l = 0;
			factor = temp.bigint[pos + 1];
			if(factor < 0x8000)
			{
				factor <<= 16;
				l = 16;
			}
			while(factor < 0x80000000)
			{
				factor <<= 1;
				++l;
			}*/
			//factor = (uint32)(((((uint64)(a<<l).bigint[pos]) << 32) + (uint64) (a<<l).bigint[pos + 1]) / ((uint64) ((temp<<l).bigint[pos + 1])));
			//factor = (uint32)((((uint64) a.bigint[pos] << (l+32)) + ((uint64) a.bigint[pos+1] << l) + ((uint64) a.bigint[pos+2] >> (32-l))) / (((uint64) /*temp.bigint[pos+1] << l*/ factor) + ((uint64) temp.bigint[pos+2] >> (32-l))));
			//factor = (uint32)((((uint64) a.bigint[pos] << (l+32)) + ((uint64) a.bigint[pos+1] << l) + ((uint64) a.bigint[pos+2] >> (32-l))) / (uint64)((uint64)((uint64)temp.bigint[pos] << (32 + l)) + ((uint64)factor) + ((uint64)temp.bigint[pos+2] >> (32-l))));

		}
		//else
		//{
		l = 0;
		factor = a.bigint[pos];
		if(factor < 0x8000)
		{
			factor <<= 16;
			l = 16;
		}
		while(factor < 0x80000000)
		{
			factor <<= 1;
			++l;
		}
			//factor = (uint32)((((uint64) a.bigint[pos] << (l+32)) + ((uint64) a.bigint[pos+1] << l) + ((uint64) a.bigint[pos+2] >> (32-l))) / (uint64)((uint64)((uint64)temp.bigint[pos] << (32 + l)) + ((uint64)factor) + ((uint64)temp.bigint[pos+2] >> (32-l))));

		//}
		factor = (uint32)((((uint64) factor << 32) + ((uint64) a.bigint[pos+1] << l) + ((uint64) a.bigint[pos+2] >> (32-l))) / (((uint64) temp.bigint[pos] << (32 + l)) + ((uint64) temp.bigint[pos+1] << l) + ((uint64) temp.bigint[pos+2] >> (32-l))));


		/*while(temp.mult(factor) > a)
			--factor;
		a -= temp.mult(factor);*/
		temp2 = temp;
		temp.multEq(factor);

		while(temp > a)
			//temp2 -= temp;
			temp-=temp2;
		a -= temp;
		//a.subEq(temp2, blength);
	}
	return a;
}

BigInt BigInt::operator++()
{
	if(this->bigint[INTCOUNT_1] < 0xFFFFFFFF)
	{
		++this->bigint[INTCOUNT_1];
		return *this;
	}
	return (*this += *one);
}

BigInt BigInt::operator++(int)
{
	BigInt ret = *this;
	if(this->bigint[INTCOUNT_1] < 0xFFFFFFFF)
	{
		++this->bigint[INTCOUNT_1];
		return *this;
	}
	*this += *one;
	return ret;
}

BigInt BigInt::operator--()
{
	if(this->bigint[INTCOUNT_1] > 1)
	{
		--this->bigint[INTCOUNT_1];
		return *this;
	}
	return (*this -= *one);
}

BigInt BigInt::operator--(int)
{
	BigInt ret = *this;
	if(this->bigint[INTCOUNT_1] > 1)
	{
		--this->bigint[INTCOUNT_1];
		return *this;
	}
	*this -= *one;
	return ret;
}

BigInt BigInt::exponentation(BigInt base, BigInt exponent)		//ret = b ^ e
{
	BigInt b = base;
	BigInt e = exponent;
	BigInt *back = new BigInt(1);
	while(e.length != 0)
	{
		while(!(e.bigint[INTCOUNT_1] & 0x1))
		{
			e.shiftOneRightEq();
			b.squareEq();
		}
		e -= *one;
		*back *= b;
	}
	return *back;
}

BigInt BigInt::modExponentation(BigInt base, BigInt exponent, BigInt modulus)			//ret = (b ^ e) % modulus
{
	BigInt b = base; 
	BigInt e = exponent;
	BigInt *back = new BigInt(1);
	while(e.length != 0)
	{
		while(!(e.bigint[INTCOUNT_1] & 0x1))
		{
			e.shiftOneRightEq();
			b.squareEq();
			b %= modulus;
		}
		e -= *one;
		*back *= b;
		*back %= modulus;
	}
	return *back;	
}

/*################################################
			Bitwise Operators
################################################*/
BigInt BigInt::operator<<(uint32 shift)
{
	ret = *this;
	register int div = shift >> 5;
	shift &= 0x1f;
	register int i = INTCOUNT - ret.length - div;
	if(div)
	{
		for(; i < INTCOUNT - div; ++i)
			ret.bigint[i] = ret.bigint[i + div];
		while(i < INTCOUNT)
		{
			ret.bigint[i] = 0;
			++i;
		}
	}
	ret.length += div;
	if(shift)
	{
		for(int i = INTCOUNT_1 - ret.length; i < INTCOUNT_1; i++)
			ret.bigint[i] = (ret.bigint[i] << shift) + (ret.bigint[i+1] >> (32-shift));
		ret.bigint[INTCOUNT_1] <<= shift;
	}
	if(ret.bigint[INTCOUNT_1 - ret.length])
		ret.length++;
	return ret;
}

BigInt BigInt::operator>>(uint32 shift)
{
	ret = *this;
	register int div = shift >> 5;
	shift &= 0x1f;
	if(!div && !shift)
		return ret;
	
	if(div > ret.length)
		return *null;
	register int h = INTCOUNT - ret.length + div;
	if(div)
	{
		for(int i = INTCOUNT_1; i >= h; --i)
		{
			ret.bigint[i] = ret.bigint[i - div];
		}
	
		for(int i = h - div; i < h; ++i)
		{
			ret.bigint[i] = 0;
		}
	}
	if(!(ret.length -= div))
		return *null;
	if(shift)
	{
		for(int i = INTCOUNT_1; i >= INTCOUNT - ret.length + 1; --i)
		{
			ret.bigint[i] = (ret.bigint[i] >> shift) + (ret.bigint[i-1] << (32-shift));
		}
		ret.bigint[INTCOUNT - ret.length] >>= shift;
	}
	if(!ret.bigint[INTCOUNT - ret.length])
		--ret.length;
	return ret;
}

BigInt BigInt::operator|(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] | a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator&(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] & a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator^(BigInt a)
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] ^ a.bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator~()
{
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = ~this->bigint[i];
	temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}


/*################################################
			Comparison Operators
################################################*/
bool BigInt::operator<(BigInt a)
{
	if(this->length < a.length)
		return true;
	else if(this->length > a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] < a.bigint[i])
				return true;
			else if(this->bigint[i] > a.bigint[i])
				return false;
		}
		return false;
	}
}

bool BigInt::operator<=(BigInt a)
{
	if(this->length < a.length)
		return true;
	else if(this->length > a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] < a.bigint[i])
				return true;
			else if(this->bigint[i] > a.bigint[i])
				return false;
		}
		return true;
	}
}

bool BigInt::operator>(BigInt a)
{
	if(this->length > a.length)
		return true;
	else if(this->length < a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] > a.bigint[i])
				return true;
			else if(this->bigint[i] < a.bigint[i])
				return false;
		}
		return false;
	}
}

bool BigInt::operator>=(BigInt a)
{
	if(this->length > a.length)
		return true;
	else if(this->length < a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] > a.bigint[i])
				return true;
			else if(this->bigint[i] < a.bigint[i])
				return false;
		}
		return true;
	}
}

bool BigInt::operator==(BigInt a)
{
	if(this->length != a.length)
		return false;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] != a.bigint[i])
				return false;
		}
		return true;
	}
}

bool BigInt::operator!=(BigInt a)
{
	if(this->length != a.length)
		return true;
	else
	{
		for(int i = INTCOUNT - this->length; i < INTCOUNT; ++i)
		{
			if(this->bigint[i] != a.bigint[i])
				return true;
		}
		return false;
	}
}

/*################################################
			RSA relevant Methods
################################################*/
BigInt BigInt::random(BigInt max)
{
	register int maxlength = max.length;
	if(max.bigint[INTCOUNT - maxlength]-1 > 1)
		ret.bigint[INTCOUNT - maxlength] = (uint32) (urand() % (max.bigint[INTCOUNT - maxlength]-1))+1;
	else
		ret.bigint[INTCOUNT - maxlength] = 1;
	for(int i = INTCOUNT - maxlength + 1; i < INTCOUNT; ++i)
		ret.bigint[i] = urand();
	ret.length = maxlength;
	return ret;
}

bool BigInt::isMillerRabinPrime(BigInt n, uint32 witnesses)
{
	if(n.length == 1 && n.bigint[INTCOUNT_1] == 2)
		return true;
	else if((n.length == 1 && n.bigint[INTCOUNT_1] == 1) || !(n.bigint[INTCOUNT_1] & 0x1))
		return false;
	bool ism;
	m = n.subOne();
	for(uint32 i = 0; i < witnesses; ++i)
	{
		int l = 0;
		x = BigInt::random(m);
		while(!(m.bigint[INTCOUNT_1] & 0x1))
		{
			++l;
			m.shiftOneRightEq();
		}
		x.modExpEq(m,n);
		m = n.subOne();
		ism = false;
		if((x.length == 1 && x.bigint[INTCOUNT_1] == 1) || x == m)
			continue;
		for(int j = l; j > 1; --j)
		{
			x.squareEq();
			x %= n;
			if(x == m)
			{
				ism = true;
				break;
			}
			if(x.length == 1 && x.bigint[INTCOUNT_1] == 0x1)
				return false;
		}
		if(!ism)
			return false;
	}
	return true;
	/*uint32 s = 0;
	BigInt r = n.subOne();
	BigInt y, a;
	while(!(r.bigint[INTCOUNT_1] & 0x1))
	{
		++s;
		r >>= 1;
	}
	for(uint32 i = 0; i < witnesses; ++i)
	{
		a = BigInt::random(x.subOne().subOne().subOne()) + *two;
		y = BigInt::modExponentation(a, r, n);
		a = n-y;
		if(!(y.bigint[INTCOUNT_1] == 0x1 && y.length == 0x1) && !(a.bigint[INTCOUNT_1] == 0x1 && a.length == 0x1))
		{
			uint32 j = 1;
			while((j < s) && !((a.bigint[INTCOUNT_1] == 0x1 && a.length == 0x1)))
			{
				y.squareEq();
				y %= n;
				if(y.bigint[INTCOUNT_1] == 0x1 && y.length == 0x1)
					return false;
				++j;
			}
			if(!(a.bigint[INTCOUNT_1] == 0x1 && a.length == 0x1))
				return false;
		}
	}
	return true;*/
}

BigInt BigInt::createPrime(int bits)
{
	--bits;
	BigInt ret;
	int j = 0;
	int i3, i5, i7;
	while(true)
	{
		j = 0;
		temp = urand();
		if(!(temp & 0x1))
			++temp;
		ret.bigint[INTCOUNT_1] = temp;
		for(int i = INTCOUNT_1 - 1; i >= INTCOUNT - bits/32; --i)
		{
			ret.bigint[i] = urand();
		}
		if(bits % 32 != 0)
		{
			ret.bigint[INTCOUNT_1 - bits/32] = (urand() % (int) pow_2[bits % 32]) + pow_2[bits % 32];
			ret.length = bits/32 + 1;
		}
		else
			ret.length = bits/32;
			
			
		switch((ret % BigInt(3)).bigint[INTCOUNT_1])
		{
			case 0:		i3 = 0;
						break;
			case 1:		i3 = 2;
						break;
			case 2:		i3 = 1;
						break;
			default:	exit(0);
		}
		
		switch((ret % BigInt(5)).bigint[INTCOUNT_1])
		{
			case 0:		i5 = 0;
						break;
			case 1:		i5 = 3;
						break;
			case 2:		i5 = 1;
						break;
			case 3:		i5 = 4;
						break;
			case 4:		i5 = 2;
						break;
			default:	exit(0);
		}
		
		switch((ret % BigInt(7)).bigint[INTCOUNT_1])
		{
			case 0:		i7 = 0;
						break;
			case 1:		i7 = 4;
						break;
			case 2:		i7 = 1;
						break;
			case 3:		i7 = 5;
						break;
			case 4:		i7 = 2;
						break;
			case 5:		i7 = 6;
						break;
			case 6:		i7 = 3;
						break;
			default:	exit(0);
		}
		while(true)
		{
			++j;
			
			if(!(i3 % 3) || !(i5 % 5) || !(i7 % 7))
			{
				++i3;
				++i5;
				++i7;
				ret.bigint[INTCOUNT_1] += 2;
				continue;
			}
			if(BigInt::isMillerRabinPrime(ret))
			{
				cout.fill(' ');
				cout.width(3);
				cout << j;
				return ret;
			}
			ret.bigint[INTCOUNT_1] += 2;
			++i3;
			++i5;
			++i7;
		}
	}
}

void BigInt::multEq(uint32 a)
{
	//BigInt ret;
	register uint64 carry = 0;
	register uint32 thislength = this->length;
	for(int i = INTCOUNT_1; i >= INTCOUNT - thislength; --i)
	{
		this->bigint[i] = (uint32) (carry = (uint64) this->bigint[i] * (uint64) a + (carry >> 32));
	}
	if(carry > 0xFFFFFFFF)
	{
		this->bigint[INTCOUNT_1 - thislength] = (uint32) (carry >> 32);
		++this->length;
	}
	//return ret;
}

BigInt BigInt::euclidianAlgorithm(BigInt a, BigInt b)
{
	BigInt h;
	while(b != *null)
	{
		h = a % b;
		a = b;
		b = h;
	}
	return a;
}

BigInt BigInt::modInverse(BigInt a, BigInt modulus)
{
	BigInt mod = modulus;
	BigInt q,r;
	BigInt u=1;
	BigInt s=0;
	bool q_n = 0, r_n = 0, u_n = 0, s_n = 0, a_n = 0, mod_n = 0;
	while(modulus>0 && !mod_n)
	{
		q=a/modulus;
		q_n = a_n ^ mod_n;
		if(a >= q*modulus)
		{
			if(a_n ^ q_n ^ mod_n)
			{
				r = a + q*modulus;
				r_n = a_n;
			}
			else
			{
				r = a - q*modulus;
				r_n = q_n ^ mod_n;
			}
		}
		else
		{
			if(a_n ^ q_n ^ mod_n)
			{
				r = a + q*modulus;
				r_n = a_n;
			}
			else
			{
				r = q*modulus - a;
				r_n = !a_n;
			}
		}
		a=modulus;
		a_n = mod_n;
		modulus=r;
		mod_n = r_n;
		if(u >= q*s)
		{
			if(u_n ^ q_n ^ s_n)
			{
				r = u + q*s;
				r_n = u_n;
			}
			else
			{
				r = u - q*s;
				r_n = q_n ^ s_n;
			}
		}
		else
		{
			if(u_n ^ q_n ^ s_n)
			{
				r = u + q*s;
				r_n = u_n;
			}
			else
			{
				r = q*s - u;
				r_n = !u_n;
			}
		}
		u=s;
		u_n = s_n;
		s=r;
		s_n = r_n;
	}
	//cout << a.toDec()<<"\n";
	if(a != *one && !a_n)
		return *null;
	if(u_n)
		return (mod - u);
	else
		return u;
}

BigInt BigInt::square()
{
	register uint64 digits, digits2, carry;
	register int thislength = this->length;
	for(int i = 0; i < thislength; ++i)
	{
		carry = 0;
		for(int j = 0; j < i; ++j)
		{
			carry = (uint64) ret.bigint[INTCOUNT_1 - i - j] + carry;
			digits2 = (uint64) this->bigint[INTCOUNT_1 - j] * (uint64) this->bigint[INTCOUNT_1 - i];
			ret.bigint[INTCOUNT_1 - i - j] = (uint32)(carry + (digits2 << 1));
			carry = (digits2 + (carry >> 1)) >> 31;
		}
		digits = (uint64) this->bigint[INTCOUNT_1 - i] * (uint64) this->bigint[INTCOUNT_1 - i] + carry;
		ret.bigint[INTCOUNT_1 - (i<<1)] = (uint32) digits;
		ret.bigint[INTCOUNT_2 - (i<<1)] = (uint32) (digits >> 32);
	}
	if(!ret.bigint[INTCOUNT - (ret.length=(thislength<<1))])
		--ret.length;
	return ret;
}

void BigInt::squareEq()
{
	register uint64 digits, digits2, carry;
	register int thislength = this->length;
	for(int i = 0; i < thislength; ++i)
	{
		carry = 0;
		digits = (uint64) this->bigint[INTCOUNT_1 - i];
		for(int j = 0; j < i; ++j)
		{
			ret.bigint[INTCOUNT_1 - i - j] = (uint32)((carry += (uint64) ret.bigint[INTCOUNT_1 - i - j]) + ((digits2 = (uint64) this->bigint[INTCOUNT_1 - j] * digits) << 1));
			carry = (digits2 + (carry >> 1)) >> 31;
		}
		digits *= digits;
		ret.bigint[INTCOUNT_1 - (i<<1)] = (uint32) (digits += carry);
		ret.bigint[INTCOUNT_2 - (i<<1)] = (uint32) (digits >> 32);
	}
	if(!ret.bigint[INTCOUNT - (ret.length = (thislength << 1))])
		--ret.length;
	*this = ret;
}

/*###############################################
			Conversion Methods
###############################################*/
void BigInt::printHex2Console()
{
	cout.fill('0');
	cout.unsetf(ios_base::dec);
	cout.setf(ios_base::hex | ios_base::uppercase | ios_base::fixed);
	for(int i = 0; i < INTCOUNT; i++)
	{
		if(i % 8 == 0)
			cout << "\n";
		cout.width(8);
		cout << this->bigint[i] << " ";
	}
	cout.unsetf(ios_base::hex);
	cout.setf(ios_base::dec);
}

string BigInt::toHex(bool uppercase)
{
	string hex = "";
	char temp;
	BigInt sixteen(16), a = *this;
	while(a != *null)
	{
		switch((a % sixteen).bigint[INTCOUNT_1])
		{
			case 0:		hex += '0';
						break;
			case 1:		hex += '1';
						break;
			case 2:		hex += '2';
						break;
			case 3:		hex += '3';
						break;
			case 4:		hex += '4';
						break;
			case 5:		hex += '5';
						break;
			case 6:		hex += '6';
						break;
			case 7:		hex += '7';
						break;
			case 8:		hex += '8';
						break;
			case 9:		hex += '9';
						break;
			case 10:	hex += 'A';
						break;
			case 11:	hex += 'B';
						break;
			case 12:	hex += 'C';
						break;
			case 13:	hex += 'D';
						break;
			case 14:	hex += 'E';
						break;
			case 15:	hex += 'F';
						break;
		}
		a = a >> 4;
	}
	if(!uppercase)
	{
		for(int i = 0; i < (int) hex.length() / 2; i++)
		{
			temp = tolower(hex[i]);
			hex[i] = tolower(hex[hex.length() - 1 - i]);
			hex[hex.length() - 1 - i] = temp;
		}
	}
	else
	{
		for(int i = 0; i < (int) hex.length() / 2; i++)
		{
			temp = hex[i];
			hex[i] = hex[hex.length() - 1 - i];
			hex[hex.length() - 1 - i] = temp;
		}
	}
	if(hex == string(""))
		hex += '0';
	hex.insert(0,"0x");
	return hex;
}

string BigInt::toDec()
{
	string dec = "";
	char temp;
	BigInt ten(10), a = *this;
	while(a != *null)
	{
		switch((a % ten).bigint[INTCOUNT_1])
		{
			case 0:		dec += '0';
						break;
			case 1:		dec += '1';
						break;
			case 2:		dec += '2';
						break;
			case 3:		dec += '3';
						break;
			case 4:		dec += '4';
						break;
			case 5:		dec += '5';
						break;
			case 6:		dec += '6';
						break;
			case 7:		dec += '7';
						break;
			case 8:		dec += '8';
						break;
			case 9:		dec += '9';
						break;
		}
		a = a / ten;
	}
	for(int i = 0; i < (int) dec.length() / 2; i++)
	{
		temp = dec[i];
		dec[i] = dec[dec.length() - 1 - i];
		dec[dec.length() - 1 - i] = temp;
	}
	
	if(dec == string(""))
		dec += '0';
	//dec.insert(0,"0d ");
	return dec;
}

void BigInt::fromHex(char *hex)
{
	int hexlength = strlen(hex);
	int start = 0;
	int chiffre = 0;
	if(hex[0] == '0' && toupper(hex[1]) == 'X' && hex[2] == ' ')
	{
		hexlength -= 3;
		start = 2;
	}
	for(int i = 0; i < hexlength; i++)
	{
		switch(toupper(hex[start + i]))
		{
			case '0':	chiffre = 0;
						break;
			case '1':	chiffre = 1;
						break;
			case '2':	chiffre = 2;
						break;
			case '3':	chiffre = 3;
						break;
			case '4':	chiffre = 4;
						break;
			case '5':	chiffre = 5;
						break;
			case '6':	chiffre = 6;
						break;
			case '7':	chiffre = 7;
						break;
			case '8':	chiffre = 8;
						break;
			case '9':	chiffre = 9;
						break;
			case 'A':	chiffre = 10;
						break;
			case 'B':	chiffre = 11;
						break;
			case 'C':	chiffre = 12;
						break;
			case 'D':	chiffre = 13;
						break;
			case 'E':	chiffre = 14;
						break;
			case 'F':	chiffre = 15;
						break;
		}
		*this = *this << 4;
		*this = *this + BigInt(chiffre);
	}
}

void BigInt::fromDec(char *dec)
{
	int declength = strlen(dec);
	BigInt ten(10);
	uint32 chiffre = 0;
	for(int i = declength - 1; i >= 0; i--)
	{
		switch(dec[i])
		{
			case '0':	chiffre = 0;
						break;
			case '1':	chiffre = 1;
						break;
			case '2':	chiffre = 2;
						break;
			case '3':	chiffre = 3;
						break;
			case '4':	chiffre = 4;
						break;
			case '5':	chiffre = 5;
						break;
			case '6':	chiffre = 6;
						break;
			case '7':	chiffre = 7;
						break;
			case '8':	chiffre = 8;
						break;
			case '9':	chiffre = 9;
						break;
		}
		*this += BigInt(chiffre) * BigInt::exponentation(ten, BigInt((uint32) declength - 1 - i));
	}
}

/*################################################
			Private O= Operators
################################################*/
BigInt BigInt::operator<<=(uint32 shift)
{
	register int div = shift >> 5;
	shift &= 0x1f;
	register int i = INTCOUNT - this->length - div;
	if(div)
	{
		for(; i < INTCOUNT - div; ++i)
			this->bigint[i] = this->bigint[i + div];
		while(i < INTCOUNT)
		{
			this->bigint[i] = 0;
			++i;
		}
	}
	this->length += div;
	if(shift)
	{
		for(i = INTCOUNT_1 - this->length; i < INTCOUNT_1; ++i)
			this->bigint[i] = (this->bigint[i] << shift) + (this->bigint[i+1] >> (32-shift));
		this->bigint[INTCOUNT_1] <<= shift;
	}
	if(this->bigint[INTCOUNT_1 - this->length])
		++this->length;
	return *this;
}

BigInt BigInt::operator>>=(uint32 shift)
{
	register int div = shift >> 5;
	shift &= 0x1f;
	register int i = INTCOUNT_1;
	if(div > this->length)
		return *null;
	register int h = INTCOUNT - this->length + div;
	if(div)
	{
		for(; i >= h; --i)
			this->bigint[i] = this->bigint[i - div];
		while(i > h - div)
		{
			this->bigint[i] = 0;
			--i;
		}
	}
	if(!(this->length -= div))
		return *null;
	if(shift)
	{
		for(h = INTCOUNT_1; h >= INTCOUNT - this->length + 1; --h)
			this->bigint[h] = (this->bigint[h] >> shift) + (this->bigint[h-1] << (32-shift));
		this->bigint[INTCOUNT - this->length] >>= shift;
	}
	if(!this->bigint[INTCOUNT - this->length])
		--this->length;
	return *this;
	/*uint32 fastshift = shift >> 5;
	shift %= 32;
	if(!shift)
	{
		for(int i = INTCOUNT_1; i >= INTCOUNT - this->length + fastshift; --i)
			this->bigint[i] = this->bigint[i-fastshift];
		for(int i = INTCOUNT - this->length; i < INTCOUNT - this->length + fastshift; ++i)
			this->bigint[i] = 0;
	}
	else
	{
		for(int i = INTCOUNT_1; i > INTCOUNT - this->length + fastshift; --i)
		{
			this->bigint[i] = (this->bigint[i-fastshift] >> shift) | (this->bigint[i + fastshift - 1] << (32 - shift));
		}
		this->bigint[INTCOUNT - this->length] >>= shift;
	}
	this->length -= fastshift;
	if(!this->bigint[INTCOUNT - this->length])
		--this->length;
	return *this;*/
}

BigInt BigInt::operator+=(BigInt a)
{
	register int newlength = this->length > a.length ? this->length : a.length;
	register uint32 carry = 0;
	for(register int i = INTCOUNT_1; i >= INTCOUNT_1 - newlength; --i)
	{
		temp = this->bigint[i];
		this->bigint[i] += a.bigint[i] + carry;
		carry = (uint32) (((uint64) temp + (uint64) a.bigint[i] + (uint64) carry) > 0xFFFFFFFF);
	}
	if(this->bigint[INTCOUNT_1 - (this->length = newlength)] != 0)
		++this->length;
	return *this;
}

BigInt BigInt::operator-=(BigInt a)
{
	/*if(a >= *this)
	{
		*this = *null;
		return *this;
	}*/
	register uint32 carry = 1;
	for(register int i = INTCOUNT_1;  i >= INTCOUNT_1 - this->length; --i)
	{
		temp = this->bigint[i];
		this->bigint[i] += ~a.bigint[i] + carry;
		carry = (uint32) (((uint64) temp + (uint64) ~a.bigint[i] + (uint64) carry) > 0xFFFFFFFF);//COMP_CARRY(temp, ~a.bigint[i], carry);
	}
	temp = INTCOUNT - this->length;
	while(!this->bigint[temp] && temp < INTCOUNT)
		++temp;
	this->length = INTCOUNT - temp;
	return *this;
}

BigInt BigInt::operator*=(BigInt a)
{
	ret = *this;
	*this = *null;
	uint64 carry, retval;
	register int retlength = ret.length;
	register int alength = a.length;
	for(int i = INTCOUNT_1; i >= INTCOUNT - retlength; --i)
	{
		retval = ret.bigint[i];
		carry = 0;
		for(int j = INTCOUNT_1; j >= INTCOUNT - alength; --j)
		{
			
			this->bigint[i + j - INTCOUNT_1] = (uint32) (carry += (uint64) this->bigint[i + j - INTCOUNT_1] + retval * (uint64) a.bigint[j]);
			carry >>= 32;
		}
		this->bigint[i - alength] = (uint32) carry;
	}
	if(!this->bigint[INTCOUNT - (this->length = alength + retlength)])
		--this->length;
	return *this;
}

BigInt BigInt::operator/=(BigInt a)
{
	BigInt *ret = new BigInt(0);
	ret->length = 1;
	if(a > *this)
		return *null;
	BigInt a1 = *this;
	int temp = ((this->length - a.length + 1) * 32);
	a <<= temp;
	for(; temp > 0; --temp)
	{
		a.shiftOneRightEq();
		ret->shiftOneLeftEq();
		if(a1 >= a)
		{
			a1 -= a;
			ret->bigint[INTCOUNT_1] += 1;
		}
	}
	*this = *ret;
	return *this;
}

BigInt BigInt::operator%=(BigInt b)
{
	if(*this < b)
		return *this;
	BigInt temp, temp2;
	register uint32 factor, pos, l;
	register int blength = b.length;
	while(*this >= b)
	{
		for(int i = INTCOUNT - blength; i < INTCOUNT; ++i)
			temp.bigint[i - this->length + blength] = b.bigint[i];
		//temp.length = this->length;
		pos = INTCOUNT - (temp.length = this->length);
		if(*this < temp)
		{
			//temp >>= 32;
			for(int i = INTCOUNT_1; i > INTCOUNT - temp.length; --i)
			{
				temp.bigint[i] = temp.bigint[i-1];
			}
			temp.bigint[pos] = 0;
			--temp.length;
			//factor = (uint32)(((((uint64)(*this<<l).bigint[pos]) << 32) + (uint64) (*this<<l).bigint[pos + 1]) / ((uint64) ((temp<<l).bigint[pos + 1])));
			//factor = (uint32)((((uint64) this->bigint[pos] << (l+32)) + ((uint64) this->bigint[pos+1] << l) + ((uint64) this->bigint[pos+2] >> (32-l))) / (((uint64) /*temp.bigint[pos+1] << l factor) + ((uint64) temp.bigint[pos+2] >> (32-l))));
			//factor = (uint32)((((uint64) this->bigint[pos] << (l+32)) + ((uint64) this->bigint[pos+1] << l) + ((uint64) this->bigint[pos+2] >> (32-l))) / (uint64)((uint64)((uint64)temp.bigint[pos] << (32 + l)) + ((uint64)factor) + ((uint64)temp.bigint[pos+2] >> (32-l))));

		}
		//else
		//{
		l = 0;
		factor = this->bigint[pos];
		if(factor < 0x8000)
		{
			factor <<= 16;
			l = 16;
		}
		while(factor < 0x80000000)
		{
			factor <<= 1;
			++l;
		}
			//factor = (uint32)((((uint64) this->bigint[pos] << (l+32)) + ((uint64) this->bigint[pos+1] << l) + ((uint64) this->bigint[pos+2] >> (32-l))) / (uint64)((uint64)((uint64)temp.bigint[pos] << (32 + l)) + ((uint64)factor) + ((uint64)temp.bigint[pos+2] >> (32-l))));

		//}
		factor = (uint32)((((uint64) factor << 32) + ((uint64) this->bigint[pos+1] << l) + ((uint64) this->bigint[pos+2] >> (32-l))) / (((uint64) temp.bigint[pos] << (32 + l)) + ((uint64) temp.bigint[pos+1] << l) + ((uint64) temp.bigint[pos+2] >> (32-l))));


			/*while(temp.mult(factor) > *this)
				--factor;
			*this -= temp.mult(factor);*/
		temp2 = temp;
		
		temp.multEq(factor);
		while(temp > *this)
			temp-=temp2;
		*this -= temp;
	}
	return *this;
}

/*################################################
			Private Operators
################################################*/
void BigInt::shiftOneLeftEq()
{
	for(int i = INTCOUNT_1 - this->length; i < INTCOUNT_1; ++i)
	{
		this->bigint[i] = (this->bigint[i] << 1) + (this->bigint[i+1] > 0x7FFFFFFF);
	}
	this->bigint[INTCOUNT_1] <<= 1;
	if(this->bigint[INTCOUNT_1 - this->length] != 0)
		++this->length;
}

void BigInt::shiftOneRightEq()
{
	for(int i = INTCOUNT_1; i >= INTCOUNT - this->length; --i)
	{
		this->bigint[i] = (this->bigint[i] >> 1) + (this->bigint[i-1] << 31);
	}
	if(this->bigint[INTCOUNT - this->length] == 0)
		--this->length;
}

BigInt BigInt::subOne()
{
	if(this->bigint[INTCOUNT_1] > 0x1)
	{
		ret = *this;
		--ret.bigint[INTCOUNT_1];
		return ret;
	}
	else
		return (*this - *one);
}

void BigInt::modExpEq(BigInt exponent, BigInt modulus)			//this = (b ^ e) % modulus
{
	BigInt b = *this;
	*this = *one;
	while(exponent.length)
	{
		while(!(exponent.bigint[INTCOUNT_1] & 0x1))
		{
			exponent.shiftOneRightEq();
			b.squareEq();
			b %= modulus;
		}
		--exponent;
		*this *= b;
		*this %= modulus;
	}
	/*while(exponent.length)
	{
		if(!(exponent.bigint[INTCOUNT_1] & 0x1))
		{
			*this *= b;
			*this %= modulus;
		}
		b *= b;
		b %= modulus;
		exponent.shiftOneRightEq();
	}*/
}
