/*
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       ^     ^
@                                                       @      /^\   /^\
@ @@@@@     @@       @@@@@@    @@   @@    @@   @@@@@@@@ @     // \\ // \\
@ @@   @@   @@     @@@   @@@   @@   @@@   @@      @@    @    @           @
@ @@   @@   @@    @@           @@   @@@@  @@      @@    @   |             |
@ @@@@@@    @@   @@   @@@@@@   @@   @@ @@ @@      @@    @   |   (o) (o)   |
@ @@   @@   @@    @@    @@@@   @@   @@  @@@@      @@    @    4           2
@ @@   @@   @@     @@  @@ @@   @@   @@   @@@      @@    @     \         /
@ @@@@@     @@      @@@@  @@   @@   @@    @@      @@    @    ___\ | | /___
@                                                       @     ___\| |/___
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         (@)      $F0X><
*/

#include "BigInt.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <iostream>
#define BITLENGTH 1280
#define INTCOUNT BITLENGTH / 32
/*#define COMP_CARRY(a,b,c)  (((((a & 0x7FFFFFFF) + (b & 0x7FFFFFFF) + c) >> 31) &	\
 ((a & 0x80000000) != 0)) | ((((a & 0x7FFFFFFF) + (b & 0x7FFFFFFF) + c) >> 31) & 	\
 ((b & 0x80000000) != 0)) | (((a & 0x80000000) != 0) & ((b & 0x80000000) != 0)))*/

unsigned int pow_2[] =  {0x00000001, 0x00000002, 0x00000004, 0x00000008, 0x00000010, 0x00000020, 0x00000040, 0x00000080,
						0x00000100, 0x00000200, 0x00000400, 0x00000800, 0x00001000, 0x00002000, 0x00004000, 0x00008000,
						0x00010000, 0x00020000, 0x00040000, 0x00080000, 0x00100000, 0x00200000, 0x00400000, 0x00800000,
						0x01000000, 0x02000000, 0x04000000, 0x08000000, 0x10000000, 0x20000000, 0x40000000, 0x80000000};


__int64 temp_carry;
inline unsigned int COMP_CARRY(unsigned int a, unsigned int b, unsigned int c)
{
	temp_carry = (__int64) a + (__int64) b + (__int64) c;
	return (unsigned int) (temp_carry >> 32);
}



/*unsigned int COMP_CARRY(unsigned int a, unsigned int b, unsigned int c)
{
	unsigned int temp = (unsigned int) (a + b), temp2 = (unsigned int) (a+c), temp3 = (unsigned int) (b+c);
	if(temp < a && temp < b)
		return 1;
	else if(temp2 == 0)
		return 1;
	else if(temp3 == 0)
		return 1;
	else
		return 0;
	//return (((temp < a) & (temp < b)) | (temp2 == 0) | (temp3 == 0));
}*/
 
/*#define COMP_CARRY(a7,a8,b7,b8,c)  (((((a7) + (b7) + c) >> 31) &	\
 ((a8) != 0)) | ((((a7) + (b7) + c) >> 31) & 	\
 ((b8) != 0)) | (((a8) != 0) & ((b8) != 0)))*/

//#define overflux(a,b,c)  ((a & b) | (a & c) | (b & c))
// a = this->bigint[i]
// b = a.bigint[i]
// c = carry
//#define COMP_CARRY(a,b,c) ((a+b+c <= a) && (a+b+c <= b) && (a >> )

//(((a & 0x7FFFFFFF) + (b & 0x7FFFFFFF) + c) >= 0x80000000),
 //((a & 0x80000000) != 0), 
//((b & 0x80000000) != 0)
using namespace std;


const BigInt *null = new BigInt(0);
const BigInt *one = new BigInt(1);
const BigInt *two = new BigInt(2);
BigInt a1,a2,ret;
unsigned int temp, carry;

BigInt::BigInt()
{
	
	for(int i = 0; i < INTCOUNT; i++)
		bigint[i] = 0;
	length = 0;
}

BigInt::BigInt(int a)
{
	for(int i = 0; i < INTCOUNT-1; i++)
		bigint[i] = 0;
	bigint[INTCOUNT-1] = a;
	length = 1;
}


BigInt BigInt::operator+(BigInt a)
{
	/*unsigned int */carry = 0;
	/*unsigned int temp*/
	//BigInt ret;
	int temp = this->length > a.length ? this->length : a.length;
	int i = INTCOUNT - 1;
	for(; i >= INTCOUNT - temp - 1; i--)
	{
		ret.bigint[i] = this->bigint[i] + a.bigint[i] + carry;
		//carry = overflux((((this->bigint[i] & 0x7FFFFFFF) + (a.bigint[i] & 0x7FFFFFFF) + carry) >= 0x80000000), (this->bigint[i] & 0x80000000) != 0, (a.bigint[i] & 0x80000000) != 0);
		carry = COMP_CARRY(this->bigint[i], a.bigint[i], carry);
		//carry = COMP_CARRY(this->bigint[i] & 0x7FFFFFFF, this->bigint[i] & 0x80000000, a.bigint[i] & 0x7FFFFFFF, a.bigint[i] & 0x80000000, carry);
	}
	if(ret.bigint[INTCOUNT - temp-1] == 0)
		ret.length = temp;
	else
		ret.length = temp+1;
	/*temp = 0;
	while(!ret.bigint[temp]/* == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;*/
	return ret;
}

BigInt BigInt::operator-(BigInt a)
{
	if(a >= *this)
		return *null;
	//BigInt ret = a;
	ret = a;
	//for(int i = 0; i < INTCOUNT; i++)
	//	ret.bigint[i] = ~ret.bigint[i];
	/*unsigned int */carry = 1;
	/*unsigned int temp;*/
	for(int i = INTCOUNT - 1;  i >= INTCOUNT - max(this->length, a.length)-1; i--)
	{
		temp = ~ret.bigint[i];
		ret.bigint[i] = this->bigint[i] + temp + carry;
		//carry = overflux((((this->bigint[i] & 0x7FFFFFFF) + (temp & 0x7FFFFFFF) + carry) >= 0x80000000), (this->bigint[i] & 0x80000000) != 0, (temp & 0x80000000) != 0);
		carry = COMP_CARRY(this->bigint[i], temp, carry);
		//carry = COMP_CARRY(this->bigint[i] & 0x7FFFFFFF, this->bigint[i] & 0x80000000, temp & 0x7FFFFFFF, temp & 0x80000000, carry);
	}
	temp = 0;
	while(!ret.bigint[temp]/* == 0*/ && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator*(BigInt a)
{
	if(this->length + a.length < INTCOUNT)
	{
		BigInt a1,a2,ret;
		if(this->length <= a.length)
		{
			a1 = a;
			a2 = *this;
		}
		else
		{
			a1 = *this;
			a2 = a;
		}
		for(int i = 1; i <= a2.length; i++)
		{
			if((a2.bigint[INTCOUNT - i] & pow_2[0]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[1]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[2]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[3]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[4]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[5]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[6]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[7]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[8]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[9]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[10]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[11]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[12]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[13]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[14]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[15]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[16]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[17]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[18]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[19]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[20]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[21]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[22]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[23]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[24]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[25]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[26]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[27]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[28]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[29]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[30]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;
			if((a2.bigint[INTCOUNT - i] & pow_2[31]) != 0)
				ret = ret + a1;
			a1 = a1 << 1;			
		}
		/*unsigned int temp = 0;
		while(!ret.bigint[temp]/* == 0 && temp < INTCOUNT)
			temp++;
		ret.length = INTCOUNT - temp;*/
		return ret;
	}
	else
		return *null;
}

BigInt BigInt::operator%(BigInt a)
{
	if(a > *this)
	{
		return *this;
	}
	/*BigInt */a1 = *this;
	/*BigInt */a2 = a << ((this->length - a.length + 1) * 32);
	for(int i = 0; i < (this->length - a.length + 1) * 32; i++)
	{
		a2 = a2 >> 1;
		if(a1 >= a2)
			a1 = a1 - a2;
	}
	/*unsigned int *///temp = 0;
	/*while(!ret.bigint[temp]/* == 0 && temp < INTCOUNT)
		temp++;
	a1.length = INTCOUNT - temp;*/
	return a1;
}

BigInt BigInt::exponentation(BigInt base, BigInt exponent)
{
	BigInt a1 = base, z1 = exponent;
	BigInt ret = *one;
	while(z1 != *null)
	{
		while(z1.bigint[INTCOUNT - 1] % 2 == 0)
		{
			z1 = z1 >> 1;
			a1 = (a1 * a1);
		}
		z1 = z1 - *one;
		
		ret = (ret * a1);
	}
	/*unsigned int temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;*/
	return ret;
}

BigInt BigInt::modExponentation(BigInt base, BigInt exponent, BigInt modulus)
{
	BigInt a1 = base; 
	BigInt z1 = exponent;
	BigInt ret = *one;
	while(z1 != *null)
	{
		while(z1.bigint[INTCOUNT - 1] % 2 == 0)
		{
			z1 = z1 >> 1;
			a1 = (a1 * a1) % modulus;
		}
		z1 = z1 - *one;
		ret = (ret * a1) % modulus;
	}
	/*unsigned int temp = 0;
	/*while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;*/
	return ret;
}

BigInt BigInt::random(BigInt max)
{
	//BigInt ret;
	ret.bigint[INTCOUNT - max.length] = ((rand()*0xff37) % (max.bigint[INTCOUNT - max.length]-1))+1;
	for(int i = INTCOUNT - max.length + 1; i < INTCOUNT; i++)
	{
		ret.bigint[i] = (unsigned int) rand()*0xff31;
	}
	/*unsigned int *//*temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;*/
	ret.length = max.length;
	//ret = (ret % (max-*one)) + *one;
	return ret;
}

/*bool BigInt::millerRabinTest()
{
	BigInt m, x, n = *this;
	int l = 0;
	m = n - *one;
	while(m % *two == *null)
	{
		l++;
		m = m >> 1;
	}
	m = n >> l;
	
	for(int i = 0; i < 30; i++)
	{
		x = BigInt::random(n);
		//cout << x.bigint[31] << "\n";
		x = BigInt::modExponentation(x,m,n);
		m = n - *one;
		if(x == *one || x == m)
			continue;
		for(int i = 1; i < l; i++)
		{
			x = (x*x)%n;
			if(x == m)
				continue;
			else if(x == *one)
				return false;
		}
		return false;
	}
	return true;
}*/

bool BigInt::isMillerRabinPrime(BigInt n)
{
/*
	if(n == *two)
		return true;
	if(n % *two == *null || n == *null || n == *one)
		return false;
	for(int i = 0; i < 30; i++)
	{
		if(!n.millerRabinTest())
			return false;
	}
	return true;*/
	if(n == *one || n == *null || n.bigint[INTCOUNT - 1] % 2 == 0)
		return false;
	BigInt m, x;
	int l = 0;
	m = n - *one;
	while(m.bigint[INTCOUNT - 1] % 2 == 0)
	{
		l++;
		m = m >> 1;
	}
	m = n >> l;
	
	for(int i = 0; i < 30; i++)
	{
		x = BigInt::random(n);
		//cout << x.bigint[31] << "\n";
		x = BigInt::modExponentation(x,m,n);
		m = n - *one;
		if(x == *one || x == m)
			continue;
		for(int i = 1; i < l; i++)
		{
			x = (x*x)%n;
			if(x == m)
				continue;
			else if(x == *one)
				return false;
		}
		return false;
	}
	return true;
}

BigInt BigInt::createPrime(int bits)
{
	bits--;
	srand(time(0));
	BigInt ret;
	while(true)
	{
		int temp = rand()*0xff0b + 0xff0a7;
		if(temp % 2 == 0)
			temp++;
		ret.bigint[INTCOUNT - 1] = temp;
		for(int i = INTCOUNT - 2; i >= INTCOUNT - bits/32; i--)
		{
			ret.bigint[i] = rand()*0xff30;
		}
		if(bits % 32 != 0)
		{
			ret.bigint[INTCOUNT - bits/32 - 1] = (rand()*0xff29) % (int) pow(2, (bits % 32)) + pow(2, bits % 32);
			ret.length = bits/32 + 1;
		}
		else
			ret.length = bits/32;
		int i = 0;
		/*while(!BigInt::isMillerRabinPrime(ret))
		{
			ret = ret + *two;
			i++;
			cout << i << "\n";
		}*/
		while(true)
		{
			if(BigInt::isMillerRabinPrime(ret))
				return ret;
			i++;
			if(i == 1000);
				break;
			
		}
	}
	
}

BigInt BigInt::operator|(BigInt a)
{
	//BigInt ret;
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] | a.bigint[i];
	/*unsigned int */temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator&(BigInt a)
{
	//BigInt ret;
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] & a.bigint[i];
	/*unsigned int */temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator^(BigInt a)
{
	//BigInt ret;
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = this->bigint[i] ^ a.bigint[i];
	/*unsigned int */temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator~()
{
	//BigInt ret;
	for(int i = 0; i < INTCOUNT; i++)
		ret.bigint[i] = ~this->bigint[i];
	/*unsigned int */temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator<<(int shift)
{
	ret = *this;
	if(shift == 1)
	{
		for(int i = 1; i < INTCOUNT; i++)
		{
			ret.bigint[i] = (ret.bigint[i] << 1) + (ret.bigint[i+1] >> 31);
		}
		ret.bigint[0] = ret.bigint[0] << 1;
		/*temp = 0;
		while(ret.bigint[temp] == 0 && temp < INTCOUNT)
			temp++;
		ret.length = INTCOUNT - temp;*/
		if(ret.bigint[INTCOUNT - ret.length - 1] != 0)
			ret.length++;
		return ret;
	}
	if(shift % 32 == 0 || shift/32 > 0)
	{
		temp = shift/32;
		for(unsigned int i = 0; i < INTCOUNT - temp; i++)
			ret.bigint[i] = ret.bigint[i + temp];
		for(int i = INTCOUNT - temp; i < INTCOUNT; i++)
			ret.bigint[i] = 0;
		ret.length += temp;
		return ret;
	}
	if(shift >= INTCOUNT * 32)
		return *null;
	for(int i = 0; i < INTCOUNT - shift/32; i++)
	{
		ret.bigint[i] = ret.bigint[i + shift/32];
	}
	for(int i = 0; i < shift/32; i++)
	{
		ret.bigint[INTCOUNT - 1 - i] = 0;
	}
	if(shift % 32 != 0)
	{
		for(int i = 0; i < INTCOUNT-1; i++)
		{
			//temp = ret.bigint[i] >> (32 - shift % 32);
			ret.bigint[i] = (ret.bigint[i] << (shift % 32)) + (ret.bigint[i+1] >> (32-(shift%32)));
		}
		ret.bigint[INTCOUNT - 1] = ret.bigint[INTCOUNT - 1] << (shift % 32);
	}
	/*unsigned int */temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

BigInt BigInt::operator>>(int shift)
{
	ret = *this;
	if(shift == 1)
	{
		for(int i = INTCOUNT - 1; i >= INTCOUNT - ret.length; i--)
		{
			ret.bigint[i] = (ret.bigint[i] >> 1) + (ret.bigint[i-1] << 31);
		}
		ret.bigint[0] = ret.bigint[0] >> 1;
		/*temp = 0;
		while(ret.bigint[temp] == 0 && temp < INTCOUNT)
			temp++;
		ret.length = INTCOUNT - temp;*/
		if(ret.bigint[INTCOUNT - ret.length] == 0)
			ret.length--;
		return ret;
	}

	if(shift >= INTCOUNT * 32)
		return *null;
	if(shift/32 > 0)
	{
		for(int i = INTCOUNT - 1; i >= shift/32; i--)
		{
			ret.bigint[i] = ret.bigint[i - shift/32];
		}
	
		for(int i = 0; i < shift/32; i++)
		{
			ret.bigint[i] = 0;
		}
	}
	if(shift % 32 != 0)
	{
		for(int i = INTCOUNT - 1; i >= 1; i--)
		{
			//temp = ret.bigint[i] >> (32 - shift % 32);
			ret.bigint[i] = (ret.bigint[i] >> (shift % 32)) + (ret.bigint[i-1] << (32-(shift%32)));
		}
		ret.bigint[0] = ret.bigint[0] >> (shift % 32);
	}
	/*unsigned int */temp = 0;
	while(ret.bigint[temp] == 0 && temp < INTCOUNT)
		temp++;
	ret.length = INTCOUNT - temp;
	return ret;
}

bool BigInt::operator<(BigInt a)
{
	/*if(this->length < a.length)
		return true;
	else if(this->length > a.length)
		return false;
	else
	{*/
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] < a.bigint[i])
				return true;
			else if(this->bigint[i] > a.bigint[i])
				return false;
		}
		return false;
	//}
}

bool BigInt::operator<=(BigInt a)
{
	/*if(this->length < a.length)
		return true;
	else if(this->length > a.length)
		return false;
	else
	{*/
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] < a.bigint[i])
				return true;
			else if(this->bigint[i] > a.bigint[i])
				return false;
		}
		return true;
	//}
}

bool BigInt::operator>(BigInt a)
{
	/*if(this->length > a.length)
		return true;
	else if(this->length < a.length)
		return false;
	else
	{*/
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] > a.bigint[i])
				return true;
			else if(this->bigint[i] < a.bigint[i])
				return false;
		}
		return false;
	//}
}

bool BigInt::operator>=(BigInt a)
{
	/*if(this->length > a.length)
		return true;
	else if(this->length < a.length)
		return false;
	else
	{*/
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] > a.bigint[i])
				return true;
			else if(this->bigint[i] < a.bigint[i])
				return false;
		}
		return true;
	//}
}

bool BigInt::operator==(BigInt a)
{
	/*if(this->length != a.length)
		return false;
	else
	{*/
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] != a.bigint[i])
				return false;
		}
		return true;
	//}
}

bool BigInt::operator!=(BigInt a)
{
	/*if(this->length != a.length)
		return true;
	else
	{*/
		for(int i = 0; i < INTCOUNT; i++)
		{
			if(this->bigint[i] != a.bigint[i])
				return true;
		}
		return false;
	//}
}