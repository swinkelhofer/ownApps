#include <QDialog>
#include <string>
class QTextEdit;
class QComboBox;
class QPushButton;
class Base64: public QDialog
{
	Q_OBJECT
public:
	Base64(QWidget *parent = 0);
private:
	QTextEdit *input, *output;
	QComboBox *sourceformat, *destinationformat;
	QPushButton *input2output, *input2file, *file2file, *file2output;
	
	std::string base2unicode(std::string base64);
	std::string unicode2base(std::string unicode);
private slots:
	void transform();
	void transform2file();
	void transform2output();
	void transformfile2file();
	void showOrHide(int i);
	void base2file();
	void unicode2file();
};
