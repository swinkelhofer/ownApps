#include "Base64.h"
#include <QtGui>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <ctype.h>
#include <windows.h>
char code_table[64] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 
					'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 
					'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
					'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

Base64::Base64(QWidget *parent):QDialog(parent)
{
	input2output = new QPushButton("Convert Input to Output");
	input2file = new QPushButton("Convert Input to File");
	file2output = new QPushButton("Convert File to Output");
	file2file = new QPushButton("Convert File to File");
	sourceformat = new QComboBox;
	sourceformat->addItem("Base64");
	sourceformat->addItem("Unicode");
	destinationformat = new QComboBox;
	destinationformat->addItem("Unicode");
	destinationformat->addItem("Base64");
	input = new QTextEdit;
	output = new QTextEdit;
	
	QVBoxLayout *leftlayout = new QVBoxLayout;
	leftlayout->addWidget(new QLabel("<b>Input:</b>"));
	leftlayout->addWidget(input);
	leftlayout->addWidget(new QLabel("Input Format:"));
	leftlayout->addWidget(sourceformat);
	leftlayout->addWidget(new QLabel("Output Format:"));
	leftlayout->addWidget(destinationformat);
	leftlayout->addWidget(new QLabel("<b>Output:</b>"));
	leftlayout->addWidget(output);
	QVBoxLayout *rightlayout = new QVBoxLayout;
	rightlayout->addStretch();
	rightlayout->addWidget(input2output);
	rightlayout->addWidget(input2file);
	rightlayout->addWidget(file2output);
	rightlayout->addWidget(file2file);
	rightlayout->addStretch();
	QHBoxLayout *mainlayout = new QHBoxLayout;
	mainlayout->addLayout(leftlayout);
	mainlayout->addLayout(rightlayout);
	setLayout(mainlayout);
	connect(input2output, SIGNAL(clicked()), this, SLOT(transform()));
	connect(sourceformat, SIGNAL(currentIndexChanged(int)), this, SLOT(showOrHide(int)));
	connect(destinationformat, SIGNAL(currentIndexChanged(int)), this, SLOT(showOrHide(int)));
	connect(input2file, SIGNAL(clicked()), this, SLOT(transform2file()));
	connect(file2output, SIGNAL(clicked()), this, SLOT(transform2output()));
	connect(file2file, SIGNAL(clicked()), this, SLOT(transformfile2file()));
}

void Base64::transform()
{
	if(sourceformat->currentIndex() == 0 && destinationformat->currentIndex() == 0)
	{
		std::string intext = input->toPlainText().toStdString();
		output->setText(QString::fromStdString(base2unicode(intext)));
	}
	else if(sourceformat->currentIndex() == 1 && destinationformat->currentIndex() == 1)
	{
		std::string intext = input->toPlainText().toStdString();
		output->setText(QString::fromStdString(unicode2base(intext)));
	}
}

void Base64::transform2file()
{
	if(sourceformat->currentIndex() == 0 && destinationformat->currentIndex() == 0)
	{
		base2file();
	}
	else if(sourceformat->currentIndex() == 1 && destinationformat->currentIndex() == 1)
	{
		unicode2file();
	}
}

void Base64::transform2output()
{
	QMessageBox msgBox;
	msgBox.setText("File2Output");
	msgBox.exec();
}

void Base64::transformfile2file()
{
	/*std::fstream in;
	in.open(QFileDialog::getSaveFileName(this, "Datei �ffnen", QDir::currentPath(), "Alle Dateien (*.*)").toStdString().c_str(), std::ios_base::in | std::ios_base::binary);
	std::fstream out;
	out.open(QFileDialog::getSaveFileName(this, "Speichern unter", QDir::currentPath(), "Alle Dateien (*.*)").toStdString().c_str(), std::ios_base::out | std::ios_base::binary);
	*/
	if(sourceformat->currentIndex() == 0 && destinationformat->currentIndex() == 0)
	{
		FILE *in = fopen(QFileDialog::getOpenFileName(this, "Datei �ffnen", QDir::currentPath(), "Alle Dateien (*.*)").toStdString().c_str(), "rb");
		FILE *out = fopen(QFileDialog::getSaveFileName(this, "Speichern unter", QDir::currentPath(), "Alle Dateien (*.*)").toStdString().c_str(), "w+b");
		if(in == NULL || out == NULL)
		{
			QMessageBox msgBox;
			msgBox.setText("Error opening Files");
			msgBox.exec();
		}
		char quart[4];
		int sign[4] = {0,0,0,0};
		while(!feof(in))
		{
			fread(quart, 1, 4, in);
			for(int i = 0; i < 64; i++)
			{
				if(quart[0] == code_table[i])
					sign[0] = i;
				if(quart[1] == code_table[i])
					sign[1] = i;
				if(quart[2] == code_table[i])
					sign[2] = i;
				if(quart[3] == code_table[i])
					sign[3] = i;
			}
			int i = 3;
			for(; i >= 0; --i)
			{
				if(quart[i] != '=')
				{
					return;
				}
			}
			if(i == 3)
			{
				fwrite((char*)((sign[0] << 2) + (sign[1] >> 4)), 1, 1, out);
				fwrite((char*)(((sign[1] & 0xf) << 4) + (sign[2] >> 2)), 1, 1, out);
				fwrite((char*)(((sign[2] & 0x3) << 6) + sign[3]), 1, 1, out);
			}
			else if(i == 2)
			{
				fwrite((char*)((sign[0] << 2) + (sign[1] >> 4)), 1, 1, out);
				fwrite((char*)(((sign[1] & 0xf) << 4) + (sign[2] >> 2)), 1, 1, out);
			}
			else if(i == 1)
			{
				fwrite((char*)((sign[0] << 2) + (sign[1] >> 4)), 1, 1, out);
			}
		}
		fclose(in);
		fclose(out);
	}
}

std::string Base64::base2unicode(std::string base64)
{
	for(int i = 0; i < (int)base64.length(); i++)
	{
		if(!isprint(base64[i]))
			base64.erase(i, 1);
	}
	int fill = base64.length()-1;
	while(base64[fill] == '=')
		fill--;
	fill = base64.length() - 1 - fill;
	std::string unicode = "";
	char quart[4];
	int sign[4] = {0,0,0,0};
	for(int i = 0; i < (int)base64.length(); i+=4)
	{
		quart[0] = base64[i];
		quart[1] = base64[i+1];
		quart[2] = base64[i+2];
		quart[3] = base64[i+3];
		for(int i = 0; i < 64; i++)
		{
			if(quart[0] == code_table[i])
				sign[0] = i;
			if(quart[1] == code_table[i])
				sign[1] = i;
			if(quart[2] == code_table[i])
				sign[2] = i;
			if(quart[3] == code_table[i])
				sign[3] = i;
		}
		unicode += (char)((sign[0] << 2) + (sign[1] >> 4));
		unicode += (char)(((sign[1] & 0xf) << 4) + (sign[2] >> 2));
		unicode += (char)(((sign[2] & 0x3) << 6) + sign[3]);
	}
	return unicode.substr(0, unicode.length() - fill);
}

void Base64::base2file()
{
	std::string base64 = input->toPlainText().toStdString();
	for(int i = 0; i < (int)base64.length(); i++)
	{
		if(!isprint(base64[i]))
			base64.erase(i, 1);
	}
	int fill = base64.length()-1;
	while(base64[fill] == '=')
		fill--;
	fill = base64.length() - 1 - fill;
	std::string unicode = "";
	char quart[4];
	int sign[4] = {0,0,0,0};
	std::fstream out;
	out.open(QFileDialog::getSaveFileName(this, "Speichern unter", QDir::currentPath(), "Alle Dateien (*.*)").toStdString().c_str(), std::ios_base::out | std::ios_base::binary);
	for(int i = 0; i < (int)base64.length()-4; i+=4)
	{
		quart[0] = base64[i];
		quart[1] = base64[i+1];
		quart[2] = base64[i+2];
		quart[3] = base64[i+3];
		for(int i = 0; i < 64; i++)
		{
			if(quart[0] == code_table[i])
				sign[0] = i;
			if(quart[1] == code_table[i])
				sign[1] = i;
			if(quart[2] == code_table[i])
				sign[2] = i;
			if(quart[3] == code_table[i])
				sign[3] = i;
		}
		
		//unicode += (char)((sign[0] << 2) + (sign[1] >> 4));
		//unicode += (char)(((sign[1] & 0xf) << 4) + (sign[2] >> 2));
		//unicode += (char)(((sign[2] & 0x3) << 6) + sign[3]);
		out << (char)((sign[0] << 2) + (sign[1] >> 4));
		out << (char)(((sign[1] & 0xf) << 4) + (sign[2] >> 2));
		out << (char)(((sign[2] & 0x3) << 6) + sign[3]);
	}
	int l = base64.length()-4;
	quart[0] = base64[l];
	quart[1] = base64[l+1];
	quart[2] = base64[l+2];
	quart[3] = base64[l+3];
	for(int i = 0; i < 64; i++)
	{
		if(quart[0] == code_table[i])
			sign[0] = i;
		if(quart[1] == code_table[i])
			sign[1] = i;
		if(quart[2] == code_table[i])
				sign[2] = i;
		if(quart[3] == code_table[i])
			sign[3] = i;
	}
	
	//unicode += (char)((sign[0] << 2) + (sign[1] >> 4));
	//unicode += (char)(((sign[1] & 0xf) << 4) + (sign[2] >> 2));
	//unicode += (char)(((sign[2] & 0x3) << 6) + sign[3]);
	quart[0] = (char)((sign[0] << 2) + (sign[1] >> 4));
	quart[1] = (char)(((sign[1] & 0xf) << 4) + (sign[2] >> 2));
	quart[2] = (char)(((sign[2] & 0x3) << 6) + sign[3]);
	for(int i = 0; i < (3 - fill); i++)
	{
		out << quart[i];
	}
	
}

std::string Base64::unicode2base(std::string unicode)
{
	std::string base64 = "";
	int length = 0;
	while(unicode.length() % 3 != 0)
	{
		unicode += (char) 0x0;
		length++;
	}
	char triple[3];
	for(int i = 0; i < (int) unicode.length(); i+=3)
	{
		triple[0] = unicode[i];
		triple[1] = unicode[i+1];
		triple[2] = unicode[i+2];
		
		base64 += code_table[((unsigned char)triple[0] >> 2)];
		base64 += code_table[(((unsigned char)triple[1] >> 4) + (((unsigned char)triple[0] & 0x3) << 4))];
		base64 += code_table[((((unsigned char)triple[1] & 0xf) << 2) + ((unsigned char)triple[2] >> 6))];
		base64 += code_table[(unsigned char)triple[2] & 0x3f];
	}
	for(int i = 0; i < length; i++)
	{
		base64[base64.length() - 1 - i] = '=';
	}
	return base64;
}

void Base64::unicode2file()
{
	std::string unicode = input->toPlainText().toStdString();
	std::string base64 = "";
	int length = 0;
	while(unicode.length() % 3 != 0)
	{
		unicode += (char) 0x0;
		length++;
	}
	char triple[3];
	for(int i = 0; i < (int) unicode.length(); i+=3)
	{
		triple[0] = unicode[i];
		triple[1] = unicode[i+1];
		triple[2] = unicode[i+2];
		
		base64 += code_table[((unsigned char)triple[0] >> 2)];
		base64 += code_table[(((unsigned char)triple[1] >> 4) + (((unsigned char)triple[0] & 0x3) << 4))];
		base64 += code_table[((((unsigned char)triple[1] & 0xf) << 2) + ((unsigned char)triple[2] >> 6))];
		base64 += code_table[(unsigned char)triple[2] & 0x3f];
	}
	for(int i = 0; i < length; i++)
	{
		base64[base64.length() - 1 - i] = '=';
	}
	std::fstream out;
	out.open(QFileDialog::getSaveFileName(this, "Speichern unter", QDir::currentPath(), "Alle Dateien (*.*)").toStdString().c_str(), std::ios_base::out | std::ios_base::binary);
	out << base64;
}

void Base64::showOrHide(int i)
{
	if(sourceformat->currentIndex() == destinationformat->currentIndex())
	{
		input2output->setEnabled(true);
		input2file->setEnabled(true);
		file2output->setEnabled(true);
		file2file->setEnabled(true);
	}
	else
	{
		input2file->setEnabled(false);
		input2output->setEnabled(false);
		file2output->setEnabled(false);
		file2file->setEnabled(false);
	}
}