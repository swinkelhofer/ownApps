/****************************************************************************
** Meta object code from reading C++ file 'Base64.h'
**
** Created: Sat 16. Apr 18:35:51 2011
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Base64.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Base64.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Base64[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x08,
      20,    7,    7,    7, 0x08,
      37,    7,    7,    7, 0x08,
      56,    7,    7,    7, 0x08,
      79,   77,    7,    7, 0x08,
      95,    7,    7,    7, 0x08,
     107,    7,    7,    7, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Base64[] = {
    "Base64\0\0transform()\0transform2file()\0"
    "transform2output()\0transformfile2file()\0"
    "i\0showOrHide(int)\0base2file()\0"
    "unicode2file()\0"
};

const QMetaObject Base64::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Base64,
      qt_meta_data_Base64, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Base64::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Base64::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Base64::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Base64))
        return static_cast<void*>(const_cast< Base64*>(this));
    return QDialog::qt_metacast(_clname);
}

int Base64::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: transform(); break;
        case 1: transform2file(); break;
        case 2: transform2output(); break;
        case 3: transformfile2file(); break;
        case 4: showOrHide((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: base2file(); break;
        case 6: unicode2file(); break;
        default: ;
        }
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
