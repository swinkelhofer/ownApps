public class IntArray
{
	private IntElement first;
	public IntArray()
	{
		first = null;
	}
	public String toString()
	{
		IntElement temp = first;
		String output = "";
		while(temp.getNext() != null)
		{
			output += String.valueOf(temp.getValue()) + " --> ";
			temp = temp.getNext();
		}
		output += String.valueOf(temp.getValue());
		return output;
	}
	void add(int newValue)
	{
		if(first == null)
		{
			IntElement a = new IntElement();
			a.setValue(newValue);
			first = a;
		}
		else
		{
			IntElement temp = first;
			while(temp.getNext() != null)
			{
				temp = temp.getNext();
			}
			IntElement a = new IntElement();
			a.setValue(newValue);
			temp.setNext(a);
		}
	}

	public static void main(String[] args)
	{
		IntArray array = new IntArray();
		array.add(5);
		array.add(7);
		array.add(12);
		System.out.println(array);
	}


}