public class IntElement
{
	private IntElement next;
	private int value;

	public IntElement()
	{
		next = null;
		value = 0;
	}
	public int getValue()
	{
		return value;
	}
	public void setValue(int newValue)
	{
		value = newValue;
	}
	public IntElement getNext()
	{
		return next;
	}
	public void setNext(IntElement newNext)
	{
		next = newNext;
	}
}